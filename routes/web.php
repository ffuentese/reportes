<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['register' => false]);

Route::group(['middleware' => 'role:admin'], function() {

   Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('', 'DashboardController@index')->name('home');

Route::get('carga', 'EntregaController@index')->name('carga');

// Formulario de búsqueda

Route::get('buscador', 'SearchController@index')->name('buscador');

// Búsqueda por POS

Route::get('busca_pos', 'POSHistoryController@index')->name('buscapos');

// Búsqueda por rótulo

Route::get('rotulo', 'SearchController@rotulo')->name('rotulo');

// Búsqueda por serial

Route::get('serial', 'SearchController@serial')->name('serial');

// Búsqueda por POS

Route::get('pos', 'POSHistoryController@posHistory')->name('pos');

// Mantenedor de tabla maestra de modelos y marcas de equipos
Route::get('mantenedor-modelos', 'ModeloMarcaController@index')->name('modelomarca');

Route::post('agregarmarca', 'ModeloMarcaController@agregarMarca')->name('agregarmarca');

Route::post('agregarmodelo', 'ModeloMarcaController@agregarModelo')->name('agregarmodelo');

Route::get('modelo', 'ModeloMarcaController@verModelo')->name('modelo');

// Carga de planillas de entrega
Route::post('importExcelActaEntrega', 'EntregaController@procesoPlanilla')->name('importExcelActaEntrega');
// carga planillas de entrega de IBM
Route::post('importExcelActaEntregaIBM', 'EntregaIBMController@procesoPlanilla')->name('importExcelActaEntregaIBM');

// Carga de planillas de reversa
Route::post('importExcelActaReversa', 'ReversaController@procesoPlanilla')->name('importExcelActaReversa');
// Carga reversas IBM
Route::post('importExcelActaReversaIBM', 'ReversaIBMController@procesoPlanilla')->name('importExcelActaReversaIBM');

// Carga de planillas de puntos vigentes
Route::post('importExcelPuntosVigentes', 'POSCheckController@procesoPlanilla')->name('importExcelPuntosVigentes');

// Proceso que descarga la información de entrega en formato Excel de planillas de entrega

Route::get('export', 'EntregaController@descargarPlanilla')->name('export');

// Carga de planillas de bodega (IQ09)
Route::post('importExcelBodega', 'BodegaController@procesoBodega')->name('importExcelBodega');

// Carga de planilla consolidado de bodega

Route::post('importConsolidadoEntrega', 'EntregaController@procesoConsolidado')->name('importConsolidadoEntrega');

Route::post('importConsolidadoReversa', 'ReversaController@procesoConsolidado')->name('importConsolidadoReversa');

// Carga planilla de técnicos en terreno

Route::post('importTecnicos', 'ServiceController@procesoPlanilla')->name('importTecnicos');

// Vistas

Route::get('nojustificadas', 'VistasController@noJustificadas')->name('nojustificadas');

Route::get('buscar_por_guia', 'VistasController@buscarPorGuia')->name('buscar_por_guia');
Route::post('borrar_guia', 'VistasController@eliminarGuia')->name('borrar_guia');
Route::get('exportar-guias', 'vistasController@descargarTodasLasGuias')->name('exportar-guias');

Route::get('guia_recepcion', 'VistasController@porGuiaRecepcion')->name('guia_recepcion');
Route::get('guia_reversa', 'VistasController@porGuiaReversa')->name('guia_reversa');

Route::get('buscar_ibm', 'VistasController@buscarPorGuiaIBM')->name('buscar_ibm');
Route::get('guia_ibm', 'VistasController@porGuiaIBM')->name('guia_ibm');
Route::get('buscar_ibm_por_mes', 'VistasController@porMesGuiaIBM')->name('buscar_ibm_por_mes');
Route::get('buscar_ibm_por_dia/{dia}', 'VistasController@porDiaIBM')->name('buscar_ibm_por_dia');

Route::get('buscar_accesorios', 'VistasController@buscarPorGuiaAccesorios')->name('buscar_accesorios');
Route::get('buscar_accesorios_por_mes', 'VistasController@porMesGuiaAccesorios')->name('buscar_accesorios_por_mes');
Route::get('guia_accesorios', 'VistasController@porGuiaAccesorios')->name('guia_accesorios');

Route::get('buscar_ibm_accesorios', 'VistasController@buscarPorGuiaIBMAccesorios')->name('buscar_ibm_accesorios');
Route::get('buscar_ibm_accesorios_por_mes', 'VistasController@porMesGuiaIBMAccesorios')->name('buscar_ibm_accesorios_por_mes');
Route::get('guia_accesorios_ibm', 'VistasController@porGuiaIBMAccesorios')->name('guia_accesorios_ibm');

Route::get('guias_recepcion_mes', 'VistasController@porMesGuiaRecepcion')->name('guias_recepcion_mes');
Route::get('guias_reversa_mes', 'VistasController@porMesGuiaReversa')->name('guias_reversa_mes');
Route::get('guias_adexus_mes', 'VistasController@porMesGuiasAdexus')->name('guias_adexus_mes');

// Exporta dispositivos por guía

Route::get('export_recepcion/{guia}', 'VistasController@exportRecepcion')->name('export_recepcion');
Route::get('export_reversa/{guia}', 'VistasController@exportReversa')->name('export_reversa');

// Procesa toda la información subida al sistema.

Route::get('procesarTodo', 'ProcessController@procesar')->name('procesarTodo');

// BCH

Route::get('inventarioBCH', 'InvBCHController@index')->name('inventarioBCH');

Route::get('cargaInventarioBCH', 'InvBCHController@carga')->name('cargaInventarioBCH');

Route::post('procesoInventarioBCH', 'InvBCHController@proceso')->name('procesoCargaBCH');

Route::get('datatableInvBCH', 'InvBCHController@getDatatable')->name('datatableInvBCH');

Route::get('exportBCH', 'InvBCHController@export')->name('exportBCH');  

// Iniciar proceso global manualmente

Route::get('proceso_global', 'ProcessController@index')->name('proceso_global');

Route::post('procesar', 'ProcessController@procesar')->name('procesar');

// Guardar guías del sistema

Route::get('lista_guias', 'UploadDocsController@index')->name('lista_guias');

Route::get('carga_guias', 'UploadDocsController@create')->name('carga_guias');

Route::post('proceso_guias', 'UploadDocsController@store')->name('proceso_guias');

// Datatable de guias

Route::get('tabla_guias', 'UploadDocsController@getDatatable')->name('tabla_guias');

Route::get('showPDF/{guia}', 'UploadDocsController@show')->name('showPDF');

// Subida de OTDigital

Route::get('carga_ot_digital', 'OTDigitalController@create')->name('carga_ot_digital')->middleware('level:3');

Route::post('proceso_ot_digital', 'OTDigitalController@store')->name('proceso_ot_digital');

// Datos de dirección en proceso

Route::get('posdireccion', 'PosDireccionController@index')->name('posdireccion');
Route::post('upd_posdireccion', 'PosDireccionController@procesar')->name('upd_posdireccion');

// Rutas de vistas POS geográficas

Route::get('posgeo', 'POSGeoController@index')->name('posgeo');

Route::get('buscar_pos', 'POSGeoController@buscarPorComuna')->name('buscar_pos');

Route::get('posporregion/{id}', 'POSGeoController@posPorRegion')->name('posporregion');

Route::get('pos_comuna/{id}', 'POSGeoController@posPorComuna')->name('pos_comuna');

Route::get('ajax_comunas/{id}', 'POSGeoController@comunasRegion')->name('ajax_comunas');

Route::get('ajax_pos/{comuna_id}', 'POSGeoController@posComunasAjax')->name('ajax_pos');


// ZPEC

Route::get('carga_zpec', 'ZpecController@create')->name('carga_zpec');

Route::post('procesoZpec', 'ZpecController@store')->name('procesoZpec');

Route::get('zpec', 'ZpecController@index')->name('zpec');

Route::get('dataZpec', 'ZpecController@getDatatable')->name('dataZpec');

Route::get('descargar-zpec', 'ZpecController@downloadAllZpec')->name('descargarZpec');

// BCH para armado del informe tabla

Route::prefix('bch')->group(function(){
    Route::post('carga_bginfo', 'InvBCHController@procesoBginfo')->name('carga_bginfo');
    Route::post('carga_sccm', 'InvBCHController@procesoSCCM')->name('carga_sccm');
    Route::post('carga_epo', 'InvBCHController@procesoEpo')->name('carga_epo');
    Route::post('carga_rrhh', 'InvBCHController@procesoRRHH')->name('carga_rrhh');
});

// IQ09

Route::get('carga_iq09', 'IQ09Controller@create')->name('carga_iq09');

Route::post('proceso_iq09', 'IQ09Controller@store')->name('proceso_iq09');

Route::get('iq09', 'IQ09Controller@index')->name('iq09');
Route::get('iq09_datatable', 'IQ09Controller@getDatatable')->name('iq09_datatable');

// Materiales

Route::get('carga_material', 'MaterialController@create')->name('carga_material');

Route::post('proceso_material', 'MaterialController@store')->name('proceso_material');

Route::get('materiales', 'MaterialController@index')->name('materiales');

Route::get('material_datatable', 'MaterialController@getDatatable')->name('material_datatable');

// Subir accesorios

Route::post('proceso_accesorios', 'AccesorioController@store')->name('proceso_accesorios');

Route::get('no_seriados', 'AccesorioController@index')->name('no_seriados');

Route::post('accesorios_fecha', 'AccesorioController@guiasPorFecha')->name('accesorios_fecha');

// Subir accesorios no seriados IBM

Route::post('proceso_accesorios_ibm', 'AccesorioIBMController@store')->name('proceso_accesorios_ibm');



// Ruta guardar guias manualmente

Route::get('agregar_guia_pos', 'GuiaController@create')->name('add_guia');
Route::post('procesa_guia', 'GuiaController@store')->name('procesa_guia');

// Cuadro Panel

Route::prefix('cuadro-panel')->group(function(){
    Route::get('puntos-vigentes-3g', 'VistasCuadroIBMController@puntosVigentes3g')->name('puntos-vigentes-3g');
    Route::get('puntos-vigentes-gprs', 'VistasCuadroIBMController@puntosVigentesGprs')->name('puntos-vigentes-gprs');
    Route::get('puntos-vigentes-verifone', 'VistasCuadroIBMController@puntosVigentesTrafoVerifone')->name('puntos-vigentes-vef');
    Route::get('puntos-vigentes-castles', 'VistasCuadroIBMController@puntosVigentesTrafoCastles')->name('puntos-vigentes-cas');
    Route::get('entregas-3g', 'VistasCuadroIBMController@entregas3g')->name('entregas-3g');
    Route::get('entregas-gprs', 'VistasCuadroIBMController@entregasGprs')->name('entregas-gprs');
    Route::get('entregas-verifone', 'VistasCuadroIBMController@entregasVef')->name('entregas-vef');
    Route::get('entregas-castles', 'VistasCuadroIBMController@entregasCas')->name('entregas-cas');
    Route::get('entregas-proyectos-3g', 'VistasCuadroIBMController@entregasProyectos3G')->name('entregas-proyectos-3g');
    Route::get('entregas-proyectos-gprs', 'VistasCuadroIBMController@entregasProyectosGprs')->name('entregas-proyectos-gprs');
    Route::get('entregas-proyectos-vef', 'VistasCuadroIBMController@entregasProyectosVef')->name('entregas-proyectos-vef');
    Route::get('entregas-proyectos-cas', 'VistasCuadroIBMController@entregasProyectosCas')->name('entregas-proyectos-cas');
    Route::get('reversas-3g', 'VistasCuadroIBMController@reversas3g')->name('reversas-3g');
    Route::get('reversas-gprs', 'VistasCuadroIBMController@reversasGprs')->name('reversas-gprs');
    Route::get('reversas-verifone', 'VistasCuadroIBMController@reversasVef')->name('reversas-vef');
    Route::get('reversas-castles', 'VistasCuadroIBMController@reversasCas')->name('reversas-cas');
});

Route::prefix('cuadro-panel-ibm')->group(function(){
    Route::get('stock-ibm-3g', 'VistasCuadroIBMController@stockPOSIBM3G')->name('stock-ibm-3g');
    Route::get('stock-ibm-gprs', 'VistasCuadroIBMController@stockPOSIBMGPRS')->name('stock-ibm-gprs');
    Route::get('stock-ibm-verifone', 'VistasCuadroIBMController@stockTrafoIBMVerifone')->name('stock-ibm-verifone');
    Route::get('stock-ibm-castles', 'VistasCuadroIBMController@stockTrafoIBMCastles')->name('stock-ibm-castles');
    
    Route::get('reversa-ibm-3g', 'VistasCuadroIBMController@revPOSIBM3G')->name('reversa-ibm-3g');
    Route::get('reversa-ibm-gprs', 'VistasCuadroIBMController@revPOSIBMGPRS')->name('reversa-ibm-gprs');
    Route::get('reversa-ibm-verifone', 'VistasCuadroIBMController@revTrafoIBMVerifone')->name('reversa-ibm-verifone');
    Route::get('reversa-ibm-castles', 'VistasCuadroIBMController@revTrafoIBMCastles')->name('reversa-ibm-castles');
});

// Micro

Route::get('micro', 'MicroBecController@index')->name('micro');
Route::post('micro', 'MicroBecController@store')->name('procesa_micro');
