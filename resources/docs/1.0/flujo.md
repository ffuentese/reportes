# Flujo

La aplicación hace uso de datos que provienen de hojas de cálculo Excel o archivos CSV. 

El usuario abre la aplicación e ingresa su usuario y contraseña. De acuerdo a sus permisos y rol el usuario podrá ejecutar ciertas funciones.

Si se trata de un usuario con un rol suministrador de datos, podrá realizar cargas de datos de hojas de cálculo al sistema. 

Hay dos tipos de cargas de datos: las que se cargan inmediatamente al sistema y las cargas que se ejecutan en procesos internos.

Las cargas inmediatas son planillas que se procesan en cuanto se suben al sistema.

Las cargas con proceso se suben a la aplicación y son procesadas posteriormente por una tarea programada. 
Este tipo de procesos está destinado a archivos pesados y/o procesos muy complejos que requieren tiempo. 

Al terminar cualquiera de las cargas de datos al sistema, el resultado es información procesada en paneles resumidos y tablas. 

- Banco Estado.
 - Roles

## Banco Estado (POS)

### Roles

Existen los siguientes roles de usuario:

- Visualizador
- Suministrador
- Administrador

#### Visualizador

El visualizador sólo necesita ingresar al sistema con sus credenciales y entrar a las páginas habilitadas. 

#### Suministrador

El usuario con rol suministrador es la persona encargada de subir las planillas al sistema y además puede visualizar la información resultante.
Este rol es responsable de suministrar los archivos de acuerdo a los estándares 
indicados y de revisar que estos no se encuentren corruptos o con campos incompletos fuera de lo normal (p. ej. una columna vacía que suele tener datos relevantes).
Si bien el sistema realiza validaciones tanto del archivo como de los datos que contiene, no puede anticiparse a todos los errores posibles y tampoco puede descartar muchos datos ya que se pierde información valiosa.

#### Administrador

El administrador del sistema puede crear o eliminar cuentas y entrar a todas las funcionalidades del sistema. 

### Flujo de trabajo

El usuario suministra al sistema:

- Actas de entrega ("directas")
- Actas de reversa
- El archivo IQ09 de acuerdo al formato indicado (bodega)
- Puntos vigentes (hoja de cálculo enviada por el banco)
- Planillas de proveedores de acuerdo al formato estandarizado. 

El usuario proporciona cada uno de estos documentos y son cargados en el sistema.

El sistema se basa fundamentalmente en los siguientes datos para realizar el cruce: 

- Serie del equipo
- Fecha de recepción
- Fecha de instalación
- Fecha de reversa del equipo retirado
- Fecha de devolución del equipo
- Localización del equipo

Cada dispositivo cumple un ciclo en el cual es recibido formalmente, es instalado y es retirado de su lugar (reversado). 
Entre tanto, otro equipo es retirado en su lugar. Un mismo dispositivo puede pasar varias veces por este mismo ciclo. 

Los procesos involucrados son:

- Importadores (de actas, de proveedores, de puntos vigentes, de bodega)
- Asignación de POS (relaciona equipo con POS)
- Relación con equipo anterior (relaciona una serie instalada con un equipo retirado)


[Más detalles técnicos sobre los importadores de planillas y los formatos de las mismas aquí](/docs/{{version}}/importadores)

[Más detalles técnicos de los procesos aquí](/docs/{{version}}/procesos).


## Cambios

Actualizado por última vez el 29-04-2019 por Francisco Fuentes <ffuentese@entel.cl>. 