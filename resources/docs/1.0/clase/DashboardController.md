# DashboardController

---

- Resumen
- Antecedentes
- Datos calculados

# Resumen

Este controlador define cómo se muestran los resultados del panel de Banco Estado. 
El panel contiene bajo su código muchas consultas para obtener sus resultados. 
Aquí se explica complementariamente al código cómo se obtienen los datos.

# Antecedentes

El panel tiene como objeto mostrar el resumen de todos los equipos en términos de ingresos y reversas de equipos del cliente Banco Estado. 
Hay dos registros paralelos: 

- Guia (y GuiaIBM) que registran las actas en sí mismas
- Device: que registra el ciclo de una serie desde que se recibe hasta que se vuelve a entregar al banco por alguna razón.

Lo que se cuenta en el cálculo son series, pero los ciclos nos dan el detalle, por ejemplo, de las validaciones, de lo que está instalado, reversado, etc. 
La tabla devices contiene ciclos, por lo tanto, las series se pueden repetir en función de si el equipo ha sido utilizado más de una vez. 

# Datos calculados

La mayoría de los cálculos y consultas a la base de datos a día de hoy se encuentran en la función index(). 

