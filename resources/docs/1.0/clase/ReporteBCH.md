# ReporteBCH

---

- Resumen
- Antecedentes
- Datos calculados

# Resumen

Es una tarea (o "job") que ejecuta el proceso de generar el inventario para el Banco de Chile. 

# Antecedentes

El proceso de inventario de Banco de Chile consiste en generar un informe en base a los siguientes documentos:

- El inventario anterior (Modelo Dev_BCH)
- SCCM más reciente (Modelo SCCM / Importador SccmImport)
- BGInfo más reciente (Modelo Bginfo / BginfoImport)
- Dotación de personal (Modelo Bch_employee / BCH_EmployeeImport)
- Ubicación de segmentos de red (Modelo Bch_ip)
- Parque instalado actualizado (Modelo ZPEC / Importador ZpecImport)
- Stock en bodega (Modelo IQ09 / Importador IQ09Import)
- Consolidado de códigos de material (Modelos Material y Material_tipos / MaterialImport)


Para esto el analista tenía que utilizar fórmulas de Excel para poder generar el 
informe en una tarea muy tediosa revisando todo el inventario anterior y luego el SCCM y el Bginfo todo el día.

# Proceso

Estos archivos se cargan al sistema cada semana y el proceso ReporteBCH los recorre 
buscando los datos necesarios para llenar el reporte en el modelo Dev_BCH. Este 
proceso se ejecuta una vez a la semana y para que funcione correctamente tienen que
cargarse correctamente los tres archivos mencionados arriba.

1. El proceso revisa todos los equipos registrados en el inventario anterior y los modifica según haga falta: Tarea ProcesaInv.
2. Se revisan todos los equipos mencionados en el SCCM y se agregan al informe cuando no se encuentran en él. Tarea ProcesaSccm.
3. Se revisan los equipos en el BGInfo que no se encontrasen en los reportes anteriores. Tarea ProcesaBginfo.

En el archivo de importadores se encuentra el detalle. 


# Datos calculados

La mayoría de los cálculos y consultas a día de hoy se encuentran en la función index(). 


