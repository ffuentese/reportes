# Roles y permisos

---

El sistema tiene una estructura de usuarios basada en roles y permisos.
Un usuario tiene un rol y éste tiene asignados ciertos permisos pero un usuario también puede tener asignados permisos específiicos.

- Tablas
- Roles
- Asignación de roles
- Asignación de permisos

## Tablas

Los modelos principales de la estructura de usuarios son:

- User (tabla users)
- Permission (tabla permissions)
- Role (tabla roles)

Y existen tablas de relación, que unen a un usuario o a un rol con un permiso. 

- permission_role
- permission_user
- role_user

## Roles

Existen 4 roles hasta la fecha:

- Admin
- Super
- Suministrador
- Visualizador

### Admin

Tiene control total. Puede realizar todas las tareas dentro del sistema y además puede ver los logs y manipular usuarios.
La persona a cargo de este rol debe tener conocimientos de programación y bases de datos.

### Super

Puede realizar todas las tareas de la aplicación en caso de necesitar apoyar a otros usuarios o para visualizar información y el programa sin restricciones.

### Suministrador

Es el rol encargado de cargar información al sistema. También puede visualizar información. Sus permisos pueden variar dependiendo de los requerimientos del negocio.

### Visualizador

Es el rol destinado a ver la información generada por el sistema. Sus permisos pueden variar dependiendo de los requerimientos del negocio.

## Asignación de roles

Asignar roles requiere:

1. Crear el rol en la base de datos: roles
2. Crear una lógica que contenga a ese rol como requisito.
3. Asignarle ese rol a usuarios: role_user

## Asignación de permisos

Asignar permisos tiene ciertas etapas: 

1. Crear el permiso en la base de datos: permissions
2. Generar la lógica para que quien tenga ese permiso pueda realizar las tareas
3. Asignar ese permiso al usuario: permission_user 

> {info} También se puede asignar un permiso a un rol: permission_role

De momento no hay una interfaz para asignar permisos y roles. Es necesario entrar a la base de datos. 

## Cambios

Actualizado por última vez el 29-04-2019 por Francisco Fuentes <ffuentese@entel.cl>. 