# Overview

---

- [First Section](#section-1)

<a name="section-1"></a>
## Resumen

Este sistema tiene por objeto automatizar la generación del reporte de cumplimiento de los POS de Banco Estado.
El usuario ingresa las planillas de directas, reversas, bodega, puntos vigentes y otras obteniendo un historial de las series ingresadas al sistema.

El sistema está construido en PHP utilizando Laravel como framework y MySQL como motor de base de datos. El esquema es MVC. 
También depende mucho de una librería llamada [Laravel Excel](https://docs.laravel-excel.com/3.1/getting-started/).

Para entender cómo funciona Laravel como Framework siempre es bueno recurrir a su [documentación](https://laravel.com/docs/) que es muy completa. 

## MVC

El Modelo-Vista-Controlador es un esquema que separa las funciones de las clases 
y archivos del proyecto para que el desarrollo sea ordenado.

La petición llega al servidor vía enrutador que en este caso es: Routes/web.php
y desde allí llega a un controlador (y a un método dentro de ese controlador), el cual se comunica con el modelo para obtener datos de la base
y con la vista, que es la página web (en este caso renderizada a través de una plantilla Blade) para mostrar la información del modelo del modo que el cliente espera.

![image](/assets/images/docs/mvc.png)

## Flujo

El usuario tiene que subir las planillas al sistema de acuerdo al modelo utilizado. Cambiar cualquier columna de lugar altera el sistema y da lugar a errores. 
Todo se carga desde la misma página pero el orden debería ser siempre el mismo:

- Actas de entrega (~~consolidado~~ o individual)
- Bodega
- Técnicos
- Puntos vigentes
- Reversas (~~consolidado~~ o individual).



## Cambios

Actualizado por última vez el 13-05-2019 por Francisco Fuentes <ffuentese@entel.cl>. 