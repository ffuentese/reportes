# Procesos

---

- Resumen
- Comandos en la aplicación
- Jobs o tareas

## Resumen

Existen algunos procesos creados para realizar operaciones en segundo plano y que pueden resultar pesadas. 
Estos procesos se ejecutan en diferido a través de una cola de procesos que a su vez se ejecuta por separado. 
[La documentación de colas de procesos (**queues**) en la documentación de Laravel](https://laravel.com/docs/master/queues).

La tarea más común en esta aplicación es la importación de archivos Excel. También sirven para procesar información como crear un reporte o actualizar una tabla masivamente con cierta información.

Los procesos se codifican en clases llamadas **jobs** (tareas o trabajos) que pueden ser procesados a través de las colas. 
Se encuentran en App\Jobs.

Otra manera  de procesar tareas es utilizando un **comando** (App\Console\Commands). 
Una clase comando puede llevar un proceso para que sea ejecutado al dar la órden por la línea de comandos (cmd o bash). 
Las clases de comandos pueden traer codificada la tarea directamente o bien instanciar *jobs*. 

Todos los comandos tienen un nombre que sirve para ejecutarlos directamente desde la línea de comandos. Se ejecuta así:

(En la carpeta principal del proyecto) php artisan [nombre del comando].

## Comandos en la aplicación

### Banco Estado

- ActaEntregaBulk: Importa masivamente los datos de las planillas de actas de directas (procesar:entregas)
- ActaReversaBulk: Importa masivamente los datos de las planillas de actas de reversas (procesar:reversas)
- ActaEntregaIBMBulk: Importa las planillas de actas de directas de Entel - IBM. (procesar:entregas_ibm)
- ActaReversaIBMBulk: Importa las planillas de actas de directas de IBM - Entel. (procesar:reversas_ibm)
- AddPreviousDevice: Busca la serie retirada de un dispositivo POS instalado. (calcular:previos)
- AddTicketToDevice: Busca el ticket en la OT digital para un dispositivo. (procesar:addticket)
- AssigPos: Consulta PVS para completar datos de pos e instalación (procesar:asignarpos)
- AssigPosDireccion: Le agrega la dirección a los POS a partir de un archivo de puntos vigentes individual. (procesar:ubicacionpos)
- CopyPVSToRel: Agrupa los datos de PVS (Puntos vigentes en bruto) para que sean más fáciles de utilizar en la tabla rel_p_v_s. (procesar:pvstorel)
- ImportPVS: Importa los datos en bruto de los puntos vigentes del banco. (procesar:pvs)
- ServiceProcess: Se encarga de procesar las planillas de servicios técnicos regionales. (procesar:str)

### Banco de Chile

- ProcessReporteBCH: Dispara la tarea de creación del reporte. 

### Jobs o tareas

### Banco Estado

- Actas: Procesa las actas subidas al sistema
- LastPVSUpdater: Crea una tabla con los datos del último archivo de puntos vigentes subido al sistema.
- IQ09Process: Actualiza los datos de la bodega. 
- ProcessUpdate: Dispara todos los procesos de reportes para Banco Estado y así actualizar la información.
- SendMailFinished y SendMailFinishedWithErrors: Envía un correo para notificar del término del proceso de Banco Estado.

### Banco de Chile

- ReporteBCH: Actualiza la información del reporte de inventario Banco de Chile.   

## Cambios

Actualizado por última vez el 23-07-2019 por Francisco Fuentes <ffuentese@entel.cl>. 