# Importadores (para planillas Excel)

---

## Resumen

El proyecto utiliza la librería [Laravel Excel](https://docs.laravel-excel.com/3.1/getting-started/) para importar las planillas.
Es muy importante leer la documentación. Los importadores se encuentran siempre en **App\Imports\** 

> {info} Los formatos detallados abajo son importantes, si una planilla no corresponde al formato, puede causar errores o no procesarse correctamente.

.

> {info} El único formato que se debe usar es el XLSX. El XLS puede causar errores.

Los importadores son:

- ActaImport
- ActaRevImport
- BodegaImport
- BulkImport
- BulkRevImport
- POSImport
- ServiceImport
- OTDigitalImport
- ImportPosDireccion
- SccmImport
- BginfoImport
- MaterialImport

### ActaImport (Acta de entrega)

Importa las **actas de entrega individuales** de Adexus tal como llegan, necesita los siguientes datos del usuario:

- Fecha de la guía
- Número de la guía

#### collection($rows)

Utiliza ToCollection (transforma los datos de Excel en una colección que puede recorrerse).

Lo que hace en resumen, es importar los datos de Excel al modelo Device (la tabla de los dispositivos pos). 
Para ello valida la información y toma los datos de las columnas que tienen información relevante. 

El formato es el siguiente:

- Los datos parten desde la línea 11. Se especifica en el método startRow (use \Maatwebsite\Excel\Concerns\WithHeadingRow implements WithStartRow)
- La planilla tiene 6 campos.

.

    $row[0] = Vacío.
    $row[1] = Número correlativo (no se usa).
    $row[2] = Número de serie.
    $row[3] = Tecnología (si es 3G, GPRS).
    $row[4] = Modelo.
    $row[5] = Marca.
    $row[6] = Observación (no se usa).

El formato tal como se ve:

![image](/assets/images/docs/formatos/formato-entrega.JPG)

#### createModel($row)

Hay un modelo llamado createModel que permite crear directamente los modelos nuevos a partir de la misma información de la planilla sin tener que ingresarlos a mano.
Utiliza la fila actual como parámetro porque necesita acceder al modelo y a la marca. 
Retorna un objeto App\Modelo.


### ActaRecImport (Acta reversas)

Es muy similar al ActaImport pero hay algunas diferencias con las columnas:

- Los datos parten desde la línea 8. 
- Los mismos campos.

.

    $row[0] = Número correlativo (no se usa).
    $row[1] = Número de serie.
    $row[2] = Tecnología (si es 3G, GPRS).
    $row[3] = Modelo.
    $row[4] = Marca.
    $row[5] = Observación (no se usa).

El formato tal como se ve:

![image](/assets/images/docs/formatos/formato-reversa.JPG)

### BodegaImport (IQ09)

Carga el IQ09 de acuerdo al formato:

    $row[0] = Equipo (no se usa)
    $row[1] = Material (en el modelo el atributo se llama "code") 
    $row[2] = Rótulo 
    $row[3] = Serial
    $row[4] = Centro de costos
    $row[5] = Ubicación (location_id en el modelo de Device)
    $row[6] = Estatus (código de estatus en bodega)
    $row[7] = Modelo
    $row[8] = Fecha de modificación (no se usa)
    $row[9] = Modificado por (no se usa)
    $row[10] = Estatus sistema (no se usa)
    $row[11] = Fecha de creación en sistema de stock (no se usa)
    $row[12] = Código PEP

Lo hace desde el método collection. Este método lo que hace es actualizar el modelo Device.

> {warning} Este proceso necesita bastante tiempo de procesamiento. Ajustar servidor para que la ejecución soporte un tiempo mayor al normal.

### BulkImport (Consolidado entregas)

Carga el **consolidado de entregas**. También implementa ToCollection. Utiliza este formato en método collection:

    $row[0] = Elemento (no se usa)
    $row[1] = Serie
    $row[2] = Tecnología (no se usa)
    $row[3] = Fecha de despacho (fecha_recepcion)
    $row[4] = Guía de recepción
    $row[5] = Acta (no se utiliza)
    $row[6] = Modelo: [Tipo] [[Marca] Modelo] = POS VERIFONE VX520 3G
    $row[7] = Homologación 
    $row[8] = Cantidad (no se usa)

> {warning} Este proceso necesita bastante tiempo de procesamiento. Ajustar servidor para que la ejecución soporte un tiempo mayor al normal.


### BulkRevImport (Consolidado reversas)

Es muy parecido a BulkImport pero carga el **consolidado de reversas** y su formato es ligeramente diferente.

    $row[0] = Elemento
    $row[1] = N° de serie
    $row[2] = Fecha de ingreso
    $row[3] = N° de guía
    $row[4] = Modelo 
    $row[5] = Homologación
    $row[6] = Cantidad (no se usa)
    $row[7] = Observaciones (no se usa).

> {warning} Este proceso necesita bastante tiempo de procesamiento. Ajustar servidor para que la ejecución soporte un tiempo mayor al normal.

### POSImport (Puntos vigentes)

Este importador toma los archivos de **puntos vigentes** y desde ellos actualiza la información de la fecha de instalación 
y también relaciona las tablas. Los campos que utiliza no son muchos pero hay muchos archivos y cada archivo tiene decenas de miles de filas. 

El importador **necesita la fecha como parámetro**. 

Los campos que efectivamente se utilizan:

    $row[1] = Id del pos (la ubicación según el banco).
    $row[4] = Estado (en el modelo de POS equivale a $pos->estado y es booleano).
    $row[28] = Número de serie.

El importador edita el modelo Device y la tabla de relación 'devices_pos'. 

> {warning} Este proceso necesita bastante tiempo de procesamiento. Ajustar servidor para que la ejecución soporte un tiempo mayor al normal.

Es utilizado por el comando App\Console\Commands\PosProcess que extrae la fecha automáticamente desde el nombre de archivo:
Utiliza este formato: Puntos Vigentes [dd/mm/yyyy].xlsx.



### ServiceImport (Técnicos)

Importa las planillas de técnicos. El formato es el siguiente:

    $row[0] = Proveedor
    $row[1] = Nombre del técnico
    $row[2] = Uso de equipo (Stock, reversa, instalado)
    $row[3] = Fecha de reversa
    $row[4] = Tipo (POS)
    $row[5] = Modelo: [POS] [[Marca] Modelo]
    $row[6] = Rótulo
    $row[7] = Serie

El importador edita el modelo Device y Service (STR) si es que hay que crear algún STR nuevo 
o Technician si es que hay que agregar algún técnico.

#### createSTR($row)

Crea un servicio técnico a partir de una fila.

#### createModel($row)

Crea un modelo si no existe a partir de una fila.

#### createTechnician($row)

Crea un técnico nuevo en un STR existente a partir de una fila.

### OTDigitalImport

#### model

Recibe un parámetro "row" (fila de Excel)

Importa los datos desde el excel de la OTDigital. Utiliza los siguientes campos del archivo:

- 'ticket'
- 'fecha_atencion'
- 'foliodespacho'
- 'folioot'
- 'cliente'
- 'sucursal'
- 'nombre'
- 'rut'
- 'direccion'
- 'ciudad'
- 'tipo1'
- 'marca1'
- 'serial1'
- 'rotulo1'
- 'tipo4'
- 'marca4'
- 'serial4'
- 'rotulo4'
- 'usuario'
- 'proveedor'

> {info} La subida del archivo reemplaza el contenido previo de la tabla. 

### ImportPosDireccion

Este importador toma la misma planilla que PVSImport pero en vez de importar todos los datos,
 existe para actualizar los datos de ubicación de cada POS. Esto permite obtener estadísticas después. 
Como el proceso PVSImport es pesado en sí mismo y sólo basta con el archivo más reciente, lo hemos dejado como un proceso aparte.

El formato de nombre de archivo es idéntico al de Puntos Vigentes:

"Puntos Vigentes 13-05-2019.xlsx" 

El importador actualiza la tabla POS en aquellas ubicaciones que están vacías e ignora las demás. 

### SccmImport

Importa los datos de la planilla de equipos "SCCM" del Banco de Chile para la construcción del informe de inventario.

Los datos hasta la fecha ingresan en el siguiente orden:

Tiene los siguientes campos a la fecha (no todas se utilizan):

- 0 = alias (se usa)
- 1 = marca (se usa)
- 2 = modelo (se usa)
- 3 = sistema operativo 
- 4 = procesador
- 5 = memoria
- 6 = disco duro
- 7 = serie (se usa)
- 8 = chassis (se usa)
- 9 = ip (se usa)
- 10 = dirección MAC
- 11 = último escaneo (se usa)
- 12 = fecha de instalación
- 13 = nombre de usuario (se usa)
- 14 = nombre real 
- 15 = arquitectura
- 16 = versión cliente
- 17 = versión bios
- 18 = resource ID 
- 19 = ultimo registro
- 20 = segmento (se usa)
- 21 = ubicacion 
- 22 = marca 
- 23 = CUI (se usa)
- 24 = sucursal (se usa)
- 25 = dirección (se usa)
- 26 = ciudad (se usa)
- 27 = región (se usa)
- 28 = red

Este importador renueva el contenido de la tabla desde cero.

### BginfoImport

Importa los datos que contiene la planilla BGinfo para preparar el informe de inventario BCH.

Contiene las siguientes columnas, no todas se utilizan:

- 0 = Time_Stamp (se usa)
- 1 = Machine_Domain
- 2 = Host_name (se usa)
- 3 = Logon_Domain
- 4 = User_Name (se usa)
- 5 = System_Type
- 6 = OS_Version
- 7 = Service_Pack
- 8 = Arquitectura
- 9 = Marca (se usa)
- 10 = Modelo (se usa)
- 11 = Serie (se usa)
- 12 = CPU
- 13 = Memory
- 14 = Volumes
- 15 = Free_Space
- 16 = Mac_Address
- 17 = Network_Card
- 18 = Network_Speed
- 19 = Network_type
- 20 = IP_Address
- 21 = IP_Activa (se usa)
- 22 = Subnet_Mask
- 23 = Default_Gateway
- 24 = DHCP_Server
- 25 = DNS_Server
- 26 = Impresoras
- 27 = IE_Version
- 28 = McAfee
- 29 = SCCM
- 30 = Snow
- 31 = Office
- 32 = Logon_server
- 33 = Hora
- 34 = Zona_horaria
- 25 = Boot_Time
- 26 = Snapshot_Time

Este importador renueva el contenido de la tabla desde cero.


### BCH_EmployeeImport

La nómina de trabajadores del Banco de Chile se utiliza para obtener datos para el inventario de BCH:

Tiene los siguientes campos, aunque no se utilizan todos. 

- 0 = RUT (se usa)
- 1 = DV (se usa)
- 2 = Nombre (se usa)
- 3 = Centro_costo
- 4 = Oficina
- 5 = Glosa_OFI
- 6 = UNIDA_CUIPR
- 7 = GLOSA_UNIDAD
- 8 = COD_CARGO
- 9 = CARGO
- 10 = GLOSA_EMP
- 11 = DIRECCION (se usa) 
- 12 = ANEXO
- 13 = OFI_DIRECTO
- 14 = FAX
- 15 = EMAIL (se usa)
- 16 = CUI_DIV
- 17 = GLS_CUI_DIV
- 18 = CUI_AREA
- 19 = GLS_CUI_ARE
- 20 = CUI_DTP
- 21 = GLS_CUI_DTP
- 22 = Nombre jefe
- 23 = UNIDA_CUIPR_jefe
- 24 = GLOSA_UNIDAD_jefe
- 25 = ANEXO_jefe
- 26 = OFI_DIRECTO_jefe
- 27 = FAX_jefe
- 28 = EMAIL_jefe

Este importador renueva el contenido de la tabla desde cero. (desde el controlador)

### MaterialImport

Este importador recupera las columnas relevantes del consolidado de códigos:

     * Código de material
     * Descripción (modelo asociado al material)
     * Perfil (si es seriado o no)
     * Tipo (Si es PC de escritorio, notebook, etc)

Al cargar se renueva el contenido de la tabla desde cero. 

## Cambios

Actualizado por última vez el 27-05-2019 por Francisco Fuentes <ffuentese@entel.cl>. 