# Modelos

---

## Resumen

Los modelos en Laravel son el nexo con la base de datos 
y [representan objetos](https://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos) 
con atributos y métodos, accesores y mutadores.

En Laravel los objetos se comunican directamente con la base de datos y obtienen de ella sus atributos que corresponden
con columnas de la tabla. Además con los accesores y mutadores 
[se pueden crear atributos anexos](https://laravel.com/docs/5.7/eloquent-mutators) 
para mostrar mejor la información o encontrar datos más fácilmente.

Los modelos de Laravel no detallan su contenido y sólo hay que codificar en ellos comportamientos específicos.
Por eso hace falta documentar en cada modelo los atributos del mismo. También todos los modelos incluyen los atributos
created_at y modified_at que se actualizan solos. 

Todos los modelos se encuentran en App

Para crear un nuevo modelo hay que escribir en la línea de comandos:

    php artisan make:model [NombreDelModelo] -m



## Modelos del proyecto

- [Brand](#brand)
- [Customer](#customer)
- [Device](#device)
- [Location](#location)
- [Modelo](#modelo)
- [Permission](#permission)
- [Pos](#pos)
- [Role](#role)
- [Service](#service)
- [Technician](#technician)
- [User](#user)
- [PV](#pv)
- [RelPVS](#relpvs)
- [Provider](#provider)
- [DocGuiaIBM](#docguiaibm)
- [GuiaIBM](#guiaibm)
- [Dev_BCH](#dev_bch) 
- [OTDigital](#OTDigital)
- [Zpec](#zpec)
- [IQ09](#iq09)
- [Material](#material)
- [Sccm](#sccm)
- [Bginfo](#bginfo)
- [Bch_employee](#bch_employee)
- [Bch_location](#bch_location)
- [Bch_ip](#bch_ip)


<a name="brand"></a>

### Brand (marca)

Corresponde a la **marca** de los equipos. Utilizamos los mismos códigos de marcas de SAP,
 así que el id es una string y por eso no utiliza el incremento.

Sus atributos son:

- id (p. ej. APL)
- name (nombre de la marca, p. ej. Apple)
- timestamps

<a name="customer"></a>

### Customer

Está para diferenciar al cliente. En este caso sólo se usa: 1 Banco Estado.

- id (1, es incremental) 
- name ('Banco Estado')

<a name="device"></a>

### Device

Es el dispositivo y es el modelo más importante, 
ya que muestra a los dispositivos y los relaciona con el resto de las tablas.

Cada fila representa a un ciclo de un serial desde que es recibido por Entel hasta que es devuelto.

Sus atributos son:

- id (incremental, es el único elemento que representa al ciclo, int)
- serial (muy importante,  el número de serie, es una string)
- rotulo (rótulo del equipo, generalmente lo edita el IQ09, string)
- code (corresponde al "material" de las planillas de bodega IQ09, string)
- cost_center (código de centro de costo, string)
- guia_recepcion (n° de guía de ingreso o "directa", int)
- guia_reversa (n° de guía de reversa del mismo serial, int)
- pep (código contable, string)
- modified_by (quién modificó el archivo, no implementado todavía, string)
- modified_on (cuándo modificó el archivo, no implementado todavía, string)
- fecha_recepcion (recepción del equipo, viene del acta de entrega, datetime)
- fecha_instalacion (fecha en que se instaló este serial, viene del análisis de puntos vigentes, datetime)
- fecha_reversa (fecha de reversa de este mismo serial, viene de las actas de reversa, datetime)
- serial_prev (serial de equipo retirado en lugar de este equipo, string)
- reversa_prev (fecha de reversa del equipo retirado, datetime)
- status_id (el id de status de bodega, int [App\Status])
- location_id (id de ubicaciones, está relacionado con los códigos de almacén 
pero también cuando están en técnicos puede cambiar a STO de stock técnico o REV de reversa, string [App\Location])
- customer_id (id incremental de cliente, p. ej. 1, Banco Estado, int [App\Customer])
- str_id (id de servicio técnico, es incremental, int [App\Service])
- model_id (id de modelo de equipo, es incremental, int [App\Modelo])
- pos_id (id NO incremental, representa a la ubicación de pos que ocupa de acuerdo a puntos vigentes, int [App\Pos])
- user_id (id de usuario del sistema, int)
- technician_id (id incremental del técnico si procede, int [App\Technician])
- ticket (string, número del ticket en caso de haber una relación directa en OT Digital)

Además este modelo incluye dos atributos dinámicos llamados:

- previous_device (el objeto que contiene la información del dispositivo anterior, App\Device)
- reversado (indica inmediatamente si el dispositivo anterior fue reversado, booleano)

<a name="location"></a>

### Location

Representa a la ubicación del equipo. Utiliza los códigos de bodega para su id y por lo tanto, no usa ids incrementales.

- Cuando el equipo queda instalado, location queda en id='0', 'SIN ALMACÉN'
- Cuando el equipo queda en poder del técnico queda en id='STO', 'STOCK TÉCNICO'
- Cuando el equipo queda en reversa desde el técnico queda en id='REV', 'REVERSA'
- Cuando el equipo está en stock de bodega aparece con el código de bodega (D007 por ejemplo). También cuando aparece en reversa (R003 p.ej.).

Sus atributos son:

- id
- name

<a name="modelo"></a>

### Modelo

(No podía ser Model porque haría conflicto con Illuminate\Database\Eloquent\Model)

Representa al modelo del equipo, está asociado con una marca (App\Brand). 

Atributos:

- id (incremental)
- name (la descripción del modelo, string)
- brand_id (App\Brand, string)

<a name="permission"></a>

### Permission

Representa los permisos de usuarios.

- id (incremental)
- slug (string)
- name (string)

<a name="pos"></a>

### Pos 

Representa a cada ubicación de POS de acuerdo a una numeración dada por el banco.

Atributos:

- id (no incremental, es dado por el banco a través de las planillas)
- vigente (boolean, ¿está activo o no?)
- direccion (string, 'Darío Urzúa 1780')
- comuna_id (integer, La clave foránea correspondiente a la comuna -tabla comunas-, por ejemplo, 501 que representa a Providencia).  La región está incluída en la comuna al hacer la relación con Eloquent: $comuna->region.

Está en una relación de muchos a muchos con Device a través de la tabla 'devices_pos' cuyos atributos son:

- id (incremental)
- device_id (id en Device, int [App\Device])
- pos_id (id en POS, int [App\Pos])

<a name="role"></a>

### Role

Representa roles de privilegios

- id (incremental)
- slug (string)
- name (string)

<a name="service"></a>

### Service

Representa a los servicios técnicos o STR.

- id (1, es incremental) 
- name ('SAO COMPUTACIÓN LTDA.', string)

<a name="technician"></a>

### Technician

Representa a los técnicos que instalan o retiran dispositivos.

- id (1, es incremental) 
- name ('Juan Pérez', string)
- str_id ([App\Service], int)

<a name="provider"></a>

### Provider

- id (1, es incremental) 
- name ('ENTEL', string)

<a name="user"></a>

### User

Representa al usuario del sistema, lo crea Laravel.

- id (incremental)
- name (string)
- email (string)
- email_verified_at (cuándo se verificó, timestamp)
- password (hash de contraseña, string)
- remember_token  (token, string)

[Documentación sobre User en Laravel](https://laravel.com/docs/5.7/authentication).

<a name="docguiaibm"></a>

### DocGuiaIBM

Almacena las imágenes de las guías. 

- guía (integer, nro de documento)
- provider_id (integer, se relaciona con el objeto Provider para identificar de qué es la guía)
- filename (string, almacena la ruta del archivo)

<a name="dev_bch"></a>

### Dev_BCH

Representa un equipo informático del inventario de Banco de Chile.

- id (incremental)
- rotulo (string)
- serie (string)
- tipo (string)
- marca (string)
- modelo (string)
- nombre (string)
- rut (string)
- region  (string)
- site  (string)

<a name="otdigital"></a>

### OTDigital

Recoge los datos de la OTDigital ingresada mediante importación CSV.

- ticket
- fecha_atencion
- foliodespacho
- folioot
- cliente
- sucursal
- nombre
- rut
- direccion
- ciudad
- tipo1
- marca1
- serial1
- rotulo1
- tipo4
- marca4
- serial4
- rotulo4
- usuario
- proveedor

<a name="region"></a>

### Region

- id (no incremental, representa al número de la región, por ejemplo, 2)
- name (el nombre de la región, por ejemplo, "ANTOFAGASTA")

<a name="comuna"></a>

### Comuna

- id (incremental, identifica a la comuna, por ejemplo, 432)
- name (string, el nombre de la comuna, por ejemplo, "Chimbarongo")
- region_id (integer, el nombre de la región, por ejemplo, 8 de "BIOBIO")

<a name="pv"></a>

### Pv

Almacena los campos de puntos vigentes que consiste en un informe de Banco Estado con todos los datos de ubicación de los POS. 

Los campos son los siguientes:

- id (incremental)
- pos_id (id original de la ubicación)
- estado (p. ej: VIGENTE)
- fecha (datetime, la fecha del documento procesado)
- serial (el POS asociado en ese momento a la ubicación)



<a name="relpvs"></a>

### RelPVS

Agrupa los datos que originalmente venían de la carga de PVS. Así la aplicación no necesita cargar todos los datos de Puntos Vigentes sino que sólo lo que necesita.

- serial (string)
- pos_id (int) relacionada con POS
- fecha (datetime)

<a name="provider"></a>

### Provider 

Este modelo se emplea para identificar al proveedor de servicios, por ejemplo, ADEXUS, IBM, ENTEL, etc.

- id (incremental)
- name (nombre, p. ej. ENTEL)

<a name="zpec"></a>

### Zpec

Este modelo mantiene los datos del Zpec para diversos cruces neecsarios para la aplicación.

- id (identifica al equipo, viene en el documento, no es incremental)
- cliente
- rótulo
- material
- serie
- rut
- direccion
- status
- operacion
- fecha
- guia 
- sap
- usuario (de SAP, no de esta aplicación)

<a name="iq09"></a>

### IQ09

Contiene la información de la tabla iq09s que refleja el inventario de bodega.
Todos los datos excepto el id son *strings*.

- id (incremental)
- material
- rotulo
- serie
- centro 
- almacen (este es el mismo valor que aparece en location_id) 
- tipo stock (similar a Status)
- nombre (descripción del equipo)
- modificado (fecha de modificación original en SAP)
- status_sistema (string, por ejemplo, "ALMA")
- pep (código contable)
- creado (datetime, no confundir con created_at)

<a name="bch_employee"></a>

### Bch_employee

Este modelo contiene la nómina de empleados de Banco de Chile. 

- id (incremental)
- name (nombre del empleado)
- username ( nombre de usuario, por ejemplo, el que aparece en su login de windows y en su correo)
- email 
- rut (string)
- direccion

<a name="bch_location"></a>

### Bch_location

Este modelo refiere a la ubicación de cada sucursal del Banco de Chile

- id (incremental)
- sucursal
- direccion
- ciudad
- region

<a name="sccm"></a>

### Sccm

Este modelo almacena el contenido de una planilla de equipos llamada Sccm (cargado con SccmImport detallado en los importadores)-

- id (es incremental)
- hostname (o alias)
- model_id (refiere a Modelo)
- chassis
- serie
- segmento (la ip)
- username (depurado, sin el dominio)
- site_id (refiere a Bch_location)
- fecha_carga (es la fecha del documento que el usuario tiene que especificar)
- ultima_conexion (fecha, denominada last_scan a veces) 
- red (producción, QA, etc)

<a name="material"></a>

### Material

El modelo contiene la tabla de códigos de material que permite identificar a los equipos por marca, propiedad, modelo, etc.
La tabla asocia un código con un modelo.

- id (es incremental)
- material (es el código de material)
- descripción (es el modelo  del equipo)
- perfil (identifica si es seriado o no)
- tipo (asocia un código a un tipo de equipo, por ejemplo PC = PC DE ESCRITORIO)

Se carga a través de MaterialImport.




## Cambios

Actualizado por última vez el 31-05-2019 por Francisco Fuentes <ffuentese@entel.cl>. 