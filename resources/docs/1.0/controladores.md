# Controladores


---

Un controlador en un [Modelo Vista Controlador](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador) es una clase que se encuentra entre el modelo de datos y la vista (la página web).

El proyecto tiene varios controladores para manipular la información que se muestra en cada página. Eso incluye las llamadas a la base de datos, cálculos, etc.

Los controladores de este proyecto están en app/Http/Controllers. 

- [BodegaController](#bodega)
- [DashboardController](#dashboard)
- [EntregaController](#entrega)
- [ModeloMarcaController]($modelomarca)
- [POSCheckController](#poscheck)
- [POSHistoryController](#poshistory)
- [EntregaIBMController](#entregaibm)
- [ReversaIBMController](#reversaibm)
- [SearchController](#search)
- [ServiceController](#service)
- [InvBCHController](#invbch)
- [VistasController](#vistas)
- [VistasCuadroIBMController](#vistascuadro)
- [Cambios](#cambios)

<a name="bodega"></a>

##  Descripción

### BodegaController

Maneja la carga de las planillas IQ09. Utiliza a BodegaImport para manipular las planillas.

El formato de las planillas del IQ09 incluye estas columnas, aunque sólo algunas se usan.

    $row[0] = Equipo (no se usa)
    $row[1] = Material (en el modelo el atributo se llama "code") 
    $row[2] = Rótulo 
    $row[3] = Serial
    $row[4] = Centro de costos
    $row[5] = Ubicación (location_id en el modelo de Device)
    $row[6] = Estatus (código de estatus en bodega)
    $row[7] = Modelo
    $row[8] = Fecha de modificación (no se usa)
    $row[9] = Modificado por (no se usa)
    $row[10] = Estatus sistema (no se usa)
    $row[11] = Fecha de creación en sistema de stock (no se usa)
    $row[12] = Código PEP

<a name="dashboard"></a>

### DashboardController

Muestra la información de resumen del sistema. En este informe hay dos vistas hasta el momento:

- POS general
- POS a cargo de IBM

El controlador contiene una serie de consultas a la base que permiten obtener las variables para crear el informe.
Es importante que no sean consultas muy pesadas para que la página cargue rápidamente. 


Los conceptos más importantes de la tabla superior son:

- Ingresos: Incluye todas las series con un acta de entrega.
- Reversas: Incluye las actas de reversa. No todo serie reversado tiene necesariamente una reversa.
- Series justificadas: Se compone de los equipos que cuentan con un acta de ingreso y una instalación 
- Series justificadas habidas: Consiste en el conjunto de equipos que se encuentran en poder de un servicio técnico o están en bodega.
- Series ilegibles: Algunas series no tienen un serie visible y son registradas sin él. 

En la tabla inferior se aprecian los totales de series que están en bodega o en poder de los servicios técnicos, separados entre stock y reversa.

<a name="entrega"></a>

### EntregaController

Registra las planillas de actas de entrega y también los consolidados de entrega. 
El controlador llama a clases importadoras que hacen el trabajo de importar y clasificar la información.

#### index

Muestra la página de las cargas.

#### procesoPlanilla

Sube la/s planilla/s de **acta de entrega** que llega/n desde el formulario y ésta es procesada posteriormente 
en una tarea programadapor App\Imports\ActaImport.
Requiere un formato de archivo específico para asegurarnos de que se trata del acta correcta.  

[Correlativo]_[Acta_Entrega_POS_CO_ENTEL]_[Fecha en formato dd-mm-yyyy]_[n° de guía].xlsx

#### procesoConsolidado

Procesa la planilla de entregas consolidadas que llega desde el formulario. Ésta es procesada por App\Imports\BulkImport
No necesita un nombre o datos especiales.

<a name="modelomarca"></a>

### ModeloMarcaController

Muestra y permite modificar los datos de modelo y marca. Es un mantenedor. Está por si llega a ser necesario modificar algún dato.

#### index

Muestra la página incluyendo las llamadas a la base para mostrar las marcas y los modelos cargados.

#### agregarMarca

Agrega una marca a la base de datos. Estas marcas están ordenadas con un  id de 3 letras igual al que tiene bodega.

#### agregarModelo

Agrega un modelo a la base de datos. Cada modelo está asociado a una marca. 

#### verModelo

Carga otra página que permite ver los equipos asociados a un modelo. 

<a name="poscheck"></a>

### POSCheckController

El procesoPlanilla carga App\Imports\POSImport para cargar los puntos vigentes. 
Necesita que el usuario ingrese la fecha de la planilla.

<a name="poshistory"></a>

### POSHistoryController

Es un buscador que permite encontrar todos los dispositivos asociados a un POS. 

#### index

Muestra el formulario para buscar.

#### procesoPlanilla

Recibe la información del formulario, el argumento de la búsqueda y luego trae los dispositivos. 

### ReversaController

Hace lo mismo que EntregaController pero para los documentos de reversa.

#### procesoPlanilla

Procesa la/s planilla/s de **acta de entrega** de Entel a Adexus que llega/n desde el formulario y ésta/s es/son procesada posteriormente 
en una tarea programada por App\Imports\ActaRecImport.
Requiere un formato de archivo específico para asegurarnos de que se trata del acta correcta.  

El formato de los nombres de archivo es el siguiente:

[Correlativo] [Acta Entrega POS ADEXUS] [fecha en formato dd-mm-yyyy] [N° de guía].[xlsx].


#### procesoConsolidado

Procesa la planilla de reversas consolidadas que llega desde el formulario. Ésta es procesada por App\Imports\BulkRevImport
No necesita un nombre o datos especiales.

<a name="entregaibm"></a>

### EntregaIBMController

Hace lo mismo que EntregaController pero para los documentos de IBM.

Formato: [Correlativo] [Acta Entrega IBM] [Fecha en formato dd-mm-yyyy] [n° guía].[xlsx]
Por ejemplo, "105 Acta Entrega IBM 30-10-2018 482978.xlsx".

#### procesoPlanilla

Procesa las planillas de entrega que llegan desde el formulario, validando su nombre de archivo y las deja en la carpeta interna correspondiente para que sean procesadas posteriormente
en una tarea programada con el importador App\Imports\ActaImportIBM validando el formato del archivo (columnas, tipos de datos, etc).

<a name="reversaibm"></a>

### ReversaIBMController

Hace lo mismo que ReversaController pero con los documentos de IBM. 

Formato: [Correlativo] [Acta Reversa IBM] [Fecha en formato dd-mm-yyyy] [n° guía].[xlsx]

#### procesoPlanilla

Procesa las planillas de entrega que llegan desde el formulario, validando su nombre de archivo y las deja en la carpeta interna correspondiente para que sean procesadas posteriormente
en una tarea programada con el importador App\Imports\ActaRecImportIBM validando el formato del archivo (columnas, tipos de datos, etc).

<a name="search"></a>

### SearchController

Es un buscador que permite encontrar los equipos por serial o rótulo.

Este buscador permite mostrar todo el historial del dispositivo, incluso el equipo ubicado anteriormente en el mismo POS (si es que estuvo instalado).

Tanto **serial()** como **rotulo()** hacen el mismo proceso de buscar una colección de dispositivos de acuerdo a lo ingresado por el usuario.

<a name="service"></a>

### ServiceController

Procesa las planillas de los técnicos de acuerdo a un formato especificado. En el modelo corresponde al modelo Service.

    $row[0] = Nombre proveedor (STR)
    $row{1] = Nombre del técnico
    $row[2] = Stock/Reversa
    $row[3] = Fecha de recepción (lo utilizan para ingresar la fecha de las reversas)
    $row[4] = Tipo (en este caso nos interesa POS)
    $row[5] = Modelo equipo 
    $row[6] = Rótulo
    $row[7] = Número de serie.

El importador es ServiceImport que es el que hace el trabajo pesado. El controlador pasa la información al importador y una vez que ha concluido esta, redirige a la página con los resultados (en este caso la misma).

<a name="invBCH"></a>

### InvBCHController

Está encargado de la carga de los datos de inventario de Banco de Chile y su visualización.

En el modelo corresponde al modelo Dev_BCH y está separado de los demás datos. 

La carga de los datos es procesada por la función proceso(). 

La visualización de los datos está procesada por index() y por getDatatable() que carga los datos vía [AJAX](https://es.wikipedia.org/wiki/AJAX).

<a name="vistas"></a>

### VistasController

Controlador que produce visualizaciones de distintos tipos de información del sistema para análisis más específicos:

#### noJustificadas

Busca los dispositivos no justificados y los muestra.

Se encuentra en /nojustificadas

#### buscarPorGuia

Muestra formulario de búsqueda de equipos por guía. También muestra las guías y un buscador por mes. 

Utiliza una paginación manual por limitaciones de Eloquent.

Se encuentra en /buscar_por_guia

#### buscarPorGuiaIBM

Muestra formulario de búsqueda de equipos de IBM por guía. Se encuentra en /buscar_ibm.

Podemos realizar 3 actividades:

1. Buscar por un número específico de guía
2. Buscar guías por mes
3. Ver las guías y equipos relacionados agrupados por mes 

#### porGuiaRecepcion

Retorna una tabla con paginación con los equipos que coinciden con una guía de recepción.

#### porGuiaReversa

Retorna una tabla con paginación con los equipos que coinciden con una guía de reversa.

#### porGuiaIBM

Retorna una tabla con paginación con los equipos que coinciden con una guía de IBM.

#### porMesGuiaIBM

Como buscarPorGuiaIBM recupera las guías pero por un mes determinado.

#### porMesGuiaRecepcion

Lista las guías de entrega por mes introducido por el usuario.

#### porMesGuiaReversa

Lista las guías de reversa por mes introducido por el usuario.

#### exportRecepcion

Exporta como Excel el detalle de las series de la guía de entrega.

#### exportReversa

Exporta como Excel el detalle de las series de la guía de reversa.

<a name="vistascuadro"></a>

### VistasCuadroIBMController

Este controlador se encarga de mostrar el detalle de los cuadros del panel. Cada número tiene un enlace a una vista que entrega tal detalle.



#### entregas3g

Retorna todas las directas que corresponden a un dispositivo 3G.

#### entregasGprs

Retorna todas las directas que corresponden a un dispositivo GPRS.

#### entregasVef

Retorna todas las directas de accesorios que corresponden a trafos Verifone.

#### entregasCas

Retorna todas las directas de accesorios que corresponden a trafos Castles.

#### reversas3g

Retorna todas las reversas que corresponden a un dispositivo 3G.

#### reversasGprs

Retorna todas las reversas que corresponden a un dispositivo GPRS.

#### reversasVef

Retorna todas las reversas que corresponden a un trafo Verifone (en Accesorios).

#### reversasCas

Retorna todas las reversas que corresponden a un trafo Castles (en Accesorios).

#### stockPOSIBM3G

Retorna los dispositivos que corresponden a un equipo 3G y están en una directa IBM

#### stockPOSIBMGPRS

Retorna los dispositivos que corresponden a un equipo GPRS y están en una directa IBM

#### stockTrafoIBMVerifone

Retorna los dispositivos que corresponden a un trafo Verifone y están en una directa IBM

#### stockTrafoIBMCastles

Retorna los dispositivos que corresponden a un trafo Castles y están en una directa IBM

#### revPOSIBM3G

Retorna los dispositivos que corresponden a un equipo 3G y están en una reversa IBM

#### revPOSIBMGPRS

Retorna los dispositivos que corresponden a un equipo GPRS y están en una reversa IBM

#### revTrafoIBMVerifone

Retorna los dispositivos que corresponden a un trafo Verifone y están en una reversa IBM

#### revTrafoIBMCastles

Retorna los dispositivos que corresponden a un trafo Castles y están en una reversa IBM

<a name="cambios"></a>

## Cambios

Actualizado por última vez el 28-06-2019 por Francisco Fuentes <ffuentese@entel.cl>. 