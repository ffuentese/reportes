@extends('layouts.app')
@section('title', 'Subir archivo de OT Digital')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Importar OTDigital</div>
                
                <div class="card-body">
                    <p>El formato es: "OTDigital [fecha].csv"</p>
                    @isset($ultima_at)
                    <p>La última atención registrada en la base es del {{$ultima_at}}</p>
                    @endisset
                     <form action="{{ route('proceso_ot_digital') }}"  method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class="col-sm-6">
                        <div class="form-group">
                            <label for='datetimepicker4'>Fecha de inicio</label>
                            <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" name='inicio' data-target="#datetimepicker4"/>
                                <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                            <label for='datetimepicker5'>Fecha de término</label>
                            <div class="input-group date" id="datetimepicker5" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" name='termino' data-target="#datetimepicker5" />
                                <div class="input-group-append" data-target="#datetimepicker5" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <label for='import_file'>Seleccione archivo</label>
                        <input type="file" name="import_file" class="form-control">
                     </div>
         
                    <br>
                    <button type="submit" class="btn btn-success">Subir OTDigital</button>
                </form>
                </div>
                
                @if ($errors->count() > 0)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  @foreach ($errors->all() as $error)  
                  <p>{{ $error }}</p>
                  @endforeach
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                @endif        
                    
                </div>
                @if (session()->has('success'))

                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>{{session('success')}}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endisset
                                
                </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker4').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $('#datetimepicker5').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false
        });
        $("#datetimepicker4").on("change.datetimepicker", function (e) {
            $('#datetimepicker5').datetimepicker('minDate', e.date);
        });
        $("#datetimepicker5").on("change.datetimepicker", function (e) {
            $('#datetimepicker4').datetimepicker('maxDate', e.date);
        });
    });
</script>
@endsection