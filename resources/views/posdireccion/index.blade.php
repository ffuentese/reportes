@extends('layouts.app')
@section('title', 'Registrar direcciones de POS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Registra direcciones de POS</div>
                
                <div class="card-body">
                     <form action="{{ route('upd_posdireccion') }}" enctype="multipart/form-data" method="POST">
                     @csrf
                     <input type="file" name="import_file" class="form-control">
         
                <br>
                <button type="submit" class="btn btn-success btn-lg">Iniciar proceso</button>
                
                </form>
                </div>
                
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif (session('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{{session('success')}}</li>
                    </ul>
                </div>
                @endif
                                
                </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection