@extends('layouts.app')
@section('title', 'Consolidado de códigos de material')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"> 
            <div class="card mt-5">
                <div class="card-header">Consolidado de códigos de material</div>
                <div class="card-body">
                <table id="material-table" class="table table-sm table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Material</th>
                        <th>Descripción</th>
                        <th>Perfil</th>
                        <th>Tipo</th>
                        <th>Subtipo</th>
                        
                    </tr>
                    </thead>
                    <tbody>
      
                    </tbody>
                </table>

            </div>
        </div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>

<script type='text/javascript'>
$('#material-table').DataTable({
    processing: false,
    serverSide: true,
    ajax: "{{route('material_datatable')}}",
    language: 
                {"url": "{{asset('assets/dt/Spanish.lang')}}"}
        ,
    columns: [
        { data: 'id', name: 'id' },
        { data: 'material', name: 'material' },
        { data: 'descripcion', name: 'descripcion' },
        { data: 'perfil', name: 'perfil' },
        { data: 'tipo', name: 'tipo' },
        { data: 'subtipo', name: 'subtipo' }
    ]
});
</script>
@endsection