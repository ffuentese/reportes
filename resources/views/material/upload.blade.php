@extends('layouts.app')
@section('title', 'Subir archivo consolidado material')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Importar consolidado de material</div>
                
                <div class="card-body">
                    <p>El formato es:</p>
                    
                     <form action="{{route('proceso_material')}}"  method="POST" enctype="multipart/form-data">
                     @csrf
                     
                     <div class="col-sm-6">
                        <label for='import_file'>Seleccione archivo</label>
                        <input type="file" name="import_file" class="form-control">
                     </div>
         
                    <br>
                    <button type="submit" class="btn btn-success">Subir consolidado de materiales</button>
                </form>
                </div>
                
                @if ($errors->count() > 0)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  @foreach ($errors->all() as $error)  
                  <p>{{ $error }}</p>
                  @endforeach
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                @endif        
                    
                </div>
                @if (session()->has('success'))

                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>{{session('success')}}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endisset
                                
                </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection