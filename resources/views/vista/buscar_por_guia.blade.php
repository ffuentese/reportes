@extends('layouts.app')
@section('title', 'Buscar guías Adexus')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            @if (session('error'))
            <div class="alert alert-warning">
                {{ session('error') }}
            </div>
            @endif
            <div class="card">
                <div class="card-header">Directa v/s Reversa diario</div>
                <div class="card-body">
                <div id="calendar"></div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Buscar guías</div>
                
                <div class="card-body">
                     <form id="buscar-guia" action="{{ route('guia_recepcion') }}">
                
                <label for="guia">N° de guía: </label>
                <div class="form-group row col-12 m-t-5">
                <input type="number" name="guia" class="form-control">
                </div>

                <div class="form-group row col-3 m-t-5">
                    <select name="option" id="option" class="form-control mr-sm-3" onchange="onSelectedOption(this);">
                        <option value="entrega">Entrega</option>
                        <option value="reversa">Reversa</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Buscar</button>
                
                </form>
                </div>

                
                                
            </div>

            </div>
        
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Buscar guías por mes</div>
                <form action="{{route('guias_adexus_mes')}}">
                    <div class="form-group row m-t-5">
                        <label for="month" class="col-sm-2 col-form-label m-l-15">Mes </label>
                        <select name="month" id="month" class="form-control col-sm-2" >
                            @foreach($col_entregas as $month)
                            <option value="{{$month->month}}-{{$month->year}}">{{$month->month}}-{{$month->year}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary m-l-5">Buscar</button>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-header">Entregas</div>
                <div class="card-body">
                      <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Mes</th>
                        <th>Cantidad</th>
                        

                        
                    </tr>
                </thead>
                <tbody>
                    @forelse($entregas as $entrega)
                    <tr>
                        <td><a href="{{route('guias_adexus_mes', ['month' => $entrega->month . '-' . $entrega->year])}}">{{$entrega->month . '-' . $entrega->year}}</a></td>
                        <td>{{$entrega->cantidad}}</td>
                        
                       
                    </tr>
                    @empty
                    <td>No hay registros en la base de datos</td>
                    @endforelse
            </tbody>
            </table>
                </div>
                    {{$entregas->appends(request()->input())->links()}}
            </div>
        </div>
            
           
            
            <div class="card">
                <div class="card-header">Reversas</div>
                <div class="card-body">
                      <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Mes</th>
                        <th>Cantidad</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @forelse($reversas as $reversa)
                    <tr>
                        <td><a href="{{route('guias_adexus_mes', ['month' => $reversa->month . '-' . $reversa->year])}}">{{$reversa->month . '-' . $reversa->year}}</a></td>
                        <td>{{$reversa->cantidad}}</td>
                        
                    </tr>
                    @empty
                    <td>No hay registros en la base de datos</td>
                    @endforelse
            </tbody>
            </table>
                </div>
                    {{$reversas->appends(request()->input())->links()}}
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('/assets/libs/fullcalendar/dist/fullcalendar.min.css')}}" />
<link rel='stylesheet' href='{{asset('/assets/libs/fullcalendar/dist/fullcalendar.print.min.css')}}' media='print' /> 
<script type="text/javascript"  src='{{asset('/assets/libs/fullcalendar/dist/locale-all.js')}}'></script>
<script type="text/javascript"  src='{{asset('/assets/libs/fullcalendar/dist/fullcalendar.min.js')}}'></script>
<script>
    function onSelectedOption(sel) {
        if ((sel.value).localeCompare('entrega') === 0) {
            document.getElementById("buscar-guia").action =
             "{{route('guia_recepcion')}}"; 
        }
        else if ((sel.value).localeCompare('reversa') === 0)
        {
            document.getElementById("buscar-guia").action =
                    "{{route('guia_reversa')}}"; 
        }
    }
</script>
<script type="text/javascript">
// Carga de calendario
    
    var today = new Date();
    var calendar = function() {
        $('#calendar').fullCalendar({
        height: 550,
        <!--Header Section Including Previous,Next and Today-->
        header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
        },
        locale: 'es',

        <!--Default Date-->
        defaultDate: today,
        editable: false,

        <!--Event Section-->
        eventLimit: true, // allow "more" link when too many events
        events: [
        @foreach($diario as $d)
            {
            
            start: "{{$d['fecha']->format('Y-m-d')}}",
            @if ($d['tipo'] == 'ENTREGA')
            title: "Directas: {{$d['cantidad']}}",
            backgroundColor: '#D7F0F7',
            textColor: 'black'
            @else 
            title: "Reversas: {{$d['cantidad']}}",
            backgroundColor: 'red',
            textColor: 'white'
            @endif

            },
        @endforeach
        ]
    });
 };
 // Renderiza el calendario.
 calendar();
</script>
@endsection