@extends('layouts.app')
@section('title', 'Guías de período ' . $fecha)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>Guías del mes: '{{$fecha}}':</p>
                    
                </div>
            </div>
            <div class="card">
                <div class="card-header">Entrega</div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Guía</th>
                        <th>Cantidad</th>
                        <th>Fecha guía</th>
                        <th>Ver PDF</th>
                    </tr>
                </thead>
                <tbody>
                @forelse ($entregas as $device)
                <tr>
                    <td><a href="{{route('guia_recepcion', ['guia' => $device->guia])}}">{{$device->guia}}</a></td>
                    <td>{{$device->cantidad}}</td>
                    <td>{{$device->fecha ? (new Carbon\Carbon($device->fecha))->format("d/m/Y") : "" }}</td>
                    @if ($device->file)
                    <td><a href="{{route('showPDF', ['guia' => $device->guia])}}">Ver PDF</a></td>
                    @else
                    <td></td>
                    @endif
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                @if (count($entregas)>0 && $entregas->currentPage() == $entregas->lastPage())
                <tr>
                    <td>{{'Total: ' . $entregas->total()}}</td>
                    <td>{{$total_entregas}}</td>
                </tr>
                @endif
                </tbody>
            </table>
                </div>
                {{$entregas->appends(request()->input())->links()}}
                </div>
            
            <div class="card">
                <div class="card-header">Reversa</div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Guía</th>
                        <th>Cantidad</th>
                        <th>Fecha guía</th>
                        <th>Ver PDF</th>
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($reversas as $device)
                <tr>
                    
                    <td><a href="{{route('guia_reversa', ['guia' => $device->guia])}}">{{$device->guia}}</a></td>
                    
                    <td>{{$device->cantidad}}</td>
                    <td>{{$device->fecha ? (new Carbon\Carbon($device->fecha))->format("d/m/Y") : "" }}</td>
                    @if ($device->file)
                    <td><a href="{{route('showPDF', ['guia' => $device->guia])}}">Ver PDF</a></td>
                    @else
                    <td></td>
                    @endif
                    
                </tr>
                
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                @if (count($reversas)>0 && $reversas->currentPage() == $reversas->lastPage())
                <tr>
                    <td>{{'Total: ' . count($reversas)}}</td>
                    <td>{{$total_reversas}}</td>
                </tr>
                @endif
                </tbody>
            </table>
                </div>
                {{$reversas->appends(request()->input())->links()}}
                </div>
         
            
         
    </div>
</div>
</div>
@endsection