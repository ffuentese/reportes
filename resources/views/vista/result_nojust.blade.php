@extends('layouts.app')
@section('title', 'Dispositivos no justificados')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($devices)
            
            <div class="card m-t-5">
                <div class="card-header">
                    No justificadas
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Id</th>
                        <th>Serial</th>
                        <th>Fecha recepción</th>
                        <th>GD provisión</th>
                        
                        
                        
                        
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr class=''>
                    <td><a href="{{route('serial', ['serial' => $device->serial])}}">{{$device->serial}}</a></td>
                    <td>{{$device->modelo}}</td>
                    <td>{{date('d-m-Y', strtotime($device->fecha_recepcion))}}</td>
                    <td><a href="{{route('guia_recepcion', ['guia' => $device->guia_recepcion])}}">{{$device->guia_recepcion}}</td>
                    
                    
                    
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                 @if ($devices->total() > 0)
                 <p>Resultados {{$devices->firstItem()}} al {{$devices->lastItem()}} de {{$devices->total()}} registros.</p>
                 @endif
                 {{ $devices->links() }}
                </div>

            @endisset
            
            </div>
        </div>
    </div>
</div>
@endsection