@extends('layouts.app')
@section('title', 'Buscar guías IBM')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Buscar guías IBM</div>
                
                <div class="card-body">
                     <form action="{{ route('guia_ibm') }}">
                
                    <label for="guia">Por guía </label>
                    <input type="number" name="guia" class="form-control" required="required">
                    <br>
                    <button type="submit" class="btn btn-success">Buscar</button>
                
                </form>
                </div>       
            </div>
            
            <div class="card">
                <div class="card-header">Buscar por mes</div>
                <form action="{{route('buscar_ibm_por_mes')}}">
                    <div class="form-group row m-t-5">
                        <label for="month" class="col-sm-2 col-form-label m-l-5">Mes </label>
                        <select name="month" id="month" class="form-control col-sm-2" >
                            @foreach($listado as $month)
                            <option value="{{$month->month}}-{{$month->year}}">{{$month->month}}-{{$month->year}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary m-l-5">Buscar</button>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-header">Entregas</div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Mes</th>
                        <th>Cantidad</th>
                        
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr>
                    
                    <td><a href="{{route('buscar_ibm_por_mes', ['month' => $device->month . '-' . $device->year])}}">{{$device->month . '-' . $device->year}}</a></td>
                    <td>{{$device->cantidad}}</td>
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$devices->appends(request()->input())->links()}}
                </div>
                
                <div class="card">
                <div class="card-header">Reversas</div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Mes</th>
                        <th>Cantidad</th>
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($reversas as $reversa)
                <tr>
                    
                    <td><a href="{{route('buscar_ibm_por_mes', ['month' => $reversa->month . '-' . $reversa->year])}}">{{$reversa->month . '-' . $reversa->year}}</a></td>
                    <td>{{$reversa->cantidad}}</td>
                   
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$reversas->appends(request()->input())->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection