@extends('layouts.app')
@section('title', 'Detalle de guía #' . $guia)
@section('content')
<div id="holder" style="display: none;">
        <div id="loader" class="loader" style="display: none;"></div>
    </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($devices)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>Equipos que aparecen con guía de {{$tipo}} '{{$guia}}':</p></div>
                <div class="card-body">
                    @if ($tipo == 'entrega' && !$devices->isEmpty())
                    <button onclick="return getExcel('{{route('export_recepcion', ['guia' => $guia]) }}');" class="btn btn-success">Excel</button>
                    @elseif ($tipo == 'reversa' && !$devices->isEmpty())
                    <button onclick="return getExcel('{{route('export_reversa', ['guia' => $guia]) }}');" class="btn btn-success">Excel</button>
                    @endif
                    @if (!$devices->isEmpty()) 
                    <button onclick="return deleteGuia('{{route('borrar_guia') }}');" class="btn btn-danger">Borrar guía</button>
                    @endif
                </div>
            </div>
            
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Serial</th>
                        <th>Modelo</th>
                        <th>GD provisión</th>
                        <th>F.provisión</th>
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr>
                    <td><a href="{{route('serial',['serial' => $device->serial])}}">{{$device->serial}}</a></td>
                    <td>{{$device->modelo}}</td>
                    <td>{{$device->guia}}</td>
                    <td>{{$device->fecha ? $device->fecha->format("d/m/Y") : "" }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$devices->appends(request()->input())->links()}}
                
               
                </form> 
                </div>
            
          @endisset
    </div>
</div>
</div>
@endsection

@section('scripts')
<style>
  .loader {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    transform: -webkit-translate(-50%, -50%);
    transform: -moz-translate(-50%, -50%);
    transform: -ms-translate(-50%, -50%);
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    z-index: 1000;
    animation: spin 2s linear infinite;
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  
  #holder 
  {
    position: absolute;
    background: grey;
    opacity: 0.5;
    z-index: 999;
    width: 100%;
    height: 100%;        
  }             
  </style>
  <script type="text/javascript">
      function getExcel(urlToSend) {
        var req = new XMLHttpRequest();
        req.open("GET", urlToSend, true);
        req.responseType = "blob";
        $('#loader').show();
        $('#holder').show();
        req.onreadystatechange = function () {
            if (req.status != 200 && req.readyState === 2){
                $('#loader').hide();
                $('#holder').hide();
                req.responseType = "text";
            }
            if(req.readyState == 4 && req.responseType == "text") {
                var msg = JSON.parse(req.response);
                alert(msg.error);
            }
            var blob = req.response;
            var contentDisp = req.getResponseHeader("Content-Disposition"); //if you have the fileName header available
            var regex = "filename[^;\n=]*=(['\"])*(?:utf-8\'\')?(.*)((1)\1|)";
            var fName = contentDisp.match(regex);
            console.log(fName);
            var fileName = fName[2].slice(0, -1);
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download=fileName;
            link.click();
            $('#loader').hide();
            $('#holder').hide();
        };
        req.send();
    }
    
    function deleteGuia(urlToSend) {
        var borra = confirm('¿Está seguro de borrar la guía?');
        if (!borra) {
            return;
        }
        var req = new XMLHttpRequest();
        var csrfToken = document.querySelector('meta[name="csrf-token"]').content;
        req.open("POST", urlToSend, true);
        req.setRequestHeader('X-CSRF-TOKEN', csrfToken);
        req.setRequestHeader('Content-Type', 'application/json');
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                if (this.readyState === this.DONE) {
                    console.log(this.responseURL);
                }
                if (req.status != 200) {
                    var msg = JSON.parse(req.response);
                    alert(msg.error);
                } else {
                    alert('Exitoso');
                }
            }
        }

        const data = {
            guia: "{{$guia ?? ''}}",
            tipo: "{{$tipo ?? ''}}"
        }
        req.send(JSON.stringify(data));
    }
      </script>
@endsection