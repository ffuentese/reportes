@extends('layouts.app')
@section('title', 'Dispositivos no justificados')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($devices)
            
            <div class="card m-t-5">
                <div class="card-header">
                    No justificadas
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Id</th>
                        <th>Serial</th>
                        <th>Rótulo</th>
                        <th>Modelo</th>
                        <th>GD provisión</th>
                        <th>F.provisión</th>
                        <th>F.instalación</th>
                        <th>F.reversa serie retirada</th>
                        <th>Serie retirada</th>
                        <th>GD retirada</th>
                        <th>Modelo serie retirada</th>
                        <th>POS</th>
                        <th>Servicio</th>
                        <th>Ubicación</th>
                        <th>Técnico</th>
                        
                        
                        
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr class='{{isset($device->previous_device)  ? 'table-success' : ""}}'>
                    <td>{{$device->id}}</td>
                    <td>{{$device->serial}}</td>
                    <td>{{$device->rotulo ?? ""}}</td>
                    <td>{{$device->modelo->name}}</td>
                    <td>{{$device->guia_recepcion}}</td>
                    <td>{{$device->fecha_recepcion ? $device->fecha_recepcion->format("d/m/Y") : "" }}</td>
                    <td>{{$device->fecha_instalacion ? $device->fecha_instalacion->format("d/m/Y") : "" }}</td>
                    <td>{{isset($device->previous_device->fecha_reversa) ? $device->previous_device->fecha_reversa->format("d/m/Y") : ""}}</td>
                    <td> <a href="{{$device->previous_device ? route('serial', ['serial' => $device->previous_device->serial]) : ""}}">{{$device->previous_device->serial ?? ""}}</a></td>
                    <td>{{$device->previous_device->guia_reversa ?? ""}}</td>
                    <td>{{$device->previous_device->modelo->name ?? ""}}</td>
                    <td><a href="{{$device->pos_id > 0 ? route('pos', ['pos' => $device->pos_id]) : ""}}">{{$device->pos_id > 0 ? $device->pos_id : ""}}</a></td>
                    <td>{{$device->service->name}}</td>
                    <td>{{$device->location->name != "SIN ALMACÉN" ? $device->location->name : ""}}</td>
                    <td>{{$device->technician->name != "DESCONOCIDO" ? $device->technician->name : ""}}</td>
                    
                    
                    
                    
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                 @if ($devices->total() > 0)
                 <p>Resultados {{$devices->firstItem()}} al {{$devices->lastItem()}} de {{$devices->total()}} registros.</p>
                 @endif
                 {{ $devices->links() }}
                </div>

            @endisset
            
            </div>
        </div>
    </div>
</div>
@endsection