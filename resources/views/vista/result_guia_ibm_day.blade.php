@extends('layouts.app')
@section('title', 'Guías de período ' . $fecha)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($diario)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>Guías del mes: '{{$fecha}}':</p>
                    
                </div>
            </div>
            <div class="card">
                <div class="card-header">Detalle</div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Guía</th>
                        <th>Cantidad</th>
                        <th>Fecha guía</th>
                        <th>Tipo</th>
                        <th>Ver PDF</th>
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($diario as $device)
                <tr>
                    <td><a href="{{route('guia_ibm', ['guia' => $device->guia])}}">{{$device->guia}}</a></td>
                    <td>{{$device->cantidad}}</td>
                    <td>{{$device->fecha ? (new Carbon\Carbon($device->fecha))->format("d/m/Y") : "" }}</td>
                    <td>{{$device->estado == 'IBM' ? 'Entrega' : 'Reversa'}}</td>
                    @if ($device->file)
                    <td><a href="{{route('showPDF', ['guia' => $device->guia])}}">Ver PDF</a></td>
                    @else
                    <td></td>
                    @endif
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                </div>
            
          
            
          @endisset
    </div>
</div>
</div>
@endsection