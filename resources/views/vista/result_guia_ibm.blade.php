@extends('layouts.app')
@section('title', 'Detalle de guía #' . $guia)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($devices)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>Equipos que aparecen con guía: '{{$guia}}':</p>
                    @isset($pdf)
                    <p><a href="{{route('showPDF', ['guia' => $guia])}}">Ver documento</a></p>
                    @endisset
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Serial</th>
                        <th>Modelo</th>
                        <th>Guía</th>
                        <th>Tipo</th>
                        <th>Fecha guía</th>
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr>
                    <td><a href="{{route('serial',['serial' => $device->serial])}}" title='Ver información completa de este serie'>{{$device->serial}}</a></td>
                    <td>{{$device->model_name}}</td>
                    <td><a href="{{route('guia_ibm', ['guia' => $device->guia])}}">{{$device->guia}}</a></td>
                    <td>{{$device->estado == 'IBM' ? 'Entrega' : 'Reversa'}}</td>
                    <td>{{$device->fecha ? $device->fecha->format("d/m/Y") : "" }}</td>
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$devices->appends(request()->input())->links()}}
                </div>
            
          @endisset
    </div>
</div>
</div>
@endsection