@extends('layouts.app')
@section('title', 'Inicio')
@section('content')
<div class="container">
    <div class="page-breadcrumb">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="page-title">Panel</h4>

            </div>
        </div>
    </div>
    <div class="container-fluid m-t-15">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="ibm-tab" data-toggle="tab" href="#ibm" role="tab" aria-controls="ibm" aria-selected="true">IBM</a>
            </li>
        </ul>
 <div class="tab-content" id="myTabContent">  
     <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
         
         <div class="table-responsive m-t-15">
             
             <table class="table table-sm table-bordered table-striped border-thick">
                 <thead>
                     <tr>
                         <th class="bg-info text-white">Hito</th>
                         <th class="bg-info text-white">POS 3G</th>
                         <th class="bg-info text-white">POS GPRS</th>
                         <th class="bg-info text-white">Transformador Verifone</th>
                         <th class="bg-info text-white">Transformador Castles</th>
                     </tr>
                 </thead>
                 <tbody>
                     <!-- Puntos vigentes -->
                     <tr>
                         <th scope="row">Puntos Vigentes</th>
                         <td><a href="{{route('puntos-vigentes-3g')}}">{{$datos_cuadro['pvs_3g']}}</a></td>
                         <td><a href="{{route('puntos-vigentes-gprs')}}">{{$datos_cuadro['pvs_gprs']}}</a></td>
                         <td><a href="{{route('puntos-vigentes-vef')}}">{{$datos_cuadro['pvs_vef']}}</a></td>
                         <td><a href="{{route('puntos-vigentes-cas')}}">{{$datos_cuadro['pvs_cas']}}</a></td>
                     </tr>
                     <tr>
                         <th scope="row">9,7% Acordado</th>
                         <td>{{$datos_cuadro['acordado_3g']}}</td>    
                         <td>{{$datos_cuadro['acordado_gprs']}}</td>
                         <td>{{$datos_cuadro['acordado_vef']}}</td>
                         <td>{{$datos_cuadro['acordado_cas']}}</td>
                     </tr>
                     <!-- Entrega Stock Adexus -->
                     <tr>
                         <th scope="row">Entrega Stock Operativo Adexus</th>
                         <td><a href="{{route('entregas-3g')}}">{{$datos_cuadro['entregas_3g']}}</a></td>    
                         <td><a href="{{route('entregas-gprs')}}">{{$datos_cuadro['entregas_gprs']}}</a></td>
                         <td><a href="{{route('entregas-vef')}}">{{$datos_cuadro['entregas_vef']}}</a></td>
                         <td><a href="{{route('entregas-cas')}}">{{$datos_cuadro['entregas_cas']}}</a></td>
                     </tr>
                     <tr>
                         <th scope="row">Entrega Stock Operativo Proyectos</th>
                         <td><a href="{{route('entregas-proyectos-3g')}}">{{$datos_cuadro['proyectos_3g']}}</a></td>    
                         <td><a href="{{route('entregas-proyectos-gprs')}}">{{$datos_cuadro['proyectos_gprs']}}</a></td>
                         <td><a href="{{route('entregas-proyectos-vef')}}">{{$datos_cuadro['proyectos_vef']}}</a></td>
                         <td><a href="{{route('entregas-proyectos-cas')}}">{{$datos_cuadro['proyectos_cas']}}</a></td>
                     </tr>
                     <tr>
                         <th scope="row">Entrega Stock Reversa Adexus</th>
                         <td><a href="{{route('reversas-3g')}}">{{$datos_cuadro['reversas_3g']}}</a></td>    
                         <td><a href="{{route('reversas-gprs')}}">{{$datos_cuadro['reversas_gprs']}}</a></td>
                         <td><a href="{{route('reversas-vef')}}">{{$datos_cuadro['reversas_vef']}}</a></td>
                         <td><a href="{{route('reversas-cas')}}">{{$datos_cuadro['reversas_cas']}}</a></td>
                     </tr>
                     <tr>
                         <th scope="row">Spare Operacional</th>
                         <td>{{$datos_cuadro['spare_3g']}}</td>    
                         <td>{{$datos_cuadro['spare_gprs']}}</td>
                         <td>{{$datos_cuadro['spare_vef']}}</td>
                         <td>{{$datos_cuadro['spare_cas']}}</td>
                         
                     </tr>
                     <tr>
                         <th scope="row">Stock Requerido CO</th>
                         <td class='{{$datos_cuadro['stock_3g'] > 0 ? "bg-success" : "bg-danger"}} text-white'>{{$datos_cuadro['stock_3g'] > 0 ? '-' : $datos_cuadro['stock_3g'] }}</td>    
                         <td class='{{$datos_cuadro['stock_gprs'] > 0 ? "bg-success" : "bg-danger"}} text-white'>{{$datos_cuadro['stock_gprs'] > 0 ? '-' : $datos_cuadro['stock_gprs']}}</td>
                         <td class='{{$datos_cuadro['stock_vef'] > 0 ? "bg-success" : "bg-danger"}} text-white'>{{$datos_cuadro['stock_vef'] > 0 ? '-' : $datos_cuadro['stock_vef']}}</td>
                         <td class='{{$datos_cuadro['stock_cas'] > 0 ? "bg-success" : "bg-danger"}} text-white'>{{$datos_cuadro['stock_cas'] > 0 ? '-' : $datos_cuadro['stock_cas']}}</td>
                         
                     </tr>
                     
                     
                     <tr>
                         <th scope="row">% Parque en spare</th>
                         <td class='{{$datos_cuadro['pc_spare_3g'] >= 10 ? "bg-success" : "bg-danger" }} text-white' >{{$datos_cuadro['pc_spare_3g']}}</td>    
                         <td class='{{$datos_cuadro['pc_spare_gprs'] >= 10 ? "bg-success" : "bg-danger" }} text-white'>{{$datos_cuadro['pc_spare_gprs']}}</td>
                         <td class='{{$datos_cuadro['pc_spare_vef'] >= 10 ? "bg-success" : "bg-danger"}} text-white'>{{$datos_cuadro['pc_spare_vef']}}</td>
                         <td class='{{$datos_cuadro['pc_spare_cas'] >= 10 ? "bg-success" : "bg-danger"}} text-white'>{{$datos_cuadro['pc_spare_cas']}}</td>
                     </tr>
                    <tr>
                        <th scope="row">Consumo Promedio Mensual</th>
                        <td>{{$datos_cuadro['cpm_3g']}}</td>    
                        <td>{{$datos_cuadro['cpm_gprs']}}</td>
                        <td>{{$datos_cuadro['cpm_vef']}}</td>
                        <td>{{$datos_cuadro['cpm_cas']}}</td>
                     </tr>
                 </tbody>
             </table>
         </div> 
        <div class="table-responsive">
            <table class="table table-bordered border-thick">
                <thead>
                    <tr class="border-thick">
                        <th><small>Equipos recibidos formalmente por Entel desde Adexus</small></th>
                        <th><small>Reversas de equipos que fueron recibidos formalmente desde Adexus.</small></th>
                        <th><small>Equipos que están instalados y tienen la reversa o que fueron instalados.</small></th>
                        <th><small>Equipos que están en poder de técnicos o en bodega y que fueron recibidas en un acta.</small></th>
                        <th></th>
                        <th><small>Series que falta por cuadrar sin considerar IBM</small></th>
                        <th><small>Series justificadas  + Series justificadas habidas</small></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr class="border-thick">
                        <th class="bg-warning" data-toggle="tooltip" data-placement="top"
                            title="Equipos recibidos formalmente por Entel desde Adexus"
                            >Ingresos Adexus > Entel</th>
                        <th class="bg-warning" data-toggle="tooltip" data-placement="top"
                            title="Reversas de equipos que fueron recibidos formalmente desde Adexus."
                            >Reversas Entel > Adexus</th>
                        <th class="bg-light-green" data-toggle="tooltip" data-placement="top"
                            title="Equipos que están instalados y tienen la reversa o que fueron instalados."
                            >Series justificadas</th>
                        <th class="bg-light-green" data-toggle="tooltip" data-placement="top"
                            title="Equipos que están en poder de técnicos o en bodega y que fueron recibidas en un acta."
                            >Series justificadas habidas</th>
                        <th class="bg-success text-white">Series ilegibles</th>
                        <th class="bg-danger text-white">No habidas</th>
                        <th class="bg-cyan text-white" data-toggle="tooltip" data-placement="top"
                            title="Series justificadas  + Series justificadas habidas">Total Justificadas</th>
                        <th class="bg-cyan text-white">Justificadas + ilegibles</th>
                        <th class="bg-yellow">% Total justificado</th>
                        <th class="bg-yellow"><strong>% Total justificado + ilegibles</strong></th>

                    </tr>
                </thead>
                <tbody>
                <tr class="border-thick">
                    <td><h5 class="m-t-5">{{number_format($ingresos, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($reversas, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($rec_rev, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($habidas, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($ilegibles, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($no_habidas, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($total_just, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($total_just_ilegibles, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{$porc_just}}%</h5></td>
                    <td><h5 class="m-t-5">{{$porc_just_ilegibles}}%</h5></td>
                </tr>
                    </tbody>
                
            </table>
        </div>
         
        <div class="row m-t-15">
            <div class="col-6">
                <div class="bg-info p-10 text-white text-center">
                    <i class="fas fa-warehouse m-b-5 font-16"></i>
                    <h5 class="m-b-0 m-t-5">{{number_format($t_disponible, 0, '', '.')}}</h5>
                    <small class="font-light">Total Disponible</small>
                </div>
            </div>
            <div class="col-6">
                <div class="bg-info p-10 text-white text-center">
                    <i class="fas fa-cart-plus m-b-5 font-16"></i>
                    <h5 class="m-b-0 m-t-5">{{number_format($t_reversas, 0, '', '.')}}</h5>
                    <small class="font-light">Total reversa</small>
                </div>
            </div>
        </div>
        
          <div class="row m-t-15">
        <div class="col-2">
            <div class="bg-success p-10 text-white text-center">
                <i class="fas fa-warehouse m-b-5 font-16"></i>
                <h5 class="m-b-0 m-t-5">{{number_format($bod_sto, 0, '', '.')}}</h5>
                <small class="font-light">Bodega stock</small>
            </div>
        </div>
        <div class="col-2">
            <div class="bg-dark p-10 text-white text-center">
                <i class="fas fa-truck-moving m-b-5 font-16"></i>
                <h5 class="m-b-0 m-t-5">0</h5>
                <small class="font-light">Traslado</small>
            </div>
       </div>
        <div class="col-2">
            <div class="bg-warning p-10 text-white text-center">
                <i class="fas fa-people-carry m-b-5 font-16"></i>
                <h5 class="m-b-0 m-t-5">{{number_format($stock_tecnicos, 0, '', '.')}}</h5>
                <small class="font-light">Stock técnicos</small>
            </div>
        </div><div class="col-2">
            <div class="bg-success p-10 text-white text-center">
                <i class="fas fa-warehouse m-b-5 font-16"></i>
                <h5 class="m-b-0 m-t-5">{{number_format($bod_reve, 0, '', '.')}}</h5>
                <small class="font-light">Bodega reversa</small>
            </div>
        </div>
        <div class="col-2">
            <div class="bg-dark p-10 text-white text-center">
                <i class="fas fa-truck-moving m-b-5 font-16"></i>
                <h5 class="m-b-0 m-t-5">0</h5>
                <small class="font-light">Traslado</small>
            </div>
        </div>
        <div class="col-2">
            <div class="bg-warning p-10 text-white text-center">
                <i class="fas fa-people-carry m-b-5 font-16"></i>
                <h5 class="m-b-0 m-t-5">{{number_format($reversa_tecnicos, 0, '', '.')}}</h5>
                <small class="font-light">Reversa técnicos</small>
            </div>
        </div>
        </div>
         <div class="row m-t-5 m-b-15">
            <div class="col-md-12">
            <h4 class="page-title">Flujo por mes</h4>
            </div>
            <div class="col-md-12">
                <canvas id="flujomes" width="200" height="180"></canvas>
            </div>
        </div>
         
   
     </div>
     <div class="tab-pane fade" id="ibm" role="tabpanel" aria-labelledby="ibm-tab">
          <div class="table-responsive m-t-15">
             
             <table class="table table-sm table-bordered table-striped border-thick">
                 <thead>
                     <tr>
                         <th class="bg-info text-white">Hito</th>
                         <th class="bg-info text-white">POS 3G</th>
                         <th class="bg-info text-white">POS GPRS</th>
                         <th class="bg-info text-white">Transformador Verifone</th>
                         <th class="bg-info text-white">Transformador Castles</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <th scope="row">Entrega Stock Operativo Entregado a IBM</th>
                         <td><a href="{{route('stock-ibm-3g')}}">{{$cuadro_ibm['pos_3g_ibm_entrega']}}</a></td>
                         <td><a href="{{route('stock-ibm-gprs')}}">{{$cuadro_ibm['pos_gprs_ibm_entrega']}}</a></td>
                         <td><a href="{{route('stock-ibm-verifone')}}">{{$cuadro_ibm['trafo_vef_ibm_entrega']}}</a></td>
                         <td><a href="{{route('stock-ibm-castles')}}">{{$cuadro_ibm['trafo_cas_ibm_entrega']}}</a></td>
                     </tr>
                     <tr>
                         <th scope="row">Entrega Stock Reversado por IBM</th>
                         <td><a href="{{route('reversa-ibm-3g')}}">{{$cuadro_ibm['pos_3g_ibm_reversa']}}</a></td>
                         <td><a href="{{route('reversa-ibm-gprs')}}">{{$cuadro_ibm['pos_gprs_ibm_reversa']}}</a></td>
                         <td><a href="{{route('reversa-ibm-verifone')}}">{{$cuadro_ibm['trafo_vef_ibm_reversa']}}</a></td>
                         <td><a href="{{route('reversa-ibm-castles')}}">{{$cuadro_ibm['trafo_cas_ibm_reversa']}}</a></td>
                     </tr>
                     <tr>
                         <th scope="row">Spare Operacional</th>
                         <td>{{$cuadro_ibm['pos_3g_ibm_entrega'] - $cuadro_ibm['pos_3g_ibm_reversa']}}</td>
                         <td>{{$cuadro_ibm['pos_gprs_ibm_entrega'] - $cuadro_ibm['pos_gprs_ibm_reversa']}}</td>
                         <td>{{$cuadro_ibm['trafo_vef_ibm_entrega'] - $cuadro_ibm['trafo_vef_ibm_reversa']}}</td>
                         <td>{{$cuadro_ibm['trafo_cas_ibm_entrega'] - $cuadro_ibm['trafo_cas_ibm_reversa']}}</td>
                     </tr>
                 </tbody>
             </table>
          </div>
         <div class="table-responsive">
            <table class="table table-bordered border-thick">
                <thead>
                    <tr class="border-thick">
                        <th><small>Equipos recibidos formalmente por Entel desde IBM</small></th>
                        <th><small>Reversas de equipos que fueron recibidos formalmente desde IBM.</small></th>
                        <th><small>Equipos que están instalados y tienen la reversa o que fueron instalados.</small></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr class="border-thick">
                        <th class="bg-warning" data-toggle="tooltip" data-placement="top"
                            title="Equipos recibidos formalmente por Entel desde IBM"
                            >Ingresos IBM > Entel</th>
                        <th class="bg-warning" data-toggle="tooltip" data-placement="top"
                            title="Reversas de equipos que fueron recibidos formalmente desde IBM."
                            >Reversas Entel > IBM</th>
                        <th class="bg-light-green" data-toggle="tooltip" data-placement="top"
                            title="Equipos que están instalados y tienen la reversa o que fueron instalados."
                            >Series justificadas</th>
                        <!--<th class="bg-light-green" data-toggle="tooltip" data-placement="top"
                            title="Equipos que están en poder de técnicos o en bodega."
                            >Series justificadas habidas</th> -->
                        <th class="bg-success text-white">Series ilegibles</th>
                       <!-- <th class="bg-cyan text-white" data-toggle="tooltip" data-placement="top"
                            title="Series justificadas  + Series justificadas habidas">Total Justificadas</th> -->
                        <th class="bg-cyan text-white">Total justificadas + ilegibles</th> 
                        <th class="bg-yellow"><strong>% Total justificado + ilegibles</strong></th>

                    </tr>
                </thead>
                <tbody>
                <tr class="border-thick">
                    <td><h5 class="m-t-5">{{number_format($ingresos_ibm, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($reversas_ibm, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($rec_rev_ibm, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($ilegibles_ibm, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{number_format($total_just_ilegibles_ibm, 0, ',', '.')}}</h5></td>
                    <td><h5 class="m-t-5">{{$porc_just_ilegibles_ibm}}%</h5></td>
                </tr>
                    </tbody>
                
            </table>
        </div>
         
        <div class="row m-t-15">
            <div class="col-6">
                <div class="bg-info p-10 text-white text-center">
                    <i class="fas fa-warehouse m-b-5 font-16"></i>
                    <h5 class="m-b-0 m-t-5">{{number_format($t_disponible_ibm, 0, '', '.')}}</h5>
                    <small class="font-light">Total Disponible</small>
                </div>
            </div>
            <div class="col-6">
                <div class="bg-info p-10 text-white text-center">
                    <i class="fas fa-cart-plus m-b-5 font-16"></i>
                    <h5 class="m-b-0 m-t-5">{{number_format($t_reversas_ibm, 0, '', '.')}}</h5>
                    <small class="font-light">Total reversa</small>
                </div>
            </div>
        </div>
            <div class="row  m-t-15">
            <div class="col-md-12">
                <h4 class="page-title">Flujo por mes</h4>
            
            </div>
                <div class="col-md-12">
                    <canvas id="flujomesibm" width="800" height="200"></canvas>
                </div>
                <div class="col-md-12">
                    <canvas id="canvas" width="800" height="200"></canvas>
                </div>
            </div>
         
   </div>
     </div>
       
</div>
    <p>Última actualización: {{$ultima_act->format('d-m-Y')}}. Última guía de IBM: {{$ultima_ibm->format('d-m-Y')}}.</p>
    </div>


@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('/assets/libs/fullcalendar/dist/fullcalendar.min.css')}}" />
<link href='{{asset('/assets/libs/fullcalendar/dist/fullcalendar.print.css')}}' rel='stylesheet' media='print' /> 
<style>
  #wrapper {
    display: inline-flex;
  }
  #legend {
    margin: auto 5px;
  }
  #placeholder{
    float: left; 
    width: 300px; 
    height: 300px;
  }
  .pieLabel{
    color: #fff;
  }
  .bg-yellow {
      background-color: #ffff00;
  }
  .bg-light-green {
      background-color: #3AD86E;
  }
  .border-thick{
    border-style:solid;
    border-width: 2px;
  }
  .footer-entel {
      position: absolute;
      bottom: 0;
      width: 100%;
      max-height: 30px;
      background-color: #0072AE;
      color: white;
  }
 
  
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src='https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.6.0'></script>
<script type='text/javascript'>
moment.locale('es'); 
moment().format('L'); 
Chart.plugins.unregister(ChartDataLabels);
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});

// gráfico de barras IBM Reversas v/s abastecimiento.


var color = Chart.helpers.color;
var entregas_ibm_dia = @json($ar_entregas_ibm_dia);
var reversas_ibm_dia = @json($ar_reversas_ibm_dia);
var fechas = @json($meses_grafico_ibm_dia);


var ctx3 = document.getElementById('canvas').getContext('2d');

var myBar = new Chart(ctx3, {
        type: 'bar',
        data: {
            labels: fechas,
            datasets: [{
                label: 'Entregas',
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderWidth: 1,
                data: entregas_ibm_dia
        }, {
                label: 'Reversas',
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderWidth: 1,
                data: reversas_ibm_dia
        }]
        },
        plugins: [ChartDataLabels],
        options: {
                responsive: true,
                legend: {
                        position: 'top',
                },
                title: {
                        display: true,
                        text: 'Reversas v/s abastecimiento'
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'series',
                        offset: true,
                        ticks: {
                            source: 'data'
                        },
                        time: {
                            unit: 'day',
                             displayFormats: {
                                day: 'DD-MM-YYYY'
                            },
                            tooltipFormat: 'll'
                        }
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: function(value, context) {
                                return value['y'];
                        }
                    }
                }
            }
});




	


// flujo Adexus
var ctx = document.getElementById('flujomes');
var meses = @json($meses_grafico);
var entregas = @json($ar_entregas);
var reversas = @json($ar_reversas);
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        datasets: [{
            label: "Número de entregas",
            data: entregas,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        },
        {
            label: "Número de reversas",
            data: reversas,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)'
            ],
            borderWidth: 1
        }
    ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                type: 'category',
                labels: meses
                }]
        },
        tooltips: {
            mode: 'x', // optional
            callbacks: {
              title: function(tooltipItems, data) {
                let tI = tooltipItems[0];
                return data.datasets[tI.datasetIndex].data[tI.index].x;
              }
            }
        },
        responsive: true,
        maintainAspectRatio: false
    }
});

//flujo IBM

var ctx1 = document.getElementById('flujomesibm');
var mesesibm = @json($meses_grafico_ibm);
var entregasibm = @json($ar_entregas_ibm);
var reversasibm = @json($ar_reversas_ibm);
var colors_entregas = [];
var border_entregas = [];
entregasibm.forEach(function (el){
    colors_entregas.push('rgba(54, 162, 235, 0.2)');
    border_entregas.push('rgba(54, 162, 235, 1)');
});
var colors_reversas = [];
var border_reversas = [];

reversasibm.forEach(function (el){
    colors_reversas.push('rgba(255, 99, 132, 0.2)');
    border_reversas.push('rgba(255, 99, 132, 1)');
});


var myChartIBM = new Chart(ctx1, {
    type: 'bar',
    plugins: [ChartDataLabels],
    data: {
        datasets: [{
            label: "Número de entregas",
            data: entregasibm,
            backgroundColor: colors_entregas,
            borderColor: border_entregas,
            borderWidth: 1
        },
        {
            label: "Número de reversas",
            data: reversasibm,
            backgroundColor: colors_reversas,
            borderColor: border_reversas,
            borderWidth: 1
        }
    ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 500
                }
            }],
            xAxes: [{
                        type: 'time',
                        distribution: 'series',
                        offset: true,
                        ticks: {
                            source: 'data'
                        },
                        time: {
                            unit: 'month',
                            tooltipFormat: 'll'
                        }
                    }]
        },
        tooltips: {
            mode: 'x', // optional
            callbacks: {
              title: function(tooltipItems, data) {
                let tI = tooltipItems[0];
                return data.datasets[tI.datasetIndex].data[tI.index].x;
              }
            }
        },
        plugins: {
            datalabels: {
                formatter: function(value, context) {
                        return value['y'];
                }
            }
        },
        responsive: true,
        maintainAspectRatio: true
    }
});

</script>

@endsection