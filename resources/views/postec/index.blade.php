@extends('layouts.app')
@section('title', 'Tecnología de POS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Tecnologías</div>
                
                <div class="card-body">
                     <form id="" action="">
                
                    <label for="modelo">Modelo: </label>
                    <div class="form-group row col-12 m-t-5">
                        <select name="modelo" class="form-control mr-sm-3">
                            <option disabled="disabled" hidden="hidden" selected="selected">Seleccione un modelo</option>
                            @foreach($modelos as $modelo)
                            <option value="{{$modelo->id}}">{{$modelo->name}} : {{$modelo->tech->tech}}</option>
                            @endforeach
                    </select>
                    </div>

                    <div class="form-group row col-3 m-t-5">
                        <label for="tech">Tecnología: </label>
                        <select name="tech" id="tech" class="form-control mr-sm-3" >
                            <option disabled="disabled" hidden="hidden" selected="selected">Seleccione una tecnología</option>
                                <option value="3G">3G</option>
                                <option value="GPRS">GPRS</option>
                                <option value="IF">IF</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Grabar</button>
                
                </form>
            </div>

                
                                
            </div>

            </div>
        
        
    </div>
</div>
@endsection

@section('scripts')
<script>
   
</script>
@endsection