@extends('layouts.app')
@section('title', 'No seriados')
@section('content')
<div class="container">
     <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <h2>Guías de artículos no seriados</h2>
            <form id="busq_fecha">
                <div class="form-group">
                    <label for="inicio">Inicio</label>
                    <input type="date" name="inicio" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label for="final">Final</label>
                    <input type="date" name="final" class="form-control" required/>
                </div>
                <input onsubmit="accesorios_fecha(); return false;" type="submit" class="btn btn-primary" value="Buscar"/>
            </form>
            
        </div>
         <div id="tabla" class="col-md-12 m-t-15">
             @include('partials.accesoriosfecha')
         </div>
     </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var accesorios_fecha = document.querySelector('form').addEventListener('submit', function(e){
        e.preventDefault();
        const formData = new FormData(e.target);
        // Now you can use formData.get('foo'), for example.
        // Don't forget e.preventDefault() if you want to stop normal form .submission
        var inicio = formData.get('inicio');
        var final = formData.get('final');
        var url = "{{route('accesorios_fecha')}}";
        var data = { inicio: inicio, final: final };
        var request = $.post(url, data); // make request
        request.done(function(data) { // success
            $('#tabla').show();
            container.html(data);
        });
    });
</script>
@endsection