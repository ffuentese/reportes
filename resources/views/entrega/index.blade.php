@extends('layouts.app')
@section('title', 'Carga de datos')
@section('content')
<div id="holder" style="display: none;">
        <div id="loader" class="loader" style="display: none;"></div>
    </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                
                <li class="nav-item">
                  <a class="nav-link active" id="entrega-tab" data-toggle="tab" href="#entrega" role="tab" aria-controls="entrega" aria-selected="true">Entrega</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="reversa-tab" data-toggle="tab" href="#reversa" role="tab" aria-controls="reversa" aria-selected="false">Reversa</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="entrega-ibm-tab" data-toggle="tab" href="#entrega-ibm" role="tab" aria-controls="entrega" aria-selected="true">Entrega (IBM)</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="reversa-ibm-tab" data-toggle="tab" href="#reversa-ibm" role="tab" aria-controls="reversa" aria-selected="false">Reversa (IBM)</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="puntos-tab" data-toggle="tab" href="#puntos_vigentes" role="tab" aria-controls="puntos_vigentes" aria-selected="false">Puntos vigentes</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="accesorios-tab" data-toggle="tab" href="#accesorios" role="tab" aria-controls="accesorios" aria-selected="false">Accesorios</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="accesoriosibm-tab" data-toggle="tab" href="#accesoriosibm" role="tab" aria-controls="accesoriosibm" aria-selected="false">Accesorios (IBM)</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="terreno-tab" data-toggle="tab" href="#terreno" role="tab" aria-controls="terreno" aria-selected="false">Stock técnicos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="micro-tab" data-toggle="tab" href="#micro" role="tab" aria-controls="micro" aria-selected="false">Microinformática</a>
                </li>
            </ul>
<div class="tab-content" id="myTabContent">                         

    <div class="tab-pane fade show active" id="entrega" role="tabpanel" aria-labelledby="entrega-tab">
        <div class="card">
                <div class="card-header">Entrega</div>

                <div class="card-body">
                     <form action="{{ route('importExcelActaEntrega') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" multiple="multiple" name="import_file[]" class="form-control">
                <br>
                
                <button type="submit" class="btn btn-success">Importar planilla</button>
                <p class="m-t-15">Formato: [Correlativo]_[Acta_Entrega_POS_CO_ENTEL]_[Fecha en formato dd-mm-yyyy]_[n° guia].[xlsx]. Por ejemplo, "105_Acta_Entrega_POS_CO_ENTEL_30-10-2018_482978.xlsx"</p>
                <p>La última acta de entrega registrada es del día {{$ultima_rec}}.</p>
                <p>Última guía registrada: {{$ultima_gd_dir}}</p>
                </form>
                </div>

            </div>
    </div>
  <div class="tab-pane fade" id="reversa" role="tabpanel" aria-labelledby="reversa-tab">
       <div class="card">
                <div class="card-header">Actas de reversa</div>

                <div class="card-body">
                     <form action="{{ route('importExcelActaReversa') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" multiple="multiple" name="import_file[]" class="form-control">
                <br>
                
                <button type="submit" class="btn btn-success">Importar planilla</button>
                <p class="m-t-15">Formato: [Correlativo] [Acta Entrega POS ADEXUS] [fecha en formato dd-mm-yyyy] [N° de guía].[xlsx]. Por ejemplo: "060 Acta Entrega POS ADEXUS 07-01-2019 11265.xlsx"</p>
                <p>La última acta de reversa registrada es del día {{$ultima_rev}}.</p>
                <p>Última guía registrada: {{$ultima_gd_rev}}</p>
                </form>
                </div>
                
                
                
            </div>
  </div>
  <div class="tab-pane fade" id="entrega-ibm" role="tabpanel" aria-labelledby="entrega-ibm-tab">
        <div class="card">
                <div class="card-header">Entrega (IBM)</div>

                <div class="card-body">
                     <form action="{{ route('importExcelActaEntregaIBM') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" multiple="multiple" name="import_file[]" class="form-control">
                <br>
                
                <button type="submit" class="btn btn-success">Importar planilla</button>
                <p class="m-t-15">Formato: [Correlativo] [Acta Entrega IBM] [Fecha en formato dd-mm-yyyy] [guía].[xlsx].  Por ejemplo, "105 Acta Entrega IBM 30-10-2018 482978.xlsx"</p>
                <p>La última acta de entrega IBM registrada es del día {{$ultima_rec_ibm}}.</p>
                <p>La última guía es del {{$ultima_gd_ibm_dir}}</p>
                </form>
                </div>
                
                
            </div>
    </div>
  <div class="tab-pane fade" id="reversa-ibm" role="tabpanel" aria-labelledby="reversa-ibm-tab">
       <div class="card">
                <div class="card-header">Actas de reversa (IBM)</div>

                <div class="card-body">
                     <form action="{{ route('importExcelActaReversaIBM') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" multiple="multiple" name="import_file[]" class="form-control">
                <br>
                <p class="m-t-15">Formato: [Correlativo] [Acta Reversa IBM] [Fecha en formato dd-mm-yyyy] [guía].[xlsx].  Por ejemplo, "060 Acta Reversa IBM 07-01-2019 11265.xlsx"</p>
                <p>La última acta de reversa IBM registrada es del día {{$ultima_rev_ibm}}.</p>
                <p>La última guía es del {{$ultima_gd_ibm_rev}}</p>
                <button type="submit" class="btn btn-success">Importar planilla</button>
                
                </form>
                </div>
                
                
                
            </div>
  </div>  
      <div class="tab-pane fade" id="accesorios" role="tabpanel" aria-labelledby="accesorios-tab">
       <div class="card">
                <div class="card-header">Accesorios</div>

                <div class="card-body">
                     <form action="{{route('proceso_accesorios')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="import_file[]" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Importar actas de accesorios</button>
                <p class="m-t-15">Formato: [Correlativo]_[Acta_Entrega_ACCESORIOS_CO_ENTEL]_[Fecha en formato dd-mm-yyyy]_[guía].[xlsx] </p>
                <p>Por ejemplo, 105_Acta_Entrega_ACCESORIOS_CO_ENTEL_30-10-2018_482978.xlsx</p>
                <p>Última directa: {{$ultima_dir_accesorios}} </p>
                <p>Última reversa: {{$ultima_rev_accesorios}}</p>
                </form>
                    
                </div>
                
                
            </div>
  </div>
    <!-- -->
    <div class="tab-pane fade" id="accesoriosibm" role="tabpanel" aria-labelledby="accesoriosibm-tab">
       <div class="card">
                <div class="card-header">Accesorios (IBM)</div>

                <div class="card-body">
                     <form action="{{route('proceso_accesorios_ibm')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="import_file[]" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Importar no seriados IBM</button>
                <p class="m-t-15">Formato: [Correlativo]_[Acta_Entrega_ACCESORIOS_CO_IBM]_[Fecha en formato dd-mm-yyyy]_[guía].[xlsx] </p>
                <p>Por ejemplo, 105_Acta_Entrega_ACCESORIOS_CO_IBM_30-10-2018_482978.xlsx</p>
                </form>
                    
                </div>

        </div>
  </div>
   <div class="tab-pane fade" id="terreno" role="tabpanel" aria-labelledby="terreno-tab">
       <div class="card">
                <div class="card-header">Stock Técnicos</div>

                <div class="card-body">
                     <form action="{{ route('importTecnicos') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="import_file" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Importar informe de técnicos</button>
                
                </form>
                    
                </div>
                
                
            </div>
  </div>  
    <div class="tab-pane fade" id="puntos_vigentes" role="tabpanel" aria-labelledby="puntos-tab">
       <div class="card">
                <div class="card-header">Puntos vigentes</div>

                <div class="card-body">
                     <form action="{{ route('importExcelPuntosVigentes') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" multiple="multiple" name="import_file[]" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Importar Puntos vigentes</button>
                <p class="m-t-15">El formato de nombre de archivo es "Puntos Vigentes [fecha en dd-mm-yyyy]"</p>
                <p>Por ejemplo, Puntos Vigentes 02-07-2019.xlsx</p>
                <p>Última fecha ingresada de puntos: {{$ultimo_pv->format('d-m-Y')}}</p>
                </form>
                    
                </div>
                
                
            </div>
  </div>
  <div class="tab-pane fade" id="micro" role="tabpanel" aria-labelledby="micro-tab">
       <div class="card">
                <div class="card-header">Microinformática</div>

                <div class="card-body">
                     <form action="{{route('procesa_micro')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" multiple="multiple" name="import_file[]" class="form-control">
                <br>
                
                <button type="submit" class="btn btn-success">Importar planilla</button>
                <p class="m-t-15">Formato: Ej. 101 Acta Entrega Micro 30-10-2018 482978.xlsx"</p>
                </form>
                </div>
                
                
                
            </div>
  </div>  
    
     @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
        </div>
        @elseif (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <ul>
                <li>{{session('success')}}</li>
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
        </div>
        @endif
</div>
            
            
            @isset($devices)
            
            <div class="card">
                <div class="card-header">Equipos</div>
                
                <button onclick="return getExcel('{{route('export')}}');" class="btn btn-link m-l-5">Descargar como planilla Excel</button>
                <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Serial</th>
                        <th>Modelo</th>
                        <th>Recepción</th>
                        <th>Instalación</th>
                        <th>Serie retirada</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($devices as $device)
                    <tr>
                        <th scope="row">
                            {{ $device->id }}
                        </td>
                        <td>
                            <a href="{{route('serial', ['serial' => $device->serial])}}">{{ $device->serial }}</a>
                        </td>
                        <td>
                            {{ $device->modelo->name }}
                        </td>
                        <td>
                            {{ $device->fecha_recepcion ? $device->fecha_recepcion->format('d/m/Y') : "" }}
                        </td>
                        <td>
                            {{ $device->fecha_instalacion ? $device->fecha_instalacion->format('d/m/Y') : "" }}
                        </td>
                        <td>
                            {{ $device->serial_prev ?? ''}}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
                {{ $devices->links() }}
                </div>
            
            @endisset
             

                
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<style>
  .loader {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    transform: -webkit-translate(-50%, -50%);
    transform: -moz-translate(-50%, -50%);
    transform: -ms-translate(-50%, -50%);
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    z-index: 1000;
    animation: spin 2s linear infinite;
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  
  #holder 
  {
    position: absolute;
    background: grey;
    opacity: 0.5;
    z-index: 999;
    width: 100%;
    height: 100%;        
  }             
  </style>
  <script type="text/javascript">
      function getExcel(urlToSend) {
        var req = new XMLHttpRequest();
        req.open("GET", urlToSend, true);
        req.responseType = "blob";
        $('#loader').show();
        $('#holder').show();
        req.onload = function (event) {
            var blob = req.response;
            var contentDisp = req.getResponseHeader("Content-Disposition"); //if you have the fileName header available
            var regex = "filename[^;\n=]*=(['\"])*(?:utf-8\'\')?(.*)((1)\1|)";
            var fName = contentDisp.match(regex);
            console.log(fName);
            var fileName = fName[2].slice(0, -1);
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download=fileName;
            link.click();
            $('#loader').hide();
            $('#holder').hide();
        };

        req.send();
    }
      </script>
      
 <script>
 //redirect to specific tab
 $(document).ready(function () {
    $('#myTab a[href="#{{ old('tab') }}"]').tab('show');
 });
</script>
@endsection