<div class="card">
    <div class="card-header">    
        <h3>Entregas</h3>
    </div>
    <div class="card-body">
        <table class='table table-sm'>
            <thead>
            <tr>
                <th>Guia</th>
                <th>Cantidad</th>
                <th>Estado</th>
                <th>Fecha</th>
            </tr>
            </thead>
            <tbody>
            @forelse($entregas as $guia)
            <tr>
                <td>{{$guia->guia}}</td>
                <td>{{$guia->cantidad}}</td>
                <td>{{$guia->estado}}</td>
                <td>{{$guia->fecha}}</td>

            </tr>
            @empty
            <tr>
                <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
            </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3>Reversas</h3>
    </div>
    <div class="card-body">
        <table class="table table-sm">
            <thead>
            <tr>
                <th>Guia</th>
                <th>Cantidad</th>
                <th>Estado</th>
                <th>Fecha</th>
            </tr>

            </thead>
            <tbody>
            @forelse($reversas as $guia)
            <tr>
                <td>{{$guia->guia}}</td>
                <td>{{$guia->cantidad}}</td>
                <td>{{$guia->estado}}</td>
                <td>{{$guia->fecha}}</td>

            </tr>
            @empty
            <tr>
                <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
            </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>