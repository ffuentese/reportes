<thead>
<tr>
    <th>Id</th>
    <th>Dirección</th>
    <th>Tiene registros</th>
</tr>
</thead>
<tbody>
@foreach($pos as $p)
<tr>
    <td>{{$p->id}}</td>
    <td><a href="{{route('pos', ['pos' => $p->id])}}">{{$p->direccion}}</a></td>
    @if(!$p->devices->isEmpty())
    <td><a href="{{route('pos', ['pos' => $p->id])}}">Ver POS</a></td>
    @else 
    <td>No</td>
    @endif
</tr>
@endforeach
</tbody>