@extends('layouts.app')
@section('title', 'Mantenedor de modelos')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            
            

            @isset($devices)

            <div class="card">
                <div class="card-header">
                    <p>Equipos asociados a modelo {{$modelo->name}}</p>
                    <p>Existen {{$devices->total()}} ciclos de equipos asociados a este modelo.</p>
                </div>
                
               
                <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Serial</th>
                        <th>Modelo</th>
                        <th>Recepción</th>
                        <th>Instalación</th>
                        <th>Serie retirada</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($devices as $device)
                    <tr>
                        <th scope="row">
                            {{ $device->id }}
                        </td>
                        <td>
                            <a href="{{route('serial', ['serial' => $device->serial])}}">{{ $device->serial }}</a>
                        </td>
                        <td>
                            {{ $device->modelo->name }}
                        </td>
                        <td>
                            {{ $device->fecha_recepcion ? $device->fecha_recepcion->format('d-m-Y') : "" }}
                        </td>
                        <td>
                            {{ $device->fecha_instalacion ? $device->fecha_instalacion->format('d-m-Y') : ""}}
                        </td>
                        <td>
                            {{ $device->serial_prev }}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                    {{ $devices->appends(request()->query())->links() }}
                    @endisset
                   
            </div>

                </div>
            
            

                
            </div>
        </div>
    </div>
</div>
@endsection

