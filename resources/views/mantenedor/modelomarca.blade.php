@extends('layouts.app')
@section('title', 'Mantenedor de modelos y marcas')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card m-t-15">
                <div class="card-header">Modelos</div>

                <div class="card-body">
                     <form action="{{route('agregarmodelo')}}" method="POST" >
                @csrf
                <label name="id">Id</label>
                <input type="number" name="id" class="form-control">
                <br>
                <label name="name">Modelo</label>
                <input type="text" name="name" class="form-control">
                <br>
                <div class="form-group">
                    <label name="brand_id">Marca</label>
                    <select name="brand_id" class="form-control">
                        @foreach ($marcas as $marca)
                            <option value="{{ $marca->id }}">{{$marca->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
                
                </form>
                @if ($errors->any())
                <div class="alert alert-danger m-t-5">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                </div>
                
                
            </div>
            
              <div class="card">
                <div class="card-header">Modelos</div>
                
           
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Modelo</th>
                        <th>Marca</th>                        
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($modelospag as $modelo)
                    <tr>
                        <th scope="row">
                            {{ $modelo->id }}
                        </td>
                        <td>
                            <a href="{{route('modelo', ['model_id' => $modelo->id])}}">{{$modelo->name}}</a>
                        </td>
                        <td>
                            {{$modelo->brand->name}}
                        </td>
                     
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                
                </form>
                
                {{ $modelospag->links() }}
                </div>
            
            
            <div class="card">
                <div class="card-header">Marcas</div>

                <div class="card-body">
                     <form action="{{route('agregarmarca')}}" method="POST" >
                @csrf
                <label name="id">Id</label>
                <input type="text" name="id" class="form-control">
                <br>
                <label name="name">Marca</label>
                <input type="text" name="name" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Crear</button>
                
                </form>
                </div>
                
                
            </div>
            
            
            
            <div class="card">
                <div class="card-header">Marcas</div>
                
                             
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Marca</th>                        
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($marcaspag as $marca)
                    <tr>
                        <th scope="row">
                            {{$marca->id}}
                        </td>
                        <td>
                            {{$marca->name}}
                        </td>
                        
                     
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                
                </form>
                {{ $marcaspag->links() }}
                </div>
            
            @if ($errors->any())
                <div class="alert alert-danger m-t-5">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            
            
            
                
                
            </div>
        </div>
    </div>
</div>
@endsection