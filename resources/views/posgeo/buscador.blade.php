@extends('layouts.app')
@section('title', 'POS por ubicación')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">
                    <p>Buscar POS por ubicación</p>
                </div>
                <div class="card-body">
                <form>
                    <div class="form-group">
                        <label for="region"></label>
                        <select name="id" id="id" class="form-control">
                            <option value="">Seleccione una región</option>
                            @foreach($regiones as $region)
                            <option value='{{$region->id}}'>{{$region->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comuna"></label>
                        <select name="comuna_id" id="select_name" class="form-control" disabled="disabled">
                            <optgroup id="comunas" label="Seleccione una comuna">
                            @isset($comunas)
                                @include('partials.poscomunaselect')
                            @endisset
                            </optgroup>
                        </select>
                    </div>
                </form>
                </div>
            </div>
            <div class="card" id="pos_card" style="display:none;">
                <div class="card-header">
                    POS por región
                </div>
                <div class='card-body'>
                <div class="table-responsive">
                    <table id="pos_comuna" class="table table-sm table-striped">
                    @isset($pos)
                        @include('partials.poscomunatable')
                    @endisset
                    </table>
                </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<style>
    table {
        font-size: x-small;
    }
</style>
<script type="text/javascript">
     $(document).ready(function()    {
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
     });
    var ajax_select = $(document).on('change', '#id', function(){  
            var region = $('#id').val();
            var container = $('#comunas');
            var url = "{{url('/ajax_comunas/')}}/" + region + "";
            var request = $.get(url); // make request
            request.done(function(data) { // success
                $('#select_name').prop('disabled', false);
                container.html(data);
            });
    });
   
        
    var ajax_table = $(document).on('change', '#select_name', function() {
        var comuna = $('#select_name').val();
        var container = $('#pos_comuna');
        var url = "{{url('/ajax_pos/')}}/" + comuna + "";
        var request = $.get(url); // make request
        request.done(function(data) { // success
            $('#pos_card').show();
            container.html(data);
        });
    });
    
</script>
@endsection