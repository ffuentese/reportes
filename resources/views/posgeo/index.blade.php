@extends('layouts.app')
@section('title', 'POS por región')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">
                    POS por región
                </div>
                <div class='card-body'>
<div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Región</th>
                        <th>Cantidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for($i = 0; $i < sizeof($ar_pos); $i++)
                    <tr>
                        <th scope="row">
                            {{ $ar_pos[$i]["region_id"] }}
                        </td>
                        <td>
                             <a href="{{route('posporregion', ['id' => $ar_pos[$i]["region_id"]])}}">{{ $ar_pos[$i]["name"] }}</a>
                        </td>
                        <td>
                            <a href="{{route('posporregion', ['id' => $ar_pos[$i]["region_id"]])}}">{{ $ar_pos[$i]["cantidad"] }}</a>
                        </td>
                        
                    </tr>
                    @endfor
                    </tbody>
                </table>
</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
