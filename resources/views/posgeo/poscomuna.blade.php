@extends('layouts.app')
@section('title', 'POS por región')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">
                    <p>Pos en comuna de {{$pos_comuna->first()->comuna->nombre}}</p>
                </div>
                <div class='card-body'>
<div class="table-responsive">
                <table id='poscomuna' class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Dirección</th>
                        <th>Tiene registros</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($pos_comuna as $pos)
                    <tr>
                        <th scope="row">
                            {{ $pos->id }}
                        </th>
                        <td>
                            <a href="{{route('pos', ['pos' => $pos->id])}}">{{ $pos->direccion }}</a>
                        </td>
                        @if(!$pos->devices->isEmpty())
                        <td><a href="{{route('pos', ['pos' => $pos->id])}}">Ver POS</a></td>
                        @else 
                        <td>No</td>
                        @endif
                        
                    </tr>
                     
                     
                    @empty
                    <p>No se encontraron equipos.</p>
                    @endforelse
                   </tbody>
                </table>

</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<style>
    .lower {
        text-transform: lowercase;
    }
    .cap {
        
        text-transform: capitalize;
    }

</style>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#poscomuna').DataTable({
        language: 
                {"url": "{{asset('assets/dt/Spanish.lang')}}"}
        
    });

} );
</script>
@endsection