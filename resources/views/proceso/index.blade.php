@extends('layouts.app')
@section('title', 'Iniciar proceso actualización de sistema')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Buscar equipos</div>
                
                <div class="card-body">
                     <form action="{{ route('procesar') }}" method="POST">
                     @csrf
         
                <br>
                <button type="submit" class="btn btn-success btn-lg">Iniciar proceso</button>
                
                </form>
                </div>
                
                @if (session('error'))
                <div class="alert alert-danger">
           
                  <p>{{ session()->get('error')->first() }}</p>
                        
                    
                </div>
                @elseif (session('success'))

                    <div class="alert alert-success">
                        <p>{{session()->get('success')->first()}}</p>
                    </div>
                @endif
                                
                </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection