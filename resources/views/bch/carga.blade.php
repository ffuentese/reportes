@extends('layouts.app')
@section('title', 'Carga de inventario Banco de Chile')
@section('content')
<div class="container">
    <div class="row">
         <div class="container-fluid m-t-15">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="bginfo-tab" data-toggle="tab" href="#bginfo" role="tab" aria-controls="bginfo" aria-selected="true">BGInfo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="sccm-tab" data-toggle="tab" href="#sccm" role="tab" aria-controls="sccm" aria-selected="true">SCCM</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="epo-tab" data-toggle="tab" href="#epo" role="tab" aria-controls="epo" aria-selected="true">EPO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="rrhh-tab" data-toggle="tab" href="#rrhh" role="tab" aria-controls="rrhh" aria-selected="true">RRHH</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="csv-tab" data-toggle="tab" href="#csv" role="tab" aria-controls="csv" aria-selected="true">CSV</a>
                </li>
                
            </ul>
         
    <div class="tab-content" id="myTabContent">  
        <div class="tab-pane fade show active" id="bginfo" role="tabpanel" aria-labelledby="bginfo-tab">
            <div class="card">
            <div class="card-header">BGinfo</div>

                <div class="card-body">
                 <form action="{{route('carga_bginfo')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="import_file" class="form-control">
                    <br>
                    <input type="date" name="fecha" class="form-control">
                    <br>
                    <button type="submit" class="btn btn-success">Importar BGINFO</button>
                </form>
                </div> 
            </div>
        </div>
        <div class="tab-pane fade" id="sccm" role="tabpanel" aria-labelledby="sccm-tab">
            <div class="card">
            <div class="card-header">SCCM</div>
                <div class="card-body">
                     <form action="{{route('carga_sccm')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="import_file" class="form-control">
                        <br>
                        <input type="date" name="fecha" class="form-control">
                        <br>
                        <button type="submit" class="btn btn-success">Importar SCCM</button>
                    </form>
                </div> 
            </div>
        </div>
        <div class="tab-pane fade" id="epo" role="tabpanel" aria-labelledby="epo-tab">
            <div class="card">
            <div class="card-header">EPO</div>
                <div class="card-body">
                     <form action="{{route('carga_epo')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="import_file" class="form-control">
                        <br>
                        <input type="date" name="fecha" class="form-control">
                        <br>
                        <button type="submit" class="btn btn-success">Importar EPO</button>
                    </form>
                </div> 
            </div>
        </div>
        <div class="tab-pane fade" id="rrhh" role="tabpanel" aria-labelledby="rrhh-tab">
        <div class="card">
            <div class="card-header">Dotación de personal BCH</div>
                <div class="card-body">
                     <form action="{{route('carga_rrhh')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="import_file" class="form-control">
                        <br>
                        <button type="submit" class="btn btn-success">Importar RRHH</button>
                    </form>
                </div> 
            </div>
        
        </div>
    <div class="tab-pane fade" id="csv" role="tabpanel" aria-labelledby="csv-tab">
        <div class="card">
            <div class="card-header">Inventario BCH</div>

            <div class="card-body">
                 <form action="{{ route('procesoCargaBCH') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="import_file" class="form-control">
                    <br>
                    <button type="submit" class="btn btn-success">Importar CSV</button>
                </form>
            </div> 
        </div>
     </div>
    
         
        @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
        </div>
        @elseif (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <ul>
                <li>{{session('success')}}</li>
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
        </div>
        @endif
        </div>
     </div>
     </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
 //redirect to specific tab
 $(document).ready(function () {
    $('#myTab a[href="#{{ old('tab') }}"]').tab('show');
 });
</script>
@endsection