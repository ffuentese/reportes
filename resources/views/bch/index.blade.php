@extends('layouts.app')
@section('title', 'Inventario Banco de Chile')
@section('content')
<div id="holder" style="display: none;">
        <div id="loader" class="loader" style="display: none;"></div>
    </div>
<div class="container cont-bch">
    
    <div class="row dtcont"> 
        <div class="col-md-12 m-t-15 table-responsive"> 
            <h4 class="page-title m-l-0">Inventario BCH</h4>
            <table id="BCHtable" class="display table table-fixed table-striped custom-table m-b-15" style="width:100%">
               <thead>
                   <tr>
                       <th style="display:none;">Id</th>
                       <th>Alias</th>
                       <th>Rótulo</th>
                       <th>Serie</th>
                       <th class="filtrable">Tipo</th>
                       <th class="filtrable">Marca</th>
                       <th>Modelo</th>
                       <th>Nombre</th>
                       <th>Rut</th>
                       <th class="filtrable">Region</th>
                       <th class="filtrable overflow">Site</th>
                       <th style="display:none;">Fecha de reporte</th>
                       <th style="display:none;">Última conexión</th>
                   </tr>
               </thead>
               <tfoot>
               <th></th>
                
                       <th></th>
                       <th></th>
                       <th></th>
                       <th class="filtrable"></th>
                       <th class="filtrable"></th>
                       <th></th>
                       <th></th>
                       <th></th>
                       <th class="filtrable"></th>
                       <th class="filtrable"></th>
                       <th></th>
                       <th></th>
               </tfoot>
             </table>
        </div>
        </div>

    <p id="footer-act" style="display:none;">Última actualización: {{$ultima_act}}</p>
    
</div>

@endsection

@section('scripts')
<style>
    .text-wrap{
    white-space:normal;
    }
    .width-200{
        width:120px;
    }
    .width-30{
        width:30px;
    }
    .cont-bch {
        min-height: 100%;
        margin-bottom: 100px;
        margin-left: 20px;
        min-width: 90%;
        
    }
    .footer-entel {
      position: fixed;
      bottom: 0;
      margin-top: 100px;
      height: 50px;
      width: 100%;
      background-color: #0072AE;
      color: white;
      min-height: 10%;
  }
  .loader {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    transform: -webkit-translate(-50%, -50%);
    transform: -moz-translate(-50%, -50%);
    transform: -ms-translate(-50%, -50%);
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    z-index: 1000;
    animation: spin 2s linear infinite;
  }
  
  .custom-table {
    font-size: 11px;
    font-family: Arial;
}

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  
  #holder 
  {
    position: absolute;
    background: grey;
    opacity: 0.5;
    z-index: 999;
    width: 100%;
    height: 100%;        
  }
  
  .overflow {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

/* Override sorting header --> DataTable Bug */
.dataTables_scrollBody > table > thead > tr {
    visibility: collapse;
    height: 0px !important;
}

tr { height: 40px; }
</style>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>

<script type="text/javascript">
    

    
$(document).ready(function()    {
    
     function getExcel(urlToSend) {
        var req = new XMLHttpRequest();
        req.open("GET", urlToSend, true);
        req.responseType = "blob";
        req.onload = function (event) {
            var blob = req.response;
            var contentDisp = req.getResponseHeader("Content-Disposition"); //if you have the fileName header available
            var regex = "filename[^;\n=]*=(['\"])*(?:utf-8\'\')?(.*)((1)\1|)";
            var fName = contentDisp.match(regex);
            console.log(fName);
            var fileName = fName[2].slice(0, -1);
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download=fileName;
            link.click();
            $('#loader').hide();
            $('#holder').hide();
        };

        req.send();
        
        
 }
 
 // Comienza datatable
    
    let buttons = $('.dt-buttons');
    var buttonCommon = {
        exportOptions: {
            
        }
    };
    var table = $('#BCHtable').DataTable( {
        orderCellsTop: true,
        fixedHeader: false,
        responsive: true,
        oSearch: {"bSmart": false},
        ajax: "{{ route('datatableInvBCH') }}",      
        dom: 'Bfrtip',
        buttons: {
            buttons: [
                { extend: 'pdfHtml5', className: 'pdfButton', orientation: 'landscape' },
                $.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5', className: 'excelButton',
                text: 'Excel filtrado'
                } ),
                {
                text: 'Excel completo',
                action: function ( e, dt, node, config ) {
                    $('#loader').show();
                    $('#holder').show();
                    getExcel("{{route('exportBCH')}}");
                    
                }
            }
            ]
        },
        language: 
                {"url": "{{asset('assets/dt/Spanish.lang')}}"}
        ,
        columns: [
            {data: 'id', name: 'id', visible: false},    
            {data: 'alias', name: 'alias'},    
        { data: 'rotulo', name: 'rotulo'},
        { data: 'serie', name: 'serie'},
        { data: 'tipo', name: 'tipo'},
        { data: 'marca', name: 'marca'},
        { data: 'modelo', name: 'modelo'},
        { data: 'nombre', name: 'nombre'},
        { data: 'rut', name: 'rut'},
        { data: 'region', name: 'region'},
        { data: 'site', name: 'site', width: "100px"},
        { data: 'fecha_reporte', name: 'fecha_reporte', visible:false},
        { data: 'ultima_conexion', name: 'ultima_conexion', visible:false}
        ],
        columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-200'>" + data + "</div>";
                    },
                    targets: 10
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-30'>" + data + "</div>";
                    },
                    targets: 6
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-30'>" + data + "</div>";
                    },
                    targets: 7
                }
             ],
        initComplete: function() {
            this.api().columns('.filtrable').every( function () {
                var column = this;
                var select = $('<select><option class="overflow" value="">Seleccione:</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+text_truncate(d, 35)+'</option>' );
                } );
            } );

            $('#footer-act').show();
            // el footer con la fecha de actualización no se muestra correctamente 
            // si los datos cargan después de mostrarlo.
            
            controlButtons($(this));
            // cuando carga
    
            $(this).buttons(['.excelButton', '.pdfButton']).disable();
            
            
        }
    } );
    text_truncate = function(str, length, ending) {
        if (length == null) {
          length = 100;
        }
        if (ending == null) {
          ending = '...';
        }
        if (str.length > length) {
          return str.substring(0, length - ending.length) + ending;
        } else {
          return str;
        }
  };
    
    $('#BCHtable thead tr').clone(true).appendTo( '#BCHtable thead' );
    $('#BCHtable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control custom-table" placeholder="'+title+'" />' );
        
        $( 'input', this ).on( 'keyup change', function ()  {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search(this.value) 
                    .draw();
                table.on('search.dt', function () {
                    controlButtons(table);
                });
            }
        } );
        
    } );
    function controlButtons(myTable) {
    let textSearched = myTable.search();
    let numberOfRows = myTable.rows({ filter: 'applied' }).count();
   

    //  If text length > 3 or number of rows (with filters) <= 1000, enable buttons
    if (textSearched.length > 3 || numberOfRows <= 1000) {
        myTable.buttons(['.excelButton', '.pdfButton']).enable();

    //  If text length <= 3 or number of rows (with filters) > 1000, disable buttons
    } else {
        myTable.buttons(['.excelButton', '.pdfButton']).disable();
    }
}
    
    
});
    
</script>
@endsection