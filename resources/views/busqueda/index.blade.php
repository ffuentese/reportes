@extends('layouts.app')
@section('title', 'Buscador de equipos')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Buscar equipos</div>
                
                
                
                <div class="card-body">
                     <form action="{{ route('pos') }}">
                
                <label for="pos">POS</label>
                <input type="number" name="pos" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Buscar</button>
                
                </form>
                </div>

                <div class="card-body">
                     <form action="{{ route('serial') }}">
                <label for="serial">Serie de equipo</label>
                <input type="text" name="serial" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Buscar</button>
                
                </form>
                </div>
                
                <div class="card-body">
                     <form action="{{ route('rotulo') }}">
                
                <label for="rotulo">Rótulo</label>
                <input type="text" name="rotulo" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Buscar</button>
                
                </form>
                </div>
                                
            </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection