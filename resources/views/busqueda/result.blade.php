@extends('layouts.app')
@section('title', 'Equipos que coinciden con ' . $input_busqueda)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($devices)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>Equipos que coinciden con '{{$input_busqueda}}':</p></div>
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Id</th>
                        <th>Serial</th>
                        <th>Rótulo</th>
                        <th>Modelo</th>
                        <th>GD provisión</th>
                        <th>F.provisión</th>
                        <th>F.instalación</th>
                        <th>F.reversa serie retirada</th>
                        <th>Serie retirada</th>
                        <th>GD retirada</th>
                        <th>Modelo serie retirada</th>
                        <th>POS</th>
                        <th>Servicio</th>
                        <th>Ubicación</th>
                        <th>Técnico</th>
                        
                        
                        
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr class='{{isset($device->previous_device) || ($device->reusada)  ? 'table-success' : ""}}'>
                    <td>{{$device->id}}</td>
                    <td>{{$device->serial}}</td>
                    <td>{{$device->rotulo ?? ""}}</td>
                    <td>{{$device->modelo->name}}</td>
                    <td>{{$device->guia_recepcion}}</td>
                    <td>{{$device->fecha_recepcion ? $device->fecha_recepcion->format("d/m/Y") : "" }}</td>
                    <td>{{$device->fecha_instalacion ? $device->fecha_instalacion->format("d/m/Y") : "" }}</td>
                    <td>{{isset($device->previous_device->fecha_reversa) ? $device->previous_device->fecha_reversa->format("d/m/Y") : ""}}</td>
                    <td> <a href="{{$device->previous_device ? route('serial', ['serial' => $device->previous_device->serial]) : ""}}">{{isset($device->previous_device->serial) ? ($device->previous_device->serial == $device->serial ? "REUTILIZADA" : $device->previous_device->serial) : ""}} </a></td>
                    <td>{{$device->previous_device->guia_reversa ?? ""}}</td>
                    <td>{{$device->previous_device->modelo->name ?? ""}}</td>
                    <td><a href="{{$device->pos_id > 0 ? route('pos', ['pos' => $device->pos_id]) : ""}}">{{$device->pos_id > 0 ? $device->pos_id : ""}}</a></td>
                    <td>{{$device->service->name}}</td>
                    <td>{{$device->location->name != "SIN ALMACÉN" ? $device->location->name : ""}}</td>
                    <td>{{$device->technician->name != "DESCONOCIDO" ? $device->technician->name : ""}}</td>
                    
                    
                    
                    
                    
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                </div>
            
            <div class="card">
                <div class="card-header">
                    <p>Información en OT Digital</p>
                    @isset($ultima_atencion)
                    <p>Última atención registrada en OT Digital cargada: {{$ultima_atencion}}</p>
                    @endisset
                </div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Fecha atención</th>
                        <th>Ticket</th>
                        <th>Serie instalada</th>
                        <th>Modelo instalado</th>
                        <th>Serie retirada</th>
                        <th>Modelo retirado</th>
                        <th>Proveedor</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($otdigital as $ot)
                    <tr>
                        <td>{{$ot->fecha_atencion->format("d/m/Y") }}</td>
                        <td><a href="http://gestionactivos/cuadratura/form_ticket.php?ticket={{$ot->ticket}}" target="_blank">{{$ot->ticket}}</a></td>
                        <td>{{$ot->serie1}}</td>
                        <td>{{$ot->modelo}}</td>
                        <td>{{$ot->serie4}}</td>
                        <td>{{$ot->modelo_out}}</td>
                        <td>{{$ot->proveedor}}</td>
                        
                    </tr>
                    @empty
                    <tr>
                        <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            @endisset
            
            </div>
        </div>
    </div>
</div>
</div>
@endsection