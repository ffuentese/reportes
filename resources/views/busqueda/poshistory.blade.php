@extends('layouts.app')
@isset($pos)
@section('title', 'Equipos en POS #' . $pos->id)
@endisset
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            
            

            @isset($devices)

            <div class="card">
                <div class="card-header">
                    <p>Equipos en POS #{{$pos->id}}</p>
                    @if ($pos->direccion)
                    <p>Ubicado en {{$pos->direccion}} en la comuna de {{$pos->comuna->name}}, región de {{$pos->comuna->region->name}}</p>
                    @endif
                    <p></p>
                </div>
                
               
                <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Serie</th>
                        <th>Modelo</th>
                        <th>Recepción</th>
                        <th>Instalación</th>
                        <th>Serie retirada</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($devices as $device)
                    <tr>
                        <th scope="row">
                            {{ $device->id }}
                        </td>
                        <td>
                            <a href="{{route('serial', ['serial' => $device->serial])}}">{{ $device->serial }}</a>
                        </td>
                        <td>
                            {{ $device->modelo->name }}
                        </td>
                        <td>
                            {{ $device->fecha_recepcion }}
                        </td>
                        <td>
                            {{ $device->fecha_instalacion }}
                        </td>
                        <td>
                            <a href="{{route('serial', ['serial' => $device->serial_prev])}}">{{ $device->serial_prev }}</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                    @endisset
                    @if (!$devices->first())
                    <p class="text-center">No hay registros asociados al POS #{{$pos->id}}.</p>
                    @endif
            </div>

                </div>
            
            

                
            </div>
        </div>
    </div>
</div>
@endsection

