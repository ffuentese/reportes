@extends('layouts.app')
@section('title', 'Buscar equipos por POS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Buscar equipos por POS</div>
                
                <div class="card-body">
                     <form action="{{ route('pos') }}">
                
                <label for="pos">POS</label>
                <input type="number" name="pos" class="form-control">
                <br>
                <button type="submit" class="btn btn-success">Buscar</button>
                
                </form>
                </div>

                                
            </div>
            
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                
                
            </div>
        </div>
    </div>
</div>
@endsection