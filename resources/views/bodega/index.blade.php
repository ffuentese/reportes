@extends('layouts.app')
@section('title', 'Ver stock en bodega')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12"> 
            <div class="card mt-5">
                <div class="card-header">Stock en bodega</div>
                <div class="card-body">
                <table id="iq09-table" class="table table-sm table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Material</th>
                        <th>Rótulo</th>
                        <th>Serial</th>
                        <th>Almacén</th>
                        <th>Tipo Stock</th>
                        <th>Nombre</th>
                        <th>Modificado por</th>
                        <th>Usuario</th>
                        <th>PEP</th>
                        <th>Creado en</th>
                        
                    </tr>
                    </thead>
                    <tbody>
      
                    </tbody>
                </table>
                    @isset($ultima_act)
                    <p>Cargado en {{$ultima_act}}</p>
                    @endisset
            </div>
        </div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>

<script type='text/javascript'>
$('#iq09-table').DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: "{{route('iq09_datatable')}}",
    language: 
                {"url": "{{asset('assets/dt/Spanish.lang')}}"}
        ,
    columns: [
        { data: 'id', name: 'id' },
        { data: 'material', name: 'material' },
        { data: 'rotulo', name: 'rotulo' },
        { data: 'serie', name: 'serie' },
        { data: 'almacen', name: 'almacen' },
        { data: 'tipo_stock', name: 'tipo_stock' },
        { data: 'nombre', name: 'nombre' },
        {
           data: 'modificado',
           type: 'num',
           render: {
              _: 'display',
              sort: 'timestamp'
           }
        },
        { data: 'usuario', name: 'usuario' },
        { data: 'pep', name: 'pep' },
        {
           data: 'creado',
           type: 'num',
           render: {
              _: 'display',
              sort: 'timestamp'
           }
        }
    ]
});
</script>
@endsection