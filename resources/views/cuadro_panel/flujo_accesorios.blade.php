@extends('layouts.app')
@section('title', $label)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($accesorios)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>{{$label}}</p>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Cantidad</th>
                        <th>Marca</th>
                        <th>Guía</th>
                        <th>Fecha</th>

                    </tr>
                </thead>
                <tbody>
                @forelse ($accesorios as $device)
                <tr class=''>
                    <td>{{$device->cantidad}}</td>
                    <td>{{$device->name}}</td>
                    <td>{{$device->guia}}</td>
                    <td>{{date_format(date_create($device->fecha), 'd-m-Y')}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$accesorios->links()}}
                </div>
            
           @endisset
    </div>
</div>
</div>
@endsection