@extends('layouts.app')
@section('title', 'Equipos en puntos vigentes')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
          
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>Puntos vigentes</p>
                </div>
                <div class="card-body">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Punto</th>
                        <th>Serial</th>
                        <th>Marca</th>
                        <th>Tecnología</th>
                        <th>Región</th>
                        <th>Comuna</th>
                        <td>Dirección</td>
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($pvs as $device)
                <tr class=''>
                    <td>{{$device->pos_id}}</td>
                    <td>{{$device->serial}}</td>
                    <td>{{$device->marca}}</td>
                    <td>{{$device->tecnologia}}</td>
                    <td>{{$device->region}}</td>
                    <td>{{$device->comuna}}</td>
                    <td>{{$device->direccion}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$pvs->links()}}
                <p>Resultados {{$pvs->firstItem()}} a {{$pvs->lastItem()}} de un total de {{$pvs->total()}} registros.</p>
                </div>
            </div>
           
    </div>
</div>
</div>
@endsection