@extends('layouts.app')
@section('title', 'Equipos en puntos vigentes')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($pvs)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p></p>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Serial</th>
                        <th>Modelo</th>
                        <th>Tecnología</th>
                        <th>Punto</th>
                        
                        
                        
                        
                    </tr>
                </thead>
                <tbody>
                @forelse ($devices as $device)
                <tr class=''>
                    <td>{{$device->serial}}</td>
                    <td>{{$device->modelo->name}}</td>
                    <td>{{$device->tech}}</td>
                    <td>{{$device->pos_id}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                </div>
            
            <div class="card">
                <div class="card-header">
                    <p>Información en OT Digital</p>
                    @isset($ultima_atencion)
                    <p>Última atención registrada en OT Digital cargada: {{$ultima_atencion}}</p>
                    @endisset
                </div>
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Fecha atención</th>
                        <th>Ticket</th>
                        <th>Serie instalada</th>
                        <th>Modelo instalado</th>
                        <th>Serie retirada</th>
                        <th>Modelo retirado</th>
                        <th>Proveedor</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($otdigital as $ot)
                    <tr>
                        <td>{{$ot->fecha_atencion->format("d/m/Y") }}</td>
                        <td><a href="http://gestionactivos/cuadratura/form_ticket.php?ticket={{$ot->ticket}}" target="_blank">{{$ot->ticket}}</a></td>
                        <td>{{$ot->serie1}}</td>
                        <td>{{$ot->modelo}}</td>
                        <td>{{$ot->serie4}}</td>
                        <td>{{$ot->modelo_out}}</td>
                        <td>{{$ot->proveedor}}</td>
                        
                    </tr>
                    @empty
                    <tr>
                        <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            @endisset
            
            </div>
        </div>
    </div>
</div>
</div>
@endsection