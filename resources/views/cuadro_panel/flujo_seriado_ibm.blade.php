@extends('layouts.app')
@section('title', $label)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                        
            @isset($datos)
            
            <div class="card m-t-5">
                <div class="card-header">
                    <p>{{$label}}</p>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                    <tr class="border-thick">
                        <th>Serial</th>
                        <th>Modelo</th>
                        <th>Tecnología</th>
                        <th>Guia</th>
                    </tr>
                </thead>
                <tbody>
                @forelse ($datos as $device)
                <tr class=''>
                    <td><a href="{{ route('serial', ['serial' => $device->serial]) }}">{{$device->serial}}</a></td>
                    <td>{{$device->modelo}}</td>
                    <td>{{$device->tecnologia}}</td>
                    <td><a href="{{ route('guia_ibm', ['guia' => $device->guia]) }}">{{$device->guia}}</a></td>
                </tr>
                @empty
                <tr>
                    <td colspan="16"> <p class="text-center">No hay registros en la base de datos.</p> </td>
                </tr>
                @endforelse
                </tbody>
            </table>
                </div>
                {{$datos->links()}}
                <p>Resultados {{$datos->firstItem()}} al {{$datos->lastItem()}} de {{$datos->total()}} registros.</p>
                </div>
            
            @endisset
    </div>
</div>
</div>
@endsection