@extends('layouts.app')
@section('title', 'Parque instalado en cliente')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">
                    <h1>Parque instalado en cliente</h1>
                </div>
                <div class='card-body'>
        <div class="table-responsive">
                <table id='ZPEC' class="table table-sm table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Rótulo</th>
                        <th>Material</th>
                        <th>Serie</th>
                        <th>Rut</th>
                        <th>Dirección</th>
                        <th>Estado</th>
                        <th>Operación</th>
                        <th>Fecha</th>
                        <th>Usuario</th>
                    </tr>
                    </thead>
                   
                    
                </table>
         
        </div>
                    @isset($ultima_act)
                    <p>Cargado en {{$ultima_act}}</p>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<style>
.table-responsive{
  font-size: 10px;
}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>

<script type="text/javascript">
    

    
$(document).ready(function()    {
    var table = $('#ZPEC').DataTable( {
        orderCellsTop: true,
        fixedHeader: false,
        responsive: true,
        serverSide: true,
        oSearch: {"bSmart": false},
        ajax: "{{route('dataZpec')}}",
        deferRender: true,
        pageLength: 50,
        dom: 'Bfrtip',
        language: 
                {"url": "{{asset('assets/dt/Spanish.lang')}}"},
        columns: [
        {data: 'id', name: 'id'},    
        { data: 'cliente', name: 'cliente'},
        { data: 'rotulo', name: 'rotulo'},
        { data: 'material', name: 'material'},
        { data: 'serie', name: 'serie'},
        { data: 'rut', name: 'rut'},
        { data: 'direccion', name: 'direccion'},
        { data: 'status', name: 'status'},
        { data: 'operacion', name: 'operacion'},
        { data: 'fecha', name: 'fecha'},
        { data: 'usuario', name: 'usuario'}
        ]
        });
    });
</script>

@endsection