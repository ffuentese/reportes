@extends('layouts.app')
@section('title', 'Subir archivo de ZPEC')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">Importar ZPEC (Parque instalado en cliente)</div>
                
                <div class="card-body">
                    <p>El formato es: "ZPEC YYYYMMDD.xlsx" p. ej: "ZPEC 20190603.xlsx". </p>
                    <p>También puede llevar el nombre de la empresa: "ZPEC Banco de Chile 20190603.xlsx"</p>
                    @isset($ultima_act)
                    <p>El último equipo registrado en la base es del {{$ultima_act->format('d-m-Y')}}</p>
                    @endisset
                     <form action="{{route('procesoZpec')}}"  method="POST" enctype="multipart/form-data">
                     @csrf
                     
                     <div class="col-sm-6">
                        <label for='import_file'>Seleccione archivo</label>
                        <input type="file" name="import_file" class="form-control">
                     </div>
         
                    <br>
                    <button type="submit" class="btn btn-success">Subir ZPEC</button>
                </form>
                </div>
                
                @if ($errors->count() > 0)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  @foreach ($errors->all() as $error)  
                  <p>{{ $error }}</p>
                  @endforeach
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                @endif        
                    
                </div>
                @if (session()->has('success'))

                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>{{session('success')}}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endisset
                                
            </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker4').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $('#datetimepicker5').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false
        });
        $("#datetimepicker4").on("change.datetimepicker", function (e) {
            $('#datetimepicker5').datetimepicker('minDate', e.date);
        });
        $("#datetimepicker5").on("change.datetimepicker", function (e) {
            $('#datetimepicker4').datetimepicker('maxDate', e.date);
        });
    });
</script>
@endsection