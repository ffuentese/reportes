@extends('layouts.app')
@section('title', 'Ver guías de micro')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-12 m-t-15">
            <div class="card">
                <div class="card-header">
                    <h1>Guías de microinformática BEC</h1>
                </div>
                <div class='card-body'>
                    
        <div class="table-responsive">
                <table id='micro' class="table table-sm table-striped">
                    <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Tecnologia</th>
                        <th>Modelo</th>
                        <th>Marca</th>
                        <th>Ticket original</th>
                        <th>Ticket espejo</th>
                        <th>Guía</th>
                        <th>Tipo</th>
                        <th>Fecha</th>
                        <th>Observaciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($micros as $micro)
                        <tr>
                            <td>{{$micro->serial}}</td>
                            <td>{{$micro->tecnologia}}</td>
                            <td>{{$micro->modelo}}</td>
                            <td>{{$micro->marca}}</td>
                            <td>{{$micro->tkt_original}}</td>
                            <td>{{$micro->tkt_espejo}}</td>
                            <td>{{$micro->guia}}</td>
                            <td>{{$micro->tipo}}</td>
                            <td>{{$micro->fecha->format('d-m-Y')}}</td>
                            <td>{{$micro->obs}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <th>Serial</th>
                        <th>Tecnologia</th>
                        <th>Modelo</th>
                        <th>Marca</th>
                        <th>Ticket original</th>
                        <th>Ticket espejo</th>
                        <th>Guía</th>
                        <th>Tipo</th>
                        <th>Fecha</th>
                        <th>Observaciones</th>
                        </tr>
                    </tfoot>
                </table>
         
        </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<style>
.table-responsive{
  font-size: 10px;
}
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
}
</style>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>

<script type='text/javascript'>
$(document).ready(function() {
    // DataTable
    var table = $('#micro').DataTable({
        language: {url: '{{asset('assets/dt/Spanish.lang')}}'},
        buttons: ['colvis', 'csv', 'copy'],
        dom: 'Bfrltip',
        initComplete: function () {
            // acá va el ordenamiento/búsqueda por columnas.
            $('#micro tfoot th').each(function() {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Busca '+title+'" />');
            });
             // Aplica el campo de búsqueda en cada campo
            table.columns().every(function() {
                var that = this;
                $('input', this.footer()).on('keyup change clear', function() {
                    if (that.search() !== this.value ) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        }
    });
});
</script>
@endsection