@extends('layouts.app')
@section('title', 'Subir guías')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">Guardar guías</div>
                
                <div class="card-body">
                    <p>El formato es: "GD Empresa 1234.pdf" 
                        Donde "empresa" es la emisora de la guía (Entel, Adexus, IBM) 
                        y el número final es el folio de la guía de despacho. Se pueden subir varias guías.</p>
                     <form action="{{ route('proceso_guias') }}"  method="POST" enctype="multipart/form-data">
                     @csrf
                     <input type="file" name="import_file[]" class="form-control">
                     
         
                    <br>
                    <button type="submit" class="btn btn-success">Subir guías</button>
                
                </form>
                </div>
                
                @if (session('error'))
                <div class="alert alert-danger">
           
                  <p>{{ session()->get('error')->first() }}</p>
                        
                    
                </div>
                @elseif (session('success'))

                    <div class="alert alert-success">
                        <p>{{session()->get('success')->first()}}</p>
                    </div>
                @endif
                                
                </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection