@extends('layouts.app')
@section('title', 'Ingresar guía de POS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">Agregar guía de POS Banco Estado</div>
                
                <div class="card-body">
                   
                     <form action="{{route('procesa_guia')}}"  method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class='row'>
                         <div class="col">
                            <label for="guia">Guia</label>
                            <input type="number" name="guia" class="form-control" min="0" required="">
                        </div>
                        <div class="col">
                            <label for="fecha">Fecha</label>
                            <input type="date" name="fecha" id="fecha" class="form-control" required="">
                         </div>
                     </div>
                     <div class='form-group'>
                         <label for="tipo">Tipo</label>
                        <select name='tipo' class="form-control col-6">
                            <option value='ENTREGA'>DIRECTA</option>
                            <option value='REVERSA'>REVERSA</option>
                        </select>
                     </div>
                     
                     <div class='form-group form-group-sm'>
                         <div class="row">
                             <div class="col"> 
                                <label for="serial[]">Número de serie</label>
                                <input type="text" name="serial[]" class="form-control" required="">
                             </div>
                             <div class="col">
                                <label for="modelo[]">Modelo</label>
                                <select name='modelo[]' class="form-control" required="">
                                    <option value="">Seleccione modelo</option>
                                    @foreach($modelos as $modelo)
                                    <option value="{{$modelo->name}}">{{$modelo->name}}</option>
                                    @endforeach
                                </select>
                             </div>
                            <div class="col">
                                <label for="tecnologia[]">Tecnología</label>
                                <select name="tecnologia[]" class="form-control" required="">
                                    <option value="3G">3G</option>
                                    <option value="GPRS">GPRS</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="proyecto[]">Proyecto</label>
                                <select name="proyecto[]" class="form-control" required="">
                                    <option value="NO">No</option>
                                    <option value="SI">Sí</option>
                                </select>
                            </div>
                         </div>
                     </div>
                     <div id="wrapper" >
                         
                     </div>
                     
         
                    <br>
                    <button type="submit" class="btn btn-success">Guardar guía</button>
                    <button type="button" class="btn btn-info add_form_field">Agregar</button>
                    
                </form>
                </div>
                
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" >
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @else

                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>{!! session()->get('success')!!}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                
                                
                </div>
            
           
                
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    document.getElementById('fecha').valueAsDate = new Date();
    var wrapper = $("#wrapper");
    var add_button = $(".add_form_field");
    var x = 1;
    $(add_button).click(function(e) {
        e.preventDefault();
        x++;
        var options = @foreach($modelos as $modelo)
                        '<option value="{{$modelo->name}}">{{$modelo->name}}</option>' @if(!$loop->last) + @else ; @endif
                      @endforeach
                      
        var fields = '<div class="row">' +
                             '<div class="col">' + 
                                '<label for="serial[]">Número de serie</label>' +
                                '<span>#' + x + '</span>' +
                                '<input type="text" name="serial[]" class="form-control col" required="">' +
                             '</div>' +
                             '<div class="col">' +
                                '<label for="modelo[]">Modelo</label>'+
                                '<select name="modelo[]" class="form-control col" required="">'+
                                    '<option value="">Seleccione modelo</option>' +
                                    options +
                                '</select>' +
                             '</div>' +
                            '<div class="col">' +
                                '<label for="tecnologia">Tecnología</label>' +
                                '<select name="tecnologia[]" class="form-control col-6" required="">' +
                                    '<option value="3G">3G</option>' +
                                    '<option value="GPRS">GPRS</option>' +
                                '</select>' +
                            '</div>' +
                            '<div class="col">' +
                                '<label for="proyecto[]">Es proyecto:</label>' +
                                '<select name="proyecto[]" class="form-control col-6" required="">' +
                                    '<option value="NO">No</option>' +
                                    '<option value="SI">Sí</option>' +
                                '</select>' +
                            '</div>' +
                         '<input type="hidden" name="nro" value="' + x + '">' +
                         '<button type="button" class="btn btn-danger delete">Borrar</button>' +
                         '</div>'; 
                         
        $(wrapper).append(fields); //add input box
    });

    $(wrapper).on("click", ".delete", function(e) {
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
});
</script>
@endsection