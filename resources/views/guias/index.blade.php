@extends('layouts.app')
@section('title', 'Guías en PDF subidas al sistema')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 m-t-15">
            <div id="calendar"></div>
        </div>
        <div class="col-md-12 m-t-15">
            <table id='GDtable' class="table display">
                <thead>
                <th>Id</th>
                <th>Guia</th>
                <th>Empresa</th>
                </thead>
            </table>

            </div>
        </div>
</div>
@endsection
@section('scripts')
<link rel="stylesheet" href="{{asset('/assets/libs/fullcalendar/dist/fullcalendar.min.css')}}" />
<link rel='stylesheet' href='{{asset('/assets/libs/fullcalendar/dist/fullcalendar.print.min.css')}}' media='print' /> 
<script type="text/javascript"  src='{{asset('/assets/libs/fullcalendar/dist/locale-all.js')}}'></script>
<script type="text/javascript"  src='{{asset('/assets/libs/fullcalendar/dist/fullcalendar.min.js')}}'></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/fh-3.1.4/r-2.2.2/sc-2.0.0/datatables.min.js"></script>

<script type="text/javascript">
$(document).ready(function()  {
    let buttons = $('.dt-buttons');
    var table = $('#GDtable').DataTable( {
        orderCellsTop: true,
        fixedHeader: false,
        responsive: true,
        oSearch: {"bSmart": false},
        ajax: "{{ route('tabla_guias') }}",
        language: 
                {"url": "{{asset('assets/dt/Spanish.lang')}}"}
        ,
        columns: [
        { data: 'id', name: 'id' },
        { data: 'guia', name: 'guia'},
        { data: 'provider', name: 'provider'}
        ],
        columnDefs: [
            {
                targets:1,
                render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<a href="showPDF/' + encodeURIComponent(data) + '" target="_blank">' + data + '</a>';
                    }

                    return data;
                }
            }
            
        ] 
        
    } );
    
    
    $('#GDtable thead tr').clone(true).appendTo( '#GDtable thead' );
    $('#GDtable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" />' );
    
        
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search(this.value) 
                    .draw();
            }
        } );
        
    } );
    
    // Carga de calendario
    
    var today = new Date();
    var calendar = function() {
        $('#calendar').fullCalendar({
        height: 550,
        <!--Header Section Including Previous,Next and Today-->
        header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
        },
        locale: 'es',

        <!--Default Date-->
        defaultDate: today,
        editable: false,

        <!--Event Section-->
        eventLimit: true, // allow "more" link when too many events
        events: [
        @foreach($diario as $d)
            {
            
            start: "{{$d['fecha']->format('Y-m-d')}}",
            url: "{{route('buscar_ibm_por_dia', ['dia' => $d['fecha']->format('Y-m-d')])}}",
            @if ($d['tipo'] == 'ENTREGA')
            title: "Directas: {{$d['cantidad']}}",
            backgroundColor: '#D7F0F7',
            textColor: 'black'
            @else 
            title: "Reversas: {{$d['cantidad']}}",
            backgroundColor: 'red'
            @endif

            },
        @endforeach
        ]
    });
 };
 // Renderiza el calendario.
 calendar();
    
});
</script>

@endsection