<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Device;

class Pos extends Model
{
    /*
     * Este modelo es específico para BancoEstado 
     * representa a la ubicación de los POS.
     * 
     * No utiliza el autoincremental. 
     */
    
    protected $table = 'pos';
    public $incrementing = false;
    
    protected $fillable = ['comuna_id'];
    
    public function devices()
    {
        //return $this->belongsToMany('App\Device', 'devices_pos')->withTimestamps();
        return $this->hasMany('App\Device');
    }
    
    public function comuna() 
    {
        return $this->belongsTo('App\Comuna');
    }
    
    public function getRegionAttribute() {
        $comuna = Comuna::find($this->comuna_id);
        if ($comuna) {
            return Region::find($comuna->region_id);
        } else {
            return null;
        }
        
    }
    
}

