<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accesorio extends Model
{
    //
    protected $guarded = ['id'];
    protected $dates = ['fecha'];
    
    public function tipo() {
        return $this->belongsTo('App\AccesorioTipo', 'tipo_id');
    }
    
    public function modelo() {
        return $this->belongsTo('App\Modelo', 'modelo_id');
    }
}

