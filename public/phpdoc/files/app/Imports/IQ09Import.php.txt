<?php

namespace App\Imports;

use App\Iq09;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithStartRow;

class IQ09Import implements ToModel, WithChunkReading, WithBatchInserts, WithStartRow
{
    /**
    * @param array $row
    * 0 = material
     * 1 = rotulo
     * 2 = serie
     * 3 = centro
     * 4 = almacen
     * 5 = tipo stock
     * 6 = nombre
     * 7 = modificado en
     * 8 = usuario
     * 9 = status sistema
     * 10 = pep
     * 11 = creado
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $serie = $row[2] ?? '';
        $almacen = $row[4] ?? '';
        $modificado = $row[7] ? date("Y-m-d H:i:s", ($row[7] - 25569) * 86400) : null;
        $usuario = $row[8] ?? '';
        $pep = $row[10] ?? '';
        $creado = $row[11] ? date("Y-m-d H:i:s", ($row[11] - 25569) * 86400) : null;
        
        return new Iq09([
            //
            'material' => $row[0],
            'rotulo' => $row[1],
            'serie' => $serie,
            'centro' => $row[3],
            'almacen' => $almacen,
            'tipo_stock' => $row[5],
            'nombre' => $row[6],
            'modificado' => $modificado,
            'usuario' => $usuario,
            'status_sistema' => $row[9],
            'pep' => $pep,
            'creado' => $creado
        ]);
    }

    public function batchSize(): int {
        return 1000;
    }

    public function chunkSize(): int {
        return 1000;
    }

    public function startRow(): int {
        return 2;
    }

}

