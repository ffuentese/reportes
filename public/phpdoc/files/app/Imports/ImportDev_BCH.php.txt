<?php

namespace App\Imports;

use App\Dev_BCH;
use App\Zpec;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class ImportDev_BCH implements ToModel, WithStartRow, WithCustomCsvSettings
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        try {
        $ultima_conexion = Carbon::parse($row[11]);
        } catch (\Exception $e) {
            $ultima_conexion = NULL;
        }
        
        $marca = trim(strtoupper($row[4]));
        $region = trim(strtoupper($row[8]));
        $tipo = trim(strtoupper($row[3]));
        $site = trim(strtoupper($row[9]));
        
        return new Dev_BCH([
            // Crea dispositivo del BCH
            'alias' => $row[0],
            'rotulo' => $row[1],
            'serie' => $row[2],
            'tipo' => $tipo,
            'marca' => $marca,
            'modelo' => $row[5],
            'nombre' => $row[6],
            'rut' => $row[7],
            'region' => $region,
            'site' => $site,
            'fecha_reporte' => $row[10],
            'ultima_conexion' => $ultima_conexion
        ]);
    }

    public function startRow(): int {
        return 2;
    }

    public function getCsvSettings(): array {
        return [
            'delimiter' => ';'
        ];
    }

}

