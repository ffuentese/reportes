<?php

namespace App\Http\Controllers;

use App\Zpec;
use App\Imports\ZpecImport;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Excel;

class ZpecController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Aquí debería estar la tabla que muestre todas las entradas del ZPEC. 
        $zpecs = Zpec::paginate(10);
        $ultima_act = Zpec::max('created_at');
        $data = [
            'zpecs' => $zpecs,
            'ultima_act' => $ultima_act
        ];
        return view('zpec.index', $data);
    }
    
    /**
     * Retorna los datos de la tabla.
     * Utiliza el paquete yajra/datatables.
     * @return \Illuminate\Http\Response 
     */
    
    public function getDatatable() {
        $zpec = DB::table('zpecs')->select(['id', 'cliente', 'rotulo', 'material', 'serie', 'rut', 'direccion', 'status', 'operacion', 'fecha', 'usuario']);
        return Datatables::of($zpec)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Aquí debe haber un formulario de carga
        $ultima_act = Zpec::max('created_at');
        return view('zpec.upload')->with('ultima_act', $ultima_act);
    }

    /**
     * Crea el objeto en el modelo de la base de datos.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $errors = new \Illuminate\Support\MessageBag();
        // Proceso POST que procesa archivos 
        $request->validate([
            'import_file' => 'required|mimes:xlsx'
        ]);
        $file = $request->file('import_file');
        $filename = $request->file('import_file')->getClientOriginalName();
        if ($this->validateFile($filename)) {
            Excel::import(new ZpecImport, $file);
            return redirect()->back()->with('success', 'Carga exitosa! Se encuentra en proceso.');
        } else {
            $error = 'El archivo ' . $filename . ' tiene un nombre no válido.';
            $errors->add($filename, $error);
            return redirect()->back()->withErrors($errors);
        }
        
    }
    
    public function validateFile($file)
    {
        $arr = explode(' ', $file);
        $fecha = substr($arr[1], 0, -5);
        if(strtoupper($arr[0]) != 'ZPEC') {
            return false;
        }
        if (!is_numeric($fecha)) {
            return false;
        }
        
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Zpec  $zpec
     * @return \Illuminate\Http\Response
     */
    public function show(Zpec $zpec)
    {
        // Muestra una entrada del ZPEC con el id o el serie?
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Zpec  $zpec
     * @return \Illuminate\Http\Response
     */
    public function edit(Zpec $zpec)
    {
        // Formulario una entrada del ZPEC con el id o el serie
        // ??
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Zpec  $zpec
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zpec $zpec)
    {
        // Proceso POST de actualización de un dato del ZPEC
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Zpec  $zpec
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zpec $zpec)
    {
        // Elimina un registro del ZPEC
    }
}

