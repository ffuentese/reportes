<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\ServiceImport;

/*
     * Este controlador recibe los datos de los STRs
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     */

class ServiceController extends Controller
{
    
    
    function __construct() {
        
        $this->middleware('auth');

    }
    
    /**
         * Procesa la planilla de los técnicos. El trabajo de importación 
         * lo realiza ServiceImport.
         * 
         * @var Request $request La petición del servidor con el archivo Excel.
         * @return View Redirección a vista de carga.
         */
    
    public function procesoPlanilla(Request $request)
    {
        
        
        $request->validate([
            'import_file' => 'required'
        ]);
        
        $path = $request->file('import_file')->getRealPath();
        Excel::import(new ServiceImport, $path);

        return redirect('/carga')->with('success', 'All good!')->withInput(['tab' => 'terreno']);
    }
}

