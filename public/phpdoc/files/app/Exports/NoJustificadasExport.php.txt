<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use \Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;

class NoJustificadasExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Buscando todas las series que por ciclo no están ordenadas.
        $subq = DB::table('devices')
                ->whereNotNull('fecha_recepcion')
                ->where(function($query){
                    $query->whereNotNull('fecha_reversa')
                          ->orWhereNotNull('fecha_instalacion');
                })
                ->select('serial')
                        ->get()->toArray();
        $series = [];
        foreach ($subq as $s) {
            $series[] = $s->serial;
        }
        
        // Sacando del conjunto a las series que son responsabilidad de IBM
        $subq_resp_ibm = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->select('serial')
                ->groupBy('serial')->get()->toArray();
        $series_ibm = [];
        foreach ($subq_resp_ibm as $s) {
            $series_ibm[] = $s->serial;
        }
        $devices = DB::table('devices')
                ->join('models', 'models.id', '=', 'devices.model_id')
                ->join('brands', 'brands.id', '=', 'models.brand_id')
                ->join('location', 'location.id', '=', 'devices.location_id')
                ->whereNotNull('fecha_recepcion')
                ->whereNotIn('serial', $series)
                ->whereNotIn('serial', $series_ibm)
                ->selectRaw('devices.id as id, devices.serial as serial, devices.guia_recepcion as guia, max(devices.fecha_recepcion) as fecha_recepcion, models.name as modelo, brands.name as marca, devices.rotulo as rotulo, devices.fecha_instalacion as fecha_instalacion, devices.pos_id as pos_id, devices.guia_prev as "Guía reversa", devices.serial_prev as "serie anterior", devices.reversa_prev, devices.ticket as ticket ')
                ->groupBy('devices.id', 'devices.serial', 'models.name', 'devices.guia_recepcion', 'brands.name', 'devices.rotulo', 'devices.fecha_instalacion', 'devices.pos_id', 'devices.guia_prev', 'devices.serial_prev', 'devices.ticket')
                ->orderBy('fecha_recepcion', 'asc')
                ->get();
        
        return $devices;
        
        
    }

    

    public function headings(): array {
        return [
            'Id',
            'Serial',
            'GD Recepción',
            'Fecha recepción',
            'Modelo',
            'Marca',
            'Rotulo',
            'Fecha inst.',
            'Pos Id',
            'GD Reversa',
            'Serie retirada',
            'Ticket'
        ];
    }

    public function map($row): array {
        // fechas
        if (!empty($device->fecha_recepcion)) {
            $fecha_recepcion = Date::dateTimeToExcel($device->fecha_recepcion);
        } else {
            $fecha_recepcion = "";
        }
        
        if (!empty($device->fecha_instalacion)) {
            $fecha_instalacion = Date::dateTimeToExcel($device->fecha_instalacion);
        } else {
            $fecha_instalacion = "";
        }
        
        if (!empty($device->reversa_prev)) {
            $fecha_reversa = Date::dateTimeToExcel($device->reversa_prev);
        } else {
            $fecha_reversa = "";
        }
        
        
        // Ahora sí:
        
        $data = [
            $device->id,
            $device->serial,
            $device->guia_recepcion,
            $fecha_recepcion,
            $device->modelo,
            $device->marca,
            $device->rotulo,
            $fecha_instalacion,
            $device->pos_id,
            $device->guia_prev,
            $device->serial_prev,
            $fecha_reversa,
            $device->ticket
               
        ];
        
    }
    
    public function columnFormats(): array {
       return [
           'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
           'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
           'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
       ];
        
        
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                // Oculta columna de id, de acuerdo a lo solicitado.
                $event->sheet->getDelegate()->getColumnDimension('A')->setVisible(false);
            },
        ];
    }

}

