<?php

namespace App\Exports;

use Date;
use App\Device;
use Maatwebsite\Excel\Concerns\WithMapping;
use \Maatwebsite\Excel\Concerns\FromCollection;
use \Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class EntregaGuiaReversaExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize, WithEvents
{
    function __construct($guia) {
        $this->guia = $guia;
    }

    
    /**
     * Permite obtener archivo Excel con todos los datos asociados a una reversa por serial.
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     * 
    * @return \Illuminate\Support\Collection
    */
    
    public function collection()
    {
        $collection = collect();
        
        Device::where('guia_reversa', '=', $this->guia)->chunk(1000, function($devices) use ($collection){
            foreach($devices as $device) {
                $collection->push($device);
            }
        });
        return $collection;
    }
        
    public function map($device): array
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        
        // fechas
        if (!empty($device->fecha_recepcion)) {
            $fecha_recepcion = Date::dateTimeToExcel($device->fecha_recepcion);
        } else {
            $fecha_recepcion = "";
        }
        
        if (!empty($device->fecha_instalacion)) {
            $fecha_instalacion = Date::dateTimeToExcel($device->fecha_instalacion);
        } else {
            $fecha_instalacion = "";
        }
        
        if (!empty($device->reversa_prev)) {
            $fecha_reversa = Date::dateTimeToExcel($device->reversa_prev);
        } else {
            $fecha_reversa = "";
        }
        
        
        
        // validar instalación
        
        if ($device->fecha_instalacion) {
            $instalado = "OK";
        } else {
            $instalado = "";
        }
        
        
        $data = [
            $device->id,
            $fecha_recepcion,
            $device->guia_recepcion,
            $device->modelo->name,
            $device->modelo->brand->name,
            $device->serial,
            $device->rotulo,
            $fecha_instalacion,
            $device->pos_id,
            $instalado,
            $device->serial_prev,
            $device->location->name,
            $device->service->name,
            $device->technician->name,
            $fecha_reversa,
            $device->guia_prev,
            $device->ticket
               
        ];
            
        return $data;
            
            
    }
    
    public function headings(): array
    {
        return [
            '#',
            'Recepción. provisión',
            'Guía de recepción',
            'Modelo',
            'Marca',
            'Serial',
            'Rótulo',
            'Fecha inst.',
            'POS',
            'Instalado',
            'Serie retirada',
            'Ubicación',
            'STR',
            'Técnico',
            'F. dev. Reversa',
            'GD dev. Reversa',
            'Ticket'
        ];
    }
    
     public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'O' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array {
        return [
        AfterSheet::class    => function(AfterSheet $event) {
            // Oculta columna de id, de acuerdo a lo solicitado.
            $event->sheet->getDelegate()->getColumnDimension('A')->setVisible(false);
        },
    ];
    }

}

