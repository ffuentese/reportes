<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Almacena las imágenes de las guías. 
*
* - guía (integer, nro de documento)
* - provider_id (integer, se relaciona con el objeto Provider para identificar de qué es la guía)
* - filename (string, almacena la ruta del archivo)
 */

class DocGuiaIBM extends Model
{
    //
    protected $fillable = ['guia', 'provider_id', 'filename'];
    
    public function providers()
    {
        return $this->belongsTo('App\Provider', 'provider_id');
    }
}
