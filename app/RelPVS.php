<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelPVS extends Model
{
    //
    protected $dates = ['fecha'];
    protected $guarded = ['id'];
}
