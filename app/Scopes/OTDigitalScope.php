<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class OTDigitalScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     * Aplica una restricción a Eloquent cuando consulte la OTDigital
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->select('id', 'fecha_atencion', 'tipo1', 'marca1', 'serie1', 'rotulo1', 'tipo4', 'marca4', 'serie4', 'rotulo4')
                ->where('fecha_atencion', '>', '01-03-2018')
                ->where('tipo1', 'POS');
    }
}