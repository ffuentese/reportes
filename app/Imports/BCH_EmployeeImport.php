<?php

namespace App\Imports;

use App\Bch_employee;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Importa la dotación de personal del Banco de Chile
 * 
 * Tiene los siguientes campos: 
 * 
 * 0 = RUT (se usa)
 * 1 = DV (se usa)
 * 2 = Nombre (se usa)
 * 3 = Centro_costo
 * 4 = Oficina
 * 5 = Glosa_OFI
 * 6 = UNIDA_CUIPR
 * 7 = GLOSA_UNIDAD
 * 8 = COD_CARGO
 * 9 = CARGO
 * 10 = GLOSA_EMP
 * 11 = DIRECCION (se usa) 
 * 12 = ANEXO
 * 13 = OFI_DIRECTO
 * 14 = FAX
 * 15 = EMAIL (se usa)
 * 16 = CUI_DIV
 * 17 = GLS_CUI_DIV
 * 18 = CUI_AREA
 * 19 = GLS_CUI_ARE
 * 20 = CUI_DTP
 * 21 = GLS_CUI_DTP
 * 22 = Nombre jefe
 * 23 = UNIDA_CUIPR_jefe
 * 24 = GLOSA_UNIDAD_jefe
 * 25 = ANEXO_jefe
 * 26 = OFI_DIRECTO_jefe
 * 27 = FAX_jefe
 * 28 = EMAIL_jefe
 * 
 */

class BCH_EmployeeImport implements ToModel, WithStartRow, WithChunkReading, WithBatchInserts, WithCalculatedFormulas, ShouldQueue
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $rut = $row[0] . $row[1];
        $emp = Bch_employee::where('rut', $rut)->exists();
        if ($emp) {
            return null;
        }
        $email = $row[15];
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $username = strstr($email, '@', true);
        } else {
            $username = "";
        } 
        
        return new Bch_employee([
            //
            'name' => $row[2],
            'rut' => $rut,
            'username' => $username,
            'email' => $email,
            'direccion' => $row[11]
            
            
        ]);
    }

    public function batchSize(): int {
        return 1000;
    }

    public function chunkSize(): int {
        return 1000;
    }

    public function startRow(): int {
        return 2;
    }

}
