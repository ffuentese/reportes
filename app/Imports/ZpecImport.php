<?php

namespace App\Imports;

use App\Zpec;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Queue\ShouldQueue;


class ZpecImport implements ToCollection, WithStartRow, WithChunkReading, ShouldQueue
{
    public function collection(Collection $collection) {
        foreach($collection as $row) 
        {
            $rotulo = ltrim($row[2],0);
            $guia = ltrim($row[14], 0);
            $sap = ltrim($row[15], 0);
            $find = ['id' => ltrim($row[0],0)];
            $data = [
                'cliente' => ltrim($row[1],0),
                'rotulo' => $rotulo,
                'material' => $row[3],
                'serie' => $row[4],
                'rut' => $row[5],
                'direccion' => $row[7],
                'status' => $row[9],
                'operacion' => $row[11],
                'fecha' => $row[12],
                'guia' => $guia,
                'sap' => $sap,
                'usuario' => $row[16]
            ];
            try {
            Zpec::updateOrCreate($find, $data);
            } catch (\Exception $e) {
                $msg = "Id es " . $row[0];
                \Log::info($msg);
            } 
              
            
        }
    }
    

    public function startRow(): int {
        return 2;
    }

    public function chunkSize(): int {
        return 1000;
    }

    

}
