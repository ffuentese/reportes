<?php

namespace App\Imports;

use App\Device;
use App\Modelo;
use App\Brand;
use App\Guia;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Log;

/**
 * Importa las planillas de reversa
 * 
 * @author Francisco Fuentes <ffuentese@entel.cl>
 * 
 * @param type $fecha
 * @param type $guia
 */
class ActaRecImport implements ToCollection, WithStartRow, WithCalculatedFormulas {

    function __construct($fecha, $guia) {
        $this->fecha = $fecha;
        $this->guia = $guia;
    }

    /**
     * Importa las planillas de reversa
     * 
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows) {
        $count = 0;
        foreach ($rows as $row) {
            // para detectar fin de documento
            if ($row[1] == "") {
                break;
            }
            

            // Busca modelo en la base
            $modelo = Modelo::where('name', $row[3])->first();
            $obs = $row[5] ?? "";
            if (!$modelo) {
                // si el modelo no está lo crea
                $modelo = $this->createModel($row);
            }
            // limpia serial
            $serial = preg_replace("/[^a-zA-Z0-9]/", "", $row[1]);
            $fecha_reversa = $this->fecha;

            // busca con el serial, el equipo en la base
            $dispositivo = Device::where('serial', $serial)
                    ->whereNull('fecha_reversa')
                    ->where('fecha_recepcion', '<=', $fecha_reversa)
                    ->where(function ($query) use ($fecha_reversa) {
                        $query->where('fecha_instalacion', '<=', $fecha_reversa)
                        ->orWhereNull('fecha_instalacion');
                    })
                    ->orderBy('fecha_recepcion', 'asc')
                    ->first();



            $datos = [
                // datos a importar desde el acta
                'serial' => $serial,
                'model_id' => $modelo->id,
                'fecha_reversa' => $this->fecha,
                'guia_reversa' => $this->guia,
                'customer_id' => 1, // datos genéricos
                'str_id' => 1, // datos genéricos
                'pos_id' => 0,
                'provider_id' => 1,
                'obs' => $obs
            ];

            // si hay un dispositivo lo actualiza
            if ($dispositivo) {
                $dispositivo->update($datos);
            }

            // para evitar que se escapen seriales que no están ingresados...
            $c = Device::where('serial', $serial)->whereNotNull('fecha_recepcion')->exists();
            
            // de no haberlo crea un dispositivo con la fecha de reversa.
            if (!$c && !$dispositivo) {
                Device::create($datos);
                
            }
            
            $datos_guia = [
                    'serial' => $serial,
                    'guia' => $this->guia,
                    'fecha' => $this->fecha,
                    'estado' => 'REVERSA',
                    'obs' => $obs
                ];
            $existe_guia = Guia::where('serial', $serial)->where('guia', $this->guia)->exists();
            if (!$existe_guia) {
                Guia::create($datos_guia);
            }
        }
        $msg = "Hay " . $count . " entradas que no existen en la base";
        Log::info($msg);
    }

    /**
     * Crea un modelo si no existe.
     * 
     * @var $row Arreglo con datos de una fila del documento.
     * @return App\Modelo Modelo recién creado.
     */
    public function createModel($row) {
        $marca = Brand::where('name', trim($row[4]))->first();
        $nuevoModelo = new Modelo;
        if ($marca) {
            $nuevoModelo->brand_id = $marca->id;
            $nuevoModelo->name = $row[3];
            $nuevoModelo->save();
        } else {
            $nuevaMarca = new Brand;
            $nuevaMarca->name = $row[4];
            $nuevaMarca->save();
            $nuevoModelo->brand_id = $nuevaMarca->id;
            $nuevoModelo->name = $row[3];
            $nuevoModelo->save();
        }

        return $nuevoModelo;
    }

    /**
     * Busca una cadena inicial en el inicio de otra más grande.
     * 
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function startsWith($haystack, $needle) {
        return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    /**
     * En qué fila se comienza a leer los datos de la hoja de cálculo.
     * @return int
     */
    public function startRow(): int {
        return 8;
    }

}
