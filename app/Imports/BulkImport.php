<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Device;
use App\Modelo;
use App\Brand;
use App\Guia;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use \Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class BulkImport implements ToCollection, WithCalculatedFormulas, 
        WithStartRow, WithChunkReading
{
    /**
     *  Este importador recupera la información del consolidado de entregas (directas)
     * 
     *  $row[0] = Elemento (no se usa)
     *  $row[1] = Serie
     *  $row[2] = Tecnología 
     *  $row[3] = Fecha de despacho (fecha_recepcion)
     *  $row[4] = Guía de recepción
     *  $row[5] = Acta (no se utiliza)
     *  $row[6] = Modelo: [Tipo] [[Marca] Modelo] = POS VERIFONE VX520 3G // Debe ser el descriptor SAP
     *  $row[7] = Homologación 
     *  $row[8] = Cantidad (no se usa)
     *  $row[9] = Si es proyecto
     *  $row[10] = IBM
     * 
     * 
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     * 
     * @var Collection @rows
     * @return void 
     *
    */
    public function collection(Collection $rows)
    {
        //
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        foreach($rows as $row)
        {
            if ($row[0] == '') {
                break;
            }
            $model_name = $row[6];
            $serial = preg_replace("/[^a-zA-Z0-9]/", "", $row[1]);
            $fecha_ingreso = date("Y-m-d H:i:s", ($row[3] - 25569)*86400);
            $guia_recepcion = $row[4] ?? 0;
            $tecnologia = $row[2];
            $obs = $row[9] ?? "";
            //$device = new Device;
            $modelo = Modelo::where('name', $model_name)->first();
            $proveedor = $row[10] ?? '';
            if(!$modelo){
                $modelo = $this->createModel($row);
            }
            
            if (strtoupper($proveedor) == 'IBM') {
                $provider = \App\Provider::where('name', 'IBM')->first();
                $str_ibm = \App\Service::where('name', 'IBM')->first();
                $str_id = $str_ibm->id;
                $location = 'IBM';
                $provider_id = $provider->id;
            } else {
                $str_entel = \App\Service::where('name', 'ENTEL')->first();
                $provider = \App\Provider::where('name', 'ENTEL')->first();
                $str_id = $str_entel->id;
                $location = '0';
                $provider_id = $provider->id;
            }

            $datos = [
                    // los datos que van a la base
                        'serial' => $serial,
                        'model_id' => $modelo->id,
                        'fecha_recepcion' => $fecha_ingreso,
                        'guia_recepcion'  => $guia_recepcion,
                        'customer_id' => 1, // BEC
                        'str_id' => $str_id, // 
                        'provider_id' => $provider_id,
                        'location_id' => $location,
                        'obs' => $obs
                    ];
            
            

            // Revisamos si no hay un equipo ingresado dentro de la misma guia
            // Así evitamos que al ingresar por error la misma guía dos veces se ingresen dos equipos
            $device = Device::where('serial', $serial)
                    ->where('guia_recepcion', $guia_recepcion)
                    ->first();
            $datos_guia = [
                    'serial' => $serial,
                    'guia' => $guia_recepcion,
                    'fecha' => $fecha_ingreso,
                    'modelo' => $model_name,
                    'tecnologia' => $tecnologia,
                    'estado' => 'ENTREGA',
                    'obs' => $obs
                ];
            if(!$device) {
                Device::create($datos);
                $existe_guia = Guia::where('serial', $serial)->where('guia', $guia_recepcion)->exists();
                if (!$existe_guia) {
                    Guia::create($datos_guia);
                }
            }
            
            if(strtoupper($proveedor) == 'IBM'){
                $datos_guia['estado'] = 'IBM';
                \App\GuiaIBM::create($datos_guia);
                
            }
        
        }
    }
    
    public function createModel($row) 
    {
        // crea modelo en sistema en caso de no existir en la base
        $sap_desc = $row[6];
        $model_name = substr(strstr($row[6], " "), 1);
        $brand_name = strtok($model_name, " ");
        $brand = Brand::where('name', $brand_name)->first(); // la marca p. ej. VERIFONE
        $nuevoModelo = new Modelo;
        if($brand) {
            $nuevoModelo->brand_id = $brand->id;
            $nuevoModelo->name = $sap_desc;
            $nuevoModelo->save();
        } else {
            $nuevoModelo->brand_id = 'UNC';
            $nuevoModelo->name = $sap_desc;
            $nuevoModelo->save();
        }
        
        return $nuevoModelo;
        
    }

    public function startRow(): int {
        return 2;
    }

    public function chunkSize(): int {
        return 2000;
    }

}
