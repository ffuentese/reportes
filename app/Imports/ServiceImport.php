<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Service;
use App\Modelo;
use App\Device;
use App\Brand;
use App\Technician;
use Carbon\Carbon;

/**
     * Este importador procesa las plantillas Excel de los STR
     * 
    * @param Collection $rows
    */

class ServiceImport implements ToCollection, WithStartRow
{
    /**
         * Procesa planilla de STRs
         * 
         * Busca el serial del equipo de la fila en la base
         * y actualiza estado de almacén, y datos de servicio técnico
         * 
         * $row[0] = Proveedor
         * $row[1] = Nombre del técnico
         * $row[2] = Uso de equipo (Stock, reversa, instalado)
         * $row[3] = Fecha de reversa
         * $row[4] = Tipo (POS)
         * $row[5] = Modelo: [POS] [[Marca] Modelo]
         * $row[6] = Rótulo
         * $row[7] = Serie
         */
    
    public function collection(Collection $rows)
    {
        
        foreach($rows as $row)
        {
            if(isset($row[4]) && $row[4] != 'POS'){
                // Si el modelo no dice POS en la columna "tipo", lo ignora.
                continue;
            }
            // el modelo del equipo
            if ($row[5]) {
                $model_name = strtoupper($row[5]);
            }
            // el nombre del servicio técnico
            if ($row[0]) {
                $str_name = strtoupper($row[0]);
            }
            // Servicio técnico
            $st = Service::where('name', $row[0])->first();
            if (!$st) {
                $st = $this->createSTR($row);
            }
            
            // $fecha Fecha de recepción o de reversa 
            $fecha = "";
            if(!is_null($row[3])){
                try {
                    $fecha_pl = date("Y-m-d H:i:s", ($row[3] - 25569)*86400);
                    $fecha = new Carbon($fecha_pl);
                } catch (Exception $e) {
                    $fecha = null;
                }
            }
            $uso = strtoupper($row[2]); 
            $serial = preg_replace("/[^a-zA-Z0-9]/", "",$row[7]);
            // Tiene que saltar la fila si no hay serial
            if(is_null($serial)){
                continue;
            }
            if(isset($row[6])){
                $rotulo = strtoupper($row[6]);
            }
            //$device = new Device;
            if (isset($row[1])){
                $technician = Technician::where('name', $row[1])->first();
                if (!$technician) {
                    $technician = $this->createTechnician($row);
                }
            }
            // usa el rótulo si existe
            if(!is_null($rotulo) && $this->startsWith($rotulo, 'BEC')){
                $device = Device::where('serial', $serial)
                    ->where('rotulo', $rotulo)
                    ->whereNull('fecha_reversa')
                    ->where('fecha_recepcion', '<=', $fecha)
                    ->first();
            } else {
                $device = Device::where('serial', $serial)
                        ->whereNull('fecha_reversa')
                        ->where('fecha_recepcion', '<=', $fecha)
                        ->orderBy('fecha_recepcion', 'asc')->first();
            }
            // Si no hay dispositivo

            if (!$device) {
                $modelo = Modelo::where('name', $model_name)->first();
                if (!$modelo) {
                    $modelo = $this->createModel($row);
                }
                $datos = [
                    'serial' => $serial,
                    'model_id' => $modelo->id,
                ];

                if(!is_null($rotulo) && $this->startsWith($rotulo, 'BEC')){
                    $datos += ['rotulo' => $rotulo];
                }

                if ($fecha && (strcmp($uso, 'REVERSA') === 0 || strcmp($uso, 'DEVOLUCION') === 0)) {
                    $datos += ['fecha_reversa' => $fecha];
                }
                if ($fecha && strcmp($uso, 'STOCK') === 0) {
                    $datos += ['fecha_recepcion' => $fecha];
                }

                $device = Device::create($datos);
            }
            
            // Si se encontró un dispositivo.
            
            if ($device && strcmp($uso, 'INSTALADO') === 0){
               // $device->fecha_instalacion = $fecha;
                $device->str_id = $st->id;
                if (isset($technician)){
                    $device->technician_id = $technician->id;
                }
                $device->save();
            }
            if ($device && (strcmp($uso, 'REVERSA') === 0 || strcmp($uso, 'DEVOLUCION') === 0)) {
                $device->location_id = "REV";
                $device->str_id = $st->id;
                if ($fecha) {
                    $device->fecha_reversa = $fecha;
                }
                $device->save();
            }
            
            if($device && strcmp($uso, 'STOCK') === 0)
            {
                $device->location_id = 'STO';
                $device->str_id = $st->id;
                if (isset($technician)){
                    $device->technician_id = $technician->id;
                }
                if ($fecha && !isset($device->fecha_recepcion)){
                    $device->fecha_recepcion = $fecha;
                }
                $device->save();
            }
            
        }
    }
    
    /**
     * Crea un servicio técnico a partir de la información de una fila.
     * 
     * @param type $row
     * @return Service
     */
    
    public function createSTR($row)
    {
        $st = new Service;
        $st->name = $row[0];
        $st->save();
        return $st;
    }
    
    
    
    function startsWith($haystack, $needle) 
    {
    // search backwards starting from haystack length characters from the end
    return $needle === ''
      || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }
    
    /**
     * crea modelo en sistema en caso de no existir en la base
     * extrae de la descripción un modelo. 
     * la estructura es "POS" "Marca" "Modelo": 
     * POS VERIFONE VX520 TRIO = [POS] [[VERIFONE] VX520 TRIO]
     * O bien sólo "Marca" "Modelo":
     * [[VERIFONE] VX520 TRIO]
     * @param type $row
     * @return Modelo
     */
    
    
    
    public function createModel($row) 
    {
        
        $arr_descripcion = array_filter(explode(" ", $row[5]));
        if($arr_descripcion[0] == 'POS') {
            $nombre_marca = $arr_descripcion[1];
            $model_name_arr = array_slice($arr_descripcion, 1);
        } else {
            $nombre_marca = $arr_descripcion[0];
            $model_name_arr = $arr_descripcion;
        }
        // el nombre del modelo
        $model_name = implode(" ",$model_name_arr);
        $marca = Brand::where('name', $nombre_marca)->first();
        $nuevoModelo = new Modelo;
        if($marca) {
            $nuevoModelo->brand_id = $marca->id;
            $nuevoModelo->name = $model_name;
            $nuevoModelo->save();
        }
                
        return $nuevoModelo; 
    }
    
    /**
     * Crea un nuevo técnico a partir de un STR ya existente.
     * 
     * @param type $row
     * @return Technician
     */
    
    public function createTechnician($row)
    {
        // Busca un servicio técnico
        $st = Service::where('name', $row[0])->first();
        // añade al técnico
        if (is_null($row[1])){
           $tech = Technician::where('id', 1)->first();
           return $tech;
        }
        $tech = new Technician;
        $tech->name = $row[1];
        $tech->str_id = $st->id;
        $tech->save();
        return $tech;
    }

    public function startRow(): int {
        return 2;
    }

}
