<?php

namespace App\Imports;

use App\OTDigital;
use Maatwebsite\Excel\Concerns\ToModel;
use \Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

        
class OTDigitalImport implements ToModel, WithBatchInserts, WithCustomCsvSettings, WithHeadingRow, WithStartRow
{
    
    

        /**
     * Importa cada línea al modelo OTDigital. 
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    
    public function model(array $row)
    {
        $fecha = strtotime($row['fecha_atencion']);
        if ($fecha){
            $fecha_atencion = date("Y-m-d H:i:s", $fecha);
        } else {
            $fecha_atencion = false;
        }
        
        $serial1 = preg_replace("/[^a-zA-Z0-9]/", "", $row['serie1']); 
        $serial4 = preg_replace("/[^a-zA-Z0-9]/", "", $row['serie4']); 
        
        return new OTDigital([
            // inserta los datos de las OTs en la base de datos
            
            'ticket' => $row['ticket'],
            'fecha_atencion' => $fecha_atencion,
            'foliodespacho' => $row['foliodespacho'],
            'folioot' => $row['folioot'],
            'cliente' => $row['cliente'],
            'sucursal' => $row['sucursal'],
            'nombre' => $row['nombre'],
            'rut' => $row['rut'],
            'direccion' => $row['direccion'],
            'ciudad' => $row['ciudad'],
            'tipo1' => $row['tipo1'],
            'marca1' => $row['marca1'],
            'serie1' => $serial1,
            'rotulo1' => $row['rotulo1'],
            'tipo4' => $row['tipo4'],
            'marca4' => $row['marca4'],
            'serie4' => $serial4,
            'rotulo4' => $row['rotulo4'],
            'usuario' => $row['usuario'],
            'proveedor' => $row['proveedor']
            
            
        ]);
    }

    /**
     * El límite de inserciones de MySQL está en 65535 por consulta. 
     * 2000 por la cantidad de líneas es un valor óptimo. 
     * @return int
     */
    public function batchSize(): int {
        return 2000;
    }

    public function getCsvSettings(): array {
        return [
            'delimiter' => ';'
        ];
    }

    public function startRow(): int {
        return 2;
    }

}
