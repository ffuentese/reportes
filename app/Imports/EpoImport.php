<?php

namespace App\Imports;

use App\Epo;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class EpoImport implements ToCollection, ShouldQueue, WithStartRow, WithChunkReading, WithBatchInserts, WithMultipleSheets
{
    function __construct($fecha) {
        $this->fecha = Carbon::parse($fecha);
    }
    
    /**
     * Importa cada fila del 
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public function collection(Collection $rows) {
        
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        
        foreach ($rows as $row)
        {
            if ($row[0] == '' || is_null($row[0])) {
                break;
            }
            
            if (isset($row[6]) && !is_null($row[6])) {
                $ultima_conexion =  Carbon::createFromFormat('j/m/y G:i:s T',$row[6]);
            } else {
                $ultima_conexion = null;
            }

            Epo::updateOrCreate(
                [
                    'hostname' => $row[0]
                ],
                [
                    'plataforma' => $row[1],
                    'ip' => $row[2],
                    'mac_address' => $row[3],
                    'dominio' => $row[4],
                    'usuario' => $row[5],
                    'ultima_conexion' => $ultima_conexion,
                    'portatil' => $row[7],
                    'fecha_carga' => $this->fecha
            ]);
        }
    }

    public function batchSize(): int {
        return 1000;
    }

    public function chunkSize(): int {
        return 1000;
    }

    public function startRow(): int {
        return 2;
    }
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    public function sheets(): array {
        return [
            0 => $this
        ];
    }

    

}
