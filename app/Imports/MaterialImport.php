<?php

namespace App\Imports;

use App\Material;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class MaterialImport implements ToModel, WithStartRow, WithChunkReading, WithBatchInserts
{
    /**
     * Importa los datos más importantes desde la planilla de consolidado de materiales. 
     * 
     * Código de material
     * Descripción (modelo asociado al material)
     * Perfil (si es seriado o no)
     * Tipo (Si es PC de escritorio, notebook, etc)
     * 
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public function model(array $row)
    {
        if(isset($row[27])) {
            $perfil = $row[27];
        } else {
            $perfil = "";
        }
        
        return new Material([
            //
            'material' => $row[1],
            'descripcion' => $row[2],
            'perfil' => $perfil,
            'subtipo' => $row[36],
            'tipo' => $row[37]
        ]);
    }

    public function chunkSize(): int {
        return 1000;
    }

    public function startRow(): int {
        return 2;
    }

    public function batchSize(): int {
        return 1000;
    }

}
