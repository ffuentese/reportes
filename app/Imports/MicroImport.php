<?php

namespace App\Imports;

use App\Micro;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class MicroImport implements ToModel
{
    /**
     * $row[0] = serial
     * $row[1] = tecnología
     * $row[2] = modelo
     * $row[3] = marca
     * $row[4] = ticket original
     * $row[5] = ticket espejo
     * $row[6] = guia
     * $row[7] = tipo (si es directa o reversa)
     * $row[8] = fecha de la guía (Datetime)
     * $row[9] = proveedor
     * $row[10] = observaciones
     * 
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public function model(array $row)
    {
        $fecha = $row[8] ? date("Y-m-d H:i:s", ($row[8] - 25569) * 86400) : null;
        return new Micro([
            //
            'serial' => $row[0],
            'tecnologia' => $row[1],
            'modelo' => $row[2],
            'marca' => $row[3],
            'tkt_original' => $row[4] ?? null,
            'tkt_espejo' => $row[5] ?? null,
            'guia' => $row[6],
            'tipo' => $row[7],
            'fecha'  => $fecha,
            'proveedor' =>  $row[9] ?? null,
            'obs' => $row[10]
        
        ]);
    }
}
