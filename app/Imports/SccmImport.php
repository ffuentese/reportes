<?php

namespace App\Imports;

use App\Sccm;
use App\Bch_location;
use App\Bginfo;
use App\Modelo;
use App\Brand;
use App\Zpec;
use App\Comuna;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Carbon\Carbon;

/**
 * Importador del archivo SCCM 
 * 
 * Tiene los siguientes campos a la fecha:
 * 0 = alias
 * 1 = marca
 * 2 = modelo
 * 3 = sistema operativo 
 * 4 = procesador
 * 5 = memoria
 * 6 = disco duro
 * 7 = serie
 * 8 = chassis
 * 9 = ip
 * 10 = dirección MAC
 * 11 = último escaneo
 * 12 = fecha de instalación
 * 13 = nombre de usuario
 * 14 = nombre real
 * 15 = arquitectura
 * 16 = versión cliente
 * 17 = versión bios
 * 18 = resource ID 
 * 19 = ultimo registro
 * 20 = segmento
 * 21 = ubicacion
 * 22 = marca 
 * 23 = CUI
 * 24 = sucursal
 * 25 = dirección
 * 26 = ciudad
 * 27 = región
 * 28 = red
 * 
 * @author Francisco Fuentes <ffuentese@entel.cl>
 */

class SccmImport implements ToModel, WithStartRow, WithChunkReading, WithBatchInserts, WithMultipleSheets, ShouldQueue
{
    function __construct($fecha) {
        $this->fecha = Carbon::parse($fecha);
    }

    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0])){
            return null;
        }
        
        if ($this->startsWith($row[0], 'TCR')) {
            return null;
        }
        
        if (isset($row[23])) {
            $ciudad = $row[26];
            $id_location = $row[23];
            $comuna = Comuna::where('name', $ciudad)->first();
            if (!$comuna) {
                $site_id = null;
            } else if ($comuna && $id_location > 0) {
                $bch_loc = Bch_location::firstOrCreate(
                        [
                            'id' => $row[23]],
                        [
                    'id' => $row[23],
                    'sucursal' => $row[24],
                    'direccion' => $row[25],
                    'comuna_id' => $comuna->id
                ]);
                $site_id = $bch_loc->id;
            } else if ($comuna && $id_location == 0) {
                $bch_loc = Bch_location::firstOrCreate(
                        [
                            'sucursal' => $row[24]],
                        [
                    'sucursal' => $row[24],
                    'direccion' => $row[25],
                    'comuna_id' => $comuna->id
                ]);
                $site_id = $bch_loc->id;
            }
        } else {
            $site_id = null;
        } 
        $serie = $row[7] ?? null;
        if ($serie) {
            $serie_zpec = Zpec::where('serie', $row[7])->orderBy('id', 'desc')->first();
            if ($serie_zpec) {
                $material_serie = \App\Material::where('material', $serie_zpec->material)->first();
                if ($material_serie && $material_serie->marca) {
                    $modelo = Modelo::firstOrCreate(['name' => $material_serie->descripcion, 'brand_id' => $material_serie->marca->id]);
                }
            }
        }
        
        if (!isset($modelo)) {
            // busca en el inventario previo
            $modelo = $row[2] ?? null;
            if ($modelo) {
                $prev_inv = \App\Dev_BCH::where('modelo', $row[2])->first();
            } else {
                $prev_inv = null;
            }
            if ($prev_inv) {
                $brand = Brand::where('name', $prev_inv->marca)->first();
            }
            if ($prev_inv && $prev_inv->name != 'SIN INFORMACION' && $brand) {
                $modelo = Modelo::firstOrCreate([
                    'name' => $prev_inv->modelo,
                    'brand_id' => $brand->id
                ]);
            } else {
                $brand = Brand::firstOrCreate(['id' => 'UNC', 'name' => 'DESCONOCIDO']);
                $modelo = Modelo::firstOrCreate(['name' => 'DESCONOCIDO', 'brand_id' => $brand->id]);
            }
            if (!$modelo) {
                $brand = Brand::firstOrCreate(['id' => 'UNC', 'name' => 'DESCONOCIDO']);
                $modelo = Modelo::firstOrCreate(['name' => 'DESCONOCIDO', 'brand_id' => $brand->id]);
            }
        }
        // verifica que haya un usuario (el "username" viene en formato dominio\usuario)
        if(!isset($row[13])){
            $row[13] = "";
        }   
        // Si trae la barra es un usuario válido, por ejemplo BCH\smunoz
        $user_valido = strpos($row[13],"\\");
        if (!$user_valido) {
            $username = $row[13];
        } else {
            $str_arr = explode('\\', $row[13]);
            $username = $str_arr[1];
        }
        // agrega la última conexión si está disponible
        if (isset($row[11])) {
            $ultima_conexion = date("Y-m-d H:i:s", ($row[11] - 25569) * 86400);
        } else {
            $ultima_conexion = null;
        }
        return new Sccm([
            // Crea el modelo y lo persiste.
            'hostname' => $row[0] ?? "",
            'model_id' => $modelo->id,
            'serie' => $row[7] ?? "",
            'chassis' => $row[8] ?? "",
            'segmento' => $row[9] ?? "",
            'ultima_conexion' => $ultima_conexion,
            'username' => $username,
            'fecha_carga' => $this->fecha,
            'site_id' => $site_id,
            'red' => $row[28] ?? ""
        ]);
    }

    public function batchSize(): int {
        return 1000;
    }

    public function chunkSize(): int {
        return 1000;
    }

    public function startRow(): int {
        return 2;
    }
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    public function sheets(): array {
        return [
            0 => $this
        ];
    }

}
