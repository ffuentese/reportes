<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Pos;
use App\Device;
use \Datetime;
use Maatwebsite\Excel\Concerns\WithStartRow;

class POSImport implements ToCollection, WithStartRow, WithChunkReading, ShouldQueue
{
    
    
   public $tries = 3;
   
   function __construct(Datetime $date) {
       $this->date = $date;
   }
   
   /**
    * Importa datos de la planilla de puntos vigentes de Banco Estado.
    * 
    * Actúa sobre Device (equipos) y POS 
    * 
    * $row[1] = id de POS
    * $row[4] = estado de vigencia
    * $row[28] = serie de equipo
    * 
    * @param Collection $rows
    */

    public function collection(Collection $rows)
    {
        //
        ini_set('max_execution_time', 600);
        foreach($rows as $row)
        {
            if(!isset($row[28])) {
                continue;
            }
            // limpia serial de ceros a la izquierda
            $serial = ltrim($row[28], '0');
            $devices = Device::where('serial', $serial)->get();
            if(!$devices){
                continue;
            }
            // Busca el POS (lugar) en la base
            $pos = Pos::where('id', $row[1])->first();
            // si no hay un "pos" registrado lo crea
            if(!$pos) {
                $pos = new Pos;
                $pos->id = $row[1];
                $pos->vigente = ($row[4] == 'VIGENTE' ? true : false);
                $pos->save();
            } else {
                $pos->vigente = ($row[4] == 'VIGENTE' ? true : false);
                $pos->save();
            }
            
            // busca serial en la base de datos
            $device = $devices->where('fecha_recepcion', '<', $this->date)
                    ->where('customer_id', 1)
                    ->orderBy('fecha_recepcion', 'asc')
                    ->first();
            // si el dispositivo está y es distinto al que tenía ese pos, lo cambia.
            if($device && $device->pos_id != $row[1] && $device->fecha_instalacion != $this->date){
                // busca el dispositivo anterior
                
                $device->pos_id = $pos->id;
                $device->fecha_instalacion = $this->date;
                $device->save();
                //asociamos dispositivo a ubicación en una relación de muchos a muchos
                $device->pos()->attach($pos);
            } 
        }
        
    }

    public function chunkSize(): int {
        return 2000;
    }

    public function startRow(): int {
        return 2;
    }

}
