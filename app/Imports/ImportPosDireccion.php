<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Region;
use App\Comuna;
use App\Pos;

class ImportPosDireccion implements ToCollection, WithStartRow
{
    /**
    * Actualiza los POS con información de dirección y comuna.
    * @param Collection $rows
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $pos_id = $row[1];
            $pos = Pos::find($pos_id);
            if($pos) {
                if($pos->comuna_id > 0){
                    continue;
                }
            }
            $region_id = $row[5];
            $region = Region::find($region_id);
            $comuna_str = $row[6];
            $comuna = Comuna::where('name', $comuna_str)->first();
            if (!$comuna) {
                $comuna = new Comuna;
                $comuna->name = $comuna_str;
                $comuna->region_id = $region->id;
                $comuna->save();
            }
            $direccion = $row[9] . ' ' . $row[10];
            if($pos) {
               $pos->direccion = $direccion;
               $pos->comuna_id = $comuna->id;
               $pos->save();
            } else {
               $pos = new Pos;
               $pos->vigente = $row[4] == 'VIGENTE' ? true : false;
               $pos->id = $pos_id;
               $pos->direccion = $direccion;
               $pos->comuna_id = $comuna->id;
               $pos->save();
            }
        }
    }

    public function startRow(): int {
        return 2;
    }

}
