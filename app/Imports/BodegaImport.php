<?php

namespace App\Imports;

use App\Device;
use App\Modelo;
use App\Status;
use App\Location;
use App\Brand;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;


class BodegaImport implements ToCollection, WithStartRow
{
    /**
     * Procesa las planillas con la información de bodega para cuadrarlas 
     * y agrega la información nueva en caso de que no estuviera ingresada.
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
    * @param array $rows
    *
    * @return void
    */
    
    public function collection(Collection $rows) 
    {
        ini_set('max_execution_time', 600);
        foreach($rows as $row)
        {
            $rotulo = $row[2];
            $serial = $row[3];
            if(is_null($rotulo) && is_null($serial)){
                // si no hay ni serial ni rótulo ignorar fila
                continue;
            }

            $model_name = substr(strstr($row[7]," "), 1);
            $modelo = Modelo::where('name', $model_name)->first();
                if(!$modelo){
                    $modelo = new Modelo;
                    // extrae el id de la marca desde el material
                    $idmarca = substr($row[1],6,3); 
                    $marca = Brand::where('id',$idmarca)->first();
                    $modelo->brand_id = $marca->id;
                    $modelo->name = $model_name;
                    $modelo->save();
                }
            $almacen = Location::where('id', $row[5])->first();
            if(!$almacen){
                // si no existe se le da un genérico
                $almacen = Location::where('id', '0')->first();
            }

            $datos = [
                   'serial' => $serial,
                   'rotulo' => $rotulo,
                   'code' => $row[1], // el material
                   'location_id' => $almacen->id, // el código del almacén
                   'status_id' => $row[6], // el estatus de almacén
                   'cost_center' => $row[4],
                   'pep' => $row[12],
                   'model_id' => $modelo->id, // identificador del modelo
                   'str_id' => 1,
                   'customer_id' => 1 // el cliente, i.e. Banco Estado
            ];


            if(is_null($serial) && !is_null($rotulo)){
                // si sólo hay rótulo

                $device = Device::where('rotulo', $rotulo)
                        ->whereNull('fecha_reversa')->first();

                if($device){
                    // si está en la base de datos
                    $device->update($datos);
                } else {
                    //$datos[] = ['rotulo' => $rotulo];
                    
                    Device::create($datos);  
                }
            }
            else if(!is_null($serial) && is_null($rotulo)) {
                // si hay serial pero no rótulo

                // busca el dispositivo 
                $device = Device::where('serial', $serial)
                        ->whereNull('fecha_reversa')
                        ->orderBy('fecha_recepcion', 'asc')
                        ->first();
                // edita el dispositivo si existe
                if($device) {
                    $device->update($datos);
                } else {
                    //$datos[] = ['serial' => $serial];
                    
                    Device::create($datos);
                }
            } else if (!is_null($rotulo) && !is_null($serial)){
                // si están los dos datos
                $device = Device::where('serial', $serial)
                        ->whereNull('fecha_reversa')
                        ->where('rotulo', $rotulo)
                        ->orderBy('fecha_recepcion', 'asc')
                        ->first();
                if(!$device){
                    $device = Device::where('serial', $serial)
                        ->whereNull('fecha_reversa')
                        ->orderBy('fecha_recepcion', 'asc')
                        ->first();
                    //$datos[] = ['rotulo' => $rotulo];
                    if($device) {
                        $device->update($datos);
                    } else {
                      //  $datos[] = ['serial' => $serial];
                        Device::create($datos);
                    }
                } else {
                    $device->update($datos);
                }
            }
        }
    }
    
    public function startRow(): int {
        // desde dónde se comienza a leer la planilla
        return 2;
    }

}
