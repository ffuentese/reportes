<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Log;
use App\Device;
use App\Modelo;
use App\Brand;
use App\Guia;

class BulkRevImport implements ToCollection, WithChunkReading, 
        WithCalculatedFormulas, WithStartRow
{
    /**
     * Recibe la importación del informe consolidado de reversas
     * 
     * 
     * 
    * @param Collection $rows
    */
    public function collection(Collection $rows)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        $count = 0;
        foreach($rows as $row)
        {
            // si la primera palabra de esa celda no dice "POS" pasa a la sgte fila
            if ($row[0] == '') {
                break;
            }
            // limpia el serial
            $serial = preg_replace("/[^a-zA-Z0-9]/", "", $row[1]);
            $fecha_reversa = date("Y-m-d H:i:s", ($row[3] - 25569)*86400);
            $guia_reversa = $row[4] ?? 0;
            $tecnologia = $row[2];
            $obs = $row[9] ?? "";
            $proveedor = $row[10] ?? "";
            $devices = Device::where('serial', $serial)
                    ->whereNull('fecha_reversa')
                    ->where('fecha_recepcion', '<=', $fecha_reversa)
                    ->where(function ($query) use ($fecha_reversa) {
                        $query->where('fecha_instalacion', '<=', $fecha_reversa)
                              ->orWhereNull('fecha_instalacion');
                    })
                    ->orderBy('fecha_recepcion', 'asc')
                    ->get();
                    
            $datos = [
              'fecha_reversa' => $fecha_reversa,
              'guia_reversa' => $guia_reversa,
              'pos_id' => 0, 
              'obs' => $obs  
            ];
            
            $c = Device::where('serial', $serial)->whereNotNull('fecha_recepcion')->exists();
            $descripcion = $row[6];
            if (!$c && $devices->count() == 0) {
                
                $modelo = Modelo::where('name', $descripcion)->first();
                if (!$modelo) {
                    $modelo = $this->createModel($row);
                }
                $datos += ['model_id' => $modelo->id, 
                           'serial' => $serial];
                Device::create($datos);      
            } else {
                $device = $devices->first();
                if($device) {
                    $device->update($datos);
                }
            }
            
            $existe_guia = Guia::where('serial', $serial)->where('guia', $guia_reversa)->where('serial', '!=', 'ILEGIBLE')->exists();
            $datos_guia = [
                    'serial' => $serial,
                    'guia' => $guia_reversa,
                    'fecha' => $fecha_reversa,
                    'modelo' => $descripcion,
                    'tecnologia' => $tecnologia,
                    'estado' => 'REVERSA',
                    'obs' => $obs
                ];
            if (!$existe_guia) {
                
                Guia::create($datos_guia);
            }
            if(strtoupper($proveedor) == 'IBM'){
                $datos_guia['estado'] = 'REV';
                \App\GuiaIBM::create($datos_guia);
                
            }
            
        }
        $msg = "Hay " . $count . " entradas que no existen en la base";
        Log::info($msg);
        
    }
    
    public function createModel($row) 
    {
        $sap_desc = $row[6];
        $model_name = substr(strstr($sap_desc, " "), 1);
        $brand_name = strtok($model_name, " ");
        $marca = Brand::where('name', $brand_name)->first();
        $nuevoModelo = new Modelo;
        if($marca) {
            $nuevoModelo->brand_id = $marca->id;
            $nuevoModelo->name = $sap_desc;
            $nuevoModelo->save();
        } else {
            $nuevoModelo->brand_id = 'UNC';
            $nuevoModelo->name = $sap_desc;
            $nuevoModelo->save();
        }
                
        return $nuevoModelo; 
    }

    public function chunkSize(): int {
        return 2000;
    }

    public function startRow(): int {
        return 2;
    }

}
