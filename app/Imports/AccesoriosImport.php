<?php

namespace App\Imports;

use App\Accesorio;
use App\AccesorioTipo;
use App\Modelo;
use App\Brand;
use App\Customer;
use App\Provider;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

/**
 * Importa actas de elementos no seriados (que se administran por cantidad)
 * 
 * @author <ffuentese@entel.cl>
 */

class AccesoriosImport implements ToCollection, WithStartRow
{
    function __construct($provider) {
        $this->provider = $provider;
    }

    /**
    * @param Collection $rows
    *
    * @return 
    */
    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            \Log::info(print_r($row, true));
            if (!is_numeric($row[0])) {
                break;
            }

            $cantidad = $row[0];
            $tipo = $row[1];
            $modelo = $row[2] ?? null;
            $fecha = date("Y-m-d H:i:s", ($row[4] - 25569)*86400);
            $estado = $row[5];
            $guia = $row[6];
            $proyecto = $row[7] ?? "";
            $accesorio_tipo = AccesorioTipo::firstOrCreate(['name' => $tipo]);
            if ($modelo) {
                $modelo_db = Modelo::where('name', $modelo)->first();
            } else {
                continue;
            }
            // Si el modelo no está en la base lo crea
            if (!$modelo_db) {
                $marca = $row[3] ?? null;
                $marca_db = \App\Brand::where('name', $marca)->first();
                if (!$marca_db) {
                    $marca_db = \App\Brand::find('UNC');
                }
                $modelo_db = Modelo::create(['name' => $modelo, 'brand_id' => $marca_db->id]);
            }
            $customer = Customer::where('name', 'BEC')->first();
            $provider = Provider::where('name', $this->provider)->first();
            if (!$provider) {
                $provider = Provider::where('name', 'ENTEL')->first();
            }
            $datos = [
                // Inserta una entrada en la tabla con cantidades incluídas
                'cantidad' => $cantidad,
                'tipo_id' => $accesorio_tipo->id,
                'modelo_id' => $modelo_db->id,
                'customer_id' => $customer->id,
                'provider_id' => $provider->id,
                'guia' => $guia,
                'estado' => strtoupper($estado),
                'fecha' => $fecha,
                'obs' => $proyecto
            ];
            try {
                $existe_dato = Accesorio::where('estado', $estado)->where('guia', $guia)->where('modelo_id', $datos['modelo_id'])->where('cantidad', $cantidad)->exists();
                if(!$existe_dato){
                    Accesorio::create($datos);
                }
            } catch(\Exception $e) {
                \Log::info($e);
            }
        }
    }

    public function startRow(): int {
        return 2;
    }

}
