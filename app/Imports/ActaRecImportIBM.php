<?php

namespace App\Imports;

use App\Device;
use App\Modelo;
use App\Brand;
use App\Provider;
use App\GuiaIBM;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Log;


/**
     * Importa las planillas de reversa
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     * 
     * @param type $fecha
     * @param type $guia
     */

class ActaRecImportIBM implements ToCollection, WithStartRow, WithCalculatedFormulas
{
    
    function __construct($fecha, $guia) {
        $this->fecha = $fecha;
        $this->guia = $guia;
    }
       
    /**
    * Importa las planillas de reversa
    * 
    * @param Collection $rows
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            // para detectar fin de documento
            if($row[1] == ""){
                break;
            }
            $tecnologia = $row[2] ?? "";
            
            // Busca modelo en la base
            $modelo = Modelo::where('name', $row[3])->first();
            if (!$modelo) {
                // si el modelo no está lo crea
                $modelo = $this->createModel($row);
            }
            // limpia serial
            $serial = preg_replace("/[^a-zA-Z0-9]/", "", $row[1]);
            $str_ibm = \App\Service::where('name', 'IBM')->first();
            $dispositivo = Device::where('serial', $serial)
                    ->where('serial', 'NOT LIKE', '%ILEGIBLE%')
                    ->latest()->first();
            $provider = Provider::where('name', 'IBM')->first();

            $datos = [
                // datos a importar desde el acta
                    'provider_id' => $provider->id,
                    'location_id' => 'REV',
                    'str_id' => $str_ibm->id
            ];
            
            if ($dispositivo) {
                $dispositivo->update($datos);
            } 
            else {
                $datos += ['serial' => $serial,
                             'model_id' => $modelo->id
                    ];
                Device::create($datos);
            }
            
            $datos_guia = [
                'serial' => $serial,
                'guia' => $this->guia,
                'fecha' => $this->fecha,
                'modelo' => $modelo->name,
                'tecnologia' => $tecnologia,
                'estado' => 'REV'
            ];
            
            $dispEnGuia = GuiaIBM::where('guia', $this->guia)->where('serial', $serial)
                    ->where('serial', 'NOT LIKE', '%ILEGIBLE%')->first();
            if (!$dispEnGuia) {
                GuiaIBM::create($datos_guia);
            }
        }

    }
    
    /**
        * Crea un modelo si no existe.
        * 
        * @var $row Arreglo con datos de una fila del documento.
        * @return App\Modelo Modelo recién creado.
        */
    
     public function createModel($row) 
    {
        $marca = Brand::where('name', trim($row[4]))->first();
        $nuevoModelo = new Modelo;
        if($marca) {
            $nuevoModelo->brand_id = $marca->id;
            $nuevoModelo->name = $row[3];
            $nuevoModelo->save();
        }
        else {
            $nuevaMarca = new Brand;
            $nuevaMarca->name = $row[4];
            $nuevaMarca->save();
            $nuevoModelo->brand_id = $nuevaMarca->id;
            $nuevoModelo->name = $row[3];
            $nuevoModelo->save();
        }
        
        return $nuevoModelo; 
    }
    
    /**
     * Busca una cadena inicial en el inicio de otra más grande.
     * 
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    
    function startsWith($haystack, $needle) 
    {
    return $needle === ''
      || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    /**
     * En qué fila se comienza a leer los datos de la hoja de cálculo.
     * @return int
     */
    
    public function startRow(): int {
        return 8;
    }

}
