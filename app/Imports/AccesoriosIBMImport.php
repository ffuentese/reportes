<?php

namespace App\Imports;

use App\GuiaIBMAccesorio;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\AccesorioTipo;
use App\Modelo;

class AccesoriosIBMImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!is_numeric($row[0])) {
                return null;
        }
        
        $cantidad = $row[0];
        $tipo = $row[1];
        $modelo = $row[2] ?? null;
        $fecha = date("Y-m-d H:i:s", ($row[4] - 25569)*86400);
        $estado = $row[5];
        $guia = $row[6];
        $proyecto = $row[7] ?? "";
        $accesorio_tipo = AccesorioTipo::firstOrCreate(['name' => $tipo]);
        $modelo_db = null;
        if ($modelo) {
                $modelo_db = Modelo::where('name', $modelo)->first();
        } 
        // Si el modelo no está en la base lo crea
        if (!$modelo_db) {
                $marca = $row[3] ?? null;
                $marca_db = \App\Brand::where('name', $marca)->first();
                if (!$marca_db) {
                    $marca_db = \App\Brand::find('UNC');
                }
                $modelo_db = Modelo::create(['name' => $modelo, 'brand_id' => $marca_db->id]);        
        }
        $existe_dato = GuiaIBMAccesorio::where('estado', $estado)->where('guia', $guia)->where('modelo_id', $datos['modelo_id'])->where('cantidad', $cantidad)->exists();
        if($existe_dato){
            return null;
        }
        return new GuiaIBMAccesorio([
            //
            'cantidad' => $cantidad,
            'tipo_id' => $accesorio_tipo->id,
            'modelo_id' => $modelo_db->id,
            'guia' => $guia,
            'fecha' => $fecha,
            'estado' => $estado,
            'obs' => $proyecto
        ]);
    }

    public function startRow(): int {
        return 2;
    }

}
