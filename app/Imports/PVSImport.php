<?php

namespace App\Imports;

use App\Pv;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Illuminate\Contracts\Queue\ShouldQueue;
use \Datetime;

/**
 * Inserta en la base todos los datos de las planillas de puntos vigentes
 */

class PVSImport implements ToModel, WithBatchInserts, WithChunkReading,
        WithStartRow, ShouldQueue, WithCalculatedFormulas
{
    /**
     * Comienza la importación con la fecha del documento.
     * 
     * @param Datetime $date
     */
    function __construct(Datetime $date) {
       $this->date = $date;
   }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[28])){
            return null;
        }
        $serial = ltrim($row[28], '0');
        return new Pv([
            // Crea el objeto
            'pos_id' => $row[1],
            'estado' => $row[4],
            'fecha' => $this->date,
            'serial' => $serial,
            'marca' => $row[18] ?? "",
            'tecnologia' => $row[20] ?? "",
            'region' => $row[5],
            'comuna' => $row[6],
            'direccion' => $row[9] . ' ' . $row[10]
        ]);
    }

    public function batchSize(): int {
        return 2000;
    }

    public function chunkSize(): int {
        return 2000;
    }

    public function startRow(): int {
        return 2;
    }

}
