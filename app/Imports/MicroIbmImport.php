<?php

namespace App\Imports;

use App\MicroIbm;
use Maatwebsite\Excel\Concerns\ToModel;

class MicroIbmImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MicroIbm([
            //
        ]);
    }
}
