<?php

namespace App\Imports;

use App\Bginfo;
use App\Brand;
use App\Modelo;
use App\Zpec;
use App\Material;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

/**
 * Importa los datos de la planilla BGinfo
 * 
 * Tiene las siguientes columnas:
 * 
 * 0 = Time_Stamp
 * 1 = Machine_Domain
 * 2 = Host_name
 * 3 = Logon_Domain
 * 4 = User_Name
 * 5 = System_Type
 * 6 = OS_Version
 * 7 = Service_Pack
 * 8 = Arquitectura
 * 9 = Marca
 * 10 = Modelo
 * 11 = Serie
 * 12 = CPU
 * 13 = Memory
 * 14 = Volumes
 * 15 = Free_Space
 * 16 = Mac_Address
 * 17 = Network_Card
 * 18 = Network_Speed
 * 19 = Network_type
 * 20 = IP_Address
 * 21 = IP_Activa
 * 22 = Subnet_Mask
 * 23 = Default_Gateway
 * 24 = DHCP_Server
 * 25 = DNS_Server
 * 26 = Impresoras
 * 27 = IE_Version
 * 28 = McAfee
 * 29 = SCCM
 * 30 = Snow
 * 31 = Office
 * 32 = Logon_server
 * 33 = Hora
 * 34 = Zona_horaria
 * 25 = Boot_Time
 * 26 = Snapshot_Time
 */

class BginfoImport implements ToModel, WithStartRow, WithChunkReading, WithBatchInserts, WithMultipleSheets, ShouldQueue
{
    function __construct($fecha) {
        $this->fecha_carga = $fecha;
    }
    
    /**
     * Importa la hoja de cálculo de Bginfo
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public function model(array $row)
    {
        $marca = strtoupper($row[9]);
        $brand = Brand::where('name', $marca)->first();
        $fecha = date("Y-m-d H:i:s", ($row[0] - 25569) * 86400);
        $alias = $row[2];
        if ($this->startsWith($alias, 'TCR')) {
            return null;
        }
        $serie_zpec = Zpec::where('serie', $row[11])->orderBy('id', 'desc')->first();
        if ($serie_zpec) {
            $material_serie = \App\Material::where('material', $serie_zpec->material)->first();
        }
        if (isset($material_serie)) {
            $modelo = Modelo::firstOrCreate(['name' => $material_serie->descripcion, 'brand_id' => $material_serie->marca->id]);
        }
        if (!isset($modelo) && $brand) {
            $modelo = Modelo::firstOrCreate([
                'name' => $row[10],
                'brand_id' => $brand->id
            ]);
        } else if (!isset($modelo) && !$brand) {
            $brand = Brand::firstOrCreate(['id' => 'UNC', 'name' => 'DESCONOCIDO']);
            $modelo = Modelo::firstOrCreate(['name' => 'DESCONOCIDO', 'brand_id' => $brand->id]);
        }
        $bginfo = Bginfo::where('hostname', $row[2])
                ->where('username', $row[2])
                ->where('serie', $row[11])
                ->where('ip', $row[21])->exists();
        if ($bginfo) {
            return null;
        }
        
        // ignorar TCRs
        if ($this->startsWith($row[2], 'TCR')) {
            return null;
        }
        
        if(!$modelo) {
            $brand = Brand::firstOrCreate(['id' => 'UNC', 'name' => 'DESCONOCIDO']);
            $modelo = Modelo::firstOrCreate(['name' => 'DESCONOCIDO', 'brand_id' => $brand->id]);
        }
        
        
        return new Bginfo([
            // Ingresa nueva línea 
            'hostname' => $row[2],
            'username' => $row[4],
            'model_id' => $modelo->id,
            'serie' => $row[11],
            'ip' => $row[21],
            'fecha_conexion' => $fecha,
            'fecha_carga' => $this->fecha_carga
        ]);
    }

    public function batchSize(): int {
        return 1000;
    }

    public function chunkSize(): int {
        return 1000;
    }

    public function startRow(): int {
        return 2;
    }
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    public function sheets(): array {
        return [
            0 => $this
        ];
    }

}
