<?php

namespace App\Imports;

use App\Device;
use App\Modelo;
use App\Brand;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use \Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Guia;

/**
     * Importa los datos de las guías que recibe del controlador 
     * EntregaController@procesoPlanilla
     * 
     * 
     * @param Date $fecha
     * @param integer $guia
     * 
     * 
     */


class ActaImport implements ToCollection, WithStartRow, 
        WithCalculatedFormulas
{
    
    
    function __construct($fecha, $guia) {
        $this->fecha = $fecha;
        $this->guia = $guia;
    }

    
    
    /**
    * @param Collection $rows
    *
    * @return \Illuminate\Database\Eloquent\Collection|null
    */
    
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            if($row[1] == "Entregado por:"){
                break;
            }
            if ($row[1] == "") {
                break;
            }
            
            // busca el modelo
            $modelo = Modelo::where('name', $row[4])->first();
            $obs = $row[6] ?? "";
            if (!$modelo) {
                // si no está lo crea
                $modelo = $this->createModel($row);
            }
            // limpia el serial en caso de que tenga caracteres incorrectos
            $serial = preg_replace("/[^a-zA-Z0-9]/", "", $row[2]); 
            // busca el dispositivo a partir del serial (aqui no contamos con rótulo)
            $dispositivo = Device::where('serial', $serial)
                    ->where('guia_recepcion', '=', $this->guia)
                    ->first();
            
            $datos = [
                // importa el acta
                    'serial' => $serial,
                    'model_id' => $modelo->id,
                    'fecha_recepcion' => $this->fecha,
                    'guia_recepcion'  => $this->guia,
                    'customer_id' => 1, // datos genéricos
                    'str_id' => 1, // datos genéricos
                    'provider_id' => 1
                ];
            if (!$dispositivo) {
                // de no existir un equipo lo crea
                Device::create($datos);
                $datos_guia = [
                    'serial' => $serial,
                    'guia' => $this->guia,
                    'fecha' => $this->fecha,
                    'estado' => 'ENTREGA',
                    'obs' => $obs
                ];
                $existe_guia = Guia::where('serial', $serial)->where('guia', $this->guia)->exists();
                if (!$existe_guia) {
                    Guia::create($datos_guia);
                }
            } 
        }   
    }
      
    /**
         * Crea un modelo si no existe.
         * 
         * @var $row Arreglo con datos de una fila del documento.
         * @return App\Modelo Modelo recién creado.
         */
    
    public function createModel($row) 
    {
        
        // crea modelo en sistema en caso de no existir en la base
        $marca = Brand::where('name', $row[5])->first();
        $nuevoModelo = new Modelo;
        if($marca) {
            $nuevoModelo->brand_id = $marca->id;
            $nuevoModelo->name = $row[4];
            $nuevoModelo->save();
        }
        else {
            // si no existe la marca tiene que crearla
            $nuevaMarca = new Brand;
            $nuevaMarca->name = $row[5];
            $nuevaMarca->save();
            $nuevoModelo->brand_id = $nuevaMarca->id;
            $nuevoModelo->name = $row[4];
            $nuevoModelo->save();
        }
        
        return $nuevoModelo; 
    }
    
    public function startsWith($haystack, $needle) 
    {
    return $needle === ''
      || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }
    
    public function startRow(): int
    {
        return 11;
    }

   

}
