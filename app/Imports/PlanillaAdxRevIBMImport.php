<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class PlanillaAdxRevIBMImport implements WithMultipleSheets, SkipsUnknownSheets
{
    function __construct($fecha, $guia) {
        $this->fecha = $fecha;
        $this->guia = $guia;
    }
    
    public function onUnknownSheet($sheetName) {
        // E.g. you can log that a sheet was not found.
        info("Sheet {$sheetName} was skipped");
    }

    public function sheets(): array {
        return [
            0 => new ActaRecImportIBM($this->fecha, $this->guia)
        ];
    }
}
