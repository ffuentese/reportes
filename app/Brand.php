<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    // Este modelo identifica a las marcas 
    
    /* Utilizamos los mismos códigos de marcas de SAP, así que el id es una string
     * Por eso no utiliza el incremento
     */
    protected $guarded = [''];
    public $incrementing = false;
    
}
