<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    
   // Modelo que administra permisos individuales
    
    public function roles() {
        return $this->belongsToMany(Role::class,'roles_permissions');
    }
}
