<?php

namespace App\Http\Controllers;

use App\Guia;
use App\Modelo;
use Illuminate\Http\Request;
use Carbon\Carbon;


class GuiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra el formulario que permite ingresar guías manualmente
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $modelos = Modelo::where('name', 'LIKE', 'POS VERIFONE%')
                ->orWhere('name', 'LIKE', 'POS CASTLES%')->get();
        return view('guias.add')->with('modelos', $modelos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'guia' => 'required|numeric',
            'fecha' => 'required|date',
            'serial' => 'required|array',
            'serial.*' => 'required|string'
        ]);
        $size = sizeof($request->serial);
        
        for ($i = 0; $i < $size; $i++) {
        
            $guia = $request->guia;
            $fecha = Carbon::parse($request->fecha);
            $estado = $request->tipo; //ENTREGA o REVERSA
            $serial = $request->serial[$i];
            $tecnologia = $request->tecnologia[$i];
            $modelo = $request->modelo[$i];
            $obs = $request->proyecto[$i] === 'SI' ? 'Proyecto' : '';
            Guia::create([
                'guia' => $guia,
                'fecha' => $fecha,
                'serial' => $serial,
                'modelo' => $modelo,
                'tecnologia' => $tecnologia,
                'obs' => $obs,
                'estado' => $estado
            ]);
            if($fecha > '2019-06-01') {
                    GuiaIBM::create([
                    'guia' => $guia,
                    'fecha' => $fecha,
                    'serial' => $serial,
                    'modelo' => $modelo,
                    'tecnologia' => $tecnologia,
                    'obs' => $obs,
                    'estado' => $estado
                ]);
            }
                
        }
        if ($estado == 'ENTREGA') {
            return redirect()->back()->with('success', 'La guía se guardó exitosamente: ' . '<a href="' . route('guia_recepcion', ['guia' => $guia]) . '">Ver guía ' . $guia . '</a>');
        } else if ($estado == 'REVERSA') {
            return redirect()->back()->with('success', 'La guía se guardó exitosamente: ' . '<a href="' . route('guia_reversa', ['guia' => $guia]) . '">Ver guía ' . $guia . '</a>');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function show(Guia $guia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function edit(Guia $guia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guia $guia)
    {
        //
    }

    /**
     * Elimina la guía de la base de datos
     * Esta función elimina todos los elementos que coincidan con número de guía y estado (ENTREGA o REVERSA)
     *
     * @param  \App\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $guia, string $estado)
    {
        //
        $elementosParaBorrar = Guia::where('guia', $guia)->where('estado', $estado);
        if($elementosParaBorrar->delete()) {
            return response(200)->json(['success' => 'Guía eliminada.']);
        }
        
        return response(400)->json(['error' => 'Guía no eliminada.']);
    }
}
