<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\OTDigital;

/*
     * Permite buscar el dispositivo por serial o por rótulo
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     */

class SearchController extends Controller
{
    
    
    /**
         * Muestra la ventana de búsqueda con sus formularios. 
         * 
         * @return view Vista con formularios.
         */
    
    public function index()
    {
        
        return view('busqueda.index');
    }
    
    /**
     * Procesa el serial entregado desde el formulario y devuelve la información.
     * 
     * @var Request $request petición del servidor con el número de serie.
     * @return view Vista con resultados de la búsqueda. 
     */
    
    public function serial(Request $request)
    {
       
        $inp_serial = $request->input('serial');
        $devices = Device::where('serial', $inp_serial)->orderBy('fecha_recepcion', 'asc')->get();
        $otdigital = OTDigital::where('serie1', $inp_serial)->orWhere('serie4', $inp_serial)->get();
        $ult_fecha = OTDigital::max('fecha_atencion');
        if ($ult_fecha > 0) {
        $ultima_atencion = date('d/m/Y', strtotime($ult_fecha));
        }
        $data = [
            'devices' => $devices,
            'input_busqueda' => $inp_serial,
            'otdigital' => $otdigital,
            'ultima_atencion' => $ultima_atencion];
        return view('busqueda.result', $data);
    }
    
    /**
         * Procesa el serial entregado desde el formulario y devuelve la información.
         * 
         * @var Request $request 
         * @return view Vista con resultados de la búsqueda. 
         * 
         */
    
    public function rotulo(Request $request)
    {
        
        $inp_rotulo = $request->input('rotulo');
        $devices = Device::where('rotulo', $inp_rotulo)->get();
        $otdigital = OTDigital::where('rotulo1', $inp_rotulo)->get();
        $ult_fecha = OTDigital::max('fecha_atencion');
        if ($ult_fecha > 0) {
        $ultima_atencion = date('d/m/Y', strtotime($ult_fecha));
        }
        $data = [
            'devices' => $devices,
            'input_busqueda' => $inp_rotulo,
            'otdigital' => $otdigital,
            'ultima_atencion' => $ultima_atencion];
        return view('busqueda.result', $data);
    }
    
    
}
