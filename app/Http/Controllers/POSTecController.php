<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Asocia manualmente un modelo de POS con una tecnología como 3G o GPRS
 * @author <ffuentese@entel.cl>
 */

class POSTecController extends Controller
{
    //
    public function index() {
        $techpos = \App\TechPos::all();
        $modelos = \App\Modelo::where('brand_id', 'VEF')->orWhere('brand_id', 'CAS')->get(); 
        $data = ['techpos' => $techpos,
            'modelos' => $modelos];
        return view('postec.index', $data);
    }
    
    public function store(Request $request) {
        $request->validate([
            'model_id' => 'required|numeric',
            'tech' => 'required|in:["GPRS", "3G", "IF"]'
        ]);
        \App\TechPos::create($request->validated());
        return redirect()->back()->with('success', 'Archivo subido exitosamente');
    }
}
