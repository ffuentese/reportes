<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Guia;

class VistasCuadroIBMController extends Controller
{
    /**
     * Retorna los puntos vigentes en el documento del banco más reciente subido al sistema.
     * Y que corresponde a la tecnología 3G
     * @return vista
     */
    public function puntosVigentes3g() {
        $pvs = DB::table('last_pvs')
                ->where('estado', '=', 'VIGENTE')
                ->where('tecnologia', '=', '3G')
                ->OrWhere('tecnologia', '=', '3GB')
                ->paginate();
        $data = ['pvs' => $pvs];
        return view('cuadro_panel.pvs', $data);
    }
    
     /**
     * Retorna los puntos vigentes en el documento del banco más reciente subido al sistema.
     * Y que corresponde a la tecnología GPRS
     * @return vista
     */
    public function puntosVigentesGprs() {
        $pvs = DB::table('last_pvs')
                ->where('estado', '=', 'VIGENTE')
                ->where('tecnologia', '=', 'EGPRS')
                ->paginate();
        $data = ['pvs' => $pvs];
        return view('cuadro_panel.pvs', $data);
    }
    
    /**
     * Retorna los Puntos Vigentes que utilizan transformadores Verifone
     * @return Vista
     */
    public function puntosVigentesTrafoVerifone() {
        $pvs = DB::table('last_pvs')
                ->where('estado', '=', 'VIGENTE')
                ->where('marca', '=', 'VERIFONE')
                ->paginate();
        $data = ['pvs' => $pvs];
        return view('cuadro_panel.pvs', $data);
        
    }
    
    /**
     * Retorna los Puntos Vigentes que utilizan transformadores Castles
     * @return Vista
     */
    public function puntosVigentesTrafoCastles() {
        $pvs = DB::table('last_pvs')
                ->where('estado', '=', 'VIGENTE')
                ->where('marca', '=', 'CASTLES')
                ->paginate();
        $data = ['pvs' => $pvs];
        return view('cuadro_panel.pvs', $data);
    }
    
    /**
     * Retorna todas las directas que corresponden a un dispositivo 3G.
     * @return Vista
     */
    public function entregas3g() {
        $entregas = Guia::where('estado', 'ENTREGA')
                ->where('tecnologia', '3G')
                ->paginate();
        $label = 'Entregas 3G';
        $data = ['datos' => $entregas,
            'label' => $label];
        return view('cuadro_panel.flujo_seriado', $data);
    }
    
    /**
     * Retorna todas las directas que corresponden a un dispositivo GPRS.
     * @return Vista
     */
    public function entregasGprs() {
        $entregas = Guia::where('estado', 'ENTREGA')
                ->where('tecnologia', 'GPRS')
                ->paginate();
        $label = 'Entregas GPRS';
        $data = ['datos' => $entregas,
            'label' => $label];
        return view('cuadro_panel.flujo_seriado', $data);
    }
    
    /**
     * Retorna todas las directas de accesorios que corresponden a trafos Verifone.
     * @return Vista
     */
    public function entregasVef() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'entrega')
                ->where('models.brand_id', 'VEF')
                ->paginate();
        $label = 'Entregas Transformadores Verifone';
        $data = ['accesorios' => $entregas,
                'label' => $label];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    /**
     * Retorna todas las directas de accesorios que corresponden a trafos Castles.
     * @return Vista
     */
    public function entregasCas() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'entrega')
                ->where('models.brand_id', 'CAS')
                ->paginate();
        $label = 'Entregas Transformadores Castles';
        $data = ['accesorios' => $entregas,
                'label' => $label];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    /**
     * Retorna todas las directas que están asociadas a proyectos.
     * @return Vista
     */
    
    public function entregasProyectos3G() {
        $entregas  = Guia::where('obs', '!=', '')->where('estado', 'ENTREGA')
                ->where('tecnologia', '3G')
                ->paginate();
        $label = 'Entregas relacionadas con proyectos 3G.';
        $data = ['datos' => $entregas,
            'label' => $label];
        return view('cuadro_panel.flujo_seriado', $data);
    }
    
    public function entregasProyectosGprs() {
        $entregas  = Guia::where('obs', '!=', '')->where('estado', 'ENTREGA')
                ->where('tecnologia', 'GPRS')
                ->paginate();
        $label = 'Entregas relacionadas con proyectos GPRS.';
        $data = ['datos' => $entregas,
            'label' => $label];
        return view('cuadro_panel.flujo_seriado', $data);
    }
    
    public function entregasProyectosVef() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('obs', '!=', '')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'entrega')
                ->where('models.brand_id', 'VEF')
                ->paginate();
        $label = 'Entregas Transformadores Verifone para proyectos';
        $data = ['accesorios' => $entregas,
                'label' => $label];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    public function entregasProyectosCas() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('obs', '!=', '')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'entrega')
                ->where('models.brand_id', 'CAS')
                ->paginate();
        $label = 'Entregas Transformadores Castles para proyectos';
        $data = ['accesorios' => $entregas,
                'label' => $label];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    

    
    /**
     * Retorna todas las reversas que corresponden a un dispositivo 3G.
     * @return Vista
     */
    public function reversas3g() {
        $reversas_3g = Guia::where('estado', 'REVERSA')
                ->where('tecnologia', '3G')
                ->where('obs', '=', '')
                ->paginate();
        $label = 'Reversa 3G';
        $data = ['datos' => $reversas_3g,
                'label' => $label];
        return view('cuadro_panel.flujo_seriado', $data);
    }
    
    /**
     * Retorna todas las reversas que corresponden a un dispositivo GPRS.
     * @return Vista
     */
    public function reversasGprs() {
        $reversas_gprs = Guia::where('estado', 'REVERSA')
                ->where('tecnologia', 'GPRS')
                ->where('obs', '=', '')
                ->paginate();
        $label = 'Reversa GPRS';
        $data = ['datos' => $reversas_gprs,
                'label' => $label];
        return view('cuadro_panel.flujo_seriado', $data);
    }
    
    /**
     * Retorna todas las reversas que corresponden a un trafo Verifone (en Accesorios).
     * @return Vista
     */
    public function reversasVef() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $reversas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'reversa')
                ->where('models.brand_id', 'VEF')
                ->paginate();
        $label = 'Reversa transformadores Verifone';
        $data = ['accesorios' => $reversas,
                'label' => $label];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    /**
     * Retorna todas las reversas que corresponden a un trafo Castles (en Accesorios).
     * @return Vista
     */
    public function reversasCas() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $reversas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'reversa')
                ->where('models.brand_id', 'CAS')
                ->paginate();
        $label = 'Reversa transformadores Castles';
        $data = ['accesorios' => $reversas,
                'label' => $label];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    // Cuadro IBM
    
    /**
     * Retorna los dispositivos que corresponden a un equipo 3G y están en una directa IBM
     * @return Vista
     */
    public function stockPOSIBM3G() {
        $entregas = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->where('tecnologia', '3G')
                ->paginate();
        $label = 'Entrega IBM POS 3G';
        $data = [
            'datos' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_seriado_ibm', $data);
    }
    
    /**
     * Retorna los dispositivos que corresponden a un equipo GPRS y están en una directa IBM
     * @return Vista
     */
    public function stockPOSIBMGPRS() {
        $entregas = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->where('tecnologia', 'GPRS')
                ->paginate();
        $label = 'Entrega IBM POS GPRS';
        $data = [
            'datos' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_seriado_ibm', $data);
    }
    
    /**
     * Retorna los dispositivos que corresponden a un trafo Verifone y están en una directa IBM
     * @return Vista
     */
    public function stockTrafoIBMVerifone() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'VEF')
                ->where('estado', 'Entrega')
                ->where('tipo_id', $tipo_trafo->id)
                ->paginate();
        $label = 'Entrega IBM Trafos Verifone';
        $data = [
            'entregas' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    /**
     * Retorna los dispositivos que corresponden a un trafo Castles y están en una directa IBM
     * @return Vista
     */
    public function stockTrafoIBMCastles() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'CAS')
                ->where('estado', 'Entrega')
                ->where('tipo_id', $tipo_trafo->id)
                ->paginate();
        $label = 'Entrega IBM Trafos Castles';
        $data = [
            'entregas' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    //
    /**
     * Retorna los dispositivos que corresponden a un equipo 3G y están en una reversa IBM
     * @return Vista
     */
    public function revPOSIBM3G() {
        $entregas = DB::table('guia_i_b_ms')
                ->where('estado', 'REV')
                ->where('tecnologia', '3G')
                ->paginate();
        $label = 'Entrega IBM POS 3G';
        $data = [
            'datos' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_seriado_ibm', $data);
    }
    
    /**
     * Retorna los dispositivos que corresponden a un equipo GPRS y están en una reversa IBM
     * @return Vista
     */
    public function revPOSIBMGPRS() {
        $entregas = DB::table('guia_i_b_ms')
                ->where('estado', 'REV')
                ->where('tecnologia', 'GPRS')
                ->paginate();
        $label = 'Entrega IBM POS 3G';
        $data = [
            'datos' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_seriado_ibm', $data);
    }
    
    /**
     * Retorna los dispositivos que corresponden a un trafo Verifone y están en una reversa IBM
     * @return Vista
     */
    public function revTrafoIBMVerifone() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'VEF')
                ->where('estado', 'Entrega')
                ->where('tipo_id', $tipo_trafo->id)
                ->paginate();
        $label = 'Entrega IBM Trafos Verifone';
        $data = [
            'entregas' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    /**
     * Retorna los dispositivos que corresponden a un trafo Castles y están en una reversa IBM
     * @return Vista
     */
    public function revTrafoIBMCastles() {
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'CAS')
                ->where('estado', 'Entrega')
                ->where('tipo_id', $tipo_trafo->id)
                ->paginate();
        $label = 'Entrega IBM Trafos Castles';
        $data = [
            'entregas' => $entregas,
            'label' => $label
        ];
        return view('cuadro_panel.flujo_accesorios', $data);
    }
    
    
}
