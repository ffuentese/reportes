<?php

namespace App\Http\Controllers;

use App\Micro;
use Illuminate\Http\Request;

class MicroBecController extends Controller
{
    /**
     * Despliega una lista de artículos de microinformática
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $micros = Micro::all();
        return view('micro.index')->with('micros', $micros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {       
            $errors = new \Illuminate\Support\MessageBag();
            if (!$request->import_file) {
                $error = 'No se subieron archivos.';
                $errors->add('no_files', $error);
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'micro']);
            }
            $files = $request->import_file;
            
            // Contamos los archivos del arreglo
            $file_count = count($files);

            // contamos cuántos archivos realmente se subieron al sistema
            
            $validate_files = $this->validaActa($files, $errors);
            $uploadcount = $validate_files['uploadcount'];
            
            if($uploadcount == $file_count){
                //se subió todo satisfactoriamente
                return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'micro']);
            }
            else {
                //ocurrieron errores.
                $errors = $validate_files['errors'];
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'micro']);
            }
    }
    
    public function validaActa($files, $errors)
    {
        $uploadcount = 0;
        foreach($files as $file) {
            $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('import_file'=> $file), $rules);
                if($validator->passes()){
                    $destinationPath = 'micro_bec';
                    $filename = $file->getClientOriginalName();
                    if($this->nombreDeArchivoValido($filename)) {
                        $file->storeAs($destinationPath, $filename);
                        $uploadcount ++;
                    } else {
                        $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta de microinformática.';
                        $errors->add($filename, $error);
                    }
                } else {
                     $errors = $validator->errors();
                }
        }
        return $validate_files = [
            'validator' => $validator,
            'errors' => $errors,
            'uploadcount' => $uploadcount];
    }
    


    
    /**
     * Valida que el nombre de archivo subido corresponda al formato:
     * 
     * 101 Acta Entrega Micro 30-10-2018 482978.xlsx
     * El tercer elemento debe ser la palabra "micro" y el cuarto una fecha.
     * @param type $filename
     * @return boolean
     */
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 6){
            return false;
        }
        if(strcasecmp($arr[3], 'Micro') != 0 ) {
            return false;
        }
        if(!$this->validateDate($arr[4])) {
            return false;
        }
        
        return true;   
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function show(Micro $micro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function edit(Micro $micro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Micro $micro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Micro $micro)
    {
        //
    }
}
