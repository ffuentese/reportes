<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\POSImport;
use Excel;
use Carbon\Carbon;
use Validator;

class POSCheckController extends Controller
{
    /*
     * Este controlador revisa los puntos vigentes del equipo. 
     * 
     * @author <ffuentese@entel.cl>  
     */
    
    function __construct() {
        
        $this->middleware('auth');

    }
    
    /**
         * Recibe las planillas validando la fecha
         * El trabajo pesado lo recibe POSImport que recibe como parámetro la fecha del documento.
         * 
         * @var Request $request Recibe la petición del servidor con el formulario.
         * 
         * @return view Vuelve a la vista de carga.
         * 
         */
    
    public function procesoPlanilla(Request $request)
    {       
            $errors = new \Illuminate\Support\MessageBag();
            if (!$request->import_file) {
                $error = 'No se subieron archivos.';
                $errors->add('no_files', $error);
                return redirect('carga')->withErrors($errors);
            }
            $files = $request->import_file;
            
            // Making counting of uploaded images
            $file_count = count($files);

            // start count how many uploaded
            $uploadcount = 0;
            
            foreach($files as $file) {
                $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('import_file'=> $file), $rules);
                    if($validator->passes()){
                        $destinationPath = 'pv';
                            $filename = $file->getClientOriginalName();
                            if($this->nombreDeArchivoValido($filename)) {
                                $upload_success = $file->storeAs($destinationPath, $filename);
                                $uploadcount ++;
                            } else {
                                $error = 'El archivo ' . $filename . ' tiene un nombre no válido para puntos vigentes.';
                                $errors->add($filename, $error);
                            }
                    }
            }

            if($uploadcount == $file_count){
                //uploaded successfully
                return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'puntos_vigentes']);
            }
            else {
                //error occurred
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'puntos_vigentes']);
            }
    }
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 3){
            return false;
        }
        if(strcasecmp($arr[0], 'Puntos') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[1], 'Vigentes') != 0 ) {
            return false;
        }
        $date = substr($arr[2], 0, -5);
        if(!$this->validateDate($date)) {
            return false;
        }
        
        return true;
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
    
}
