<?php

namespace App\Http\Controllers;

use App\GuiaIBMAccesorio;
use App\Imports\AccesoriosIBMImport;
use Illuminate\Http\Request;
use Validator;
use Excel;

class AccesorioIBMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $errors = new \Illuminate\Support\MessageBag();
        if (!$request->import_file) {
            $error = 'No se subieron archivos.';
            $errors->add('no_files', $error);
            return redirect('carga')->withErrors($errors);
        }
        $files = $request->import_file;

        // Contamos los archivos del arreglo
        $file_count = count($files);

        // contamos cuántos archivos realmente se subieron al sistema

        $validate_files = $this->validateFiles($files, $errors);
        $uploadcount = $validate_files['uploadcount'];

        if($uploadcount == $file_count){
            //se subió todo satisfactoriamente
            return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'accesoriosibm']);
        }
        else {
            //ocurrieron errores.
            $errors = $validate_files['errors'];
            return redirect('carga')->withErrors($errors)->withInput(['tab' => 'accesoriosibm']);
        }
    }
    
    /**
     * Valida que cada archivo sea un XLSX y que además tenga el formato correcto
     * 
     * @param string $files
     * @param MessageBag $errors
     * @return Array $validate_files
     */
    
    public function validateFiles($files, $errors) {
        $uploadcount = 0;
        foreach($files as $file) {
            $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('import_file'=> $file), $rules);
                if($validator->passes()){
                    $destinationPath = 'accesoriosibm';
                    $filename = $file->getClientOriginalName();
                    if($this->nombreDeArchivoValido($filename)) {
                        $file->storeAs($destinationPath, $filename);
                        Excel::import(new AccesoriosIBMImport, $file);
                        $uploadcount++;
                    } else {
                        $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta.';
                        $errors->add($filename, $error);
                    }
                } else {
                     $errors = $validator->errors();
                }
        }
        return $validate_files = [
            'validator' => $validator,
            'errors' => $errors,
            'uploadcount' => $uploadcount];
    }
    
    /**
     * Valida que el nombre de archivo subido corresponda al formato:
     * 105_Acta_Entrega_ACCESORIOS_CO_IBM_30-10-2018.xlsx
     * 97_Acta_Reversa_ACCESORIOS_CO_IBM_30-10-2018.xlsx
     * @param type $filename
     * @return boolean
     */
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode('_', $filename);
        if(!is_numeric($arr[0])){
            \Log::warning('Correlativo incorrecto no numérico: ' . $arr[0]);
            return false;
        }
        if(strcasecmp($arr[1], 'Acta') != 0 ) {
            \Log::warning('Acta no aparece: ' . $arr[1]);
            return false;
        }
        if(strcasecmp($arr[2], 'Entrega') == 0 || strcasecmp($arr[2], 'Reversa') == 0) {
            \Log::warning('Campo de estado incorrecto: '. $arr[2]);
            return true;
        } else {
            return false;
        }
        // Comprueba que haya una fecha
        if(!$this->validateDate($arr[6])) {
            \Log::warning('Fecha incorrecta: ' . $arr[6]);
            return false;
        }
        return true;
        
    }
    
      public function validateDate($date_str) {
        try {
            Carbon::parse($date_str);
            return true;
        }
        catch (\Exception $e) {
            return false;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GuiaIBMAccesorio  $guiaIBMAccesorio
     * @return \Illuminate\Http\Response
     */
    public function show(GuiaIBMAccesorio $guiaIBMAccesorio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GuiaIBMAccesorio  $guiaIBMAccesorio
     * @return \Illuminate\Http\Response
     */
    public function edit(GuiaIBMAccesorio $guiaIBMAccesorio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GuiaIBMAccesorio  $guiaIBMAccesorio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GuiaIBMAccesorio $guiaIBMAccesorio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GuiaIBMAccesorio  $guiaIBMAccesorio
     * @return \Illuminate\Http\Response
     */
    public function destroy(GuiaIBMAccesorio $guiaIBMAccesorio)
    {
        //
    }
}
