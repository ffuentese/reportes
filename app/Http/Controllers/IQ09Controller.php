<?php

namespace App\Http\Controllers;

use App\Iq09;
use App\Imports\IQ09Import;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Excel;

class IQ09Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ultima_act = IQ09::max('created_at');
        return view('bodega.index')->with('ultima_act', $ultima_act);
    }
    
    /**
     * $row[0] = Equipo (no se usa)
     * $row[1] = Material ($device->code)
     * $row[2] = Rótulo ($device->rotulo)
     * $row[3] = Serial ($device->serial)
     * $row[4] = Centro de costos ($device->cost_center)
     * $row[5] = Ubicación ($device->location_id)
     * $row[6] = Status ($device->status_id)
     * $row[7] = Descripción ($device->modelo)
     * $row[8] = Modificación 
     * $row[9] = Usuario que modifica
     * $row[10] = Status sistema (ALMA)
     * $row[11] = Fecha de creación en sistema
     * $row[12] = PEP
     */
    
    public function getDatatable() {
        $rows = Iq09::select('id', 'material', 'rotulo', 'serie', 'almacen', 'tipo_stock', 'nombre', 'modificado', 'usuario', 'pep', 'creado');
        return Datatables::of($rows)
        ->editColumn('modificado', function ($row) {
           return [
              'display' => e($row->modificado->format('d/m/Y')),
              'timestamp' => $row->modificado->timestamp
           ];
        })
        ->editColumn('creado', function ($row) {
           return [
              'display' => e($row->creado->format('d/m/Y')),
              'timestamp' => $row->creado->timestamp
           ];
        })
        ->filterColumn('modificado', function ($query, $keyword) {
           $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') LIKE ?", ["%$keyword%"]);
        })
        ->filterColumn('creado', function ($query, $keyword) {
           $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') LIKE ?", ["%$keyword%"]);
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        return view('bodega.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $request->validate([
            'import_file' => 'required|mimes:xlsx'
        ]);
        
        $path = $request->file('import_file')->getRealPath();
        Iq09::truncate();
        Excel::import(new IQ09Import, $path);
        
        return redirect()->back()->with('success', 'Archivo subido exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Iq09  $iq09
     * @return \Illuminate\Http\Response
     */
    public function show(Iq09 $iq09)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Iq09  $iq09
     * @return \Illuminate\Http\Response
     */
    public function edit(Iq09 $iq09)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Iq09  $iq09
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Iq09 $iq09)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Iq09  $iq09
     * @return \Illuminate\Http\Response
     */
    public function destroy(Iq09 $iq09)
    {
        //
    }
}
