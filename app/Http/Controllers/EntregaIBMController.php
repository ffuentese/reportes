<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\EntregaExport;
use App\Device;
use App\Imports\BulkImport;
use Validator;
use App\GuiaIBM;

class EntregaIBMController extends Controller
{
    /**
     * Controlador de "/carga" desde donde se suben todos los formularios 
     * y de proceso de carga de planilla de actas de entrega (procesoPlanilla).
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     */
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    /**
         * Controlador de "/carga"
         * 
         * Muestra el contenido de la página, incluyendo el listado de dispositivos.
         * 
         * @return view
         */
    public function index()
    {
        
        $devices = Device::paginate(15);
        return view('entrega.index')->with('devices', $devices);
    }
    
     /**
         * Procesa la planilla de cargas de actas de entrega
         * 
         * @var $request formulario de carga de hoja Excel además de fecha y n° de guía.
         * 
         * @return \Illuminate\Http\RedirectResponse view
         */
    
    public function procesoPlanilla(Request $request)
    {       
            $errors = new \Illuminate\Support\MessageBag();
            if (!$request->import_file) {
                $error = 'No se subieron archivos.';
                $errors->add('no_files', $error);
                return redirect('carga')->withErrors($errors);
            }
            $files = $request->import_file;
            
            // Contamos los archivos del arreglo
            $file_count = count($files);

            // contamos cuántos archivos realmente se subieron al sistema
            
            $validate_files = $this->validaActa($files, $errors);
            $uploadcount = $validate_files['uploadcount'];
            
            if($uploadcount == $file_count){
                //se subió todo satisfactoriamente
                return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'entrega-ibm']);
            }
            else {
                //ocurrieron errores.
                $errors = $validate_files['errors'];
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'entrega-ibm']);
            }
    }
    
    public function validaActa($files, $errors)
    {
        $uploadcount = 0;
        foreach($files as $file) {
            $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('import_file'=> $file), $rules);
                if($validator->passes()){
                    $destinationPath = 'directas_ibm';
                    $filename = $file->getClientOriginalName();
                    if($this->nombreDeArchivoValido($filename)) {
                        $file->storeAs($destinationPath, $filename);
                        $uploadcount ++;
                    } else {
                        $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta de entrega.';
                        $errors->add($filename, $error);
                    }
                } else {
                     $errors = $validator->errors();
                }
        }
        return $validate_files = [
            'validator' => $validator,
            'errors' => $errors,
            'uploadcount' => $uploadcount];
    }
    


    
    /**
     * Valida que el nombre de archivo subido corresponda al formato:
     * [Correlativo] [Acta Entrega IBM] [Fecha en formato dd-mm-yyyy] [n° guía].[xlsx]
     * 105 Acta Entrega IBM 30-10-2018 482978.xlsx
     * @param type $filename
     * @return boolean
     */
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 6){
            return false;
        }
        if(!is_numeric($arr[0])){
            return false;
        }
        if(strcasecmp($arr[1], 'Acta') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[2], 'Entrega') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[3], 'IBM') != 0 ) {
            return false;
        }
        if(!$this->validateDate($arr[4])) {
            return false;
        }
        if(!is_numeric(substr($arr[5], 0, -5))) {
            return false;
        }
        
        
        return true;
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

}
