<?php

namespace App\Http\Controllers;

use App\Material;
use App\Imports\MaterialImport;
use Illuminate\Http\Request;
use Excel;
use Yajra\DataTables\Facades\DataTables;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Ver datos en un formulario
        return view('material.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('material.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Carga el contenido de la planilla en sistema
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        
        $request->validate([
            'import_file' => 'required|mimes:xlsx'
        ]);
        
        $path = $request->file('import_file')->getRealPath();
        Material::truncate();
        Excel::import(new MaterialImport, $path);
        
        return redirect()->back()->with('success', 'Archivo subido exitosamente');
    }
    
    public function getDatatable() {
        $rows = Material::select('id', 'material', 'descripcion', 'perfil', 'tipo', 'subtipo');
        return Datatables::of($rows)->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function show(Material $material)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material $material)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {
        //
    }
}
