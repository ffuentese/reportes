<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\ActaRecImport;
use App\Imports\PlanillaAdxRevImport;
use Carbon\Carbon;
use App\Imports\BulkRevImport;
use Validator;


class ReversaController extends Controller
{
    /**
     * Recibe las actas de reversa, tanto individual (por guía) como consolidada
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     */
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    
    public function procesoPlanilla(Request $request)
    {       
            $errors = new \Illuminate\Support\MessageBag();
            if (!$request->import_file) {
                $error = 'No se subieron archivos.';
                $errors->add('no_files', $error);
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'reversa']);
            }
            $files = $request->import_file;
            
            // Making counting of uploaded images
            $file_count = count($files);

            // start count how many uploaded
            $uploadcount = 0;
            
            foreach($files as $file) {
                $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('import_file'=> $file), $rules);
                    if($validator->passes()){
                        $destinationPath = 'reversas';
                            $filename = $file->getClientOriginalName();
                            if($this->nombreDeArchivoValido($filename)) {
                                $upload_success = $file->storeAs($destinationPath, $filename);
                                $uploadcount ++;
                            } else {
                                $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta de reversa.';
                                $errors->add($filename, $error);
                            }
                    } else {
                        $errors = $validator->errors();
                    }
            }

            if($uploadcount == $file_count){
                //uploaded successfully
                return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'reversa']);
            }
            else {
                //error occurred
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'reversa']);
            }
    }
    
    /**
     * Valida que el nombre de archivo subido corresponda al formato:
     * [Correlativo] [Acta Entrega POS ADEXUS] [fecha en formato dd-mm-yyy] [N° de guía].[extensión xlsx]
     * Por ejemplo: 060 Acta Entrega POS ADEXUS 07-01-2019 11265.xlsx
     * @param type $filename
     * @return boolean
     */
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 7){
            return false;
        }
        if(!is_numeric($arr[0])){
            return false;
        }
        if(strcasecmp($arr[1], 'Acta') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[2], 'Entrega') != 0 ) {
            return false;
        }
        if(!$this->validateDate($arr[5])) {
            return false;
        }
        if(!is_numeric(substr($arr[6], 0, -5))) {
            return false;
        }
        
        return true;
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
}
