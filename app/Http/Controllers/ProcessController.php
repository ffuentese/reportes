<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessUpdate;
use Illuminate\Support\Facades\DB;

class ProcessController extends Controller
{
    //
    function __construct() {
        
        $this->middleware('auth');

    }
    
    public function index(Request $request)
    {
        $mensajes = new \Illuminate\Support\MessageBag();
        if(\Auth::user()->hasRole('admin')) {
            return view('proceso.index')->with('mensajes', $mensajes);
        } else {
            abort(403);
        }
    }
    
    public function procesar(Request $request) {
        $mensajes = new \Illuminate\Support\MessageBag();
        $queue = DB::table('jobs')->count();
        if($request->user()->hasRole('admin')) {
            if (!$queue){
                ProcessUpdate::dispatchNow();
                $mensajes->add('success', 'El proceso se ha iniciado.');
                return redirect('proceso_global')->with('success', $mensajes);
            } else {
                $mensajes->add('error','Hay tareas en proceso.');
                return redirect('proceso_global')->with('error', $mensajes);
            }
        } else {
            $mensajes->add('error','No tiene los permisos para iniciar el proceso.');
            return redirect('proceso_global')->with('error', $mensajes);
        }
    }
}
