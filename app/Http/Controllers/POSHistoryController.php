<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Device;
use App\Pos;

class POSHistoryController extends Controller
{
    /**
     * Genera un listado por POS (ubicación según Banco) de todos los dispositivos
     * que han estado en esa ubicación.
     * 
     * @author <ffuentese@entel.cl> 
     * 
     */
    
    /**
         * Muestra la vista con el buscador donde se debe digitar el n° de POS.
         * 
         * 
         */
    public function index()
    {
        
        return view('busqueda.buscapos');
    }
    
     /**
         * Retorna el resultado de la búsqueda.
         * 
         * @var Request $request Recibe el dato del usuario. El número de POS.
         * 
         * @return view Vista con el resultado de la búsqueda.
         */
    
    public function posHistory(Request $request)
    {
       
       
        $validator = Validator::make($request->all(), [
            'pos' => 'required|numeric'
        ]);
        
        if ($validator->fails()) {
            return redirect('busca_pos')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $inp_pos = $request->input('pos');
        $pos = Pos::find($inp_pos);
        $devices = collect([]);
        if ($pos) {
            //$devices = $pos->devices->unique()->sortByDesc('id');
            $devices = Device::where('pos_id', $pos->id)->orderBy('fecha_instalacion', 'desc')->get();
        } 
        
        $data = [
            'pos' => $pos,
            'devices' => $devices
        ];
        return view('busqueda.poshistory', $data);
    }
}
