<?php

namespace App\Http\Controllers;

use App\OTDigital;
use Illuminate\Http\Request;
use \App\Imports\OTDigitalImport;
use Illuminate\Support\Facades\DB;
use Excel;
use Carbon\Carbon;

class OTDigitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra el formulario de carga de la OT Digital.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $messages = new \Illuminate\Support\MessageBag();
        $fecha_atencion = OTDigital::max('fecha_atencion');
        if (!is_null($fecha_atencion)) {
            $ultima_at = date('d/m/Y', strtotime(OTDigital::max('fecha_atencion')));
        }
        $data = ['messages' => $messages,
                'ultima_at' => $ultima_at];
        return view('otdigital.upload', $data);
    }

    /**
     * Almacena la información que proviene de la planilla
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $messages = new \Illuminate\Support\MessageBag();
        $request->validate([
            'import_file' => 'required|mimetypes:text/plain',
            'inicio' => 'required',
            'termino' => 'required'
        ]);
        
        $inicio = Carbon::createFromFormat('d/m/Y', $request->inicio);
        $termino = Carbon::createFromFormat('d/m/Y', $request->termino);
        
        $path = $request->file('import_file')->getRealPath();
        $filename = $request->file('import_file')->getClientOriginalName();
        if ($this->validaNombreArchivo($filename)) {
            try {
                //OTDigital::truncate();
                //Excel::import(new OTDigitalImport, $path);
                DB::transaction(function () use ($inicio, $termino, $path){
                    OTDigital::where('fecha_atencion', '>=', $inicio)
                            ->where('fecha_atencion', '<=', $termino)->delete();
                    Excel::import(new OTDigitalImport, $path);
                });
                return redirect('/carga_ot_digital')->with('success', 'Carga exitosa.');
            } catch (\Exception $e) {
                $error_message = 'Hubo un error con el archivo: ' . $e->getMessage() . ' on ' . $e->getLine() . '\n trace: ' . $e->getTraceAsString();
                $messages->add('error', $error_message);
                \Log::warning($error_message);
            }
        } else {
            $error_message = 'El archivo no cumplía con el formato de nombre: ' . $filename;
            $messages->add('error', $error_message);
            \Log::warning($error_message);
        }

        return redirect('/carga_ot_digital')->withErrors($messages);

            
    }
    
    
    
    /**
     * Valida que el nombre de archivo corresponda al dump de OTDigital
     * 'OTDigital [fecha].csv'
     * @param type $filename
     * @return boolean
     */
    
    public function validaNombreArchivo($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 2){
            return false;
        }
        
        if(strcasecmp($arr[0], 'OTDigital') != 0 ) {
            return false;
        }
        $fecha = substr($arr[1], 0, -4);
        if(!$this->validateDate($fecha)) {
            return false;
        }
        
        return true;
        
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OTDigital  $oTDigital
     * @return \Illuminate\Http\Response
     */
    public function show(OTDigital $oTDigital)
    {
        //
    }

}
