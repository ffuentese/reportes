<?php

namespace App\Http\Controllers;

use App\DocGuiaIBM;
use App\Provider;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Validator;


class UploadDocsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    
    public function index()
    {
        // Lista los documentos ingresados.
        $guias = DocGuiaIBM::paginate(15);
        // Para el calendario
        // Las entregas diarias de IBM
        $diario_entregas = DB::table('guia_i_b_ms')
                ->selectRaw('fecha, COUNT(guia_i_b_ms.SERIAL) cantidad')
                ->where('estado', 'IBM')
                ->groupBy('fecha')
                ->orderBy('fecha', 'asc')
                ->get();
        // Las reversas por día.
        $diario_reversas = DB::table('guia_i_b_ms')
                ->selectRaw('fecha, COUNT(guia_i_b_ms.SERIAL) cantidad')
                ->where('estado', 'REV')
                ->groupBy('fecha')
                ->orderBy('fecha', 'asc')
                ->get();
        $diario = [];
        foreach($diario_entregas as $ent)
        {
            $diario[] = [
                'fecha' => Carbon::parse($ent->fecha),
                'cantidad' => $ent->cantidad,
                'tipo' => 'ENTREGA'
            ];
        }
        foreach($diario_reversas as $rev)
        {
            $diario[] = [
                'fecha' => Carbon::parse($rev->fecha),
                'cantidad' => $rev->cantidad,
                'tipo' => 'REVERSA'
            ];
        }
        $data = [
            'guias' => $guias,
            'diario' => $diario
        ];
                
        return view('guias.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Formulario de subida de guias
        return view('guias.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $errors = new \Illuminate\Support\MessageBag();
        $destinationPath = 'guias';
        if (!$request->import_file) {
            $error = 'No se subieron archivos.';
            $errors->add('no_files', $error);
            return redirect('carga')->withErrors($errors);
        }
        $files = $request->import_file;
        // Contamos los archivos del arreglo
        $file_count = count($files);
        // contamos cuántos archivos realmente se subieron al sistema
        $uploadcount = 0;
        foreach($files as $file) {
            $rules = array('import_file' => 'required|mimes:pdf');
            $validator = Validator::make(array('import_file'=> $file), $rules);
            $filename = $file->getClientOriginalName();
            if($validator->passes()){
                if($this->nombreDeArchivoValido($filename) && $this->registraDB($filename)){
                    $file->storeAs($destinationPath, $filename);
                    $uploadcount ++;
                } else {
                    $error = 'El archivo ' . $filename . ' tiene un nombre no válido para una guía o ya fue ingresada.';
                    $errors->add($filename, $error);
                }
            } else {
                $error = 'El formato del archivo ' . $filename . ' es incorrecto. ';
                $errors->add($filename, $error);
            }
        }
        if($uploadcount == $file_count){
            //se subió todo satisfactoriamente
            $success = 'Todos los archivos se subieron exitosamente.';
            $errors->add('success', $success);
            return redirect('carga_guias')->with('success', $errors);
        }
        else {
            //ocurrieron errores.
            return redirect('carga_guias')->with('error',$errors);
        }
    }
    
  
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 3){
            return false;
        }
        if(strcasecmp($arr[0], 'GD') != 0 ) {
            return false;
        }
        if(!is_numeric(substr($arr[2], 0, -4))) {
            return false;
        }
        $guia = substr($arr[2], 0, -4);
        if(DocGuiaIBM::where('guia', $guia)->exists()){
            return false;
        }
        
        return true;
        
    }
    
    public function registraDB($filename)
    {
        $arr = explode(' ', $filename);
        $provider = Provider::where('name', $arr[1])->first();
        if (!$provider) {
            return false;
        }
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        $guia = substr($arr[2], 0, -4);
        $datos = [
            'guia' => $guia,
            'provider_id' => $provider->id,
            'filename' => $filename,
            'created_at' => $now,
            'updated_at' => $now
        ];
        try {
            DocGuiaIBM::create($datos);
        } catch (\Exception $e) {
            \Log::warning($e);
            return false;
        }
        return true;
    }
    
    /**
     * Método para mostrar tabla en /lista_guias con todos los PDFs.
     * @return type
     */
    
    public function getDatatable()
    {
        $model = DocGuiaIBM::with('providers');
        return DataTables::eloquent($model)
                ->addColumn('provider', function (DocGuiaIBM $docguia) {
                    return $docguia->providers->get()->first(function($provider) use($docguia) {
                        return $provider->id == $docguia->provider_id ;
                    })->name; // esta es la relación con la tabla providers
                })
                ->toJson();
    }
    

    /**
     * Display the specified resource.
     *
     * @param  integer  $guia
     * @return \Illuminate\Http\Response
     */
    public function show($guia)
    {
        // Muestra el PDF solicitado
        $dir = 'guias';
        $doc = DocGuiaIBM::where('guia', $guia)->first();
        if ($doc) {
            return Storage::response($dir . '/' . $doc->filename);
        } else {
            abort(404);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DocGuiaIBM  $docGuiaIBM
     * @return \Illuminate\Http\Response
     */
    public function edit(DocGuiaIBM $docGuiaIBM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DocGuiaIBM  $docGuiaIBM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocGuiaIBM $docGuiaIBM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DocGuiaIBM  $docGuiaIBM
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocGuiaIBM $docGuiaIBM)
    {
        //
    }
}
