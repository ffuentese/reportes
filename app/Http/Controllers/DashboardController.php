<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Device;
use App\GuiaIBM;
use App\Provider;
use App\Guia;
use App\Zpec;
use Carbon\Carbon;



    /**
         * Controlador de la página del panel de inicio 
         * con el resumen de todos los datos del sistema
         * 
         * @author <ffuentese@entel.cl>
         * 
         * 
         */

class DashboardController extends Controller
{
    /*
     * Muestra el panel del usuario 
     * y calcula los datos a mostrar.
     * 
     * @author <ffuentese@entel.cl>
     * 
     * @return \Illuminate\View\View view
     */
    
    public function index()
    {
        // Todas las series que han ingresado o reingresado
        //$ingresos = Device::whereNotNull('fecha_recepcion')->count(); 
        $ingresos = Guia::where('estado', 'ENTREGA')
                ->where(function($query){
                    $query->where('tecnologia', '3G')
                          ->orWhere('tecnologia', 'GPRS');
                })
                ->count();
        // Todas las reversas que hay en el sistema
        //$reversas = Device::whereNotNull('fecha_reversa')->count();
        $reversas = Guia::where('estado', 'REVERSA')
                ->where(function($query){
                    $query->where('tecnologia', '3G')
                          ->orWhere('tecnologia', 'GPRS');
                })->count();


        $total_validadas = sizeof(DB::select("SELECT * FROM guias WHERE estado = 'ENTREGA' AND (SERIAL, fecha) IN "
                . "( SELECT SERIAL, fecha_recepcion FROM devices WHERE fecha_recepcion IS NOT NULL "
                . "AND (fecha_instalacion IS NOT NULL OR fecha_reversa IS NOT NULL) )"));
        
        $reutilizadas =  sizeof(DB::select('SELECT id FROM devices d WHERE fecha_recepcion IS NOT NULL '
                . 'AND fecha_instalacion is NULL AND fecha_reversa IS NULL AND EXISTS '
                . '( SELECT 1 FROM devices WHERE SERIAL = d.SERIAL AND fecha_recepcion > d.fecha_recepcion)'));
        
        // Equipos que no tienen serial pero que están registrados en el sistema.
        $ilegibles = Guia::where(function($query){
                    $query->where('serial', 'LIKE', 'BEC%')
                    ->orWhere('serial', 'LIKE' ,'%ILEGIBLE%');
                })
        ->count();

        // Stock de bodega separado por tipo
        $stock_bod_s = Device::whereNull('fecha_reversa')
                ->where('location_id', 'LIKE', 'D%')
                ->where('customer_id', 1)
                ->count();
        $stock_bod_r = Device::whereNull('fecha_reversa')
                ->where('location_id', 'LIKE', 'R%')
                ->where('customer_id', 1)
                ->count();
        // Stock de bodega unificado y descartados los series sin recepción
        $stock_bod = Device::whereNull('fecha_reversa')
                ->where(function($query){
                    $query->where('location_id', 'LIKE', 'D%')
                          ->OrWhere('location_id', 'LIKE', 'R%');
                })
                ->where(function($query){
                    $query->where('serial', 'NOT LIKE', 'BEC%')
                    ->orWhere('serial', 'NOT LIKE' ,'%ILEGIBLE%');
                })
                ->where('customer_id', 1)
                ->whereNotNull('fecha_recepcion')
                ->count();
        // Stock técnico separado por tipo
        $sto_tec_s = Device::whereNull('fecha_reversa')
                ->where('location_id', 'STO')
                ->where('customer_id', 1)
                ->count();
        $sto_tec_r = Device::whereNull('fecha_reversa')
                ->where('location_id', 'REV')
                ->where('customer_id', 1)
                ->count();
        // Stock técnico unificado y descartados los series sin recepción
        $stock_tec = Device::whereNull('fecha_reversa')
                ->where(function($query){
                    $query->where('location_id', 'STO')
                          ->OrWhere('location_id', 'REV');
                })
                ->where(function($query){
                    $query->where('serial', 'NOT LIKE', 'BEC%')
                    ->orWhere('serial', 'NOT LIKE' ,'%ILEGIBLE%');
                })
                ->where('customer_id', 1)
                ->whereNotNull('fecha_recepcion')
                ->count();
        $habidas =  $stock_tec + $stock_bod;
        $total_just = $total_validadas+$habidas+$reutilizadas;
        $total_just_ilegibles = $total_just + $ilegibles;
        if ($ingresos > 0) {
            $porc_just = round((((float) $total_just / (float) $ingresos)*100),2);
            $porc_just_ilegibles = round((((float) $total_just_ilegibles / (float) $ingresos)*100),2);
        } else {
            $porc_just = 0;
            $porc_just_ilegibles = 0;
        }
        
        // IBM (buscamos al proveedor con el id de la tabla).
        $provider = Provider::where('name', 'IBM')->first();
        $ingresos_ibm = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->where(function ($query){
                    $query->where('tecnologia', '3G')
                    ->orWhere('tecnologia', 'GPRS');
                })
                ->count();
        $reversas_ibm = DB::table('guia_i_b_ms')
                ->where('estado', 'REV')
                ->where(function ($query){
                    $query->where('tecnologia', '3G')
                    ->orWhere('tecnologia', 'GPRS');
                })
                ->count();
                
        $subq_validadas = Device::where('customer_id', 1)
         ->where('provider_id', $provider->id)
         ->whereNotNull('fecha_recepcion')
         ->where(function ($query){
            $query->whereNotNull('fecha_instalacion')
                ->OrWhereNotNull('fecha_reversa');
            })->select('serial')->get();
        $serials = [];    
        foreach ($subq_validadas as $sub) 
        {
            $serials[] = $sub->serial;
        }
             
        $validadas_ibm = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->where(function ($query){
                    $query->where('tecnologia', '3G')
                    ->orWhere('tecnologia', 'GPRS');
                })
                ->whereIn('serial', $serials)      
                ->count();

        $total_validadas_ibm = $validadas_ibm;
        
        // Equipos que no tienen serial pero que están registrados en el sistema.
        $ilegibles_ibm = GuiaIBM::where('estado', 'IBM')
                ->where(function ($query){
                    $query->where('serial', 'LIKE', '%ILEGIBLE%')
                    ->OrWhere('serial', 'LIKE', 'BEC%');
                })
                ->count();
                
        $subq_instalados = Zpec::where('cliente', '970300007')->select('serie')->get();
        $series_zpec = [];
        foreach($subq_instalados as $item)
        {
            $series_zpec[] =  $item->serie;
        }
                
        
        $stock_bod_s_ibm = Device::whereNull('fecha_reversa')
                ->where('location_id', 'IBM')
                ->where('customer_id', 1)
                ->where('provider_id', $provider->id)
                ->count();
        $stock_bod_r_ibm = Device::where('location_id', 'REV')
                ->where('customer_id', 1)
                ->where('provider_id', $provider->id)
                ->count();
        
        
        $no_instalado_ibm = Device::where('provider_id', $provider->id)
                ->whereNotNull('fecha_recepcion')
                ->whereNull('fecha_reversa')
                ->whereNull('fecha_instalacion')
                ->where('location_id', 'IBM')
                ->whereNotIn('serial', $series_zpec)
                ->count();
        
        $reversado_ibm = $reversas_ibm;
        $total_disponible_ibm = $no_instalado_ibm;

        $stock_bod_ibm = $stock_bod_r_ibm + $stock_bod_s_ibm;
        $habidas_ibm =  $stock_bod_ibm;
        $total_just_ibm = $total_validadas_ibm;
        $total_just_ilegibles_ibm = $total_just_ibm + $ilegibles_ibm;
        if ($ingresos_ibm > 0 ) {
            //$porc_just_ibm = round((((float) $total_just_ibm / (float) $ingresos_ibm)*100),2);
            $porc_just_ilegibles_ibm = round((((float) $total_just_ilegibles_ibm / (float) $ingresos_ibm)*100),2);
        } else {
            //$porc_just_ibm = 0;
            $porc_just_ilegibles_ibm = 0;
        }
        
        $ultima_act = Device::all()->max('updated_at');
        $ultima_gd_dir = Guia::where('estado', 'ENTREGA')->max('guia');
        $ultima_gd_rev = Guia::where('estado', 'REVERSA')->max('guia');
        $ultima_ibm = GuiaIBM::all()->max('fecha');
        $ultima_gd_ibm_dir = GuiaIBM::where('estado', 'ENTREGA')->max('guia');
        $ultima_gd_ibm_rev = GuiaIBM::where('estado', 'REVERSA')->max('guia');
        $ultimo_pv = Carbon::parse(DB::table('rel_p_v_s')->max('fecha'));
        
        // Fuentes del gráfico Adexus
        
        $col_entregas = Guia::select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->whereNotNull('fecha')->whereNotNull('guia')->where('estado', 'entrega')->groupBy('year', 'month')->orderBy('year', 'asc')->orderBy('month', 'asc')->get();
        $col_reversas = Guia::select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->whereNotNull('fecha')->whereNotNull('guia')->where('estado', 'reversa')->groupBy('year', 'month')->orderBy('year', 'asc')->orderBy('month', 'asc')->get();        
        $ar_entregas = [];
        $ar_reversas = [];
        $meses_grafico =  [];
        foreach($col_entregas as $entrega)
        {
            $fecha = $entrega->year . '-' . $entrega->month;
            $meses_grafico[] = $fecha;
            $ar_entregas[] = ["x" => $fecha,
                              "y" => $entrega->cantidad];
        }
        foreach($col_reversas as $reversa)
        {
            $fecha = $reversa->year . '-' . $reversa->month;
            $ar_reversas[] = [
                "x" => $fecha,
                "y" => $reversa->cantidad];
        }
        
    
        
        // Fuentes de cuadro 
        
        $q_pvs_3g = DB::table('last_pvs')
                ->selectRaw('count(*) as cantidad')
                ->where('estado', '=', 'VIGENTE')
                ->where('tecnologia', '=', '3G')
                ->OrWhere('tecnologia', '=', '3GB')
                ->first();
        if ($q_pvs_3g) {
            $pvs_3g = $q_pvs_3g->cantidad;
        } else {
            $pvs_3g = 0;
        }
        $q_pvs_gprs = DB::table('last_pvs')
                ->selectRaw('count(*) as cantidad')
                ->where('tecnologia', '=', 'EGPRS')
                ->where('estado', '=', 'VIGENTE')
                ->first();
        if ($q_pvs_gprs) {
            $pvs_gprs = $q_pvs_gprs->cantidad;
        } else {
            $pvs_gprs  = 0;
        }
        // Transformadores Verifone
        $q_pvs_vef = DB::table('last_pvs')
                ->selectRaw('count(*) as cantidad')
                ->where('marca', '=', 'VERIFONE')
                ->where('estado', '=', 'VIGENTE')
                ->first();
        if ($q_pvs_vef) {
            $pvs_vef = $q_pvs_vef->cantidad;
        } else {
            $pvs_vef = 0;
        }
        // Transformadores Castles
        $q_pvs_cas = DB::table('last_pvs')
                ->selectRaw('count(*) as cantidad')
                ->where('marca', '=', 'CASTLES')
                ->where('estado', '=', 'VIGENTE')
                ->first();
        if ($q_pvs_cas) {
            $pvs_cas = $q_pvs_cas->cantidad;
        } else {
            $pvs_cas = 0;
        }
        
        // Entregas de equipos 
        
        $tipo_trafo = \App\AccesorioTipo::where('name', 'trafo')->first();
        $entregas_3g = DB::table('guias')
                ->where('estado', 'ENTREGA')
                ->where('tecnologia', '3G')
                ->count();
        $entregas_gprs = DB::table('guias')
                ->where('estado', 'ENTREGA')
                ->where('tecnologia', 'GPRS')
                ->count();
        $entregas_vef = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'entrega')
                ->where('models.brand_id', 'VEF')
                ->sum('accesorios.cantidad');
        $entregas_cas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'entrega')
                ->where('models.brand_id', 'CAS')
                ->sum('accesorios.cantidad');
        
       // Reversas de equipos
        
        $reversas_3g = DB::table('guias')
                ->where('estado', 'REVERSA')
                ->where('tecnologia', '3G')
                ->count();
        $reversas_gprs = DB::table('guias')
                ->where('estado', 'REVERSA')
                ->where('tecnologia', 'GPRS')
                ->count();
        $reversas_vef = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'reversa')
                ->where('models.brand_id', 'VEF')
                ->sum('accesorios.cantidad');
        $reversas_cas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('tipo_id', $tipo_trafo->id)
                ->where('estado', 'reversa')
                ->where('models.brand_id', 'CAS')
                ->sum('accesorios.cantidad');
        
        // Consumo promedio mensual
        $cpm_3g = "";
        $cpm_gprs = "";
        $subq_cpm = DB::select('SELECT round(avg(cm),0) AS CPM, tec
                FROM (
                SELECT COUNT(*) AS cm, tecnologia AS tec, MONTH(fecha), YEAR(fecha)
                FROM GUIAS
                WHERE fecha IS NOT NULL 
                AND estado = "entrega"
                GROUP BY tec, MONTH(fecha), YEAR(fecha)) v
                GROUP BY tec;
        ');
        foreach($subq_cpm as $cpm) {
            if ($cpm->tec == '3G') {
                $cpm_3g = $cpm->CPM;
            }
            if ($cpm->tec == 'GPRS') {
                $cpm_gprs = $cpm->CPM;
            }
        }
        
        $cpm_vef = "";
        $cpm_cas = "";
        // Esta consulta calcula el consumo promedio mensual de trafos
        $subq_cpm_trafos = DB::select("SELECT round(AVG(cant),0) AS CPM, marca
            FROM (
            SELECT SUM(cantidad) AS cant, models.brand_id AS marca, MONTH(accesorios.fecha), YEAR(accesorios.fecha)
            FROM accesorios
            JOIN models ON models.id = accesorios.modelo_id
            JOIN accesorio_tipos ON accesorios.tipo_id = accesorio_tipos.id
            WHERE accesorio_tipos.NAME = 'trafo'
            GROUP BY marca,
            MONTH(accesorios.fecha), YEAR(accesorios.fecha)) v
            GROUP BY marca;
        ");
        
        foreach($subq_cpm_trafos as $cpm) {
            if ($cpm->marca == 'VEF') {
                $cpm_vef = $cpm->CPM;
            }
            if ($cpm->marca == 'CAS') {
                $cpm_cas = $cpm->CPM;
            }
        }
        
        
        
        // Spare (sobrante)
        
        $spare_3g = $entregas_3g - $reversas_3g;
        $spare_gprs = $entregas_gprs - $reversas_gprs;
        $spare_vef = $entregas_vef - $reversas_vef;
        $spare_cas = $entregas_cas - $reversas_cas;
        
        // Porcentaje acordado 
        $acordado_3g = $pvs_3g > 0 ? round($pvs_3g * 0.097, 0) : 0;
        $acordado_gprs = $pvs_gprs > 0 ? round($pvs_gprs * 0.097, 0) : 0;
        $acordado_vef = $pvs_vef > 0 ? round($pvs_vef * 0.097, 0) : 0;
        $acordado_cas = $pvs_cas > 0 ? round($pvs_cas * 0.097, 0) : 0;
        
        // Stock CO
        
        $stock_3g = $spare_3g - $acordado_3g;
        $stock_gprs = $spare_gprs - $acordado_gprs;
        $stock_vef = $spare_vef - $acordado_vef;
        $stock_cas = $spare_cas - $acordado_cas;
        
        
        
        // Proyectos
        
        $proyectos_3g = Guia::where('obs', '!=', '')->where('tecnologia', '3G')->where('estado', 'entrega')->count();
        $proyectos_gprs = Guia::where('obs', '!=', '')->where('tecnologia', 'GPRS')->where('estado', 'entrega')->count();
        $proyectos_vef = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', '=', 'VEF')
                ->where(function($query){
                    $query->where('accesorios.obs', '=', 'Proyecto')
                    ->orWhere('accesorios.obs', '=', 'IF');
                })
                ->count();
        $proyectos_cas = DB::table('accesorios')
                ->join('models', 'accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', '=', 'CAS')
                ->where(function($query){
                    $query->where('accesorios.obs', '=', 'Proyecto')
                    ->orWhere('accesorios.obs', '=', 'IF');
                })
                ->count();
                
        $spare_3g = $spare_3g - $proyectos_3g;
        $spare_gprs = $spare_gprs - $proyectos_gprs;
        $spare_cas = $spare_cas - $proyectos_cas;
        $spare_vef = $spare_vef - $proyectos_vef;
        
        $pc_spare_3g = round(($spare_3g / $pvs_3g) * 100);
        $pc_spare_gprs = round(($spare_gprs / $pvs_gprs) * 100);
        $pc_spare_vef = round(($spare_vef / $pvs_vef) * 100);
        $pc_spare_cas = round(($spare_cas / $pvs_cas) * 100);
        
        $datos_cuadro = [
            'pvs_3g' => $pvs_3g,
            'pvs_gprs' => $pvs_gprs,
            'pvs_vef' => $pvs_vef,
            'pvs_cas' => $pvs_cas,
            'entregas_3g' => $entregas_3g,
            'entregas_gprs' => $entregas_gprs,
            'entregas_vef' => $entregas_vef,
            'entregas_cas' => $entregas_cas,
            'reversas_3g' => $reversas_3g,
            'reversas_gprs' => $reversas_gprs,
            'reversas_vef' => $reversas_vef,
            'reversas_cas' => $reversas_cas,
            'cpm_3g' => $cpm_3g,
            'cpm_gprs' => $cpm_gprs,
            'cpm_vef' => $cpm_vef,
            'cpm_cas' => $cpm_cas,
            'spare_3g' => $spare_3g,
            'spare_gprs' => $spare_gprs,
            'spare_vef' => $spare_vef,
            'spare_cas' => $spare_cas,
            'acordado_3g' => $acordado_3g,
            'acordado_gprs' => $acordado_gprs,
            'acordado_vef' => $acordado_vef,
            'acordado_cas' => $acordado_cas,
            'stock_3g' => $stock_3g,
            'stock_gprs' => $stock_gprs,
            'stock_vef' => $stock_vef,
            'stock_cas' => $stock_cas,
            'pc_spare_3g' => $pc_spare_3g,
            'pc_spare_gprs' => $pc_spare_gprs,
            'pc_spare_vef' => $pc_spare_vef,
            'pc_spare_cas' => $pc_spare_cas,
            'proyectos_3g' => $proyectos_3g,
            'proyectos_gprs' => $proyectos_gprs,
            'proyectos_vef' => $proyectos_vef,
            'proyectos_cas' => $proyectos_cas
        ];
        
        // Fuentes del cuadro IBM
        
        $pos_3g_ibm_entrega = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->where('tecnologia', '3G')
                ->count();
        $pos_gprs_ibm_entrega = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->where('tecnologia', 'GPRS')
                ->count();
        $trafo_vef_ibm_entrega = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'VEF')
                ->where('estado', 'Entrega')
                ->where('tipo_id', $tipo_trafo->id)
                ->sum('guia_i_b_m_accesorios.cantidad');
                
        $trafo_cas_ibm_entrega = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'CAS')
                ->where('estado', 'Entrega')
                ->where('tipo_id', $tipo_trafo->id)
                ->sum('guia_i_b_m_accesorios.cantidad');
        
        // reversas
        $pos_3g_ibm_reversa = DB::table('guia_i_b_ms')
                ->where('estado', 'REV')
                ->where('tecnologia', '3G')
                ->count();
        $pos_gprs_ibm_reversa = DB::table('guia_i_b_ms')
                ->where('estado', 'REV')
                ->where('tecnologia', 'GPRS')
                ->count();
        $trafo_vef_ibm_reversa = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'VEF')
                ->where('estado', 'Reversa')
                ->where('tipo_id', $tipo_trafo->id)
                ->sum('guia_i_b_m_accesorios.cantidad');
        $trafo_cas_ibm_reversa = DB::table('guia_i_b_m_accesorios')
                ->join('models', 'guia_i_b_m_accesorios.modelo_id', '=', 'models.id')
                ->where('models.brand_id', 'CAS')
                ->where('estado', 'Reversa')
                ->where('tipo_id', $tipo_trafo->id)
                ->sum('guia_i_b_m_accesorios.cantidad');
        
        $cuadro_ibm = [
            'pos_3g_ibm_entrega' => $pos_3g_ibm_entrega,
            'pos_gprs_ibm_entrega' => $pos_gprs_ibm_entrega,
            'trafo_vef_ibm_entrega' => $trafo_vef_ibm_entrega,
            'trafo_cas_ibm_entrega' => $trafo_cas_ibm_entrega,
            
            'pos_3g_ibm_reversa' => $pos_3g_ibm_reversa,
            'pos_gprs_ibm_reversa' => $pos_gprs_ibm_reversa,
            'trafo_vef_ibm_reversa' => $trafo_vef_ibm_reversa,
            'trafo_cas_ibm_reversa' => $trafo_cas_ibm_reversa
        ];
        
        // Fuentes del gráfico IBM
        
        $col_devices_ibm = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'IBM')->groupBy('year', 'month')->get();
        $col_reversas_ibm = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'REV')->groupBy('year', 'month')->get();
        $ar_entregas_ibm = [];
        $ar_reversas_ibm  = [];
        $meses_grafico_ibm = [];
        foreach($col_devices_ibm as $entrega)
        {
            $fecha = $entrega->year . '-' . $entrega->month;
            $meses_grafico_ibm[] = $fecha;
            $ar_entregas_ibm[] = ["x" => $fecha,
                              "y" => $entrega->cantidad];
        }
        foreach($col_reversas_ibm as $reversa)
        {
            $fecha = $reversa->year . '-' . $reversa->month;
            $ar_reversas_ibm[] = [
                "x" => $fecha,
                "y" => $reversa->cantidad];
        }
        
        // gráfico por día
        $col_devices_ibm_dia = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, fecha"))->where('estado', 'IBM')->groupBy('fecha')->get();
        $col_reversas_ibm_dia = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, fecha"))->where('estado', 'REV')->groupBy('fecha')->get();
        $ar_entregas_ibm_dia = [];
        $ar_reversas_ibm_dia  = [];
        $meses_grafico_ibm_dia = [];
        foreach($col_devices_ibm_dia as $entrega)
        {
            $fecha = Carbon::parse($entrega->fecha);
            $meses_grafico_ibm_dia[] = $fecha->format('Y-m-d');
            $ar_entregas_ibm_dia[] = ['t' => $fecha->format('Y-m-d'),
                'y' => $entrega->cantidad];
        }
        foreach($col_reversas_ibm_dia as $reversa)
        {
            $fecha = Carbon::parse($reversa->fecha);
            if (!in_array($fecha, $meses_grafico_ibm_dia)) {
                $meses_grafico_ibm_dia[] = $fecha->format('Y-m-d'); 
            }
            $ar_reversas_ibm_dia[] = ['t' => $fecha->format('Y-m-d'),
                'y' => $reversa->cantidad];
        }
        
        // no habidas
        
        $series_no_habidas = 0;
        $saldo_global = $ingresos - $reversas;
        $saldo_ibm = $ingresos_ibm - $reversado_ibm;
        if ($saldo_global > 0 && $saldo_ibm > 0 && $saldo_ibm <= $saldo_global ) {
            $series_no_habidas = $saldo_global - $saldo_ibm;
        }
        
                
        $data = [
            'ingresos' => $ingresos,
            'reversas' => $reversas,
            'rec_rev' => $total_validadas,
            'ilegibles' => $ilegibles,
            'stock_tec' => $stock_tec,
            'stock_bod' => $stock_bod,
            'habidas' => $stock_tec + $stock_bod,
            'total_just' => $total_just,
            'total_just_ilegibles' => $total_just_ilegibles,
            'porc_just' => $porc_just,
            'porc_just_ilegibles' => $porc_just_ilegibles,
            'bod_sto' => $stock_bod_s,
            'bod_reve' => $stock_bod_r,
            'stock_tecnicos' => $sto_tec_s,
            'reversa_tecnicos' => $sto_tec_r,
            't_disponible' => $stock_bod_s + $sto_tec_s,
            't_reversas' => $stock_bod_r + $sto_tec_r,
            'ingresos_ibm' => $ingresos_ibm,
            'reversas_ibm' => $reversas_ibm,
            'rec_rev_ibm' => $total_validadas_ibm,
            'ilegibles_ibm' => $ilegibles_ibm,
            'stock_bod_ibm' => $stock_bod_ibm,
            'habidas_ibm' => $habidas_ibm,
            'total_just_ibm' => $total_just_ibm,
            'total_just_ilegibles_ibm' => $total_just_ilegibles_ibm,
            'porc_just_ilegibles_ibm' => $porc_just_ilegibles_ibm,
            'bod_sto_ibm' => $stock_bod_s_ibm,
            'bod_reve_ibm' =>$stock_bod_r_ibm,
            't_disponible_ibm' => $total_disponible_ibm,
            't_reversas_ibm' => $reversas_ibm,
            'ultima_act' => $ultima_act,
            'ultima_ibm' => $ultima_ibm,
            'ar_entregas' => $ar_entregas,
            'ar_reversas' => $ar_reversas,
            'meses_grafico' => $meses_grafico,
            'ar_entregas_ibm' => $ar_entregas_ibm,
            'ar_reversas_ibm' => $ar_reversas_ibm,
            'meses_grafico_ibm' => $meses_grafico_ibm,
            'datos_cuadro' => $datos_cuadro,
            'cuadro_ibm' => $cuadro_ibm,
            'ar_entregas_ibm_dia' => $ar_entregas_ibm_dia,
            'ar_reversas_ibm_dia' => $ar_reversas_ibm_dia,
            'meses_grafico_ibm_dia' => $meses_grafico_ibm_dia,
            'no_habidas' => $series_no_habidas
            
        ];
       return view("dashboard.index", $data);
    }
    
    
}
