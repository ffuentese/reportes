<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ImportDev_BCH;
use App\Imports\BginfoImport;
use App\Imports\SccmImport;
use App\Imports\BCH_EmployeeImport;
use App\Imports\EpoImport;
use App\Exports\BCHExport;
use App\Dev_BCH;
use App\Zpec;
use App\Jobs\BCHZpecSolped;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Excel;
use Validator;
use Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;

    /**
         * Procesa el archivo Excel del inventario del BCH a través del importador ImportDev_BCH.
         * Retorna redirección a vista de carga.
         * 
         * @author <ffuentese@entel.cl>
         * 
         * @param \Illuminate\Http\Request $request
         */

class InvBCHController extends Controller
{

    /**
     * Muestra la vista donde se desplegará la tabla. 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {
        $ultima_act = Dev_BCH::max('updated_at');
        return view('bch.index')->with('ultima_act', $ultima_act);
    }
    
    /**
     * Muestra una vista que contiene un formulario para cargar el archivo CSV.
     * @return \Illuminate\Http\Response 
     */
    public function carga()
    {
        return view('bch.carga');
    }
    
    /**
     * Retorna los datos de la tabla.
     * Utiliza el paquete yajra/datatables.
     * Dev_BCH::query()
     * @return \Illuminate\Http\Response 
     */
    public function getDatatable()
    {
        //$devs = DB::table('dev__b_c_hs')->select(['id', 'alias', 'rotulo', 'serie', 'tipo', 'marca', 'modelo', 'nombre', 'rut', 'region', 'site', 'fecha_reporte', 'ultima_conexion']);
        return Datatables::of(Dev_BCH::query())->make(true);
    }
   
    /**
     * Procesa la subida del archivo CSV
     * 
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function proceso(Request $request)
    {
        ini_set('max_execution_time', 0);
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimetypes:text/plain'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'csv']);
        }
        
        $path = $request->file('import_file')->getRealPath();
        Dev_BCH::truncate();
        Excel::import(new ImportDev_BCH, $path);
        $job = new BCHZpecSolped;
        $job->dispatch();
        
        return redirect('cargaInventarioBCH')
                ->with('success', 'Carga exitosa!')->withInput(['tab' => 'csv']);
    }
    
    /**
     * Procesa el archivo de BGinfo para la conf. del reporte BCH
     * @param Request $request
     */
    
    public function procesoBginfo(Request $request) {
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx',
            'fecha' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'bginfo']);
        }
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'BGINFO';
        $fecha = $request->fecha;
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Bginfo::truncate();
            Excel::import(new BginfoImport($fecha), $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de BGINFO en proceso')
                    ->withInput(['tab' => 'bginfo']);
        } else {
            $error = 'El archivo ' . $path . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'bginfo']);
        }
    }
    
    /**
     * Procesa el archivo de SCCM para el reporte BCH
     * @param Request $request
     */
    
    public function procesoSCCM(Request $request) {
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx',
            'fecha' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'sccm']);
        }
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'SCCM';
        $fecha = $request->fecha;
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Sccm::truncate();
            Excel::import(new SccmImport($fecha), $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de SCCM en proceso')->withInput(['tab' => 'sccm']);
        } else {
            $error = 'El archivo ' . $path . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'sccm']);
        }
    }
    
    /**
     * Procesa el archivo de RRHH (Dotación)
     * @param Request $request
     */
    
    public function procesoRRHH(Request $request) {
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'rrhh']);
        }
        
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'DOTACION';
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Bch_employee::truncate();
            Excel::import(new BCH_EmployeeImport, $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de Dotación en proceso.')
                    ->withInput(['tab' => 'rrhh']);
        } else {
            $error = 'El archivo ' . $filename . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'rrhh']);
        }
    }
    
    public function procesoEpo(Request $request) {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'epo']);
        }
        
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'EPO';
        $fecha = $request->fecha;
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Epo::truncate();
            
            (new FastExcel)->import($path, function ($row) use ($fecha) {
                if (isset($row['ULTIMA_CONEXION']) && !is_null($row['ULTIMA_CONEXION']) && $row['ULTIMA_CONEXION'] != '') {
                    $ultima_conexion =  Carbon::createFromFormat('j/m/y G:i:s',substr($row['ULTIMA_CONEXION'],0,-4));
                } else {
                    $ultima_conexion = null;
                }
                if($row['HOSTNAME'] === '') {
                    return;
                }
                return \App\Epo::updateOrCreate(
                    [
                        'hostname' => $row['HOSTNAME']
                    ],
                    [
                        'plataforma' => $row['PLATAFORMA'],
                        'ip' => $row['IP'],
                        'mac_address' => $row['MAC'],
                        'dominio' => $row['DOMINIO'],
                        'usuario' => $row['USUARIO'],
                        'ultima_conexion' => $ultima_conexion,
                        'portatil' => $row['PORTATIL'],
                        'fecha_carga' => $fecha
                    ]);
            });
            //Excel::import(new EpoImport($fecha), $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de EPO en proceso')
                    ->withInput(['tab' => 'epo']);
        } else {
            $error = 'El archivo ' . $filename . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'epo']);
        }
    }
    
    public function export() 
    {
        $devbch = Dev_BCH::get();
        $fecha = date('d-m-Y H:i:s');
        $filename = 'inventarioBCH_'. $fecha .'.xlsx';
        return (new FastExcel($devbch))->download($filename);
    }
    
    public function validateFile($filename, $type) {
        $arr = explode('_', $filename);
        if(!isset($arr[1])) {
            return false;
        }
        $fecha = substr(end($arr), 0, -5);
        if(strtoupper($arr[0]) != $type) {
            return false;
        }
        if (!is_numeric($fecha)) {
            return false;
        }
        
        return true;
    }
    
}
