<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\EntregaExport;
use App\Device;
use App\GuiaIBM;
use App\Guia;
use App\Accesorio;
use App\GuiaIBMAccesorio;
use App\Imports\BulkImport;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;


class EntregaController extends Controller
{
    /**
     * Controlador de "/carga" desde donde se suben todos los formularios 
     * y de proceso de carga de planilla de actas de entrega (procesoPlanilla).
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     */
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    /**
         * Controlador de "/carga"
         * 
         * Muestra el contenido de la página, incluyendo el listado de dispositivos.
         * 
         * @return view
         */
    public function index()
    {
        
        $devices = Device::paginate(15);
        $urec = Device::max('fecha_recepcion');
        $urev = Device::max('fecha_reversa');
        $urec_ibm = GuiaIBM::where('estado', 'IBM')->max('fecha');
        $urev_ibm = GuiaIBM::where('estado', 'REV')->max('fecha');
        if ($urec) {
            $ultima_rec = date('d/m/Y', strtotime($urec));
        } else {
            $ultima_rec = "";
        }
        if ($urev) {
            $ultima_rev = date('d/m/Y', strtotime($urev));
        } else {
            $ultima_rev = "";
        }
        if ($urec_ibm) {
            $ultima_rec_ibm = date('d/m/Y', strtotime($urec_ibm));
        } else {
            $ultima_rec_ibm = "";
        }
        if ($urev_ibm) {
            $ultima_rev_ibm = date('d/m/Y', strtotime($urev_ibm));
        } else {
            $ultima_rev_ibm = "";
        }
        $ultima_gd_dir = Guia::where('estado', 'ENTREGA')->max('guia');
        $ultima_gd_rev = Guia::where('estado', 'REVERSA')->max('guia');
        $ultima_ibm = GuiaIBM::all()->max('fecha');
        $ultima_gd_ibm_dir = GuiaIBM::where('estado', 'IBM')->max('guia');
        $ultima_gd_ibm_rev = GuiaIBM::where('estado', 'REV')->max('guia');
        $ultimo_pv = Carbon::parse(DB::table('rel_p_v_s')->max('fecha'));
        $ultima_dir_accesorios = Accesorio::where('estado', 'ENTREGA')->max('fecha') ?? '';
        if ($ultima_dir_accesorios != '') {
            $ultima_dir_accesorios = Carbon::parse($ultima_dir_accesorios)->format('d-m-Y');
        } 
        $ultima_rev_accesorios = Accesorio::where('estado', 'REVERSA')->max('fecha') ?? '';
        if ($ultima_rev_accesorios != '') {
            $ultima_rev_accesorios = Carbon::parse($ultima_rev_accesorios)->format('d-m-Y');
        }
        $data = [
            'devices' => $devices,
            'ultima_rec' => $ultima_rec,
            'ultima_rev' => $ultima_rev,
            'ultima_rec_ibm' => $ultima_rec_ibm,
            'ultima_rev_ibm' => $ultima_rev_ibm,
            'ultima_gd_dir' => $ultima_gd_dir,
            'ultima_gd_rev' => $ultima_gd_rev,
            'ultima_gd_ibm_dir' => $ultima_gd_ibm_dir,
            'ultima_gd_ibm_rev' => $ultima_gd_ibm_rev,
            'ultimo_pv' => $ultimo_pv,
            'ultima_dir_accesorios' => $ultima_dir_accesorios,
            'ultima_rev_accesorios' => $ultima_rev_accesorios
        ];
        return view('entrega.index', $data);
    }
    
     /**
         * Procesa la planilla de cargas de actas de entrega
         * 
         * @var $request formulario de carga de hoja Excel además de fecha y n° de guía.
         * 
         * @return \Illuminate\Http\RedirectResponse view
         */
    
    public function procesoPlanilla(Request $request)
    {       
            $errors = new \Illuminate\Support\MessageBag();
            if (!$request->import_file) {
                $error = 'No se subieron archivos.';
                $errors->add('no_files', $error);
                return redirect('carga')->withErrors($errors);
            }
            $files = $request->import_file;
            
            // Contamos los archivos del arreglo
            $file_count = count($files);

            // contamos cuántos archivos realmente se subieron al sistema
            
            $validate_files = $this->validaActa($files, $errors);
            $uploadcount = $validate_files['uploadcount'];
            
            if($uploadcount == $file_count){
                //se subió todo satisfactoriamente
                return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'entrega']);
            }
            else {
                //ocurrieron errores.
                $errors = $validate_files['errors'];
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'entrega']);
            }
    }
    
    public function validaActa($files, $errors)
    {
        $uploadcount = 0;
        foreach($files as $file) {
            $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('import_file'=> $file), $rules);
                if($validator->passes()){
                    $destinationPath = 'directas';
                    $filename = $file->getClientOriginalName();
                    if($this->nombreDeArchivoValido($filename)) {
                        $upload_success = $file->storeAs($destinationPath, $filename);
                        $uploadcount ++;
                    } else {
                        $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta de entrega.';
                        $errors->add($filename, $error);
                    }
                } else {
                     $errors = $validator->errors();
                }
        }
        return $validate_files = [
            'validator' => $validator,
            'errors' => $errors,
            'uploadcount' => $uploadcount];
    }
    
     /**
         * Procesa la planilla de cargas de actas de entrega consolidadas
         * 
         * @var $request formulario de carga de hoja Excel. 
         * Cada archivo debe incluir en su nombre de archivo la fecha al final.
         * 
         * 
         * @return \Illuminate\Http\RedirectResponse view
         */
    
    public function procesoConsolidado(Request $request) {
       
        $request->validate([
            'import_file' => 'required'
        ]);
        
        $path = $request->file('import_file')->getRealPath();
        Excel::import(new BulkImport, $path);
        
        return redirect('carga')->with('success', 'All good!');
    }
    
     /**
         *  Descarga los datos que aparecen en pantalla en una planilla Excel
         * 
         *  @return Excel Planilla excel con la información de los dispositivos.
         */
    
    public function descargarPlanilla()
    {
       
        $fecha = date('d-m-Y H:i:s');
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        $devices = Device::with('modelo')->with('location')->with('service')->get();
        return (new FastExcel($devices))->download('devices-' . $fecha . '.xlsx', function($device){
            return [ 
                 'Id' => $device->id,
                 'Serial' => $device->serial,
                 'Fecha recepción' => $device->fecha_recepcion ? $device->fecha_recepcion->format('Y-m-d') : '',                 
                 'GD recepción' => $device->guia_recepcion,
                 'Modelo' => $device->modelo->name,
                 'Marca' => $device->modelo->brand->name,
                 'Rótulo' => $device->rotulo,
                 'Fecha instalación' => $device->fecha_instalacion ? $device->fecha_instalacion->format('Y-m-d') : '',
                 'POS' => $device->pos_id,
                 'Instalado' => $device->fecha_instalacion ? 'SI' : 'NO',
                 'Serie retirada' => $device->serial_prev,
                 'Ubicación' => $device->location->name,
                 'Técnico' => $device->service->name,
                 'Fecha reversa' => $device->fecha_reversa ? $device->fecha_reversa->format('Y-m-d') : '',
                 'GD dev. Reversa' => $device->guia_prev,
                 'Ticket' => $device->ticket
                 ];
                 
        });
        //return Excel::download(new EntregaExport, 'Dispositivos '.$fecha. '.xlsx');
    }
    
    /**
     * Valida que el nombre de archivo subido corresponda al formato:
     * 105_Acta_Entrega_POS_CO_ENTEL_30-10-2018_482978.xlsx
     * @param type $filename
     * @return boolean
     */
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode('_', $filename);
        if (count($arr) != 8){
            return false;
        }
        if(!is_numeric($arr[0])){
            return false;
        }
        if(strcasecmp($arr[1], 'Acta') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[2], 'Entrega') != 0 ) {
            return false;
        }
        if(!$this->validateDate($arr[6])) {
            return false;
        }
        if(!is_numeric(substr($arr[7], 0, -5))) {
            return false;
        }
        
        return true;
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

}
