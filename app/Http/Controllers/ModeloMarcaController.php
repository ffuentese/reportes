<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelo;
use App\Brand;
use App\Device;
use Validator;

class ModeloMarcaController extends Controller
{
    /*
     * Este controlador permite agregar o modificar modelos y marcas del sistema
     * Utiliza los modelos Modelo y Brand
     * 
     * @author <ffuentese@entel.cl>
     * 
     * 
     */
    
    function __construct() {
        
        $this->middleware('auth');

    }
    
    /**
         * Muestra en pantalla tablas de modelos y marcas
         * 
         * @return view
         */
    
    public function index()
    {
        
        $modelos = Modelo::all();
        $modelospag = Modelo::paginate(20, ['*'], 'modelos');
        $marcas = Brand::all();
        $marcaspag = Brand::paginate(20, ['*'], 'marcas');
        $data = [
            'modelos' => $modelos,
            'marcas' => $marcas,
            'modelospag' => $modelospag,
            'marcaspag' => $marcaspag
        ];
        return view('mantenedor.modelomarca', $data);
    }
    
    /**
         * Agrega marcas de dispositivos.
         * 
         * @var Request $request Recibe los datos del formulario como id y nombre
         */
    
    public function agregarMarca(Request $request)
    {
        
        $request->validate([
            'id' => 'required|alpha|max:3',
            'name' => 'required'
        ]);
        
        $marca = new Brand;
        $marca->id = $request['id'];
        $marca->name = $request['name'];
        $marca->save();
    }
    
    /**
         * Recibe los datos del modelo y los agrega
         * 
         * @var Request $request Datos del formulario como nombre y marca
         */
    
    public function agregarModelo(Request $request)
    {
        
        $request->validate([
            'id' => 'required|number',
            'name' => 'required|unique:models,name',
            'brand_id' => 'required'
        ]);
        
        $modelo = new Modelo;
        $modelo->id = $request['id'];
        $modelo->name = $request['name'];
        $modelo->brand_id = $request['brand_id'];
        $modelo->save();
    }
    
    /**
         * Lista todos los dispositivos asociados a un modelo.
         * 
         * @var Request $request Recibe el id del modelo.
         * 
         * @return view Vista con los datos.
         */
    
    public function verModelo(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'model_id' => 'required|numeric'
        ]);
        
        if ($validator->fails()) {
            return redirect('modelo')
                        ->withErrors($validator)
                        ->withInput();
        }
        $model_id = $request->input('model_id');
        $devices = Device::where('model_id', $model_id)
                ->where('customer_id', 1)
                ->paginate(15);
        $modelo = Modelo::find($model_id);
        $data = [
            'modelo' => $modelo,
            'devices' => $devices
        ];
        return view('mantenedor.vermodelos', $data);
    }
}
