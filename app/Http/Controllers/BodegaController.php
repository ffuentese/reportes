<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\BodegaImport;
use App\Device;
use Excel;

class BodegaController extends Controller
{
    /** Este controlador permite ingresar las planillas de bodega (IQ09)
     * 
     * Éste es el formato que debe cumplir el archivo:
     * $row[0] = Equipo (no se usa)
     * $row[1] = Material ($device->code)
     * $row[2] = Rótulo ($device->rotulo)
     * $row[3] = Serial ($device->serial)
     * $row[4] = Centro de costos ($device->cost_center)
     * $row[5] = Ubicación ($device->location_id)
     * $row[6] = Status ($device->status_id)
     * $row[7] = Descripción ($device->modelo)
     * $row[8] = Modificación 
     * $row[9] = Usuario que modifica
     * $row[10] = Status sistema (ALMA)
     * $row[11] = Fecha de creación en sistema
     * $row[12] = PEP
     * 
     * @author ffuentese <ffuentese@entel.cl>
     * 
     * 
     * 
     */
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    
    /**
         * Procesa el archivo Excel de bodega a través del importador BodegaImport.
         * Retorna redirección a vista de carga.
         * 
         * @author <ffuentese@entel.cl>
         * 
         * @param $request Datos del formulario. En este caso, archivo XLSX. 
         */
    
    public function procesoBodega(Request $request)
    {
        
        $request->validate([
            'import_file' => 'required'
        ]);
        
        $path = $request->file('import_file')->getRealPath();
        Excel::import(new BodegaImport, $path);
        
        return redirect('carga')->with('success', 'All good!')->withInput(['tab' => 'bodega']);
    }
    
    
    
}
