<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;

class EditController extends Controller
{
    /*
     * Este controlador permite editar ciclos de cada serial
     */
    
    public function editarDisp(Request $request)
    {
        $device = Device::where('id', $request->input('id'));
        $datos = [
            'rotulo' => $request->input('rotulo'),
            'guia_recepcion' => $request->input('guia_recepcion'),
            'fecha_recepcion' => $request->input('fecha_recepcion'),
            'guia_reversa' => $request->input('guia_reversa'),
            'fecha_reversa' => $request->input('fecha_reversa'),
            'location_id' => $request->input('location_id'),
            'pos_id' => $request->input('pos_id')
        ];
        $device->update($datos);
        
        $inp_serial = $request->input('serial');
        $devices = Device::where('serial', $inp_serial)->get();
        $data = [
            'devices' => $devices,
            'input_busqueda' => $inp_serial];
        return view('busqueda.result', $data);
    }
    
    
}
