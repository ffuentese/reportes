<?php

namespace App\Http\Controllers;

use App\Accesorio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Excel;
use Validator;
use Carbon\Carbon;
use App\Imports\AccesoriosImport;

class AccesorioController extends Controller
{
    function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $col_accesorios_entrega = DB::table('accesorios')->selectRaw('guia, sum(cantidad) as cantidad, estado, fecha')
                ->groupBy('guia', 'estado', 'fecha')
                ->where('estado', 'entrega')
                ->get();
        
        $col_accesorios_reversa = DB::table('accesorios')->selectRaw('guia, sum(cantidad) as cantidad, estado, fecha')
                ->groupBy('guia', 'estado', 'fecha')
                ->where('estado', 'reversa')
                ->get();
        
        $total_entregas = count($col_accesorios_entrega);
        $total_reversas = count($col_accesorios_reversa);
        $perPage = 10;
        
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        
        // El punto de partida u "offset"
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        // La colección se convierte en un arreglo y se "rebana" para que se pueda mostrar la info paginada.
        $entregas_sliced = array_slice($col_accesorios_entrega->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_accesorios_reversa->toArray(), $start_reversas, $perPage);
        // Con todo esto se crea el paginador
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        $data = [
            'entregas' => $entregas,
            'reversas' => $reversas
        ];
        
        return view('accesorios.index', $data);
        
    }
    
    public function guiasPorFecha(Request $request) {
        $request->validate([
            'inicio' => 'required|date',
            'final' => 'required|date'
        ]); 
        $inicio = $request->input('inicio');
        $final = $request->input('final');
        $col_accesorios_entrega = DB::table('accesorios')->selectRaw('guia, sum(cantidad) as cantidad, estado, fecha')
                ->groupBy('guia', 'estado', 'fecha')
                ->where('estado', 'entrega')
                ->whereDate('fecha', '>=', $inicio)
                ->whereDate('fecha', '<=', $final)
                ->get();
        
        $col_accesorios_reversa = DB::table('accesorios')->selectRaw('guia, sum(cantidad) as cantidad, estado, fecha')
                ->groupBy('guia', 'estado', 'fecha')
                ->where('estado', 'reversa')
                ->whereDate('fecha', '>=', $inicio)
                ->whereDate('fecha', '<=', $final)
                ->get();
        
        $total_entregas = count($col_accesorios_entrega);
        $total_reversas = count($col_accesorios_reversa);
        $perPage = 10;
        
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        
        // El punto de partida u "offset"
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        // La colección se convierte en un arreglo y se "rebana" para que se pueda mostrar la info paginada.
        $entregas_sliced = array_slice($col_accesorios_entrega->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_accesorios_reversa->toArray(), $start_reversas, $perPage);
        // Con todo esto se crea el paginador
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        $data = [
            'entregas' => $entregas,
            'reversas' => $reversas
        ];
        
        $html = view('partials.accesoriosfecha', $data)->render();
        return response()->json($html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = new \Illuminate\Support\MessageBag();
        if (!$request->import_file) {
            $error = 'No se subieron archivos.';
            $errors->add('no_files', $error);
            return redirect('carga')->withErrors($errors);
        }
        $files = $request->import_file;

        // Contamos los archivos del arreglo
        $file_count = count($files);

        // contamos cuántos archivos realmente se subieron al sistema

        $validate_files = $this->validateFiles($files, $errors);
        $uploadcount = $validate_files['uploadcount'];

        if($uploadcount == $file_count){
            //se subió todo satisfactoriamente
            return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'accesorios']);
        }
        else {
            //ocurrieron errores.
            $errors = $validate_files['errors'];
            return redirect('carga')->withErrors($errors)->withInput(['tab' => 'accesorios']);
        }
    
    }
    
    /**
     * Valida que cada archivo sea un XLSX y que además tenga el formato correcto
     * 
     * @param string $files
     * @param MessageBag $errors
     * @return Array $validate_files
     */
    
    public function validateFiles($files, $errors) {
        $uploadcount = 0;
        foreach($files as $file) {
            $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('import_file'=> $file), $rules);
                if($validator->passes()){
                    $destinationPath = 'accesorios';
                    $filename = $file->getClientOriginalName();
                    $provider = $this->provider($filename);
                    if($this->nombreDeArchivoValido($filename) && $provider) {
                        $file->storeAs($destinationPath, $filename);
                        Excel::import(new AccesoriosImport($provider), $file);
                        $uploadcount++;
                    } else {
                        $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta.';
                        $errors->add($filename, $error);
                    }
                } else {
                     $errors = $validator->errors();
                }
        }
        return $validate_files = [
            'validator' => $validator,
            'errors' => $errors,
            'uploadcount' => $uploadcount];
    }
    
    /**
     * Valida que el nombre de archivo subido corresponda al formato:
     * 105_Acta_Entrega_ACCESORIOS_CO_ENTEL_30-10-2018.xlsx
     * 97_Acta_Reversa_ACCESORIOS_CO_ENTEL_30-10-2018.xlsx
     * @param type $filename
     * @return boolean
     */
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode('_', $filename);
        if(!is_numeric($arr[0])){
            \Log::warning('Correlativo incorrecto no numérico: ' . $arr[0]);
            return false;
        }
        if(strcasecmp($arr[1], 'Acta') != 0 ) {
            \Log::warning('Acta no aparece: ' . $arr[1]);
            return false;
        }
        if(strcasecmp($arr[2], 'Entrega') == 0 || strcasecmp($arr[2], 'Reversa') == 0) {
            \Log::warning('Campo de estado incorrecto: '. $arr[2]);
            return true;
        } else {
            return false;
        }
        // Comprueba que haya una fecha
        if(!$this->validateDate($arr[6])) {
            \Log::warning('Fecha incorrecta: ' . $arr[6]);
            return false;
        }
        return true;
        
    }
    
    public function provider($filename) {
        $arr = explode('_', $filename);
        $provider_str = $arr[5] ?? null;
        if (!$provider_str) {
            \Log::warning('Proveedor incorrecto: ' . $arr[5]);
            return null;
        }
        $provider = \App\Provider::where('name', $provider_str)->first();
        return $provider;
    }
   
    
    
    public function validateDate($date_str) {
        try {
            Carbon::parse($date_str);
            return true;
        }
        catch (\Exception $e) {
            return false;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Accesorio  $accesorio
     * @return \Illuminate\Http\Response
     */
    public function show(Accesorio $accesorio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Accesorio  $accesorio
     * @return \Illuminate\Http\Response
     */
    public function edit(Accesorio $accesorio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Accesorio  $accesorio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Accesorio $accesorio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Accesorio  $accesorio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Accesorio $accesorio)
    {
        //
    }
}
