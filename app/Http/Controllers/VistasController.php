<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Device;
use App\GuiaIBM;
use App\Provider;
use App\Accesorio;
use App\Guia;
use App\GuiaIBMAccesorio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Excel;
use Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;

/**
 * Controlador que produce visualizaciones de distintos tipos de información
 */

class VistasController extends Controller
{
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    
    /**
     * Busca los dispositivos no justificados y los muestra en la vista.
     * @return \Illuminate\Http\Response
     */
    public function noJustificadas()
    {
        
        $subq = DB::table('devices')
                ->whereNotNull('fecha_recepcion')
                ->where(function($query){
                    $query->whereNotNull('fecha_reversa')
                          ->orWhereNotNull('fecha_instalacion');
                })
                ->select('serial')
                        ->get()->toArray();
        $series = [];
        foreach ($subq as $s) {
            $series[] = $s->serial;
        }
        $subq_resp_ibm = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->select('serial')
                ->groupBy('serial')->get()->toArray();
        $series_ibm = [];
        foreach ($subq_resp_ibm as $s) {
            $series_ibm[] = $s->serial;
        }
        $devices = DB::table('devices')
                ->join('models', 'models.id', '=', 'devices.model_id')
                ->whereNotNull('fecha_recepcion')
                ->whereNotIn('serial', $series)
                ->whereNotIn('serial', $series_ibm)
                ->selectRaw('devices.serial as serial, models.name as modelo, devices.guia_recepcion, max(devices.fecha_recepcion) as fecha_recepcion')
                ->groupBy('devices.serial', 'models.name', 'devices.guia_recepcion')
                ->orderBy('fecha_recepcion', 'asc')
                ->paginate();
        
        
        
        /**
         * SELECT SERIAL, models.NAME, MAX(fecha_recepcion)
FROM devices
JOIN models ON models.id = devices.model_id
WHERE fecha_recepcion IS NOT NULL 
AND fecha_reversa IS NULL
AND fecha_instalacion IS NULL
AND SERIAL NOT IN 
(
SELECT SERIAL
FROM devices 
WHERE fecha_recepcion IS NOT NULL
AND (fecha_reversa IS NOT NULL OR fecha_instalacion IS NOT NULL)
)
AND (models.brand_id = 'VEF' OR models.brand_id = 'CAS')
GROUP BY devices.SERIAL, models.name;
         */
        $data = ['devices' => $devices];
        
        return view("vista.result_nojust", $data);
    }
    
    public function exportNoJustificadas($query) {
        $messageBag = new MessageBag;
        try {
            return Excel::download(new \App\Exports\NoJustificadasExport(), 'Dispositivos no justificados ' . date('m-d-Y_hia') . '.xlsx');
        } catch (\Exception $e) {
            \Log::warning('Algo salió mal al intentar exportar: ' . $e);
            $messageBag->add('error', 'Algo salió mal al intentar exportar');
            return redirect()->back()->withErrors($messageBag, 'error');
        }
    }
    
    /**
     * Muestra formulario de búsqueda de equipos por guía.
     * También muestra las guías y un buscador por mes.
     * Utiliza una paginación manual por limitaciones de Eloquent.
     * 
     * @return Illuminate\Http\Response
     */
    
    public function buscarPorGuia(Request $request)
    {
        $col_entregas = DB::table('guias')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->whereNotNull('fecha')->where('estado', 'ENTREGA')->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $col_reversas = DB::table('guias')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->whereNotNull('fecha')->where('estado', 'REVERSA')->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $total_entregas = count($col_entregas);
        $total_reversas = count($col_reversas);
        // Tuve que hacer la paginación manualmente:
        $perPage = 12; // Entradas por página
        // Parámetros de número de página, uno para cada tabla.
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        // Cuando se carga la página desde cero los parámetros llegan en 0 así que los cambiamos a 1.
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        // El punto de partida u "offset"
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        // La colección se convierte en un arreglo y se "rebana" para que se pueda mostrar la info paginada.
        $entregas_sliced = array_slice($col_entregas->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_reversas->toArray(), $start_reversas, $perPage);
        // Con todo esto se crea el paginador
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        // Para el calendario
        // Las entregas diarias de Banco
        $diario_entregas = DB::table('guias')
                ->selectRaw('fecha, COUNT(guias.SERIAL) cantidad')
                ->where('estado', 'ENTREGA')
                ->groupBy('fecha')
                ->orderBy('fecha', 'asc')
                ->get();
        // Las reversas por día.
        $diario_reversas = DB::table('guias')
                ->selectRaw('fecha, COUNT(guias.SERIAL) cantidad')
                ->where('estado', 'REVERSA')
                ->groupBy('fecha')
                ->orderBy('fecha', 'asc')
                ->get();
        $diario = [];
        foreach($diario_entregas as $ent)
        {
            $diario[] = [
                'fecha' => Carbon::parse($ent->fecha),
                'cantidad' => $ent->cantidad,
                'tipo' => 'ENTREGA'
            ];
        }
        foreach($diario_reversas as $rev)
        {
            $diario[] = [
                'fecha' => Carbon::parse($rev->fecha),
                'cantidad' => $rev->cantidad,
                'tipo' => 'REVERSA'
            ];
        }
        
        $data = [
            'entregas' => $entregas,
            'reversas' => $reversas,
            'col_entregas' => $col_entregas,
            'col_reversas' => $col_reversas,
            'diario' => $diario
        ];
        return view("vista.buscar_por_guia", $data);
    }
    
    public function descargarTodasLasGuias() {
        $fecha = date('d-m-Y H:i:s');
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        $entrega = Guia::where('estado', 'entrega')->selectRaw("serial, modelo, tecnologia, guia, date_format(fecha, '%d-%m-%Y') as 'fecha gd', estado, obs as observaciones")->get();
        $reversa = Guia::where('estado', 'reversa')->selectRaw("serial, modelo, tecnologia, guia, date_format(fecha, '%d-%m-%Y') as 'fecha gd', estado, obs as observaciones")->get();
        $hojas = new SheetCollection([
            'Directas' => $entrega,
            'Reversas' => $reversa
        ]);
        return (new FastExcel($hojas))->download('Guias ' . $fecha . '.xlsx');
    }
    
    /**
     * Muestra formulario de búsqueda de equipos de IBM por guía
     * 
     * @return Illuminate\Http\Response
     */
    
    public function buscarPorGuiaIBM(Request $request)
    {
        $col_devices = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'IBM')->groupBy('year', 'month')->get();
        $col_reversas = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'REV')->groupBy('year', 'month')->get();
        $listado = DB::table('guia_i_b_ms')->select(DB::raw("year(fecha) as year, month(fecha) as month"))->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $total_entregas = count($col_devices);
        $total_reversas = count($col_reversas);
        // Tuve que hacer la paginación manualmente:
        $perPage = 12; // Entradas por página
        // Parámetros de número de página, uno para cada tabla.
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        // Cuando se carga la página desde cero los parámetros llegan en 0 así que los cambiamos a 1.
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        $entregas_sliced = array_slice($col_devices->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_reversas->toArray(), $start_reversas, $perPage);
        
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        $data = [
            'col_devices' => $col_devices,
            'col_reversas' => $col_reversas,
            'devices' => $entregas,
            'reversas' => $reversas,
            'listado' => $listado
        ];
        return view("vista.buscar_ibm", $data);
    }
    
    /**
     * Muestra formulario de búsqueda de accesorios por n° de guía y despliega un agregado de cantidades por mes
     * para que el usuario navegue por mes.
     * @return Illuminate\Http\Response
     */
    
    public function buscarPorGuiaAccesorios(Request $request)
    {
        $col_devices = DB::table('accesorios')->select(DB::raw("sum(cantidad) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'ENTREGA')->groupBy('year', 'month')->get();
        $col_reversas = DB::table('accesorios')->select(DB::raw("sum(cantidad) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'REVERSA')->groupBy('year', 'month')->get();
        $listado = DB::table('accesorios')->select(DB::raw("year(fecha) as year, month(fecha) as month"))->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $total_entregas = count($col_devices);
        $total_reversas = count($col_reversas);
        // Tuve que hacer la paginación manualmente:
        $perPage = 12; // Entradas por página
        // Parámetros de número de página, uno para cada tabla.
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        // Cuando se carga la página desde cero los parámetros llegan en 0 así que los cambiamos a 1.
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        $entregas_sliced = array_slice($col_devices->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_reversas->toArray(), $start_reversas, $perPage);
        
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        $data = [
            'col_devices' => $col_devices,
            'col_reversas' => $col_reversas,
            'devices' => $entregas,
            'reversas' => $reversas,
            'listado' => $listado
        ];
        return view("vista.buscar_accesorios", $data);
    }
    
    /**
     * Muestra formulario de búsqueda de accesorios IBM por n° de guía y despliega un agregado de cantidades por mes
     * para que el usuario navegue por mes.
     * @return Illuminate\Http\Response
     */
    
    public function buscarPorGuiaIBMAccesorios(Request $request)
    {
        $col_devices = DB::table('guia_i_b_m_accesorios')->select(DB::raw("sum(cantidad) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'ENTREGA')->groupBy('year', 'month')->get();
        $col_reversas = DB::table('guia_i_b_m_accesorios')->select(DB::raw("sum(cantidad) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'REVERSA')->groupBy('year', 'month')->get();
        $listado = DB::table('guia_i_b_m_accesorios')->select(DB::raw("year(fecha) as year, month(fecha) as month"))->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $total_entregas = count($col_devices);
        $total_reversas = count($col_reversas);
        // Tuve que hacer la paginación manualmente:
        $perPage = 12; // Entradas por página
        // Parámetros de número de página, uno para cada tabla.
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        // Cuando se carga la página desde cero los parámetros llegan en 0 así que los cambiamos a 1.
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        $entregas_sliced = array_slice($col_devices->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_reversas->toArray(), $start_reversas, $perPage);
        
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        $data = [
            'col_devices' => $col_devices,
            'col_reversas' => $col_reversas,
            'devices' => $entregas,
            'reversas' => $reversas,
            'listado' => $listado
        ];
        return view("vista.buscar_ibm_accesorios", $data);
    }
    
    /**
     * Retorna una tabla con paginación con los equipos que coinciden con una guía de recepción
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaRecepcion(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = Guia::where('guia', '=', $inp_guia)
                ->where('estado', '=', 'ENTREGA')
                ->paginate(15);
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'tipo' => 'entrega'];
        return view("vista.result_guia", $data);
        
        
    }
    
    /**
     * Retorna una tabla con paginación con los equipos que coinciden con una guía de reversa
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaReversa(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = Guia::where('guia', '=', $inp_guia)
                ->where('estado', '=', 'REVERSA')
                ->paginate(15);
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'tipo' => 'reversa'];
        return view("vista.result_guia", $data);
        
        
    }
    
    /**
     * Retorna una tabla con paginación con los equipos que coinciden con una guía de IBM
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaIBM(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = GuiaIBM::where('guia', '=', $inp_guia)
                ->paginate(15);
        
        $pdfguia = \App\DocGuiaIBM::where('guia', $inp_guia)->where('provider_id', 'IBM')->first();
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'pdf' => $pdfguia];
        return view("vista.result_guia_ibm", $data);
    }
    
    /**
     * Devuelve la guia de accesorios de acuerdo al n° de guía ingresado por el usuario.
     * @param Request $request "guia"
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaAccesorios(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = Accesorio::where('guia', '=', $inp_guia)
                ->paginate(15);
        
        $pdfguia = \App\DocGuiaIBM::where('guia', $inp_guia)->first();
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'pdf' => $pdfguia];
        return view("vista.result_guia_accesorios", $data);
    }
    
    /**
     * Devuelve la guia de accesorios de acuerdo al n° de guía ingresado por el usuario.
     * Para IBM
     * @param Request $request "guia"
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaIBMAccesorios(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = GuiaIBMAccesorio::where('guia', '=', $inp_guia)
                ->paginate(15);
        
        $pdfguia = \App\DocGuiaIBM::where('guia', $inp_guia)->first();
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'pdf' => $pdfguia];
        return view("vista.result_guia_ibm_accesorios", $data);
    }
    
    /**
     * Como buscarPorGuiaIBM recupera las guías pero por un mes determinado.
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porMesGuiaIBM(Request $request)
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $devices = DB::table('guia_i_b_ms')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_ms.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guia_i_b_ms.fecha', '=', $mes)
                ->whereYear('guia_i_b_ms.fecha', '=', $anio)
                ->where('guia_i_b_ms.estado', 'IBM')
                ->groupBy('guia_i_b_ms.guia', 'guia_i_b_ms.fecha')
                ->selectRaw('guia_i_b_ms.fecha, guia_i_b_ms.guia, count(guia_i_b_ms.serial) as cantidad, doc_guia_i_b_ms.guia as file')
                ->orderBy('guia_i_b_ms.fecha', 'desc')
                ->paginate(15);
        $reversas = DB::table('guia_i_b_ms')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_ms.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guia_i_b_ms.fecha', '=', $mes)
                ->whereYear('guia_i_b_ms.fecha', '=', $anio)
                ->where('guia_i_b_ms.estado', 'REV')
                ->groupBy('guia_i_b_ms.guia', 'guia_i_b_ms.fecha')
                ->selectRaw('guia_i_b_ms.fecha, guia_i_b_ms.guia, count(guia_i_b_ms.serial) as cantidad, doc_guia_i_b_ms.guia as file')
                ->orderBy('guia_i_b_ms.fecha', 'desc')
                ->paginate(15);
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'reversas' => $reversas
        ];
        return view('vista.result_guia_ibm_months', $data);
   
    }
    
    /**
     * Obtiene la cantidad de accesorios para el mes indicado en el formato MM-YYYY.
     * @param Request $request
     * @return Illuminate\Http\Response La vista
     */
    
    public function porMesGuiaAccesorios(Request $request)
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $devices = DB::table('accesorios')
                ->leftJoin('doc_guia_i_b_ms', 'accesorios.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('accesorios.fecha', '=', $mes)
                ->whereYear('accesorios.fecha', '=', $anio)
                ->where('accesorios.estado', 'ENTREGA')
                ->groupBy('accesorios.guia', 'accesorios.fecha')
                ->selectRaw('accesorios.fecha, accesorios.guia, sum(accesorios.cantidad) as cantidad, doc_guia_i_b_ms.guia as file')
                ->orderBy('accesorios.fecha', 'desc')
                ->paginate(15);
        $reversas = DB::table('accesorios')
                ->leftJoin('doc_guia_i_b_ms', 'accesorios.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('accesorios.fecha', '=', $mes)
                ->whereYear('accesorios.fecha', '=', $anio)
                ->where('accesorios.estado', 'REVERSA')
                ->groupBy('accesorios.guia', 'accesorios.fecha')
                ->selectRaw('accesorios.fecha, accesorios.guia, sum(accesorios.cantidad) as cantidad, doc_guia_i_b_ms.guia as file')
                ->orderBy('accesorios.fecha', 'desc')
                ->paginate(15);
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'reversas' => $reversas
        ];
        return view('vista.result_guia_acc_months', $data);
   
    }
    
    /**
     * Obtiene la cantidad de accesorios para el mes indicado en el formato MM-YYYY.
     * Lo mismo de porMesGuiaAccesorios pero para IBM.
     * @param Request $request
     * @return Illuminate\Http\Response La vista
     */
    
    public function porMesGuiaIBMAccesorios(Request $request)
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $devices = DB::table('guia_i_b_m_accesorios')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_m_accesorios.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guia_i_b_m_accesorios.fecha', '=', $mes)
                ->whereYear('guia_i_b_m_accesorios.fecha', '=', $anio)
                ->where('guia_i_b_m_accesorios.estado', 'ENTREGA')
                ->groupBy('guia_i_b_m_accesorios.guia', 'guia_i_b_m_accesorios.fecha')
                ->selectRaw('guia_i_b_m_accesorios.fecha, guia_i_b_m_accesorios.guia, sum(guia_i_b_m_accesorios.cantidad) as cantidad, doc_guia_i_b_ms.guia as file')
                ->orderBy('guia_i_b_m_accesorios.fecha', 'desc')
                ->paginate(15);
        $reversas = DB::table('guia_i_b_m_accesorios')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_m_accesorios.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guia_i_b_m_accesorios.fecha', '=', $mes)
                ->whereYear('guia_i_b_m_accesorios.fecha', '=', $anio)
                ->where('guia_i_b_m_accesorios.estado', 'REVERSA')
                ->groupBy('guia_i_b_m_accesorios.guia', 'guia_i_b_m_accesorios.fecha')
                ->selectRaw('guia_i_b_m_accesorios.fecha, guia_i_b_m_accesorios.guia, sum(guia_i_b_m_accesorios.cantidad) as cantidad, doc_guia_i_b_ms.guia as file')
                ->orderBy('guia_i_b_m_accesorios.fecha', 'desc')
                ->paginate(15);
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'reversas' => $reversas
        ];
        return view('vista.result_guia_ibm_acc_months', $data);
   
    }
    
    /**
     * Lista las guías de entrega por mes introducido por el usuario.
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porMesGuiaRecepcion(Request $request) 
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $provider = Provider::where('name', 'IBM')->first();
        $devices = Device::whereNotNull('devices.guia_recepcion')
                ->whereNotNull('devices.fecha_recepcion')
                ->leftJoin('doc_guia_i_b_ms', 'devices.guia_recepcion', '=', 'doc_guia_i_b_ms.guia')
                //->where('doc_guia_i_b_ms.provider_id', '<>', $provider->id)
                //->orWhereNull('devices.provider_id')
                ->whereMonth('devices.fecha_recepcion', '=', $mes)
                ->whereYear('devices.fecha_recepcion', '=', $anio)
                ->groupBy('devices.guia_recepcion', 'devices.fecha_recepcion')
                ->selectRaw('devices.guia_recepcion, count(devices.guia_recepcion) as cantidad, devices.fecha_recepcion, doc_guia_i_b_ms.guia as file')
                ->orderBy('fecha_recepcion', 'desc')
                ->paginate(15);
        $tipo = 'entrega';
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'tipo' => $tipo
        ];
                
        return view('vista.result_guia_months', $data);
    }
    
    /**
     * Lista las guías de reversa por mes introducido por el usuario
     *  
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porMesGuiaReversa(Request $request) 
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $provider = Provider::where('name', 'IBM')->first();
        $devices = Device::whereNotNull('devices.guia_reversa')
                ->whereNotNull('devices.fecha_reversa')
                ->leftJoin('doc_guia_i_b_ms', 'devices.guia_reversa', '=', 'doc_guia_i_b_ms.guia')
                //->where('doc_guia_i_b_ms.provider_id', '<>', $provider->id)
                //->orWhereNull('devices.provider_id')
                ->whereMonth('devices.fecha_reversa', '=', $mes)
                ->whereYear('devices.fecha_reversa', '=', $anio)
                ->groupBy('devices.guia_reversa', 'devices.fecha_reversa')
                ->selectRaw('devices.guia_reversa, count(devices.guia_reversa) as cantidad, devices.fecha_reversa, doc_guia_i_b_ms.guia as file')
                ->orderBy('fecha_reversa', 'desc')
                ->paginate(15);
        $tipo = 'reversa';
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'tipo' => $tipo
        ];
                
        return view('vista.result_guia_months', $data);
    }
    
    public function porMesGuiasAdexus(Request $request) {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $provider = Provider::where('name', 'IBM')->first();
        $qentregas = Guia::leftJoin('doc_guia_i_b_ms', 'guias.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guias.fecha', '=', $mes)
                ->whereYear('guias.fecha', '=', $anio)
                ->where('guias.estado', '=', 'ENTREGA')
                ->groupBy('guias.guia', 'guias.fecha')
                ->selectRaw('guias.guia, count(guias.guia) as cantidad, guias.fecha, doc_guia_i_b_ms.guia as file')
                ->orderBy('guias.fecha', 'desc');
        $total_entregas = $qentregas->get()->sum('cantidad');
        $entregas = $qentregas->paginate(15, ['*'], 'entregas');
        $qreversas = Guia::leftJoin('doc_guia_i_b_ms', 'guias.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guias.fecha', '=', $mes)
                ->whereYear('guias.fecha', '=', $anio)
                ->where('guias.estado', '=', 'REVERSA')
                ->groupBy('guias.guia', 'guias.fecha')
                ->selectRaw('guias.guia, count(guias.guia) as cantidad, guias.fecha, doc_guia_i_b_ms.guia as file')
                ->orderBy('guias.fecha', 'desc');
        $total_reversas = $qreversas->get()->sum('cantidad');
        $reversas = $qreversas->paginate(15, ['*'], 'reversas');
        $data = ['fecha' => $inp_fecha,
                'entregas' => $entregas,
                'reversas' => $reversas,
                'total_entregas' => $total_entregas,
                'total_reversas' => $total_reversas];
        return view('vista.result_guia_months', $data);
    }
    
    /**
     * Exporta como Excel el detalle de las series de la guía de entrega.
     * 
     * @param type $guia
     * @return type
     */
    
    public function exportRecepcion($guia) {
        $messageBag = new MessageBag;
        $device = Device::where('guia_recepcion', '=', $guia)->exists();
        if (!$device) {
            $messageBag->add('error', 'La guía no existe o su trazabilidad no ha sido procesada todavía.');
            return response()->json(['error' => 'La guía no existe o su trazabilidad no ha sido procesada todavía.'], 400);
        }
        try {
            return Excel::download(new \App\Exports\EntregaGuiaRecepcionExport($guia), 'Dispositivos GD Directa ' . $guia . '.xlsx');
        } catch (\Exception $e) {
            \Log::warning('Algo salió mal al intentar exportar GD directa ' . $guia . ' Error: ' . $e);
            $messageBag->add('error', 'Algo salió mal al intentar exportar GD Directa');
            return redirect()->back()->withErrors($messageBag, 'error');
        }
    }
    
    /**
     * Exporta como Excel el detalle de las series de la guía de reversa.
     * 
     * @param type $guia
     * @return type
     */
    
    public function exportReversa($guia) {
        $messageBag = new MessageBag;
        $device = Device::where('guia_recepcion', '=', $guia)->exists();
        if (!$device) {
            $messageBag->add('error', 'La guía no existe o su trazabilidad no ha sido procesada todavía.');
            return redirect()->back()->withErrors($messageBag, 'error');
        }
        try {
           return Excel::download(new \App\Exports\EntregaGuiaReversaExport($guia), 'Dispositivos GD Directa ' . $guia . '.xlsx');
        } catch (\Exception $e) {
            \Log::warning('Algo salió mal al intentar exportar GD reversa ' . $guia . ' Error: ' . $e);
            $messageBag->add('error', 'Algo salió mal al intentar exportar GD Reversa');
            return redirect()->back()->withErrors($messageBag, 'error');
        }
    }
    
    /**
     * Entrega el detalle por día de las reversas y entregas relacionadas con IBM
     * 
     * @param type $dia
     * @return Array $data 
     */
    
    public function porDiaIBM($dia) {
        $diario = DB::table('guia_i_b_ms')
                ->selectRaw('guia_i_b_ms.fecha, COUNT(guia_i_b_ms.SERIAL) cantidad, guia_i_b_ms.guia, guia_i_b_ms.estado, doc_guia_i_b_ms.guia  as file')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_ms.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereDate('fecha', $dia)
                ->groupBy('fecha', 'estado', 'guia')
                ->orderBy('fecha', 'asc')
                ->get();
        $total_entregas = DB::table('guia_i_b_ms')
                ->selectRaw('fecha, COUNT(guia_i_b_ms.SERIAL) cantidad, estado')
                ->whereDate('fecha', $dia)
                ->where('estado', 'IBM')
                ->groupBy('fecha', 'estado')
                ->get();
        $total_reversas = DB::table('guia_i_b_ms')
                ->selectRaw('fecha, COUNT(guia_i_b_ms.SERIAL) cantidad, estado')
                ->whereDate('fecha', $dia)
                ->where('estado', 'REV')
                ->groupBy('fecha', 'estado')
                ->get();
        $data = [
            'diario' => $diario,
            'total_entregas' => $total_entregas,
            'total_reversas' => $total_reversas,
            'fecha' => $dia
        ];
        return view('vista.result_guia_ibm_day', $data);
        
    }
    
    // Elimina guías Adexus (Entel - Banco)
    
    public function eliminarGuia(Request $request) {
        $validator = Validator::make($request->all(), [ 
            'guia' => 'required',
            'tipo' => 'required'
            ]
         );
        if ($validator->fails()) {
            return response(400)->json(['error', $validator->getMessageBag()->toArray()]);
        }
        $guia = (int)$request->input('guia');
        $tipo = $request->input('tipo');
        \Log::info('Guia' . $guia . ' Tipo: '. $tipo);
        if (strtoupper($tipo) === 'ENTREGA'){
            $borra_guia = Guia::where('guia', $guia)->where('estado', $tipo)->delete();
            $borra_ciclos = Device::where('guia_recepcion', $guia)->delete();
            if(!$borra_guia) {
                return response(400)->json(['error', 'La guía no se encontró.']);
            }
        } else if (strtoupper($tipo) === 'REVERSA') {
            $borra_guia = Guia::where('guia', $guia)->where('estado', $tipo)->delete();
            $devices = Device::where('guia_reversa', $guia)->get();
            if (!$borra_guia){
                return response(400)->json(['error', 'La guía no se encontró.']);
            }
            foreach($devices as $device)
            {
                if (!$device->fecha_recepcion) {
                    $device->delete();
                } else {
                    $device->guia_reversa = 0;
                    $device->fecha_reversa = null;
                    $device->save();
                }
            }
        } else {
            return response(400)->json(['error', 'La guía no se encontró.']);
        }
        return response(200);
        
    }

    
}
