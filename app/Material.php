<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Brand;

/**
 * El modelo contiene la tabla de códigos de material que permite identificar a los equipos por marca, propiedad, modelo, etc.
* La tabla asocia un código con un modelo.

* - id (es incremental)
* - material (es el código de material)
* - descripción (es el modelo  del equipo)
* - perfil (identifica si es seriado o no)
* - tipo (asocia un código a un tipo de equipo, por ejemplo PC = PC DE ESCRITORIO)

* Se carga a través de MaterialImport.
 */

class Material extends Model
{
    // Permite carga masiva
    protected $guarded = [];
    
    /**
     * Obtiene la marca del equipo (de existir) a partir del material
     * 
     * @return App\Brand|null
     */
    public function getMarcaAttribute() 
    {
        $material_arr = explode('.', $this->material);
        $id = $material_arr[2]; 
        $marca = Brand::find($id);
        return $marca;
    }
}
