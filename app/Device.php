<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pos;
use Illuminate\Support\Facades\DB;

class Device extends Model
{
    //
    protected $fillable = ['code', 'serial', 'rotulo', 'model_id', 
        'location_id', 'fecha_recepcion', 'guia_recepcion', 'fecha_instalacion',
        'fecha_reversa', 'guia_reversa', 'pep', 'status_id', 'serial_prev', 
        'reversa_prev', 'str_id', 'customer_id', 'technician_id', 'provider_id', 'obs'];
    
    protected $attributes = [
        'pos_id' => 0,
        'status_id' => 0,
        'cost_center' => 'CI00',
        'location_id' => 0,
        'user_id' => 1,
        'str_id' => 1,
        'pep' => '',    
        'code' => '',
        'modified_by' => 'Admin',
        'technician_id' => '1',
        'customer_id' => 1
    ]; 
    
    protected $dates = [
        'fecha_recepcion',
        'fecha_instalacion',
        'fecha_reversa',
        'reversa_prev'
    ];
    
    
    public function modelo()
    {
        return $this->belongsTo('App\Modelo', 'model_id');
    }
    
    /**
     * //El almacén o estatus de ubicación del equipo
     * @return App\Location
     */
    
    public function location()
    {
        
        return $this->belongsTo('App\Location', 'location_id');
    }
    
    /**
     * El almacén o estatus de ubicación del equipo
     * @return App\Service
     */
    
    public function service()
    {
        
        return $this->belongsTo('App\Service', 'str_id');
    }
    
    /**
     * El técnico a cargo del equipo si es el caso.
     * @return App\Technician
     */
    
    public function technician()
    {
        return $this->belongsTo('App\Technician', 'technician_id');
    }
    
    /**
     * La ubicación del pos en (relación de muchos a muchos).
     * @return App\Pos
     */
    
    public function pos()
    {
        return $this->belongsTo('App\Pos', 'pos_id');
    }
    
    /**
     * Obtiene el dispositivo retirado por el instalado 
     * a partir de la información de puntos vigentes.
     * @return App\Device
     */
    
    public function getPreviousDeviceAttribute()
    {
        if ($this->pos_id > 0 && !is_null($this->fecha_instalacion) && !is_null($this->fecha_recepcion) ){

            $device = Device::where('pos_id', $this->pos_id)
                    ->whereNotNull('fecha_instalacion')
                    ->where('fecha_instalacion', '<', $this->fecha_instalacion)
                    ->where('fecha_recepcion', '<', $this->fecha_recepcion)
                    ->where(function ($query){
                        $query->whereNotNull('fecha_reversa')
                               ->where('fecha_reversa', '>=', $this->fecha_instalacion);
                    })
                    ->orderBy('id', 'desc')
                    ->first();
            if (is_null($device)) {        
                $device = $this->buscarEnPVS();  
                if (is_null($device)) {
                    // por reutilizada
                    $device = $this->buscarReutilizada();
                }
            }
            return $device;
                    
        } else if ($this->pos_id > 0 && !is_null($this->fecha_instalacion) && is_null($this->fecha_recepcion) ){
            $device = $this->buscarEnPVS();
            if (!$device) {
                $device = $this->buscarReutilizada();
            }
            return $device;
        } else if (is_null($this->fecha_instalacion) && !is_null($this->fecha_reversa) && !is_null($this->fecha_recepcion)) {
            return $this->buscarReutilizada();
        } 
        else {
            return null;
        }
        
    }
    
    /**
     * Realiza una consulta en la tabla PVS para obtener el dispositivo retirado.
     * Si aparece en PVS, lo buscará en Device, pero si no lo encuentra lo crea con los datos que tiene.
     *  
     * @return \App\Device
     */
    
    public function buscarEnPVS()
    {
        if (!$this->fecha_instalacion){
            return null;
        }
        $q = DB::table('rel_p_v_s')
            ->where('pos_id', $this->pos_id)
            ->where('fecha', '<', $this->fecha_instalacion)
            ->orderBy('fecha', 'desc')
            ->first();
        if($q){
            $device = Device::where('serial', $q->serial)
                ->where('fecha_instalacion', $q->fecha)->first();
            if (!$device) {
                $device = new Device;
                $device->serial = $q->serial;
                $device->pos_id = $q->pos_id;
                $device->fecha_instalacion = $q->fecha;
                $device->model_id = $this->model_id;
                $device->save();
            }
            return $device;
        } else {
            return null;
        }
        
    }
    
    /**
     * En caso de que el dispositivo retirado no aparezca podemos revisar si el serial ha sido reutilizado.
     * En esa situación el dispositivo retirado es el mismo que el dispositivo provisionado.
     * 
     * @return App\Device 
     */
    
    public function buscarReutilizada()
    {
        if (!$this->fecha_instalacion) {
            return null;
        }
        $reutilizada = Device::where('serial', $this->serial)
                            ->whereNotNull('fecha_recepcion')
                            ->where('fecha_recepcion', '>', $this->fecha_instalacion)
                            ->first();
        if($reutilizada) {
            return $reutilizada;
        } else {
            return null;
        }
        
    }
    
    /**
     * Informa si el dispositivo anterior fue reversado
     * @return boolean
     */
    
    public function getReversadoAttribute()
    {
        if ($this->pos_id > 0){
            $device = $this->previous_device;
            //$device = Device::find($prev);
            if($device){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }
    
    // Si la misma serie ha sido provisionada después, se puede justiifcar esa serie.
    
    public function getReusadaAttribute() {
        if(is_null($this->fecha_recepcion)) {
            return false;
        }
        $reusada = Device::where('serial', '=', $this->serial)->where('fecha_recepcion', '>', $this->fecha_recepcion)->first();
        if($reusada) {
            return true;
        } 
        return false;
    }
}
