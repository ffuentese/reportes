<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Representa a un dato de punto vigentes sin procesar
 * 
 * Atributos:
 * 
 * - id incremental
 * - id del pos
 * - estado
 * - serial
 * - fecha del informe
 * 
 */

class Pv extends Model
{
    protected $guarded = ['id'];
    
    protected $dates = ['fecha'];
}
