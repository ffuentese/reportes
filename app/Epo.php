<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epo extends Model
{
    //
    protected $guarded = ['id'];
    protected $dates = [
        'fecha_carga', 'ultima_conexion'
    ];
}
