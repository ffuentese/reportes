<?php

namespace App\Exports;

use App\Dev_BCH;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class BCHExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {        
        $collection = collect();
        
        Dev_BCH::chunk(1000, function($devices) use ($collection){
            foreach($devices as $device) {
                $collection->push($device);
            }
        });
        return $collection;
    }

    public function headings(): array {
        return [
            'ALIAS',
            'RÓTULO',
            'SERIE',
            'TIPO',
            'MARCA',
            'MODELO',
            'NOMBRE',
            'RUT',
            'GUIA',
            'SAP',
            'FECHA INSTALACIÓN',
            'TICKET',
            'REGIÓN',
            'SITE',
            'FECHA DE REPORTE',
            'ÚLTIMA CONEXIÓN'
        ];
    }

    public function map($row): array {
        return [
            $row->alias,
            $row->rotulo,
            $row->serie,
            $row->tipo,
            $row->marca,
            $row->modelo,
            $row->nombre,
            $row->rut,
            $row->guia,
            $row->sap,
            $row->fecha_instalacion,
            $row->ticket,
            $row->region,
            $row->site,
            $row->fecha_reporte,
            $row->ultima_conexion
        ];
    }

    public function columnFormats(): array {
        return [
            'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'P' => NumberFormat::FORMAT_DATE_DDMMYYYY
        ];
    }

}
