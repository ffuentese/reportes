<?php

namespace App\Exports;

use Date;
use Maatwebsite\Excel\Concerns\FromCollection;
use \Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NoJustificadasExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // Buscando todas las series que por ciclo no están ordenadas.
        $subq = DB::table('devices')
                ->whereNotNull('fecha_recepcion')
                ->where(function($query){
                    $query->whereNotNull('fecha_reversa')
                          ->orWhereNotNull('fecha_instalacion');
                })
                ->select('serial')
                        ->get()->toArray();
        $series = [];
        foreach ($subq as $s) {
            $series[] = $s->serial;
        }
        
        // Sacando del conjunto a las series que son responsabilidad de IBM
        $subq_resp_ibm = DB::table('guia_i_b_ms')
                ->where('estado', 'IBM')
                ->select('serial')
                ->groupBy('serial')->get()->toArray();
        $series_ibm = [];
        foreach ($subq_resp_ibm as $s) {
            $series_ibm[] = $s->serial;
        }
        $devices = DB::table('devices')
                ->join('models', 'models.id', '=', 'devices.model_id')
                ->join('brands', 'brands.id', '=', 'models.brand_id')
                ->join('locations', 'locations.id', '=', 'devices.location_id')
                ->whereNotNull('fecha_recepcion')
                ->whereNotIn('serial', $series)
                ->whereNotIn('serial', $series_ibm)
                ->selectRaw('devices.id as id, devices.serial as serial, devices.guia_recepcion as guia, max(devices.fecha_recepcion) as fecha_recepcion, models.name as modelo, brands.name as marca, devices.rotulo as rotulo, devices.fecha_instalacion as fecha_instalacion, devices.pos_id as pos_id, devices.guia_prev as "guia_reversa", devices.serial_prev as "serie_anterior", devices.reversa_prev, devices.ticket as ticket ')
                ->groupBy('devices.id', 'devices.serial', 'devices.guia_recepcion', 'fecha_recepcion', 'models.name', 'brands.name', 'devices.rotulo', 'devices.fecha_instalacion', 'devices.pos_id', 'devices.guia_prev', 'devices.serial_prev', 'devices.reversa_prev', 'devices.ticket')
                ->orderBy('fecha_recepcion', 'asc')
                ->get();
        
        return $devices;
        
        
    }

    /**
     * Define los encabezados de cada columna 
     * @return array
     */
    

    public function headings(): array {
        // 'devices.id as id, devices.serial as serial, devices.guia_recepcion as guia, max(devices.fecha_recepcion) as fecha_recepcion, 
        // models.name as modelo, brands.name as marca, devices.rotulo as rotulo, 
        // devices.fecha_instalacion as fecha_instalacion, devices.pos_id as pos_id, devices.guia_prev as "guia_reversa", 
        // devices.serial_prev as "serie_anterior", devices.reversa_prev, devices.ticket as ticket
        return [
            'Id',
            'Serial',
            'GD Recepción',
            'Fecha recepción',
            'Modelo',
            'Marca',
            'Rotulo',
            'Fecha inst.',
            'Pos Id',
            'GD Reversa',
            'Serie retirada',
            'Fecha reversa',
            'Ticket'
        ];
    }
    
    /**
     * Define los datos de cada columna del documento
     * 
     * @param type $row
     * @return array
     */

    public function map($row): array {
         // 'devices.id as id, devices.serial as serial, devices.guia_recepcion as guia, max(devices.fecha_recepcion) as fecha_recepcion, 
        // models.name as modelo, brands.name as marca, devices.rotulo as rotulo, 
        // devices.fecha_instalacion as fecha_instalacion, devices.pos_id as pos_id, devices.guia_prev as "guia_reversa", 
        // devices.serial_prev as "serie_anterior", devices.reversa_prev, devices.ticket as ticket
        // fechas
        if (!empty($row->fecha_recepcion)) {
            $fecha_rec_carbon = Carbon::parse($row->fecha_recepcion);
            $fecha_recepcion = Date::dateTimeToExcel($fecha_rec_carbon);
        } else {
            $fecha_recepcion = "";
        }
        
        if (!empty($row->fecha_instalacion)) {
            $fecha_rec_inst = Carbon::parse($row->fecha_instalacion);
            $fecha_instalacion = Date::dateTimeToExcel($fecha_rec_inst);
        } else {
            $fecha_instalacion = "";
        }
        
        if (!empty($row->reversa_prev)) {
            $fecha_rec_reversa = Carbon::parse($row->reversa_prev);
            $fecha_reversa = Date::dateTimeToExcel($fecha_rec_reversa);
        } else {
            $fecha_reversa = "";
        }
        
        
        // Ahora sí:
        
        $data = [
            $row->id,
            $row->serial,
            $row->guia,
            $fecha_recepcion,
            $row->modelo,
            $row->marca,
            $row->rotulo,
            $fecha_instalacion,
            $row->pos_id,
            $row->guia_reversa,
            $row->serie_anterior,
            $fecha_reversa,
            $row->ticket
               
        ];
        
        return $data;
        
    }
    
    public function columnFormats(): array {
       return [
           'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
           'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
           'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
       ];
        
        
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                // Oculta columna de id, de acuerdo a lo solicitado.
                $event->sheet->getDelegate()->getColumnDimension('A')->setVisible(false);
            },
        ];
    }

}
