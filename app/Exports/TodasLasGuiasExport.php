<?php

namespace App\Exports;

use App\Guia;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Exports\GuiasExport;

class TodasLasGuiasExport implements WithMultipleSheets
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function sheets(): array {
         $sheets = [];
         $sheets[] = new GuiasExport('entrega');
         $sheets[] = new GuiasExport('reversa');
         return $sheets;
    }

}
