<?php

namespace App\Exports;

use Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Guia;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class GuiasExport implements FromCollection, WithMapping, WithHeadings, WithTitle, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $estado;

    public function __construct(string $estado)
    {
        $this->estado = $estado;
    }

    public function collection(): \Illuminate\Support\Collection {
        return Guia::where('estado', $this->estado)
                ->selectRaw("serial, modelo, tecnologia, guia, fecha, estado, obs as observaciones")
                ->get();
    }

    public function map($row): array {
        
        return [
            $row->serial,
            $row->modelo,
            $row->tecnologia,
            $row->guia,
            Date::dateTimeToExcel($row->fecha),
            $row->estado,
            $row->obs
        ];
    }

    public function headings(): array {
        return [
            'Serial',
            'Modelo',
            'Tecnología',
            'GD',
            'Fecha',
            'Estado',
            'Observaciones'
        ];
    }

    public function title(): string {
        if($this->estado === 'entrega') {
            return 'Directa';
        } else {
            return 'Reversa';
        }
    }

    public function columnFormats(): array {
        return [
            'E' => NumberFormat::FORMAT_DATE_DDMMYYYY
        ];
    }

}
