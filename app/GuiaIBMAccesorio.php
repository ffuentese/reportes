<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuiaIBMAccesorio extends Model
{
    //
    protected $guarded = ['id'];
    protected $dates = ['fecha'];
    
    public function modelo() {
        return $this->belongsTo('App\Modelo', 'modelo_id');
    }
}
