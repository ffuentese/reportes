<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Contiene la información de la tabla iq09s que refleja el inventario de bodega.
* Todos los datos excepto el id son *strings*.

* - id (incremental)
* - material
* - rotulo
* - serie
* - centro 
* - almacen (este es el mismo valor que aparece en location_id) 
* - tipo stock (similar a Status)
* - nombre (descripción del equipo)
* - modificado (fecha de modificación original en SAP)
* - status_sistema (string, por ejemplo, "ALMA")
* - pep (código contable)
* - creado (datetime, no confundir con created_at)
 */

class Iq09 extends Model
{
    //
    protected $guarded = [];
    
    protected $dates = [
        'modificado',
        'creado'
    ];
}
