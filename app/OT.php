<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\OTDigitalScope;

/**
 * Base de OTDigital en SQL Server
 * 
 * @author Francisco Fuentes <ffuentese@entel.cl>
 */

class OT extends Model
{
    /**
     * Especifica que conexión es a SQL Server 
     * (definida en .env y config/database.php)
     * 
     * @var string 
     */
    protected $connection = 'sqlsrv';
    
    /**
     * Tabla de origen de los datos
     * @var string 
     */
    protected $table = 'dbo.base';
    
    protected $dates = [
        'fecha_atencion'
    ];
    
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OTDigitalScope);
    }
    
    public function getSerieAttribute()
    {
        return preg_replace('/[^a-zA-Z0-9]/', '', $this->serie1);
    }
    
    public function getSerieReversaAttribute()
    {
        return preg_replace('/[^a-zA-Z0-9]/', '', $this->serie4); 
    }
    
}
