<?php

namespace App\DataTables;

use App\Dev_BCH;
use Yajra\DataTables\Services\DataTable;


class Dev_BCHDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Dev_BCH $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Dev_BCH $model)
    {
        return $model->newQuery()->select($this->getColumns());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'dom'          => 'Bfrtip',
                        'buttons' => ['excel', 'pdf']
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'rotulo',
            'serie',
            'tipo',
            'marca',
            'modelo',
            'nombre',
            'rut',
            'region',
            'site',
            'fecha_reporte',
            'ultima_conexion'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Dev_BCH_' . date('YmdHis');
    }
}
