<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Contiene el detalle de las guías de IBM separadas del detalle de cada serie en Device.
 * 
 * @author <ffuentese@entel.cl>
 * 
 */

class GuiaIBM extends Model
{
    //
    
    protected $fillable = ['serial', 'guia', 'fecha', 'modelo', 'tecnologia', 'estado', 'obs'];
    
    protected $dates = ['fecha'];
    
    /**
     * Nos permite acceder directamente y sin un join al modelo del equipo
     * Lo utilizamos en VistasController@porGuiaIBM
     * @return String
     */
    public function getModelNameAttribute() {
        return Device::where('serial', $this->serial)->first()->modelo->name;
    }
}
