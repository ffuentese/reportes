<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bch_employee;

/**
 * Este modelo almacena el contenido de una planilla de equipos llamada Sccm (cargado con SccmImport detallado en los importadores)

* - id (es incremental)
* - hostname (o alias)
* - model_id (refiere a Modelo)
* - chassis
* - serie
* - segmento (la ip)
* - username (depurado, sin el dominio)
* - site_id (refiere a Bch_location)
* - fecha_carga (es la fecha del documento que el usuario tiene que especificar)
* - ultima_conexion (fecha, denominada last_scan a veces) 
* - red (producción, QA, etc)
 */

class Sccm extends Model
{
    //
    protected $guarded = ['id'];
    protected $dates = [
        'fecha_carga', 'ultima_conexion'
    ];
    
    public function modelo() {
        return $this->belongsTo('App\Modelo', 'model_id');
    }
    
    public function location() {
        return $this->belongsTo('App\Bch_location', 'site_id');
    }
    
    public function getEmployeeAttribute() {
        return Bch_employee::where('username', $this->username)->first();
    }
    
}
