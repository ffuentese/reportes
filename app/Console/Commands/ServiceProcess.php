<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\ServiceImport;
use Illuminate\Support\Facades\Storage;
use Excel;

class ServiceProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:str';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Procesa las planillas de proveedores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Ejecuta el comando de consola.
     *
     * @return mixed
     */
    public function handle()
    {
        $directory = 'servicios';
        $files = Storage::allFiles($directory);
        \Log::info('Comienza a procesar planillas de técnicos');
        $bar =  $this->output->createProgressBar(count($files));
        $bar->start();
        $start = microtime(true);
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '1024M');
        foreach($files as $file)
        {
            Excel::import(new ServiceImport, $file);
            Storage::move($file, 'procesado/' . $file);
            $bar->advance();
        }
        $bar->finish();
        $time = microtime(true) - $start;     
        $msg = 'Proceso terminado en ' . $time . ' segundos.';
        \Log::info($msg);
        
        
    }
}
