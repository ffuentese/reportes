<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Excel;
use App\Imports\POSImport;
use App\Jobs\ProcessPreviousDevices;
use Illuminate\Support\Facades\Mail;
use App\Mail\TasksFinished;

class PosProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calcular:pos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso de todos los puntos vigentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $directory = 'pv';
        $files = Storage::allFiles($directory);
        \Log::info('Comienza proceso de Puntos vigentes.');
        $start = microtime(true);
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '1024M');
        foreach($files as $file)
        {
            $fname = basename($file);
            \Log::info('Procesando ',[$fname]);
            $arr = explode(" ", $fname);
            $day = substr($arr[2], 0, 10);
            $date = Carbon::parse($day);
            Excel::import(new POSImport($date), $file);
        }
        $time = microtime(true) - $start;
        $me = 'ffuentese@entel.cl';
        $msg = 'Proceso de puntos vigentes terminado en '. $time.' segundos.';
        $subject = 'proceso de cálculo de pos terminado.';
        Mail::to($me)->queue(new TasksFinished($msg, $subject));
        $this->call('calcular:previos', [
        '--queue' => 'default'
        ]);
        \Log::info('Proceso terminado.');
    }
}
