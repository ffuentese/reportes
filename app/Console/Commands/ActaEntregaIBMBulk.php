<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Excel;
use App\Imports\PlanillaRecIBMImport;

class ActaEntregaIBMBulk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:entregasibm';

    /**
     * Procesa las actas de entrega y las guarda en el modelo Device.
     *
     * @var string
     */
    protected $description = 'Procesa las entregas en bruto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $directory = 'directas_ibm';
        $files = Storage::allFiles($directory);
        \Log::info('Comienza a procesar actas de entrega de IBM');
        $bar =  $this->output->createProgressBar(count($files));
        $bar->start();
        $start = microtime(true);
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '1024M');
        foreach($files as $file)
        {
            $fname = basename($file);
            \Log::info('Procesando ',[$fname]);
            $arr = explode(" ", $fname);
            if ($arr[3] != 'IBM') {
                continue;
            }
            $day = substr($arr[4], 0, 10);
            $date = Carbon::parse($day);
            if (isset($arr[5])) {
                $guia = substr($arr[5], 0, -5);
            } else {
                $guia = 0;
            }
            Excel::import(new PlanillaRecIBMImport($date, $guia), $file);
            Storage::move($file, 'procesado/directas_ibm/' . basename($file, '.xlsx') . '-' . time() . '.xlsx');
            $bar->advance();
        }
        $bar->finish();
        $time = microtime(true) - $start;       
         \Log::info('Proceso terminado ' . 'en ' . $time);

    }
}
