<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\ImportPosDireccion;
use Illuminate\Support\Facades\Storage;
use Excel;

class AssigPosDireccion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:ubicacionpos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'asigna ubicación a cada pos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('Comienza proceso de ubicación de pos');
        $files = Storage::allFiles('pv');
        if(empty($files)){
            $msg = 'No hay archivos disponibles';
            \Log::warn($msg);
            return 0;
        }
        $file = end($files);
        if (!$file) {
            return;
        }
        try {
            Excel::import(new ImportPosDireccion, $file);
            $msg = 'La importación de direcciones terminó satisfactoriamente.';
            $this->info($msg);
            \Log::info($msg);
        } catch (\Exception $e) {
            $msg = "La importación no pudo completarse: " . $e->getCode() . " " . $e->getMessage();
            $this->warn($msg);
            \Log::warn($msg);
        }
    }
}
