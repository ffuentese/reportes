<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
Use Exception;
use App\Pos;
use App\Device;

class AssigPos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:asignarpos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consulta PVS para completar datos de pos e instalación';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        \Log::info('Proceso de importación de puntos vigentes iniciado.');
        $this->info('Comienza proceso de copia');
        $this->info('Consulta a PVS en progreso...');
        $devices = Device::whereNull('fecha_instalacion')->get();
        $bar =  $this->output->createProgressBar(count($devices));
        $start = microtime(true);
        ini_set('max_execution_time', 0);
        foreach($devices as $device)
        {
            $devs = DB::table('rel_p_v_s')
                    ->where('serial', $device->serial)
                    ->orderBy('fecha')->get();
            foreach($devs as $dev)
            {
                // para evitar fechas de instalación duplicadas
                $existeFechaInst = Device::where('serial', $device->serial)->where('fecha_instalacion', $dev->fecha)->exists();
                if ($existeFechaInst){
                    continue;
                }
                // verifica que fecha sea coherente con instalación de equipo
                if ($device->fecha_recepcion <= $dev->fecha && ($device->fecha_reversa >= $dev->fecha || is_null($device->fecha_reversa))){
                    $posn = $dev->pos_id;

                    $pos = Pos::where('id', $posn)->first();
                    if (!$pos) {
                        $pos = new Pos;
                        $pos->id = $posn;
                        $pos->vigente = true;
                        $pos->direccion = "";
                        $pos->save();
                    }
                        $device->pos_id = $pos->id;

                        $device->fecha_instalacion = $dev->fecha;
                        $device->save();
                        break;
                }
                
            }
            $bar->advance();
        }
        $bar->finish();
        $time = microtime(true) - $start;
        $this->info("\n Tarea terminada.");
        dump($time);
        \Log::info('Proceso terminado ' . 'en ' . $time);
        
    }
}
