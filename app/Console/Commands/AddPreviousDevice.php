<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Device;

class AddPreviousDevice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calcular:previos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agrega los campos del dispositivo previo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Agrega los dispositivos previos
        \Log::info('Comienza proceso de agregar dispositivos previos');
        $start = microtime(true);
        $devices  = Device::where('customer_id', 1)->where('pos_id',  '>', 0)->get();
        $bar =  $this->output->createProgressBar(count($devices));
        $bar->start();
        foreach ($devices as $device)
        {
            $previo = $device->previous_device;
            if (is_null($previo)){
                if (!is_null($device->serial_prev)) {
                    $device->serial_prev = null;
                    $device->reversa_prev = null;
                    $device->guia_prev = null;
                    $device->save();
                } 
                
            } else {
                $device->serial_prev = $previo->serial;
                $device->reversa_prev = $previo->fecha_reversa;
                $device->guia_prev = $previo->guia_reversa;
                $device->save();
            }
            
            $bar->advance();
        }
        $bar->finish();
        $time = microtime(true) - $start;
        $this->info("\n Tarea terminada en ");
        dump($time);
        \Log::info('Proceso terminado ' . 'en ' . $time);
    }
}
