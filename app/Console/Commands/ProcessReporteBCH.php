<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ReporteBCH;

class ProcessReporteBCH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:bch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Procesa los datos de Banco de Chile para generar inventario.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        \Log::info('Comienza creación reporte');
        $job = new ReporteBCH;
        
        $job->dispatch();
        
    }
}
