<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AddTicketToDevice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:addticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agrega ticket a Device cuando corresponde';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Busca una serie en la tabla devices y su serie retirada. De existir ambas las busca en ot_digital_s
     *
     * @return mixed
     */
    public function handle()
    {
        //
        \Log::info('Proceso de asociación de tickets iniciado.');
        $start = microtime(true);
        $devices = \App\Device::whereNotNull('serial_prev')->whereNull('ticket')->get();
        $bar =  $this->output->createProgressBar(count($devices));
        foreach($devices as $device) {
            $ot = \App\OTDigital::where('serie1', $device->serial)->where('serie4', $device->serial_prev)->select('ticket')->first();
            if($ot) {
                $device->ticket = $ot->ticket;
                $device->save();
            }
            $bar->advance();
        }
        $bar->finish();
        $time = microtime(true) - $start;
        $msg = 'Proceso de asociación de tickets terminado en ' . $time . ' segundos.';
        \Log::info($msg);
    }
}
