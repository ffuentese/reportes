<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Es importante que una cola esté funcionando para que la tarea funcione
        //          
        $schedule->job(new \App\Jobs\ProcessUpdate())->dailyAt('4:00');
        $schedule->job(new \App\Jobs\Actas())->hourlyAt(30)->between('8:30', '19:00');
        $schedule->job(new \App\Jobs\ReporteBCH())->weekly()->wednesdays();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
