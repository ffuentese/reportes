<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Este modelo está diseñado para registrar las guías/actas de Banco Estado
 * 
 * @author <ffuentese@entel.cl>
 */

class Guia extends Model
{
    
    protected $fillable = ['serial', 'guia', 'fecha', 'estado', 'modelo', 'tecnologia', 'obs'];
    
    protected $dates = ['fecha'];
    
    /**
     * Nos permite acceder directamente y sin un join al modelo del equipo
     * 
     * @return String
     */
    public function getModelNameAttribute() {
        return Device::where('serial', $this->serial)->first()->modelo->name;
    }
}
