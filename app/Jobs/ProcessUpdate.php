<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;
use App\Jobs\SendMailFinished;
use App\Jobs\SendMailFinishedWithErrors;
use App\Jobs\IQ09Process;

/**
 * Este trabajo ejecuta todos los comandos necesarios 
 * para actualizar las actas de entrega, de reversa y los puntos vigentes
 * 
 * @author <ffuentese@entel.cl>
 */

class ProcessUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Crea una nueva instancia de este trabajo 
     * no tiene parámetros.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Ejecuta el trabajo.
     *
     * @return void
     */
    public function handle()
    {
        // 1. Proceso de actas de entrega
        $entregas = Artisan::call('procesar:entregas');
        
        // Proceso de actas de reversa se gatilla si entregas funcionaron OK
        if($entregas != 0) {
            \Log::info('Proceso de entregas terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de entrega falló');
        }
        
        // Proceso de actas de entrega IBM
        $entregasibm = Artisan::call('procesar:entregasibm');
        
        // Proceso de actas de reversa se gatilla si entregas funcionaron OK
        if($entregasibm === 0) {
            \Log::info('Proceso de entregas ibm terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de entrega ibm falló');
        }
        
        // Proceso IQ09 que actualiza localización de dispositivos
        
        $job = new IQ09Process;
        $job->dispatch();
        
        
        // 3. Proceso de actas de reversa
        
        $reversas = Artisan::call('procesar:reversas');
        
        if($reversas === 0) {
            \Log::info('Proceso de reversas terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de reversas falló');
        }
        
        // 4. Proceso de reversas de IBM
        
        $reversasibm = Artisan::call('procesar:reversasibm');
        
        if($reversasibm == 0) {
            \Log::info('Proceso de reversas ibm terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de reversas ibm falló');
        }
        
        // 5. Proceso de actualización de PVS
        
        $pvs = Artisan::call('procesar:pvs');
        
        // Proceso de asignación de POS e instalaciones.
        if($pvs == 0) {
             \Log::info('Proceso de puntos vigentes terminó exitosamente.');
        } else {
            \Log::warning('Proceso de puntos vigentes falló');
        }
        
        // 6. Traspasa las fechas a RelPVS para hacer más rápida la consulta de $asignarpos a cont.
        
        $relpvs = Artisan::call('procesar:pvstorel');
        
        if($relpvs == 0) {
             \Log::info('Proceso de traspaso pvstorel terminó exitosamente.');
        } else {
            \Log::warning('Proceso de traspaso pvstorel falló');
        }
        
        $last_pvs = new LastPvsUpdater;
        $last_pvs->dispatch();
        
        // 7. Asigna dispositivo retirado 
        try {
            Artisan::call('procesar:ubicacionpos');
        } catch (\Exception $e) {
            \Log::warning('Proceso de asignación de direcciones a POS falló.' . $e->getMessage());
        }
        $asignarpos = Artisan::call('procesar:asignarpos');
        
        
        if($asignarpos == 0) {
            \Log::info('Proceso de asignación de disp retirado terminó exitosamente.');
        } else {
            \Log::warning('Proceso de asignación de disp retirado falló');
        }
        
        // 8. Dispositivos previos
        
        $previo = Artisan::call('calcular:previos');
        
        if ($previo == 0) {
            \Log::info('Proceso de cálculo de disps previos terminó exitosamente.');
        } else {
            \Log::warning('Proceso de cálculo de disps previos falló');
        }
        
        $addticket = Artisan::call('procesar:addticket');
        
        if ($addticket == 0) {
            \Log::info('Proceso de tickets desde OTDigital terminó exitosamente.');
        } else {
            \Log::warning('Proceso de tickets desde OTDigital falló');
        }
        
        // Si todo salió bien, va a enviarse un correo con un mensaje de éxito.
        
        if($entregas == 0 && $previo == 0 && $asignarpos == 0 
                && $pvs == 0 && $reversas == 0 && $relpvs == 0 && $addticket == 0 ) {
            \Log::info('Proceso de actualización terminó exitosamente.');
            //Va a enviar un correo electrónico notificando que el proceso finalizó.
            $msg = 'Proceso terminado';
            $subj = 'Procesamiento de planillas';
            $mailto = env('MAIL_TO');
            SendMailFinished::dispatch($msg, $subj, $mailto);
        } else {
            $sendmailfailed = new \App\Jobs\SendMailFinishedWithErrors;
            $sendmailfailed->dispatch();
            \Log::warning('Hubo algunos errores en el proceso de actualización.');
        }
        
    }
}
