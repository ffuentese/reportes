<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Sccm;
use App\Bginfo;
use App\Bch_employee;
use App\Zpec;
use App\Dev_BCH;
use App\Material_tipo;
use Carbon\Carbon;
use Exception;
use App\Jobs\ProcesaInv;
use App\Jobs\ProcesaSccm;
use App\Jobs\ProcesaBginfo;

/**
 * Construye el informe del Banco de Chile que se localiza en Dev_BCH (tabla dev__b_c_hs)
 * 
 * En primer lugar busca los valores que se encuentran en el modelo SCCM y descarta 
 * los que ya están en el informe y los que van a aparecer como retiro o venta.
 * 
 * Luego va completando líneas con los equipos que faltan. 
 */

class ReporteBCH implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    


    /**
     * Create a new job instance.
     * No hay que pasar ninguna variable.
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Busca todos los equipos del inventario, luego revisa el SCCM con sus relaciones y el bginfo
     *
     * @return void
     */
    public function handle()
    {
        $msg = 'Proceso de inventario BCH terminado. Ver ' . url('') . '/inventarioBCH';
        $subj = 'Proceso BCH';
        $mailto = env('MAIL_TO_BCH');
        try {
        ProcesaInv::withChain([
            new ProcesaSccm,
            new ProcesaBginfo, 
            new ProcesaEpo,
            new SendMailFinished($msg, $subj, $mailto)
        ])->dispatch();
        } catch (\Exception $e) {
            \Log::error($exception->getMessage());
        }
    }
    
    public function failed(\Exception $exception)
    {
        // Send user notification of failure, etc...
        \Log::error($exception->getMessage());
    }

}
