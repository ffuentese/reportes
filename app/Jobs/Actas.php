<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;

/**
 * Procesa todas las actas que se hayan subido. 
 */

class Actas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $entregas = Artisan::call('procesar:entregas');
        
        // Proceso de actas de reversa se gatilla si entregas funcionaron OK
        if($entregas != 0) {
            \Log::info('Proceso de entregas terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de entrega falló');
        }
        
        // Proceso de actas de entrega IBM
        $entregasibm = Artisan::call('procesar:entregasibm');
        
        // Proceso de actas de reversa se gatilla si entregas funcionaron OK
        if($entregasibm != 0) {
            \Log::info('Proceso de entregas ibm terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de entrega ibm falló');
        }
        
        // Proceso de actas de reversa
        
        $reversas = Artisan::call('procesar:reversas');
        
        if($reversas == 0) {
            \Log::info('Proceso de reversas terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de reversas falló');
        }
        
        // Proceso de reversas de IBM
        
        $reversasibm = Artisan::call('procesar:reversasibm');
        
        if($reversasibm == 0) {
            \Log::info('Proceso de reversas ibm terminó exitosamente.');
        } else {
            \Log::warning('Proceso de actas de reversas ibm falló');
        }
        
        // Proceso IQ09 que actualiza localización de dispositivos
        
        $job = new IQ09Process;
        $job->dispatch();
    }
}
