<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Device;
use App\Iq09;

class IQ09Process implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $inventario = Iq09::all();
        $count = 0;
        $arr = [];
        foreach ($inventario as $item) 
        {
            $device = Device::where('serial', '=', $item->serie)->whereNotNull('fecha_recepcion')
                    ->whereNull('fecha_reversa')->orderBy('fecha_recepcion', 'desc')->first();
            if (!$device) {
                continue;
            }
            $datos = [
                'code' => $item->material,
                'rotulo' => $item->rotulo,
                'location_id' => $item->almacen,
                'pep' => $item->pep
            ];
            $device->update($datos);
            $count++;
            $arr[] = $item->serie; 
        }
        Device::whereNotIn('serial', $arr)->where('location_id', '!=', 'IBM')->update(['location_id' => "0"]);
        \Log::info('Dispositivos modificados: ' . $count);
    }
}
