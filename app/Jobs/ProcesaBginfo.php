<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Sccm;
use App\Bginfo;
use App\Bch_employee;
use App\Zpec;
use App\Dev_BCH;
use App\Solped;
use Carbon\Carbon;
use App\Material_tipo;

class ProcesaBginfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Procesa el informe Bginfo para preparar el reporte de inventario de BCH.
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->procesaBginfo();
    }
    
    public function procesaBginfo() {
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        $users_zpec = array('RABELLEIRA', 'HCOTEIZA.ESE', 'EALVARADO');
        $users_carga_inicial = array('FBERNSTEIN', 'ZEXPRIMEGROU');
        $dataToInsert = [];
        $bginfos = Bginfo::with('modelo')->whereNotIn('serie', function($query) {
            $query->select('serie')->from('sccms');  
        })->whereNotIn('serie', function($query) { 
            $query->select('serie')->from('dev__b_c_hs');
        })
        ->get();
        
        // Recorre todos los equipos 
        
        foreach ($bginfos as $bginfo)
        {
//            if ($this->existe($bginfo)) {
//                continue;
//            }
            
            if ($this->excluir($bginfo)) {
                continue;
            }
            
            // busca tipo de equipo en inventario y ZPEC
            
            $chassis_mod = Dev_BCH::where('modelo', $bginfo->modelo->name)->first();
            $zpec_serie = Zpec::where('serie', $bginfo->serie)->orderBy('id', 'desc')->first();
            if($chassis_mod) {
                $chassis = strtoupper($chassis_mod->tipo);
            } else if ($zpec_serie){
                $material = explode('.',$zpec_serie->material);
                $cod_tipo = Material_tipo::where('code', $material[1])->first();
                switch ($cod_tipo->code) {
                    case 'PC':
                        $chassis = 'DESKTOP';    
                        break;
                    case 'NB':
                        $chassis = 'NOTEBOOK';
                         break;
                    case 'TB':
                        $chassis = 'TABLET';
                        break;
                    default:
                        $chassis = $cod_tipo->name;
                        break;
                }
            } else {
                $chassis = "SIN INFORMACION";
            }
            
            // Buscamos detalles de empleado (nombre y rut) en Bch_employees (tabla de personal BCH)
            // De no encontrarse allí lo busca en la tabla del inventario.
            
            $employee = Bch_employee::where('username', $bginfo->username)->first();
            $dev_previo = Dev_BCH::where('serie', $bginfo->serie)->first();
            if ($employee) {
                $employeeName = $employee->name;
                $employeeRut = $employee->rut;
            } else if ($dev_previo) {
                $employeeName = $dev_previo->nombre;
                $employeeRut = $dev_previo->rut;
            } else {
                $employeeName = "SIN INFORMACION";
                $employeeRut = "SIN INFORMACION";
            }
            
            // Buscamos site y región a partir de la tabla de segmentos.
            
            if ($this->isValidIP($bginfo->ip)){
                $ip_arr = explode('.', $bginfo->ip);
                $segmento = $ip_arr[0] . '.' . $ip_arr[1] . '.' . $ip_arr[2] . '.0';
                $loc = \App\Bch_ip::where('segmento', $segmento)->first();
            } else if($bginfo->ip != '') {
                if (strstr('_x000D_ ', $bginfo->ip)) {
                    $ips = explode('_x000D_ ', $bginfo->ip);
                    $ip_arr = explode('.', $ips[0]);
                    $segmento = $ip_arr[0] . '.' . $ip_arr[1] . '.' . $ip_arr[2] . '.0';
                    $loc = \App\Bch_ip::where('segmento', $segmento)->first();
                }
            }
            if ($loc) {
                $region = $loc->ciudad;
                $site = $loc->sucursal;
            } else {
                $region = "SIN INFORMACION";
                $site = "SIN INFORMACION";
            }
            
            // Busca el rótulo en el ZPEC
            
            $zpec = Zpec::where('serie', $bginfo->serie)->orderBy('id', 'desc')->first();
            if ($zpec) {
                $rotulo = $zpec->rotulo;
                $guia = $zpec->guia;
                $sap = $zpec->sap;
                $fecha_inst = $zpec->fecha;
                if ($guia == '' && $sap == '' && $fecha_inst) {
                    if (in_array($zpec->usuario, $users_zpec)) {
                        $guia = 'CARGA POR INVENTARIO';
                    } else if (in_array($zpec->usuario, $users_carga_inicial)) {
                        $guia = 'CARGA INICIAL';
                    }
                } else if ($guia == '' && $sap == '' && !$fecha_inst) {
                    $guia = 'CARGA POR HERRAMIENTA DE BUSQUEDA SEGÚN SCCM';
                }

            } else {
                $rotulo = "SIN INFORMACION";
                $guia = "";
                $sap = "";
                $fecha_inst = null;
            }
            
            if (strlen($sap) != 0) {
                $solped = \App\Traza::where('N_DOCUMENTO', $sap)->first();
                if ($solped) {
                    $ticket = $solped->TICKET;
                } else {
                    $ticket = "";
                }
            } else {
                $ticket = "";
            }

            
            // Unifica HEWLETT-PACKARD y HP en "HP"
            
            $marca = $bginfo->modelo->brand->name == "HEWLETT-PACKARD" ? "HP" : $bginfo->modelo->brand->name;
            
            // Completa los datos que van a la base.
            
            $data = [
                'alias' => $bginfo->hostname,
                'rotulo' => $rotulo,
                'serie' => $bginfo->serie,
                'marca' => $marca,
                'modelo' => $bginfo->modelo->name,
                'tipo' => $chassis,
                'nombre' => $employeeName,
                'rut' => $employeeRut,
                'guia' => $guia,
                'sap' => $sap,
                'fecha_instalacion' => $fecha_inst,
                'ticket' => $ticket,
                'region' => $region,
                'site' => $site,
                'fecha_reporte' => 'BGINFO ' . $bginfo->fecha_carga->format('d-m-Y'),
                'ultima_conexion' => $bginfo->fecha_conexion,
                'created_at' => $now,
                'updated_at' => $now
            ];
            $dataToInsert[] = $data;
        }
        $alias_unicos = $this->unique_multidim_array($dataToInsert, 'alias');
        $datos_unicos = $this->unique_multidim_array($alias_unicos, 'serie');
        $collection = collect($datos_unicos);
        // Copia la colección al reporte
        $collection->chunk(1000, function ($subset) {
            foreach ($subset->toArray() as $dev) {
                Dev_BCH::insert($dev);
            }
        });
    }
    
    /**
     * Verifica si existe el modelo en la base.
     * 
     * @param type $model
     * @return boolean
     */
    public function existe($model) {
        
            $dev = Dev_BCH::where('serie', $model->serie)->first();
            $retiro_venta = $this->checkZpecRetiroVenta($model->serie);
            // si el equipo existe lo busca en el ZPEC
            if ($dev) {
                
                 // Si está como 'RETIRO' o 'VENTA' lo quita del inventario.
                if($retiro_venta) {
                    $dev->delete();
                }
                
                return true;
            } else {
                if ($retiro_venta) {
                    return true;
                } else {
                    return false;
                }
            }
    }
    
    /**
     * Consulta si la serie se encuentra con operación retiro o venta.
     * @param type $serie
     * @return boolean
     */
    
    public function checkZpecRetiroVenta($serie) {
        $retiro_venta = Zpec::where('serie', $serie)
                        ->where(function ($query) {
                            $query->where('operacion', 'RETIRO')
                                    ->orWhere('operacion', 'VENTA');
                        })->exists();
        return $retiro_venta;
    }
    
    /**
     * Verifica si una cadena se encuentra al inicio de otra.
     * 
     * @param type $string
     * @param type $startString
     * @return int
     */
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 
    
    /**
     * Excluye a ciertos modelos del inventario que corresponden a máquinas virtuales, etc
     * @param type $model
     * @return boolean
     */
    
    public function excluir($model) {
        // ignora TCRs
        if ($this->startsWith($model->hostname, 'TCR') 
                || $this->startsWith($model->hostname, 'VM')
                || $this->startsWith($model->hostname, 'TRC') ) { 
            return true;
        }
        if ($this->startsWith($model->serie, 'VM')) {
            return true;
        }
        return false;
    }
    
/**
 * Verifica si una cadena es una ip válida o no.
 * 
 * @param $str la cadena a verificar
 * @return bool si la cadena $str es una ip válida o no
 */
    function isValidIP($str)
    {
        /**
         * Alternative solution using filter_var
         *
         * return (bool)filter_var($str, FILTER_VALIDATE_IP);
         */

        /**
         * Normally preg_match returns "1" when there is a match between the pattern and the provided $str.
         * By casting it to (bool), we ensure that our result is a boolean.
         * This regex should validate every IPv4 addresses, which is no local adress (e.g. 127.0.0.1 or 192.168.2.1).
         * This pattern was debugged using https://regex101.com/
         */
        return (bool)preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $str);
    }
    
    /**
     * Verifica que un arreglo no tenga una clave ($key) duplicada.
     * 
     * @param Array $array El arreglo original
     * @param string $key La clave que no tiene que duplicarse
     * @return Array El arreglo sin los elementos duplicados.
     */
    
    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 

        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
    
    /**
     * Devuelve usuario al recibir por parámetro un alias determinado.
     * 
     * @param Dev_BCH $dev
     * @return string|null
     */
    
    public function usuarioPorAlias($dev) {
        $sccm = Sccm::where('hostname', $dev->alias)->first();
        if ($sccm) {
            $username = $sccm->username;
        }
        if (isset($username) && $username != "") {
            return $username;
        } else {
            $bginfo = Bginfo::where('hostname', $dev->alias)->first();
            if ($bginfo) {
                $username = $bginfo->username;
            }
            if (isset($username) && $username != "") {
                return $username;
            }
        }
        return null;
    }
    
    /**
     * Notifica al usuario de una excepción.
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        // Send user notification of failure, etc...
        \Log::error($exception->getMessage());
    }
}
