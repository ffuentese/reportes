<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\TasksFinished;

class SendMailFinished implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 3;
    
    public $msg;
    
    public $subj;
    
    public $mailto;

    public function __construct($msg, $subj, $mailto)
    {
        //
        $this->msg = $msg;
        $this->subj = $subj;
        $this->mailto = $mailto;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Envia correo cuando termine cola.
        Mail::to($this->mailto)->queue(new TasksFinished($this->msg, $this->subj));
    }
}
