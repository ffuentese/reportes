<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Zpec;
use App\Solped;

class BCHZpecSolped implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $inventario = \App\Dev_BCH::whereNull('guia')->get();
        foreach ($inventario as $dev)
        {
            $zpec_col = Zpec::where('serie', $dev->serie)->select('id', 'guia', 'sap', 'fecha')->orderBy('id', 'desc')->first();
            if ($zpec_col) {
                $dev->guia = $zpec_col->guia;
                $dev->sap = $zpec_col->sap;
                $dev->fecha_instalacion = $zpec_col->fecha;
                $dev->ticket = "";
                if (strlen($dev->sap) != 0) {
                    $solped = \App\Solped::where('n_documento', $dev->sap)->first();
                    $dev->ticket = $solped ? $solped->TICKET : "";
                }
                $dev->save();
            } 
        }
    }
}
