<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Sccm;
use App\Bginfo;
use App\Bch_employee;
use App\Zpec;
use App\Dev_BCH;
use App\Material_tipo;
use Carbon\Carbon;
use Exception;

class ProcesaSccm implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Procesa los datos del SCCM para obtener el inventario
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->procesaSccm();
    }
    
    public function procesaSccm() {
        $sccm = Sccm::with('modelo')->with('location')->get();
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        $users_zpec = array('RABELLEIRA', 'HCOTEIZA.ESE', 'EALVARADO');
        $users_carga_inicial = array('FBERNSTEIN', 'ZEXPRIMEGROU');
        $dataToInsert = [];
        foreach ($sccm as $s) {
            if ($this->existe($s)) {
                continue;
            }
            
            if ($this->excluir($s)) {
                continue;
            }
            // buscamos el equipo en bginfo para obtener:
            // nombre de usuario
            // fecha de conexión.
            $bginfo = Bginfo::where('hostname', $s->hostname)->first();
            // Si el empleado no existía en SCCM lo buscamos en el bginfo
            if(!$s->employee) {
                if ($bginfo) {
                    $employeeName = $bginfo->username;
                }
            } else {
                $employeeName = $s->employee->name;
            }
            // Si el nombre del empleado no existe se escribe "SIN INFORMACIÓN"
            if(!isset($employeeName)) {
                $employeeName = "SIN INFORMACION";
            }
            $ultima_conexion = $s->ultima_conexion;
            // Si no estuviese el dato de la última conexión, lo busca en el bginfo
            if(!$ultima_conexion && $bginfo) {
                $ultima_conexion = $bginfo->fecha_conexion;
            }
            // Se asegura de que el empleado esté en la dotación de RRHH antes de buscarlo.
            if (!$s->employee) {
                $employeeRut = "SIN INFORMACION";
            } else {
                $employeeRut = $s->employee->rut;
            }
            // Verificando site (region y site)
            if (!$s->site_id) {
                $region = "SIN INFORMACION";
                $site = "SIN INFORMACION";
            } else {
                $region = $s->location->ciudad;
                $site = $s->location->sucursal;
            }
            
            // Busca el rótulo en el ZPEC 
            
            $zpec = Zpec::where('serie', $s->serie)->orderBy('id','desc')->first();
            if ($zpec) {
                $rotulo = $zpec->rotulo;
                $guia = $zpec->guia;
                $sap = $zpec->sap;
                $fecha_inst = $zpec->fecha;
                if ($guia == '' && $sap == '' && $fecha_inst) {
                    if (in_array($zpec->usuario, $users_zpec)) {
                        $guia = 'CARGA POR INVENTARIO';
                    } else if (in_array($zpec->usuario, $users_carga_inicial)) {
                        $guia = 'CARGA INICIAL';
                    }
                } else if ($guia == '' && $sap == '' && !$fecha_inst) {
                    $guia = 'CARGA POR HERRAMIENTA DE BUSQUEDA SEGÚN SCCM';
                }
                        
            } else {
                $rotulo = "SIN INFORMACION";
                $guia = "";
                $sap = "";
                $fecha_inst = null;
            }
            
            if (strlen($sap) != 0) {
                $solped = \App\Traza::where('N_DOCUMENTO', $sap)->first();
                if ($solped) {
                    $ticket = $solped->TICKET;
                } else {
                    $ticket = "";
                }
            } else {
                $ticket = "";
            }
            
            
            
            // Unifica HEWLETT-PACKARD y HP en "HP"
            
            $marca = $s->modelo->brand->name == "HEWLETT-PACKARD" ? "HP" : $s->modelo->brand->name;
            $marca = $s->modelo->brand->name == "DESCONOCIDO" ? "SIN INFORMACION" : $s->modelo->brand->name;
            $modelo = $s->modelo->name == "DESCONOCIDO" ? "SIN INFORMACION" : $s->modelo->name;
             
            $chassis = strtoupper($s->chassis);
            $data = [
                'alias' => $s->hostname,
                'rotulo' => $rotulo,
                'serie' => $s->serie,
                'marca' => $marca,
                'modelo' => $modelo,
                'tipo' => $chassis,
                'nombre' => $employeeName,
                'rut' => $employeeRut,
                'guia' => $guia,
                'sap' => $sap,
                'fecha_instalacion' => $fecha_inst,
                'ticket' => $ticket,
                'region' => $region,
                'site' => $site,
                'fecha_reporte' => 'SCCM ' . $s->fecha_carga->format('d-m-Y'),
                'ultima_conexion' => $ultima_conexion,
                'created_at' => $now,
                'updated_at' => $now
            ];
            // Añade a arreglo
            $dataToInsert[] = $data;
        }
        $alias_unicos = $this->unique_multidim_array($dataToInsert, 'alias');
        $datos_unicos = $this->unique_multidim_array($alias_unicos, 'serie');
        $collection = collect($datos_unicos);
        // Copia al reporte todas las filas.
        $collection->chunk(1000, function ($subset) {
            foreach ($subset->toArray() as $dev) {
                Dev_BCH::insert($dev);
            }
        });
    }
    
    /**
     * Determina si un dato de SCCM ya está registrado.
     * @param type $model
     * @return boolean
     */
    public function existe($model) {
        
            $dev = Dev_BCH::where('serie', $model->serie)->first();
            $retiro_venta = $this->checkZpecRetiroVenta($model->serie);
            // si el equipo existe lo busca en el ZPEC
            if ($dev) {
                
                 // Si está como 'RETIRO' o 'VENTA' lo quita del inventario.
                if($retiro_venta) {
                    $dev->delete();
                    return true;
                }
                
                return true;
            } else {
                if ($retiro_venta) {
                    return true;
                } else {
                    return false;
                }
            }
    }
    
    /**
     * Consulta si la serie se encuentra con operación retiro o venta.
     * @param type $serie
     * @return boolean
     */
    
    public function checkZpecRetiroVenta($serie) {
        $retiro_venta = Zpec::where('serie', $serie)
                        ->where(function ($query) {
                            $query->where('operacion', 'RETIRO')
                                    ->orWhere('operacion', 'VENTA');
                        })->exists();
        return $retiro_venta;
    }
    
    /**
     * Verifica si una cadena se encuentra al inicio de otra.
     * 
     * @param type $string
     * @param type $startString
     * @return int
     */
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 
    
    public function excluir($model) {
        // ignora TCRs
        if ($this->startsWith($model->hostname, 'TCR') 
                || $this->startsWith($model->hostname, 'VM')
                || $this->startsWith($model->hostname, 'TRC') ) { 
            return true;
        }
        if ($this->startsWith($model->serie, 'VM')) {
            return true;
        }
        return false;
    }
    
/**
 * Verifica si una cadena es una ip válida o no.
 * 
 * @param $str la cadena a verificar
 * @return bool si la cadena $str es una ip válida o no
 */
    function isValidIP($str)
    {
        /**
         * Alternative solution using filter_var
         *
         * return (bool)filter_var($str, FILTER_VALIDATE_IP);
         */

        /**
         * Normally preg_match returns "1" when there is a match between the pattern and the provided $str.
         * By casting it to (bool), we ensure that our result is a boolean.
         * This regex should validate every IPv4 addresses, which is no local adress (e.g. 127.0.0.1 or 192.168.2.1).
         * This pattern was debugged using https://regex101.com/
         */
        return (bool)preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $str);
    }
    
    /**
     * Verifica que un arreglo no tenga una clave ($key) duplicada.
     * 
     * @param Array $array El arreglo original
     * @param string $key La clave que no tiene que duplicarse
     * @return Array El arreglo sin los elementos duplicados.
     */
    
    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 

        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
    
    /**
     * Devuelve usuario al recibir por parámetro un alias determinado.
     * 
     * @param Dev_BCH $dev
     * @return string|null
     */
    
    public function usuarioPorAlias($dev) {
        $sccm = Sccm::where('hostname', $dev->alias)->first();
        if ($sccm) {
            $username = $sccm->username;
        }
        if (isset($username) && $username != "") {
            return $username;
        } else {
            $bginfo = Bginfo::where('hostname', $dev->alias)->first();
            if ($bginfo) {
                $username = $bginfo->username;
            }
            if (isset($username) && $username != "") {
                return $username;
            }
        }
        return null;
    }
    
    /**
     * Notifica al usuario de una excepción.
     * @param \Exception $exception
     * @return void
     */
    
    public function failed(\Exception $exception)
    {
        // Send user notification of failure, etc...
        \Log::error($exception->getMessage());
    }
    
}
