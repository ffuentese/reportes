<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Epo;
use App\Dev_BCH;

class ProcesaEpo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
    
    public function procesaEpo() {
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        $epos = Epo::leftJoin('bch_employees', function($join) {
            $join->on('epos.usuario', '=', 'bch_employees.username')
                  ->where('epos.usuario', '!=', '');  
            })
            ->leftJoin('bch_ips', 'bch_ips.segmento', '=', DB::raw("concat(SUBSTRING_INDEX(epos.ip, '.', 3), '.0')"))
            ->whereNotIn('hostname', function($query) {
                $query->select('hostname')->from('sccms');  
            })->whereNotIn('hostname', function($query) { 
                $query->select('hostname')->from('bginfos');
            })
            ->get();
        
        $dev_bch = Dev_BCH::select('alias')->get()->pluck('alias')->toArray();
        $actualizar = $epos->whereIn('hostname', $dev_bch);
        $agregar = $epos->whereNotIn('hostname', $dev_bch);
        foreach($actualizar as $epo) 
        {
            $data = [
                'alias' => $epo->hostname,
                'rotulo' => 'SIN INFORMACION',
                'serie' => 'SIN INFORMACION',
                'marca' => 'SIN INFORMACION',
                'modelo' => 'SIN INFORMACION',
                'tipo' => $epo->portatil = 'No' ? 'DESKTOP' : 'NOTEBOOK' ,
                'nombre' => $epo->name,
                'rut' => $epo->rut,
                'guia' => 'SIN INFORMACION',
                'sap' => 'SIN INFORMACION',
                'fecha_instalacion' => '',
                'ticket' => 'SIN INFORMACION',
                'region' => $epo->ciudad,
                'site' => $epo->sucursal,
                'fecha_reporte' => 'EPO ' . $epo->fecha_carga->format('d-m-Y'),
                'ultima_conexion' => $epo->ultima_conexion,
                'created_at' => $now,
                'updated_at' => $now
            ];
            $epo->update($data);
        }
        
        foreach($agregar as $epo) 
        {
            $data = [
                'alias' => $epo->hostname,
                'rotulo' => 'SIN INFORMACION',
                'serie' => 'SIN INFORMACION',
                'marca' => 'SIN INFORMACION',
                'modelo' => 'SIN INFORMACION',
                'tipo' => $epo->portatil = 'No' ? 'DESKTOP' : 'NOTEBOOK' ,
                'nombre' => $epo->name,
                'rut' => $epo->rut,
                'guia' => 'SIN INFORMACION',
                'sap' => 'SIN INFORMACION',
                'fecha_instalacion' => '',
                'ticket' => 'SIN INFORMACION',
                'region' => $epo->ciudad,
                'site' => $epo->sucursal,
                'fecha_reporte' => 'EPO ' . $epo->fecha_carga->format('d-m-Y'),
                'ultima_conexion' => $epo->ultima_conexion,
                'created_at' => $now,
                'updated_at' => $now
            ];
            $epo->create($data);
        }
        
        
        
        
        
    }
    
    /**
 * Verifica si una cadena es una ip válida o no.
 * 
 * @param $str la cadena a verificar
 * @return bool si la cadena $str es una ip válida o no
 */
    function isValidIP($str)
    {
        /**
         * Alternative solution using filter_var
         *
         * return (bool)filter_var($str, FILTER_VALIDATE_IP);
         */

        /**
         * Normally preg_match returns "1" when there is a match between the pattern and the provided $str.
         * By casting it to (bool), we ensure that our result is a boolean.
         * This regex should validate every IPv4 addresses, which is no local adress (e.g. 127.0.0.1 or 192.168.2.1).
         * This pattern was debugged using https://regex101.com/
         */
        return (bool)preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $str);
    }
}
