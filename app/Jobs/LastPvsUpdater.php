<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class LastPvsUpdater implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        DB::table('last_pvs')->truncate();
        $maxfecha = DB::table('pvs')->max('fecha');
        $str = "INSERT INTO last_pvs
            (SELECT *
            FROM pvs WHERE fecha = '" . $maxfecha . "'
            AND estado = 'VIGENTE') 
            ON DUPLICATE KEY update last_pvs.serial = pvs.serial";
        DB::statement($str);
    }
}
