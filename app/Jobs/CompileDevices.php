<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;
use \Illuminate\Support\Facades\Artisan;
use App\Guia;
use App\Device;

/**
 * Regenera todo el modelo Device (tabla de trazabilidad de series) a partir 
 * de las guías. Lo único que necesita es que las guías tengan la información correcta 
 * y se puede regenerar cada vez que sea necesario.
 * 
 * @author <ffuentese@entel.cl>
 */

class CompileDevices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Crea un respaldo de la tabla y si es exitoso:
     * 1. Trunca la tabla 
     * 2. Toma los datos desde las guías y los agrega a la tabla generando una trazabilidad.
     *
     * @return void
     */
    
    public function handle()
    {
        //  insert devuelve 0 si el respaldo funciona correctamente.
        $backup = $this->insertStatements();
        if ($backup === 0) {
            \App\Device::truncate();
            $this->agregaDirectas();
            $this->agregaReversas();
            // Desde los puntos vigentes ingresados
            $this->agregaInstalacion();
        } else {
            logger('CompileDevices: No se pudo crear el respaldo. No se continúa.');
        }
        
    }
    
    /**
     * Ingresa en Device todas las series provenientes de actas de directas
     */
    
    public function agregaDirectas() {
        $directas = Guia::where('estado', 'entrega')->get();
        
        foreach ($directas as $device) 
        {
            $model_id = \App\Modelo::where('name', $device->modelo)->first() ?? \App\Modelo::where('id', 'UNC')->first();            
            $datos = [
                'serial' => $device->serial,
                'guia_recepcion' => $device->guia,
                'fecha_recepcion' => $device->fecha,
                'provider_id' => 1,
                'model_id' => $model_id->id,
                'obs' => $device->obs  
            ];
            try {
                Device::create($datos);    
            } catch (\Exception $exc) {
                logger($exc->getMessage());
            }
             
        }
        
    }
    
    /**
     * Crea todos los datos relacionados con actas de reversa.
     * Si existe un dispositivo ingresado como directa, lo actualiza. De no existir lo crea.
     */
    
    public function agregaReversas() {
        $reversas = Guia::where('estado', 'reversa')->get();
        foreach ($reversas as $device) 
        {
            $model_id = \App\Modelo::where('name', $device->modelo)->first() ?? \App\Modelo::where('id', 'UNC')->first();            
            $datos = [
                'serial' => $device->serial,
                'guia_reversa' => $device->guia,
                'fecha_reversa' => $device->fecha,
                'provider_id' => 1,
                'model_id' => $model_id->id,
                'obs' => $device->obs  
            ];
            $equipo = Device::where('serial', $device->serial)
                        ->whereNotNull('fecha_recepcion')
                        ->whereNull('fecha_reversa')
                        ->where('fecha_recepcion', '<', $datos['fecha_reversa'])
                        ->orderBy('fecha_recepcion', 'asc')
                        ->first();
            if (!$equipo)  {
                try {
                    Device::create($datos);   
                } catch (\Exception $exc) {
                    logger($exc->getMessage());
                }
            } else {
                try {
                    $equipo->update($datos);
                } catch (\Exception $exc) {
                    logger($exc->getMessage());
                }

                
            }
        }
    }
    
    /**
     * Ejecuta el comando que asigna las ubicaciones de POS así como las fechas de instalación
     * 
     * 
     */
    
    public function agregaInstalacion() {
        Artisan::call('procesar:asignarpos');
    }
    
    /**
     * Crea un CSV con todos los dispositivos existentes
     */
    
    public function respaldaDevices() {
        $model = Device::get();
        $csvExport = new \Laracsv\Export();
        $campos = ['serial', 'code', 'rotulo', 'model_id', 
        'location_id', 'fecha_recepcion', 'guia_recepcion', 'fecha_instalacion',
        'fecha_reversa', 'guia_reversa', 'pep', 'status_id', 'serial_prev', 
        'reversa_prev', 'str_id', 'customer_id', 'technician_id', 'provider_id', 'obs'];
        $content = $csvExport->build($model,$campos)->getWriter()->getContent();
        $date = date('d-m-Y H-i-s');
        Storage::disk('public')->put('respaldo-devices-' . $date . '.csv', $content);
    }
    
    /**
     * Crea un respaldo de los datos de la tabla devices 
     * @return int La respuesta de mysqldump. 0 significa que el comando funcionó exitosamente.
     */
    
    public function insertStatements() {
        $process = new Process(
                    "mysqldump " . sprintf('-u%s', getenv('DB_USERNAME')) . " ".  " " . "reportes " . "devices > storage\app\devices-".time().".sql --no-create-info"
        );
        return $process->run();
        
    }
}
