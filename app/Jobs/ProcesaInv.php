<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Sccm;
use App\Bginfo;
use App\Bch_employee;
use App\Zpec;
use App\Dev_BCH;
use App\Traza;
use Carbon\Carbon;

class ProcesaInv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Procesa el inventario anterior de BCH
     *
     * @return void
     */
    public function handle()
    {
        //Va a probar si la conexión funciona
        Log::info('Intentando procesar inventario');
        $this->procesaInventario();
        Log::info('Terminando de procesar inventario');
    }
    
    public function procesaInventario() {
        Log::info('Procesando...');
        $inventario = Dev_BCH::all();
        $users_zpec = array('RABELLEIRA', 'HCOTEIZA.ESE', 'EALVARADO');
        $users_carga_inicial = array('FBERNSTEIN', 'ZEXPRIMEGROU');
        
        foreach ($inventario as $dev) 
        {
            // Revisa si el equipo está en retiro o venta para eliminarlo.
            if ($this->checkZpecRetiroVenta($dev->serie)) {
                $dev->delete();
                continue;
            }
            // Busca el rótulo si no lo tuviera en el ZPEC.
            if ($dev->rotulo == "SIN INFORMACION") {
                $zpec_rotulo = Zpec::where('serie', $dev->serie)->orderBy('id','desc')->first();
                if ($zpec_rotulo) {
                    $dev->rotulo = $zpec_rotulo->rotulo;
                    $dev->guia = $zpec_rotulo->guia;
                    $dev->sap = $zpec_rotulo->sap;
                    $dev->fecha_instalacion = $zpec_rotulo->fecha;
                }
                // Busca el SAP y el nro de ticket en la Solped.
                if (strlen($dev->sap) > 0 ) {
                    $solped = Traza::where('N_DOCUMENTO', $dev->sap)->orderBy('id', 'desc')->first();
                    if ($solped) {
                        $dev->ticket = $solped->TICKET;
                    } else {
                        $dev->ticket = "";
                    }
                }
            } else {
                $zpec_rotulo = Zpec::where('serie', $dev->serie)->orderBy('id','desc')->first();
                if ($zpec_rotulo) {
                    $dev->guia = $zpec_rotulo->guia;
                    $dev->sap = $zpec_rotulo->sap;
                    $dev->fecha_instalacion = $zpec_rotulo->fecha;
                }
                // Busca el SAP y el nro de ticket en la Solped.
                if (strlen($dev->sap) > 0 ) {
                    try{
                        $solped = Traza::where('N_DOCUMENTO', $dev->sap)->orderBy('id', 'desc')->first();
                    } catch (\Exception $exception) {
                        \Log::warning('algo salió mal al consultar solped.');
                    }
                    if ($solped) {
                        $dev->ticket = $solped->TICKET;
                    } else {
                        $dev->ticket = "";
                    }
                }
            }
            
            $doc = explode(' ', $dev->fecha_reporte);
            // agregamos fechas
            if($doc[0] === 'SCCM') {
                $sccm = Sccm::where('serie', $dev->serie)->first();
                if($sccm) {
                    
                    $dev->fecha_reporte = 'SCCM ' . $sccm->fecha_carga->format('d-m-Y');
                    $dev->ultima_conexion = $sccm->ultima_conexion;
                }
            } else if ($doc[0] === 'BGINFO') {
                
                $bginfo = Bginfo::where('serie', $dev->serie)->first();
                if($bginfo) {
                    $dev->fecha_reporte = 'BGINFO ' . $bginfo->fecha_carga->format('d-m-Y');
                    $dev->ultima_conexion = $bginfo->fecha_conexion;
                }
            }
            // En la columna GD catalogar como “CARGA POR INVENTARIO” si el usuario es RABELLEIRA, HCOTEIZA.ESE, EALVARADO.  
            // FBERNSTEIN y ZEXPRIMEGROU (Carga Inicial)
           if (strlen($dev->guia) === 0 && strlen($dev->sap) === 0 && $dev->fecha_instalacion) {
               \Log::info('Catalogando por falta de datos');
                $zpec = Zpec::where('serie', $dev->serie)->orderBy('id','desc')->first();
                    if (in_array($zpec->usuario, $users_zpec)) {
                        $dev->guia = 'CARGA POR INVENTARIO';
                        
                    } else if (in_array($zpec->usuario, $users_carga_inicial)) {
                        $dev->guia = 'CARGA INICIAL';
                        
                    }
            } else if (strlen($dev->guia) === 0 && strlen($dev->sap) === 0  && !$dev->fecha_instalacion) {
                
                //Si en GD y fecha de instalación figuran vacías, catalogar como “CARGA POR HERRAMIENTA DE BUSQUEDA SEGÚN SCCM/BGINFO” 
                $dev->guia = 'CARGA POR HERRAMIENTA DE BUSQUEDA SEGÚN ' . $doc[0];
                
            }
            
            
            // si no tenía el nombre lo buscamos en la nómina de empleados
            if ($dev->nombre == "SIN INFORMACION") {
                $username = $this->usuarioPorAlias($dev);
                if ($username) {
                    $emp = Bch_employee::where('username', $username)->first();
                }
                if (isset($emp)) {
                    $dev->nombre = $emp->name;
                    $dev->rut = $emp->rut;
                    //$dev->save();
                    
                }
            }
            $dev->save();
           
        }
        
        
    }
    
    /**
     * Consulta si la serie se encuentra con operación retiro o venta.
     * @param type $serie
     * @return boolean
     */
    
    public function checkZpecRetiroVenta($serie) {
        $retiro_venta = Zpec::where('serie', $serie)
                        ->where(function ($query) {
                            $query->where('operacion', 'RETIRO')
                                    ->orWhere('operacion', 'VENTA');
                        })->exists();
        return $retiro_venta;
    }
    
    /**
     * Verifica si una cadena se encuentra al inicio de otra.
     * 
     * @param type $string
     * @param type $startString
     * @return int
     */
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }
    
    /**
     * Devuelve usuario al recibir por parámetro un alias determinado.
     * 
     * @param Dev_BCH $dev
     * @return string|null
     */
 
    public function usuarioPorAlias($dev) {
        $sccm = Sccm::where('hostname', $dev->alias)->first();
        if ($sccm) {
            $username = $sccm->username;
        }
        if (isset($username) && $username != "") {
            return $username;
        } else {
            $bginfo = Bginfo::where('hostname', $dev->alias)->first();
            if ($bginfo) {
                $username = $bginfo->username;
            }
            if (isset($username) && $username != "") {
                return $username;
            }
        }
        return null;
    }
    
    /**
     * Notifica al usuario de una excepción.
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        // Send user notification of failure, etc...
        \Log::error($exception->getMessage());
    }
}
