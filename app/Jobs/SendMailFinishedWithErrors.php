<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\TasksFinished;

class SendMailFinishedWithErrors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 3;
    
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Envia correo cuando termine cola.
        $me = env('MAIL_TO');
        $host = gethostname();
        $ip = gethostbyname($host);
        $url = 'https://' . $ip . '/logs';
        $msg = 'Proceso terminó con algunos errores. Por favor revise el log de la aplicación en '. $url;
        $subj = 'Procesamiento de planillas [ERROR]';
        Mail::to($me)->queue(new TasksFinished($msg, $subj));
    }
}
