<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    /*
     * Modelo representa a técnicos individuales
     */
    
    
    public function service()
    {
        return $this->hasOne('App\Service');
    }
    
}
