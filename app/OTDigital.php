<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo OTDigital
 * (OT viene desde una base externa, OTDigital tiene datos locales y se carga desde un Excel)
 * 
 * @author Francisco Fuentes <ffuentese@entel.cl>
 */

class OTDigital extends Model
{
    
    protected $dates = ['fecha_atencion'];
    protected $guarded = ['id'];
    
    /**
     * Accesor retorna modelo de equipo provisionado en mayúsculas
     * @return string
     */
    public function getModeloAttribute()
    {
        return strtoupper($this->marca1);
    }
    
    /**
     * Accesor retorna modelo de equipo retirado en mayúsculas
     * @return string
     */
    public function getModeloOutAttribute()
    {
        return strtoupper($this->marca4);
    }
    
}
