<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Representa un item del inventario del Banco de Chile.
 * 
 * - id (incremental)
* - rotulo (string)
* - serie (string)
* - tipo (string)
* - marca (string)
* - modelo (string)
* - nombre (string)
* - rut (string)
* - region  (string)
* - site  (string)
 */

class Dev_BCH extends Model
{
    //
    
    protected $table = 'dev__b_c_hs';
    protected $guarded = ['id'];
    protected $dates = ['fecha_instalacion', 'ultima_conexion'];
    
}
