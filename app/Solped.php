<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solped extends Model
{
    //
    protected $connection = 'gestionactivos';
    protected $table = 'solped';
}
