<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{
    //
    public function region()
    {
        
        return $this->belongsTo('App\Region', 'region_id');
    }
    
    public function getNombreAttribute() {
        return ucfirst(strtolower($this->name));
    }
}
