<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /* Este modelo representa a las bodegas
    /* Este id es un string basado en los códigos de SAP así 
     * que no emplea el autoincrementals
     */
    public $incrementing = false;
    
    protected $attributes = [
        'id' => 0,
        'name' => 'SIN ALMACÉN'
    ];
}
