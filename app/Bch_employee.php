<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Contiene la nómina de empleados de BCH
 * 
 * Los atributos que contiene son:
 * 
 * Este modelo contiene la nómina de empleados de Banco de Chile. 
* - id (incremental)
* - name (nombre del empleado)
* - username ( nombre de usuario, por ejemplo, el que aparece en su login de windows y en su correo)
* - email 
* - rut (string)
* - direccion
 */

class Bch_employee extends Model
{
    //
    protected $guarded = ['id'];
    
    
}
