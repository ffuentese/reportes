<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Device;

/**
 * Este modelo representa a los modelos de equipo
* y está relacionado directamente con Brand (las marcas)
*/

class Modelo extends Model
{
    
    protected $table = 'models';
    protected $guarded = ['id'];
    
    public function brand()
    {        
        return $this->belongsTo('App\Brand');
    }
    
    public function getCountAttribute()
    {
        /*
         * Cuenta las series únicas asociadas a este modelo.
         */
        
        $count = Device::where('model_id', $this->id)
                ->where('customer_id', 1)
                ->distinct('serial')
                ->count('serial');
        return $count;
    }
    
    public function getTechAttribute() {
        if ($this->brand_id == 'CAS' || $this->brand_id == 'VEF') {
            return TechPos::where('model_id', $this->id)->first();
        } else {
            return null;
        }
    }
}
