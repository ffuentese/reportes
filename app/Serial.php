<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Device;

/**
 * Devuelve series únicas.
 */

class Serial extends Model
{
    // Se relaciona con Modelo
    public function modelo() {
        return $this->belongsTo('App\Modelo', 'model_id');
    }
    
    /**
     * Retorna los ciclos en que aparece en el modelo Device 
     * $this->cycles
     * @return \Illuminate\Support\Collection Ciclos en que aparece esta serie en Device
     */
    public function getCyclesAttribute() {
        return Device::where('serial', $this->serial)->get();
    }
}
