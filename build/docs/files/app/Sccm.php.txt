<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bch_employee;

class Sccm extends Model
{
    //
    protected $guarded = ['id'];
    protected $dates = [
        'fecha_carga', 'ultima_conexion'
    ];
    
    public function modelo() {
        return $this->belongsTo('App\Modelo', 'model_id');
    }
    
    public function location() {
        return $this->belongsTo('App\Bch_location', 'site_id');
    }
    
    public function getEmployeeAttribute() {
        return Bch_employee::where('username', $this->username)->first();
    }
    
}

