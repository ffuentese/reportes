<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bginfo extends Model
{
    //
    protected $guarded = ['id'];
    protected $dates = [
        'fecha_conexion',
        'fecha_carga'
    ];
    
    public function modelo() {
        return $this->belongsTo('App\Modelo', 'model_id');
    }
}

