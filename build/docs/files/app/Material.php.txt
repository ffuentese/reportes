<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Brand;

class Material extends Model
{
    // Permite carga masiva
    protected $guarded = [];
    
    /**
     * Obtiene la marca del equipo (de existir) a partir del material
     * 
     * @return App\Brand|null
     */
    public function getMarcaAttribute() 
    {
        $material_arr = explode('.', $this->material);
        $id = $material_arr[2]; 
        $marca = Brand::find($id);
        return $marca;
    }
}

