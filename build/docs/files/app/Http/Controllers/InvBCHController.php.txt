<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ImportDev_BCH;
use App\Imports\BginfoImport;
use App\Imports\SccmImport;
use App\Imports\BCH_EmployeeImport;
use App\Exports\BCHExport;
use App\Dev_BCH;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Excel;
use Validator;

    /**
         * Procesa el archivo Excel del inventario del BCH a través del importador ImportDev_BCH.
         * Retorna redirección a vista de carga.
         * 
         * @author <ffuentese@entel.cl>
         * 
         * @param \Illuminate\Http\Request $request
         */

class InvBCHController extends Controller
{

    /**
     * Muestra la vista donde se desplegará la tabla. 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {
        $ultima_act = Dev_BCH::max('updated_at');
        return view('bch.index')->with('ultima_act', $ultima_act);
    }
    
    /**
     * Muestra una vista que contiene un formulario para cargar el archivo CSV.
     * @return \Illuminate\Http\Response 
     */
    public function carga()
    {
        return view('bch.carga');
    }
    
    /**
     * Retorna los datos de la tabla.
     * Utiliza el paquete yajra/datatables.
     * @return \Illuminate\Http\Response 
     */
    public function getDatatable()
    {
        //$devs = DB::table('dev__b_c_hs')->select(['id', 'alias', 'rotulo', 'serie', 'tipo', 'marca', 'modelo', 'nombre', 'rut', 'region', 'site', 'fecha_reporte', 'ultima_conexion']);
        return Datatables::of(Dev_BCH::query())->make(true);
    }
   
    /**
     * Procesa la subida del archivo CSV
     * 
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function proceso(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimetypes:text/plain'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'csv']);
        }
        
        $path = $request->file('import_file')->getRealPath();
        Dev_BCH::truncate();
        Excel::import(new ImportDev_BCH, $path);
        
        return redirect('cargaInventarioBCH')
                ->with('success', 'Carga exitosa!')->withInput(['tab' => 'csv']);
    }
    
    /**
     * Procesa el archivo de BGinfo para la conf. del reporte BCH
     * @param Request $request
     */
    
    public function procesoBginfo(Request $request) {
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx',
            'fecha' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'bginfo']);
        }
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'BGINFO';
        $fecha = $request->fecha;
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Bginfo::truncate();
            Excel::import(new BginfoImport($fecha), $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de BGINFO exitosa!')
                    ->withInput(['tab' => 'bginfo']);
        } else {
            $error = 'El archivo ' . $path . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'bginfo']);
        }
    }
    
    /**
     * Procesa el archivo de SCCM para el reporte BCH
     * @param Request $request
     */
    
    public function procesoSCCM(Request $request) {
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx',
            'fecha' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'sccm']);
        }
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'SCCM';
        $fecha = $request->fecha;
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Sccm::truncate();
            Excel::import(new SccmImport($fecha), $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de SCCM exitosa!')->withInput(['tab' => 'sccm']);
        } else {
            $error = 'El archivo ' . $path . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'sccm']);
        }
    }
    
    /**
     * Procesa el archivo de RRHH (Dotación)
     * @param Request $request
     */
    
    public function procesoRRHH(Request $request) {
        $errors = new \Illuminate\Support\MessageBag();
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xlsx'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput(['tab' => 'rrhh']);
        }
        
        $file = $request->file('import_file');
        $path = $file->getRealPath();
        $type = 'Dotacion';
        $filename = $file->getClientOriginalName();
        if ($this->validateFile($filename, $type)) {
            \App\Bch_employee::truncate();
            Excel::import(new BCH_EmployeeImport, $path);
            return redirect('cargaInventarioBCH')->with('success', 'Carga de Dotación exitosa!')
                    ->withInput(['tab' => 'rrhh']);
        } else {
            $error = 'El archivo ' . $filename . ' tiene un nombre no válido.';
            $errors->add($type, $error);
            return redirect()->back()->withErrors($errors)->withInput(['tab' => 'rrhh']);
        }
    }
    
    public function export() 
    {
        ini_set('max_execution_time', 0);
        $fecha = date('d-m-Y H:i:s');
        $filename = 'inventarioBCH_'. $fecha .'.xlsx';
        return Excel::download(new BCHExport, $filename);
    }
    
    public function validateFile($filename, $type) {
        $arr = explode('_', $filename);
        $fecha = substr($arr[1], 0, -5);
        if($arr[0] != $type) {
            return false;
        }
        if (!is_numeric($fecha)) {
            return false;
        }
        
        return true;
    }
    
}

