<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\ActaRecImport;
use App\Imports\PlanillaAdxRevImport;
use Carbon\Carbon;
use App\Imports\BulkRevImport;
use Validator;
use App\GuiaIBM;

class ReversaIBMController extends Controller
{
    /**
     * Recibe las actas de reversa, tanto individual (por guía) como consolidada
     * 
     * @author Francisco Fuentes <ffuentese@entel.cl>
     */
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    
    public function procesoPlanilla(Request $request)
    {       
            $errors = new \Illuminate\Support\MessageBag();
            if (!$request->import_file) {
                $error = 'No se subieron archivos.';
                $errors->add('no_files', $error);
                return redirect('carga')->withErrors($errors);
            }
            $files = $request->import_file;
            
            // Making counting of uploaded images
            $file_count = count($files);

            // start count how many uploaded
            $uploadcount = 0;
            
            foreach($files as $file) {
                $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('import_file'=> $file), $rules);
                    if($validator->passes()){
                        $destinationPath = 'reversas_ibm';
                            $filename = $file->getClientOriginalName();
                            if($this->nombreDeArchivoValido($filename)) {
                                $file->storeAs($destinationPath, $filename);
                                $uploadcount ++;
                            } else {
                                $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un acta de reversa.';
                                $errors->add($filename, $error);
                            }
                    } else {
                         $errors = $validator->errors();
                    }
            }

            if($uploadcount == $file_count){
                //uploaded successfully
                return redirect('carga')->with('success', 'Todos los archivos se subieron exitosamente.')->withInput(['tab' => 'reversa-ibm']);
            }
            else {
                //error occurred
                return redirect('carga')->withErrors($errors)->withInput(['tab' => 'reversa-ibm']);
            }
    }
    
    /**
     * El formato en este caso es 
     * Formato: [Correlativo] [Acta Reversa IBM] [Fecha en formato dd-mm-yyyy] [n° guía].[xlsx]
     * 060 Acta Reversa IBM 07-01-2019 11265.xlsx
     * Por lo tanto, el orden de los datos en el nombre es muy importante.
     * 
     * @param type $filename
     * @return boolean
     */
   
    
    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 6){
            return false;
        }
        if(!is_numeric($arr[0])){
            return false;
        }
        if(strcasecmp($arr[1], 'Acta') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[2], 'Reversa') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[3], 'IBM') != 0 ) {
            return false;
        }
        if(!$this->validateDate($arr[4])) {
            return false;
        }
        if(!is_numeric(substr($arr[5], 0, -5))) {
            return false;
        }
        
        
        return true;
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
}

