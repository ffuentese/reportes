<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\GuiaIBM;
use App\Provider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Excel;

/**
 * Controlador que produce visualizaciones de distintos tipos de información
 */

class VistasController extends Controller
{
    
    function __construct() {
        
        $this->middleware('auth');
    
    }
    
    /**
     * Busca los dispositivos no justificados y los muestra en la vista.
     * @return \Illuminate\Http\Response
     */
    public function noJustificadas()
    {
        $devices = Device::whereNull('serial_prev')
                ->whereNotNull('fecha_recepcion')
                ->whereNull('fecha_instalacion')
                ->whereNull('fecha_reversa')->paginate(15);
        $data = ['devices' => $devices];
        
        return view("vista.result", $data);
    }
    
    /**
     * Muestra formulario de búsqueda de equipos por guía.
     * También muestra las guías y un buscador por mes.
     * Utiliza una paginación manual por limitaciones de Eloquent.
     * 
     * @return Illuminate\Http\Response
     */
    
    public function buscarPorGuia(Request $request)
    {
        $col_entregas = DB::table('devices')->select(DB::raw("count(serial) as cantidad, year(fecha_recepcion) as year, month(fecha_recepcion) as month"))->whereNotNull('fecha_recepcion')->whereNotNull('guia_recepcion')->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $col_reversas = DB::table('devices')->select(DB::raw("count(serial) as cantidad, year(fecha_reversa) as year, month(fecha_reversa) as month"))->whereNotNull('fecha_reversa')->whereNotNull('guia_reversa')->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $total_entregas = count($col_entregas);
        $total_reversas = count($col_reversas);
        // Tuve que hacer la paginación manualmente:
        $perPage = 12; // Entradas por página
        // Parámetros de número de página, uno para cada tabla.
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        // Cuando se carga la página desde cero los parámetros llegan en 0 así que los cambiamos a 1.
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        // El punto de partida u "offset"
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        // La colección se convierte en un arreglo y se "rebana" para que se pueda mostrar la info paginada.
        $entregas_sliced = array_slice($col_entregas->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_reversas->toArray(), $start_reversas, $perPage);
        // Con todo esto se crea el paginador
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        
        $data = [
            'entregas' => $entregas,
            'reversas' => $reversas,
            'col_entregas' => $col_entregas,
            'col_reversas' => $col_reversas
        ];
        return view("vista.buscar_por_guia", $data);
    }
    
    /**
     * Muestra formulario de búsqueda de equipos de IBM por guía
     * 
     * @return Illuminate\Http\Response
     */
    
    public function buscarPorGuiaIBM(Request $request)
    {
        $col_devices = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'IBM')->groupBy('year', 'month')->get();
        $col_reversas = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'REV')->groupBy('year', 'month')->get();
        $listado = DB::table('guia_i_b_ms')->select(DB::raw("year(fecha) as year, month(fecha) as month"))->groupBy('year', 'month')->orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $total_entregas = count($col_devices);
        $total_reversas = count($col_reversas);
        // Tuve que hacer la paginación manualmente:
        $perPage = 12; // Entradas por página
        // Parámetros de número de página, uno para cada tabla.
        $page_entregas = $request->input('entregas');
        $page_reversas = $request->input('reversas');
        // Cuando se carga la página desde cero los parámetros llegan en 0 así que los cambiamos a 1.
        if($page_entregas == 0) {
            $page_entregas = 1;
        }
        if($page_reversas == 0) {
            $page_reversas = 1;
        }
        
        $start_entregas = ($page_entregas - 1) * $perPage;
        $start_reversas = ($page_reversas - 1) * $perPage;
        $entregas_sliced = array_slice($col_devices->toArray(), $start_entregas, $perPage);
        $reversas_sliced = array_slice($col_reversas->toArray(), $start_reversas, $perPage);
        
        $entregas = new Paginator($entregas_sliced, $total_entregas, $perPage, $page_entregas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        // Por último se le cambia el nombre al parámetro para que coincida con $page_entregas
        $entregas->setPageName('entregas');
        // La misma creación del paginador para la segunda tabla
        $reversas = new Paginator($reversas_sliced, $total_reversas, $perPage, $page_reversas, [
            'path'  => $request->url(),
            'query' => $request->query()
        ]);
        $reversas->setPageName('reversas');
        
        $data = [
            'col_devices' => $col_devices,
            'col_reversas' => $col_reversas,
            'devices' => $entregas,
            'reversas' => $reversas,
            'listado' => $listado
        ];
        return view("vista.buscar_ibm", $data);
    }
    
    /**
     * Retorna una tabla con paginación con los equipos que coinciden con una guía de recepción
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaRecepcion(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $provider = Provider::where('name', 'IBM')->first();
        $devices = Device::where('devices.guia_recepcion', '=', $inp_guia)
                ->paginate(15);
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'tipo' => 'entrega'];
        return view("vista.result_guia", $data);
        
        
    }
    
    /**
     * Retorna una tabla con paginación con los equipos que coinciden con una guía de reversa
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaReversa(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = Device::where('guia_reversa', '=', $inp_guia)->paginate(15);
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'tipo' => 'reversa'];
        return view("vista.result_guia", $data);
        
        
    }
    
    /**
     * Retorna una tabla con paginación con los equipos que coinciden con una guía de IBM
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porGuiaIBM(Request $request)
    {
        $request->validate([
            'guia' => 'required|numeric'
        ]); 
        $inp_guia = $request->input('guia');
        $devices = GuiaIBM::where('guia', '=', $inp_guia)
                ->paginate(15);
        
        $pdfguia = \App\DocGuiaIBM::where('guia', $inp_guia)->where('provider_id', 'IBM')->first();
        $data = ['guia' => $inp_guia,
                'devices' => $devices,
                'pdf' => $pdfguia];
        return view("vista.result_guia_ibm", $data);
    }
    
    /**
     * Como buscarPorGuiaIBM recupera las guías pero por un mes determinado.
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porMesGuiaIBM(Request $request)
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $devices = DB::table('guia_i_b_ms')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_ms.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guia_i_b_ms.fecha', '=', $mes)
                ->whereYear('guia_i_b_ms.fecha', '=', $anio)
                ->where('guia_i_b_ms.estado', 'IBM')
                ->groupBy('guia_i_b_ms.guia', 'guia_i_b_ms.fecha')
                ->selectRaw('guia_i_b_ms.fecha, guia_i_b_ms.guia, count(guia_i_b_ms.serial) as cantidad, doc_guia_i_b_ms.guia as file')
                ->paginate(15);
        $reversas = DB::table('guia_i_b_ms')
                ->leftJoin('doc_guia_i_b_ms', 'guia_i_b_ms.guia', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('guia_i_b_ms.fecha', '=', $mes)
                ->whereYear('guia_i_b_ms.fecha', '=', $anio)
                ->where('guia_i_b_ms.estado', 'REV')
                ->groupBy('guia_i_b_ms.guia', 'guia_i_b_ms.fecha')
                ->selectRaw('guia_i_b_ms.fecha, guia_i_b_ms.guia, count(guia_i_b_ms.serial) as cantidad, doc_guia_i_b_ms.guia as file')
                ->paginate(15);
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'reversas' => $reversas
        ];
        return view('vista.result_guia_ibm_months', $data);
   
    }
    
    /**
     * Lista las guías de entrega por mes introducido por el usuario
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porMesGuiaRecepcion(Request $request) 
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $provider = Provider::where('name', 'IBM')->first();
        $devices = Device::whereNotNull('devices.guia_recepcion')
                ->whereNotNull('devices.fecha_recepcion')
                ->leftJoin('doc_guia_i_b_ms', 'devices.guia_recepcion', '=', 'doc_guia_i_b_ms.guia')
                //->where('doc_guia_i_b_ms.provider_id', '<>', $provider->id)
                //->orWhereNull('devices.provider_id')
                ->whereMonth('devices.fecha_recepcion', '=', $mes)
                ->whereYear('devices.fecha_recepcion', '=', $anio)
                ->groupBy('devices.guia_recepcion', 'devices.fecha_recepcion')
                ->selectRaw('devices.guia_recepcion, count(devices.guia_recepcion) as cantidad, devices.fecha_recepcion, doc_guia_i_b_ms.guia as file')
                ->orderBy('fecha_recepcion', 'desc')
                ->paginate(15);
        $tipo = 'entrega';
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'tipo' => $tipo
        ];
                
        return view('vista.result_guia_months', $data);
    }
    
    /**
     * Lista las guías de reversa por mes introducido por el usuario
     *  
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    
    public function porMesGuiaReversa(Request $request) 
    {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $provider = Provider::where('name', 'IBM')->first();
        $devices = Device::whereNotNull('devices.guia_reversa')
                ->whereNotNull('devices.fecha_reversa')
                ->leftJoin('doc_guia_i_b_ms', 'devices.guia_reversa', '=', 'doc_guia_i_b_ms.guia')
                //->where('doc_guia_i_b_ms.provider_id', '<>', $provider->id)
                //->orWhereNull('devices.provider_id')
                ->whereMonth('devices.fecha_reversa', '=', $mes)
                ->whereYear('devices.fecha_reversa', '=', $anio)
                ->groupBy('devices.guia_reversa', 'devices.fecha_reversa')
                ->selectRaw('devices.guia_reversa, count(devices.guia_reversa) as cantidad, devices.fecha_reversa, doc_guia_i_b_ms.guia as file')
                ->orderBy('fecha_reversa', 'desc')
                ->paginate(15);
        $tipo = 'reversa';
        $data = [
            'fecha' => $inp_fecha,
            'devices' => $devices,
            'tipo' => $tipo
        ];
                
        return view('vista.result_guia_months', $data);
    }
    
    public function porMesGuiasAdexus(Request $request) {
        $inp_fecha = $request->input('month');
        $fecha = explode('-', $inp_fecha);
        $mes = $fecha[0];
        $anio = $fecha[1];
        $provider = Provider::where('name', 'IBM')->first();
        $qentregas = Device::whereNotNull('devices.guia_recepcion')
                ->whereNotNull('devices.fecha_recepcion')
                ->leftJoin('doc_guia_i_b_ms', 'devices.guia_recepcion', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('devices.fecha_recepcion', '=', $mes)
                ->whereYear('devices.fecha_recepcion', '=', $anio)
                ->groupBy('devices.guia_recepcion', 'devices.fecha_recepcion')
                ->selectRaw('devices.guia_recepcion, count(devices.guia_recepcion) as cantidad, devices.fecha_recepcion, doc_guia_i_b_ms.guia as file')
                ->orderBy('fecha_recepcion', 'desc');
        $total_entregas = $qentregas->get()->sum('cantidad');
        $entregas = $qentregas->paginate(15, ['*'], 'entregas');
        $qreversas = Device::whereNotNull('devices.guia_reversa')
                ->whereNotNull('devices.fecha_reversa')
                ->leftJoin('doc_guia_i_b_ms', 'devices.guia_reversa', '=', 'doc_guia_i_b_ms.guia')
                ->whereMonth('devices.fecha_reversa', '=', $mes)
                ->whereYear('devices.fecha_reversa', '=', $anio)
                ->groupBy('devices.guia_reversa', 'devices.fecha_reversa')
                ->selectRaw('devices.guia_reversa, count(devices.guia_reversa) as cantidad, devices.fecha_reversa, doc_guia_i_b_ms.guia as file')
                ->orderBy('fecha_reversa', 'desc');
        $total_reversas = $qreversas->get()->sum('cantidad');
        $reversas = $qreversas->paginate(15, ['*'], 'reversas');
        $data = ['fecha' => $inp_fecha,
                'entregas' => $entregas,
                'reversas' => $reversas,
                'total_entregas' => $total_entregas,
                'total_reversas' => $total_reversas];
        return view('vista.result_guia_months', $data);
    }
    
    /**
     * Exporta como Excel el detalle de las series de la guía de entrega.
     * 
     * @param type $guia
     * @return type
     */
    
    public function exportRecepcion($guia) {
        $messageBag = new MessageBag;
        try {
            return Excel::download(new \App\Exports\EntregaGuiaRecepcionExport($guia), 'Dispositivos GD Directa ' . $guia . '.xlsx');
        } catch (\Exception $e) {
            \Log::warning('Algo salió mal al intentar exportar GD directa ' . $guia . ' Error: ' . $e);
            $messageBag->add('error', 'Algo salió mal al intentar exportar GD Directa');
            return redirect()->back()->withErrors($messageBag, 'error');
        }
    }
    
    /**
     * Exporta como Excel el detalle de las series de la guía de reversa.
     * 
     * @param type $guia
     * @return type
     */
    
    public function exportReversa($guia) {
        $messageBag = new MessageBag;
        try {
           return Excel::download(new \App\Exports\EntregaGuiaReversaExport($guia), 'Dispositivos GD Directa ' . $guia . '.xlsx');
        } catch (\Exception $e) {
            \Log::warning('Algo salió mal al intentar exportar GD reversa ' . $guia . ' Error: ' . $e);
            $messageBag->add('error', 'Algo salió mal al intentar exportar GD Reversa');
            return redirect()->back()->withErrors($messageBag, 'error');
        }
    }

    
}

