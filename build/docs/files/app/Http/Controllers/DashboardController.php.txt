<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Device;
use App\GuiaIBM;
use App\Provider;



    /**
         * Controlador de la página del panel de inicio 
         * con el resumen de todos los datos del sistema
         * 
         * @author <ffuentese@entel.cl>
         * 
         * 
         */

class DashboardController extends Controller
{
    /*
     * Muestra el panel del usuario 
     * y calcula los datos a mostrar.
     * 
     * @author <ffuentese@entel.cl>
     * 
     * @return \Illuminate\View\View view
     */
    
    public function index()
    {
        // Todas las series que han ingresado o reingresado
        $ingresos = Device::whereNotNull('fecha_recepcion')
                ->where('customer_id', 1)
                ->count();
        // Todas las reversas que hay en el sistema
        $reversas = Device::whereNotNull('fecha_reversa')
                ->where('customer_id', 1)
                ->count();
        // equipos que han sido instalados 
        // y que cuentan con una reversa para el equipo que reemplazó
        $validadas = Device::whereNotNull('fecha_instalacion')
                ->where('customer_id', 1)
                ->whereNotNull('fecha_recepcion')
                ->whereNotNull('reversa_prev')
                ->count();
        $instaladas_sr = Device::whereNotNull('fecha_instalacion')
                ->whereNotNull('fecha_recepcion')
                ->where('customer_id', 1)
                ->whereNull('reversa_prev')
                ->count();
        // equipos que han llegado, y no están instalados pero que fueron reversados.
        $ok_no_instaladas = Device::whereNotNull('fecha_recepcion')
                ->whereNull('fecha_instalacion')
                ->whereNotNull('fecha_reversa')
                ->where('customer_id', 1)
                ->count();
        $total_validadas = $validadas + $ok_no_instaladas + $instaladas_sr;
        // Equipos que no tienen serial pero que están registrados en el sistema.
        $ilegibles = Device::whereNull('serial')
                ->OrWhere('serial', 'LIKE', 'BEC%')
                ->OrWhere('serial', 'LIKE' ,'%ILEGIBLE%')
                ->where('customer_id', 1)
                ->count();
   

        // Stock de bodega separado por tipo
        $stock_bod_s = Device::whereNull('fecha_reversa')
                ->where('location_id', 'LIKE', 'D%')
                ->where('customer_id', 1)
                ->count();
        $stock_bod_r = Device::whereNull('fecha_reversa')
                ->where('location_id', 'LIKE', 'R%')
                ->where('customer_id', 1)
                ->count();
        // Stock de bodega unificado y descartados los series sin recepción
        $stock_bod = Device::whereNull('fecha_reversa')
                ->where(function($query){
                    $query->where('location_id', 'LIKE', 'D%')
                          ->OrWhere('location_id', 'LIKE', 'R%');
                })
                ->where('customer_id', 1)
                ->whereNotNull('fecha_recepcion')
                ->count();
        // Stock técnico separado por tipo
        $sto_tec_s = Device::whereNull('fecha_reversa')
                ->where('location_id', 'STO')
                ->where('customer_id', 1)
                ->count();
        $sto_tec_r = Device::whereNull('fecha_reversa')
                ->where('location_id', 'REV')
                ->where('customer_id', 1)
                ->count();
        // Stock técnico unificado y descartados los series sin recepción
        $stock_tec = Device::whereNull('fecha_reversa')
                ->where(function($query){
                    $query->where('location_id', 'STO')
                          ->OrWhere('location_id', 'REV');
                })
                ->where('customer_id', 1)
                ->whereNotNull('fecha_recepcion')
                ->count();
        $habidas =  $stock_tec + $stock_bod;
        $total_just = $total_validadas+$habidas;
        $total_just_ilegibles = $total_just + $ilegibles;
        if ($ingresos > 0) {
            $porc_just = round((((float) $total_just / (float) $ingresos)*100),2);
            $porc_just_ilegibles = round((((float) $total_just_ilegibles / (float) $ingresos)*100),2);
        } else {
            $porc_just = 0;
            $porc_just_ilegibles = 0;
        }
        
        // IBM (buscamos al proveedor con el id de la tabla).
        $provider = Provider::where('name', 'IBM')->first();
        $ibm_total = Device::where('provider_id', $provider->id)->count();
        $ingresos_ibm = Device::where('provider_id', $provider->id)->where('location_id', 'IBM')->count();
        $reversas_ibm = Device::where('provider_id', $provider->id)->where('location_id', 'REV')->count();
        $validadas_ibm = Device::whereNotNull('fecha_instalacion')
                ->where('customer_id', 1)
                ->where('provider_id', $provider->id)
                ->whereNotNull('fecha_recepcion')
                ->whereNotNull('reversa_prev')
                ->count();
        $instaladas_sr_ibm = Device::whereNotNull('fecha_instalacion')
                ->whereNotNull('fecha_recepcion')
                ->where('provider_id', $provider->id)
                ->where('customer_id', 1)
                ->whereNull('reversa_prev')
                ->count();
        // equipos que han llegado, y no están instalados pero que fueron reversados.
        $ok_no_instaladas_ibm = Device::whereNotNull('fecha_recepcion')
                ->where('provider_id', $provider->id)
                ->whereNull('fecha_instalacion')
                ->whereNotNull('fecha_reversa')
                ->where('customer_id', 1)
                ->count();
        $total_validadas_ibm = $validadas_ibm + $ok_no_instaladas_ibm + $instaladas_sr_ibm;
        
        // Equipos que no tienen serial pero que están registrados en el sistema.
        $ilegibles_ibm = Device::where('provider_id', $provider->id)
                ->where(function ($query){
                    $query->whereNull('serial')
                    ->OrWhere('serial', 'LIKE', 'BEC%')
                    ->OrWhere('serial', 'LIKE', '%ILEGIBLE%');
                })
                ->count();
        
        $stock_bod_s_ibm = Device::whereNull('fecha_reversa')
                ->where('location_id', 'IBM')
                ->where('customer_id', 1)
                ->where('provider_id', $provider->id)
                ->count();
        $stock_bod_r_ibm = Device::where('location_id', 'REV')
                ->where('customer_id', 1)
                ->where('provider_id', $provider->id)
                ->count();
        
        $no_instalado_ibm = Device::where('provider_id', $provider->id)
                ->whereNull('fecha_reversa')
                ->whereNull('fecha_instalacion')
                ->where('location_id', 'IBM')->count();
        
        $reversado_ibm = Device::where('provider_id', 3)->where('location_id', 'REV')->count();
        $total_disponible_ibm = $no_instalado_ibm;

        $stock_bod_ibm = $stock_bod_r_ibm + $stock_bod_s_ibm;
        $habidas_ibm =  $stock_bod_ibm;
        $total_just_ibm = $total_validadas_ibm;
        $total_just_ilegibles_ibm = $total_just_ibm + $ilegibles_ibm;
        if ($ingresos_ibm > 0) {
            //$porc_just_ibm = round((((float) $total_just_ibm / (float) $ingresos_ibm)*100),2);
            $porc_just_ilegibles_ibm = round((((float) $total_just_ilegibles_ibm / (float) $ibm_total)*100),2);
        } else {
            //$porc_just_ibm = 0;
            $porc_just_ilegibles_ibm = 0;
        }
        
        $ultima_act = Device::all()->max('updated_at');
        $ultima_ibm = GuiaIBM::all()->max('fecha');
        
        // Fuentes del gráfico Adexus
        
        $col_entregas = Device::select(DB::raw("count(serial) as cantidad, year(fecha_recepcion) as year, month(fecha_recepcion) as month"))->whereNotNull('fecha_recepcion')->whereNotNull('guia_recepcion')->groupBy('year', 'month')->orderBy('year', 'asc')->orderBy('month', 'asc')->get();
        $col_reversas = Device::select(DB::raw("count(serial) as cantidad, year(fecha_reversa) as year, month(fecha_reversa) as month"))->whereNotNull('fecha_reversa')->whereNotNull('guia_reversa')->groupBy('year', 'month')->orderBy('year', 'asc')->orderBy('month', 'asc')->get();
        
        $ar_entregas = [];
        $ar_reversas = [];
        $meses_grafico =  [];
        foreach($col_entregas as $entrega)
        {
            $fecha = $entrega->year . '-' . $entrega->month;
            $meses_grafico[] = $fecha;
            $ar_entregas[] = ["x" => $fecha,
                              "y" => $entrega->cantidad];
        }
        foreach($col_reversas as $reversa)
        {
            $fecha = $reversa->year . '-' . $reversa->month;
            $ar_reversas[] = [
                "x" => $fecha,
                "y" => $reversa->cantidad];
        }
        
        // Fuentes del gráfico IBM
        
        $col_devices_ibm = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'IBM')->groupBy('year', 'month')->get();
        $col_reversas_ibm = DB::table('guia_i_b_ms')->select(DB::raw("count(serial) as cantidad, year(fecha) as year, month(fecha) as month"))->where('estado', 'REV')->groupBy('year', 'month')->get();
        $ar_entregas_ibm = [];
        $ar_reversas_ibm  = [];
        $meses_grafico_ibm = [];
        foreach($col_devices_ibm as $entregas)
        {
            $fecha = $entrega->year . '-' . $entrega->month;
            $meses_grafico_ibm[] = $fecha;
            $ar_entregas_ibm[] = ["x" => $fecha,
                              "y" => $entrega->cantidad];
        }
        foreach($col_reversas_ibm as $reversa)
        {
            $fecha = $reversa->year . '-' . $reversa->month;
            $ar_reversas_ibm[] = [
                "x" => $fecha,
                "y" => $reversa->cantidad];
        }
                
        $data = [
            'ingresos' => $ingresos,
            'reversas' => $reversas,
            'rec_rev' => $total_validadas,
            'ilegibles' => $ilegibles,
            'stock_tec' => $stock_tec,
            'stock_bod' => $stock_bod,
            'habidas' => $stock_tec + $stock_bod,
            'total_just' => $total_just,
            'total_just_ilegibles' => $total_just_ilegibles,
            'porc_just' => $porc_just,
            'porc_just_ilegibles' => $porc_just_ilegibles,
            'bod_sto' => $stock_bod_s,
            'bod_reve' => $stock_bod_r,
            'stock_tecnicos' => $sto_tec_s,
            'reversa_tecnicos' => $sto_tec_r,
            't_disponible' => $stock_bod_s + $sto_tec_s,
            't_reversas' => $stock_bod_r + $sto_tec_r,
            'ingresos_ibm' => $ingresos_ibm,
            'reversas_ibm' => $reversas_ibm,
            'rec_rev_ibm' => $total_validadas_ibm,
            'ilegibles_ibm' => $ilegibles_ibm,
            'stock_bod_ibm' => $stock_bod_ibm,
            'habidas_ibm' => $habidas_ibm,
            'total_just_ibm' => $total_just_ibm,
            'total_just_ilegibles_ibm' => $total_just_ilegibles_ibm,
            'porc_just_ilegibles_ibm' => $porc_just_ilegibles_ibm,
            'bod_sto_ibm' => $stock_bod_s_ibm,
            'bod_reve_ibm' =>$stock_bod_r_ibm,
            't_disponible_ibm' => $stock_bod_s_ibm,
            't_reversas_ibm' => $stock_bod_r_ibm,
            'ultima_act' => $ultima_act,
            'ultima_ibm' => $ultima_ibm,
            'ar_entregas' => $ar_entregas,
            'ar_reversas' => $ar_reversas,
            'meses_grafico' => $meses_grafico,
            'ar_entregas_ibm' => $ar_entregas_ibm,
            'ar_reversas_ibm' => $ar_reversas_ibm,
            'meses_grafico_ibm' => $meses_grafico_ibm
                
                
        ];
       return view("dashboard.index", $data);
    }
    
    
}

