<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pos;
use App\Region;
use App\Comuna;

/**
 * Información relacionada a la ubicación de los POS
 */

class POSGeoController extends Controller
{
    /**
     *  Muestra un resumen agregado de equipos POS por región
     * @return \Illuminate\Http\Response la vista. 
     */
    public function index() {
        $pos = Pos::with('comuna')->whereNotNull('comuna_id')->get();
        $col_pos = $pos->groupBy('comuna.region_id')->all();
        $arr = [];
        foreach ($col_pos as $posxregion)
        {
            $arr[] = [
                'region_id' => $posxregion->first()->comuna->region_id,
                'name' => $posxregion->first()->comuna->region->name,
                'cantidad' => $posxregion->count()
                ];
        }
        // ordeno el arreglo por el id de la región.
        usort($arr, array($this, "cmp"));
        $data = [
            'ar_pos' => $arr
        ];
        return view('posgeo.index', $data);
    }
    
    /**
     * Compara los valores para ordenar el arreglo por el número de región
     * @param int $a
     * @param int $b
     * @return boolean
     */
    public function cmp($a, $b) {
        return $a['region_id'] < $b['region_id'] ? -1 : 1;
    }
    
    /**
     * Agrupa ubicaciones por región y los lista.
     * @param int $id El id de la región. 
     * @return \Illuminate\Http\Response la vista. 
     */
    
    public function posPorRegion($id) {
        $pos = Pos::with('comuna')->get();
        $col_pos = $pos->where('comuna.region_id', $id)->groupBy('comuna_id')->all();
        $pos_comuna = [];
        foreach ($col_pos as $posxcomuna) {
            $pos_comuna[] = [
                'comuna_id' => $posxcomuna->first()->comuna_id,
                'name' => $posxcomuna->first()->comuna->name,
                'cantidad' => $posxcomuna->count()
            ];
        }
        usort($pos_comuna, array($this, "cmpAlpha"));
        $data = ['pos_comuna' => $pos_comuna];
        return view('posgeo.posregion', $data);
        
    }
    
    public function cmpAlpha($a, $b) {
        return $a['name'] < $b['name'] ? -1 : 1;
    }
    
    /**
     * Agrupa POS por la comuna.
     * @param int $id El id de la comuna
     * @return \Illuminate\Http\Response la vista. 
     */
    
    public function posPorComuna($id) {
        $pos_comuna = Pos::where('comuna_id', $id)->paginate(10);
        $data = ['pos_comuna' => $pos_comuna];
        return view('posgeo.poscomuna', $data);
    }
    /**
     * 
     * @param Request $request
     * @param integer $id identificador de la región
     * @return \Illuminate\Http\Response json con la vista parcial.
     */
    public function comunasRegion(Request $request, $id) {
        $region = Region::find($id);
        $comunas = $region->comunas->sortBy('name');
        $html = view('partials.poscomunaselect')->with('comunas', $comunas)->render();
        return response()->json($html);
    }
    
    public function posComunasAjax(Request $request, $comuna_id) {
        $pos = Pos::where('comuna_id', $comuna_id)->get();
        $html = view('partials.poscomunatable')->with('pos', $pos)->render();
        return response()->json($html);
    }
    /**
     * Genera vista para mostrar ubicación de POS por comuna así como 
     * @return View
     */
    public function buscarPorComuna() {
        $regiones = Region::all();
        return view('posgeo.buscador')->with('regiones', $regiones);
    }
    
}

