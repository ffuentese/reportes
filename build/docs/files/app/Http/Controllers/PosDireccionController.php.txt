<?php

namespace App\Http\Controllers;

use App\Pos;
use Illuminate\Http\Request;
use App\Imports\ImportPosDireccion;
use Validator;
use Excel;
use Auth;

class PosDireccionController extends Controller
{
    /**
     * Muestra el formulario
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mensajes = new \Illuminate\Support\MessageBag();
        $user = Auth::user();
        if (!$user) {
            abort(403);
        }
        if($user->hasRole('admin')) {
            return view('posdireccion.index')->with('mensajes', $mensajes);
        } else {
            abort(403);
        }
    }
    
    /**
     * Da inicio al proceso de actualización de POS
     * @param type $request
     */
    
    public function procesar(Request $request) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $errors = new \Illuminate\Support\MessageBag();
        $file = $request->import_file;
        $rules = array('import_file' => 'required|mimes:xlsx'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('import_file'=> $file), $rules);
        if($validator->passes()){
            $filename = $file->getClientOriginalName();
            if(!$this->nombreDeArchivoValido($filename)){
                $error = 'El archivo ' . $filename . ' tiene un nombre no válido para un archivo de puntos vigentes.';
                $errors->add($filename, $error);
                \Log::error($error);
                return redirect('posdireccion')->withErrors($errors);
            }
        
            $path = $request->file('import_file')->getRealPath();
            Excel::import(new ImportPosDireccion, $path);

            return redirect('posdireccion')->with('success', 'Carga exitosa!');
        } else {
            return redirect('posdireccion')->withErrors($validator->errors());
        }
    }

    public function nombreDeArchivoValido($filename)
    {
        $arr = explode(' ', $filename);
        if (count($arr) != 3){
            return false;
        }
        if(strcasecmp($arr[0], 'Puntos') != 0 ) {
            return false;
        }
        if(strcasecmp($arr[1], 'Vigentes') != 0 ) {
            return false;
        }
        $date = substr($arr[2], 0, -5);
        if(!$this->validateDate($date)) {
            return false;
        }
        
        return true;
        
    }
    
    function validateDate($date, $format = 'd-m-Y'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
}

