<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    public $incrementing = false;
    
    public function comunas() {
        return $this->hasMany('App\Comuna');
    }
}

