<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Excel;
use \App\Imports\PlanillaAdxRevIBMImport;

class ActaReversaIBMBulk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:reversasibm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Procesa las actas de reversa asociadas a IBM desde /reversas_ibm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Realiza la importación
        $directory = 'reversas_ibm';
        $files = Storage::allFiles($directory);
        \Log::info('Comienza a procesar actas de reversa de IBM');
        $bar =  $this->output->createProgressBar(count($files));
        $bar->start();
        $start = microtime(true);
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '1024M');
        foreach($files as $file)
        {
            $fname = basename($file);
            \Log::info('Procesando ',[$fname]);
            $arr = explode(" ", $fname);
            if(!isset($arr[5])){
                continue;
            }
            $day = substr($arr[4], 0, 10);
            $date = Carbon::parse($day);
            $guia = substr($arr[5], 0, -5);
            if (!is_numeric($guia)) {
                $guia = 0;
            }
            Excel::import(new PlanillaAdxRevIBMImport($date, $guia), $file);
            Storage::move($file, 'procesado/reversas_ibm/' . basename($file, '.xlsx') . '-' . time() . '.xlsx');
            $bar->advance();
        }
        $bar->finish();
        $time = microtime(true) - $start;
        \Log::info('Proceso terminado ' . 'en ' . $time);
    }
}

