<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Device;
use App\OT;

class CheckOT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:ot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revisa las OTs digitales para validar series';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Revisa la OT digital para encontrar fechas de instalación 
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $devices = Device::whereNotNull('fecha_instalacion')
                ->whereNull('serial_prev')
                ->orderBy('fecha_recepcion', 'asc')
                ->get();
        $ots = OT::whereNotNull('serie4')->get();
        $count = 0; // cuenta de equipos con serial previa no registrados.
        $c = 0; // cuenta de equipos revisados
        $bar =  $this->output->createProgressBar(count($devices));
        $bar->start();
        foreach ($devices as $device)
        {
            // Ojo, acá está haciendo una consulta a la colección con el accesor
            // getSerieAttribute, así que es un poco más lento que ir directo a 
            // la base.
            $d = $ots->where('serie', $device->serial)->first();
            if($d){
            
                if($d->serie_reversa && $d->fecha_atencion < $device->fecha_reversa){
                    $datos = [
                        'serial_prev' => $d->serie_reversa,
                        'reversa_prev' => $d->fecha_atencion
                    ];
                    $device->update($datos);
                    $count++;
                }
            }
            $bar->advance();
        }
        $bar->finish();
        echo "\n El total de modificables es " . $count;
    }
}

