<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\TasksFinished;
use Excel;
use Carbon\Carbon;
use App\Pv;
use App\Imports\PVSImport;

class ImportPVS extends Command
{
    /**
     * La firma del proceso
     *
     * @var string
     */
    protected $signature = 'procesar:pvs';

    /**
     * Descripción del comando.
     *
     * @var string
     */
    protected $description = 'Importa los puntos vigentes en bruto';

    /**
     * Crea una nueva instancia del proceso.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Ejecuta la importación desde la carpeta pv del "storage" de Laravel.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);
        // El formato es "Puntos Vigentes dd-mm-yyyy.xlsx"
        // Es importante que sea xlsx y no otro.
        $directory = 'pv';
        $files = Storage::allFiles($directory);
        \Log::info('Comienza proceso de puntos vigentes.');
        $bar =  $this->output->createProgressBar(count($files));
        $bar->start();
        foreach($files as $file)
        {
            $fname = basename($file);
            \Log::info('Procesando ',[$fname]);
            $arr = explode(" ", $fname);
            $day = substr($arr[2], 0, 10);
            $date = Carbon::parse($day);
            $was_uploaded = Pv::where('fecha', $date)->count();
            if($was_uploaded == 0) {
                \Log::info('Importando ' . $file  . ' de dia '. $date);
                try {
                Excel::import(new PVSImport($date), $file);
                } catch (\Exception $e) {
                    $error_message = 'La importación del archivo ' . $file . ' falló: ' . $e->getCode() . ' : ' . $e->getMessage();
                    \Log::warning($error_message);
                }
            } 
            Storage::move($file, 'procesado/' . $file);
            $bar->advance();
        }
        $time = microtime(true) - $start;
        
        $bar->finish();
        \Log::info('Proceso terminado.' . 'en ' . $time);
    }
}

