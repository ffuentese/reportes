<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
Use Exception;
use App\Pos;
use App\Device;

class CopyPVSToRel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:pvstorel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copia los resultados de la consulta a PVS a devices_pos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Ejecuta el comando
     * 
     * Primero  genera una consulta para obtener los datos desde pvs e insertarlos a RelPVS
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('Comienza proceso de copia');
        $this->info('Consulta a PVS en progreso...');
         $q = DB::table('pvs')
                    ->groupBy('serial', 'pos_id')
                    ->orderBy('fecha')
                    ->selectRaw('serial, pos_id, min(fecha) as fecha');
         $this->info('Consulta generada.');
         
         $bindings = $q->getBindings();
         $this->info('Este proceso puede tomar algunos minutos...');
         $this->info('Consultando datos...');
         $insertQuery = 'INSERT INTO rel_p_v_s (serial, pos_id, fecha) ' . $q->toSql();
         DB::beginTransaction();
           $start = microtime(true);
         DB::table('rel_p_v_s')->truncate();
         try {
            \DB::insert($insertQuery, $bindings);
         } catch (Exception $e) {
             $this->warn($e->getMessage());
             DB::rollBack();
         }
         DB::commit();

         $time = microtime(true) - $start;
         $msg = 'Tarea terminada en '. $time . ' segundos.';
         $this->info($msg);
         
         
         
         
    }
    
    
    
}

