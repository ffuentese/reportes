<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /*
     * Este modelo representa al estatus de bodega
     * "en traslado", "en stock", etc.
     */
    protected $table = "status";
}

