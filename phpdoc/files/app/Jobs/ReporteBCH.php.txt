<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Sccm;
use App\Bginfo;
use App\Bch_employee;
use App\Bch_location;
use App\Zpec;
use App\Dev_BCH;
use App\Material_tipo;
use Carbon\Carbon;

/**
 * Construye el informe del Banco de Chile que se localiza en Dev_BCH (tabla dev__b_c_hs)
 * 
 * En primer lugar busca los valores que se encuentran en el modelo SCCM y descarta 
 * los que ya están en el informe y los que van a aparecer como retiro o venta.
 * 
 * Luego va completando líneas con los equipos que faltan. 
 */

class ReporteBCH implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Busca todos los equipos del SCCM con sus relaciones
        $sccm = Sccm::with('modelo')->with('location')->get();
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        // Crea un arreglo donde se encuentren todos los datos. 
        $dataToInsert = [];
        foreach ($sccm as $s) {
            if ($this->existe($s)) {
                continue;
            }
            
            if ($this->excluir($s)) {
                continue;
            }
            // buscamos el equipo en bginfo para obtener:
            // nombre de usuario
            // fecha de conexión.
            $bginfo = Bginfo::where('hostname', $s->hostname)->first();
            // Si el empleado no existía en SCCM lo buscamos en el bginfo
            if(!$s->employee) {
                if ($bginfo) {
                    $employeeName = $bginfo->username;
                }
            } else {
                $employeeName = $s->employee->name;
            }
            // Si el nombre del empleado no existe se escribe "SIN INFORMACIÓN"
            if(!$employeeName) {
                $employeeName = "SIN INFORMACION";
            }
            $ultima_conexion = $s->ultima_conexion;
            // Si no estuviese el dato de la última conexión, lo busca en el bginfo
            if(!$ultima_conexion && $bginfo) {
                $ultima_conexion = $bginfo->fecha_conexion;
            }
            // Se asegura de que el empleado esté en la dotación de RRHH antes de buscarlo.
            if (!$s->employee) {
                $employeeRut = "SIN INFORMACION";
            } else {
                $employeeRut = $s->employee->rut;
            }
            // Verificando site (region y site)
            if (!$s->site_id) {
                $region = "SIN INFORMACION";
                $site = "SIN INFORMACION";
            } else {
                $region = $s->location->ciudad;
                $site = $s->location->sucursal;
            }
            
            // Busca el rótulo en el ZPEC 
            
            $zpec = Zpec::where('serie', $s->serie)->orderBy('id','desc')->first();
            if ($zpec) {
                $rotulo = $zpec->rotulo;
            } else {
                $rotulo = "SIN INFORMACION";
            }
            
            // Unifica HEWLETT-PACKARD y HP en "HP"
            
            $marca = $s->modelo->brand->name == "HEWLETT-PACKARD" ? "HP" : $s->modelo->brand->name;
            $marca = $s->modelo->brand->name == "DESCONOCIDO" ? "SIN INFORMACION" : $s->modelo->brand->name;
            $modelo = $s->modelo->name == "DESCONOCIDO" ? "SIN INFORMACION" : $s->modelo->name;
            $chassis = strtoupper($s->chassis);
            $data = [
                'alias' => $s->hostname,
                'rotulo' => $rotulo,
                'serie' => $s->serie,
                'marca' => $marca,
                'modelo' => $modelo,
                'tipo' => $chassis,
                'nombre' => $employeeName,
                'rut' => $employeeRut,
                'region' => $region,
                'site' => $site,
                'fecha_reporte' => 'SCCM ' . $s->fecha_carga->format('d-m-Y'),
                'ultima_conexion' => $s->ultima_conexion,
                'created_at' => $now,
                'updated_at' => $now
            ];
            // Añade a arreglo
            $dataToInsert[] = $data;
        }
        
        // Busca todos los equipos en BGInfo que no estén en el SCCM ni en el inventario.
        
        $bginfos = Bginfo::whereNotIn('serie', function($query) {
            $query->select('serie')->from('sccms');  
        })->whereNotIn('serie', function($query) { 
            $query->select('serie')->from('dev__b_c_hs');
        })
        ->get();
        
        // Recorre todos los equipos 
        
        foreach ($bginfos as $bginfo)
        {
            if ($this->existe($bginfo)) {
                continue;
            }
            
            if ($this->excluir($bginfo)) {
                continue;
            }
            
            // busca tipo de equipo en inventario y ZPEC
            
            $chassis_mod = Dev_BCH::where('modelo', $bginfo->modelo->name)->first();
            $zpec_serie = Zpec::where('serie', $bginfo->serie)->orderBy('id', 'desc')->first();
            if($chassis_mod) {
                $chassis = strtoupper($chassis_mod->tipo);
            } else if ($zpec_serie){
                $material = explode('.',$zpec_serie->material);
                $cod_tipo = Material_tipo::where('code', $material[1])->first();
                switch ($cod_tipo->code) {
                    case 'PC':
                        $chassis = 'DESKTOP';    
                        break;
                    case 'NB':
                        $chassis = 'NOTEBOOK';
                         break;
                    case 'TB':
                        $chassis = 'TABLET';
                        break;
                    default:
                        $chassis = $cod_tipo->name;
                        break;
                }
            } else {
                $chassis = "SIN INFORMACION";
            }
            
            // Buscamos detalles de empleado (nombre y rut) en Bch_employees (tabla de personal BCH)
            // De no encontrarse allí lo busca en la tabla del inventario.
            
            $employee = Bch_employee::where('username', $bginfo->username)->first();
            $dev_previo = Dev_BCH::where('serie', $bginfo->serie)->first();
            if ($employee) {
                $employeeName = $employee->name;
                $employeeRut = $employee->rut;
            } else if ($dev_previo) {
                $employeeName = $dev_previo->nombre;
                $employeeRut = $dev_previo->rut;
            } else {
                $employeeName = "SIN INFORMACION";
                $employeeRut = "SIN INFORMACION";
            }
            
            // Buscamos site y región a partir de la tabla de segmentos.
            
            if ($this->isValidIP($bginfo->ip)){
                $ip_arr = explode('.', $bginfo->ip);
                $segmento = $ip_arr[0] . '.' . $ip_arr[1] . '.' . $ip_arr[2] . '.0';
                $loc = \App\Bch_ip::where('segmento', $segmento)->first();
            } else if($bginfo->ip != '') {
                if (strstr('_x000D_ ', $bginfo->ip)) {
                    $ips = explode('_x000D_ ', $bginfo->ip);
                    $ip_arr = explode('.', $ips[0]);
                    $segmento = $ip_arr[0] . '.' . $ip_arr[1] . '.' . $ip_arr[2] . '.0';
                    $loc = \App\Bch_ip::where('segmento', $segmento)->first();
                }
            }
            if ($loc) {
                $region = $loc->ciudad;
                $site = $loc->sucursal;
            } else {
                $region = "SIN INFORMACION";
                $site = "SIN INFORMACION";
            }
            
            // Busca el rótulo en el ZPEC
            
            $zpec = Zpec::where('serie', $s->serie)->first();
            if ($zpec) {
                $rotulo = $zpec->rotulo;
            } else {
                $rotulo = "SIN INFORMACION";
            }
            
            // Unifica HEWLETT-PACKARD y HP en "HP"
            
            $marca = $bginfo->modelo->brand->name == "HEWLETT-PACKARD" ? "HP" : $bginfo->modelo->brand->name;

            $data = [
                'alias' => $bginfo->hostname,
                'rotulo' => $rotulo,
                'serie' => $bginfo->serie,
                'marca' => $marca,
                'modelo' => $bginfo->modelo->name,
                'tipo' => $chassis,
                'nombre' => $employeeName,
                'rut' => $employeeRut,
                'region' => $region,
                'site' => $site,
                'fecha_reporte' => 'BGINFO ' . $bginfo->fecha_carga->format('d-m-Y'),
                'ultima_conexion' => $bginfo->fecha_conexion,
                'created_at' => $now,
                'updated_at' => $now
            ];
            
            // Añade a arreglo
            
            $dataToInsert[] = $data;
            
        }
        Dev_BCH::insert($dataToInsert);
        
        
    }
    
    /**
     * Verifica si el dispositivo existe en el inventario
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return boolean
     */
    
    public function existe($model) {
        
            $dev = Dev_BCH::where('serie', $model->serie)->first();
            $retiro_venta = $this->checkZpecRetiroVenta($model->serie);
            // si el equipo existe lo busca en el ZPEC
            if ($dev) {
                
                 // Si está como 'RETIRO' o 'VENTA' lo quita del inventario.
                if($retiro_venta) {
                    $dev->delete();
                    return true;
                }
                // De lo contrario lo mantiene y pasa al siguiente item.
                else {
                    return true;
                }
            } else {
                if ($retiro_venta) {
                    return true;
                } else {
                    return false;
                }
            }
    }
    
    public function checkZpecRetiroVenta($serie) {
        $retiro_venta = Zpec::where('serie', $serie)
                        ->where(function ($query) {
                            $query->where('operacion', 'RETIRO')
                                    ->orWhere('operacion', 'VENTA');
                        })->exists();
        return $retiro_venta;
    }
    
    function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 
    
    public function excluir($model) {
        // ignora TCRs
        if ($this->startsWith($model->hostname, 'TCR') 
                || $this->startsWith($model->hostname, 'VM')
                || $this->startsWith($model->hostname, 'TRC') ) { 
            return true;
        }
        if ($this->startsWith($model->serie, 'VM')) {
            return true;
        }
        return false;
    }
    
    /**
 * @param $str the string to check
 * @return bool whether the String $str is a valid ip or not
 */
    function isValidIP($str)
    {
        /**
         * Alternative solution using filter_var
         *
         * return (bool)filter_var($str, FILTER_VALIDATE_IP);
         */

        /**
         * Normally preg_match returns "1" when there is a match between the pattern and the provided $str.
         * By casting it to (bool), we ensure that our result is a boolean.
         * This regex should validate every IPv4 addresses, which is no local adress (e.g. 127.0.0.1 or 192.168.2.1).
         * This pattern was debugged using https://regex101.com/
         */
        return (bool)preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $str);
    }
}

