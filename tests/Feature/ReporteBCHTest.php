<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Jobs\ReporteBCH;
use Illuminate\Support\Facades\Queue;
//use Illuminate\Foundation\Testing\RefreshDatabase;

class ReporteBCHTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReporteBCH()
    {
        Queue::fake();
        $job = new ReporteBCH; 
        $job->dispatch();
        $sccm = \App\Sccm::first();
        
        Queue::assertPushed(ReporteBCH::class, function ($job) use ($sccm) {
            return \App\Dev_BCH::where('serie', $sccm->serie)->exists();
        });

        Queue::assertPushedOn('default', ReporteBCH::class);

    }
}
