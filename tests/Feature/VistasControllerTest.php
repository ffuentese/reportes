<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Device;


class VistasControllerTest extends TestCase
{
    /**
     * Prueba que al ingresar a /guia_recepcion con un usuario y una guía real el detalle es mostrado.
     *
     * @return void
     */
    
    public function testGuiaEntregaOk()
    {
        $user = \App\User::find(2);
        $device = Device::whereNotNull('guia_recepcion')->first();
        $params = ['guia' => $device->guia_recepcion];
        $response = $this->actingAs($user)->followingRedirects()->call('GET','/guia_recepcion',$params);
        $view = "vista.result_guia";
        $response->assertViewIs($view);
    }
    
    public function testGuiaReversaOk()
    {
        $user = \App\User::find(2);
        $device = Device::whereNotNull('guia_reversa')->first();
        $params = ['guia' => $device->guia_reversa];
        $response = $this->actingAs($user)->followingRedirects()->call('GET','/guia_reversa',$params);
        $view = "vista.result_guia";
        $response->assertViewIs($view);
    }
    
    
}
