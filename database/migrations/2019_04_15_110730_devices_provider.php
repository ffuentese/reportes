<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DevicesProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function (Blueprint $table) {
            //
            $table->integer('provider_id')->unsigned()->nullable();
        });
        
        Schema::table('devices', function (Blueprint $table) {
            //
            $table->foreign('provider_id')->references('id')->on('providers');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            //
            $table->dropForeign('provider_id');
            $table->dropColumn('provider_id');
        });
    }
}
