<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiaIBMAccesoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('guia_i_b_m_accesorios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cantidad');
            $table->unsignedInteger('tipo_id');
            $table->unsignedInteger('modelo_id');
            $table->unsignedInteger('guia');
            $table->dateTime('fecha');
            $table->string('estado'); // IBM (entrada o REV (reversa)
            $table->timestamps();
        });
        
        Schema::table('guia_i_b_m_accesorios', function (Blueprint $table) {
            $table->foreign('modelo_id')->on('models')->references('id');
            $table->foreign('tipo_id')->on('accesorio_tipos')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guia_i_b_m_accesorios');
    }
}
