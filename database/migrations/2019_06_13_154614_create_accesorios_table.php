<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accesorios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cantidad');
            $table->unsignedInteger('tipo_id');
            $table->unsignedInteger('modelo_id');
            $table->unsignedInteger('guia');
            $table->string('estado');
            $table->dateTime('fecha');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('provider_id');
            $table->timestamps();
        });
        
        Schema::table('accesorios', function (Blueprint $table) {
            $table->foreign('tipo_id')->on('accesorio_tipos')->references('id');
            $table->foreign('modelo_id')->on('models')->references('id');
            $table->foreign('customer_id')->on('customers')->references('id');
            $table->foreign('provider_id')->on('providers')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesorios');
    }
}
