<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuiaIBMAccesoriosProyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guia_i_b_m_accesorios', function (Blueprint $table) {
            //
             $table->string('obs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guia_i_b_m_accesorios', function (Blueprint $table) {
            //
            $table->dropColumn('obs');
        });
    }
}
