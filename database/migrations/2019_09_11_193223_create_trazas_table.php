<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrazasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solped', function (Blueprint $table) {
            $table->integer('ID')->primary();
            $table->string('PEDIDO')->index();
            $table->string('N_DOCUMENTO')->nullable();
            $table->string('CODMATERIAL')->nullable();
            $table->string('ROTULO')->nullable();
            $table->string('SERIE')->nullable();
            $table->string('GD')->nullable();
            $table->string('MATERIAL')->nullable();
            $table->string('TIPO_PEDIDO')->nullable();
            $table->string('CTD_PEDIDO')->nullable();
            $table->string('CTD_ENTREGA')->nullable();
            $table->string('CTD_PENDIENTE')->nullable();
            $table->date('FECHA_ENTREGA')->nullable();
            $table->string('CREADO_POR')->nullable();
            $table->string('RUT')->nullable();
            $table->string('CLIENTE')->nullable();
            $table->string('CTD_CONFIRMADA')->nullable();
            $table->string('ALMACEN')->nullable();
            $table->string('ESTADO')->nullable();
            $table->string('MOTIVO_RECHAZO')->nullable();
            $table->string('TICKET')->nullable()->index();
            $table->string('ARME')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solped');
    }
}
