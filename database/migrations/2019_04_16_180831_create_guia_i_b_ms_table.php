<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiaIBMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guia_i_b_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial');
            $table->integer('guia');
            $table->dateTime('fecha');
            $table->string('estado'); // STO o REV igual que en Device->location_id 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guia_i_b_ms');
    }
}
