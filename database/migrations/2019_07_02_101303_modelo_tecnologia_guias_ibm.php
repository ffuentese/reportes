<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModeloTecnologiaGuiasIbm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guia_i_b_ms', function (Blueprint $table) {
            //
            $table->string('modelo')->after('serial');
            $table->string('tecnologia')->after('modelo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guia_i_b_ms', function (Blueprint $table) {
            //
            $table->dropColumn('modelo');
            $table->dropColumn('tecnologia');
        });
    }
}
