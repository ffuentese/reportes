<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LastPVS extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('last_pvs', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->unsignedInteger('pos_id');
            $table->string('estado');
            $table->dateTime('fecha');
            $table->string('serial');
            $table->timestamps();
        });
        
        Schema::table('last_pvs', function (Blueprint $table) {
           $table->primary('id');
           $table->unique('serial');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('last_pvs');
    }
}
