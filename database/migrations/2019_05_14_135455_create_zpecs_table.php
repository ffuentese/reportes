<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZpecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zpecs', function (Blueprint $table) {
            $table->unsignedInteger('id')->primary();
            $table->string('cliente')->nullable();
            $table->string('rotulo')->nullable();
            $table->string('material')->nullable();
            $table->string('serie')->nullable();
            $table->string('rut')->nullable();
            $table->string('direccion')->nullable();
            $table->string('status')->nullable();
            $table->string('operacion')->nullable();
            $table->dateTime('fecha')->nullable();
            $table->string('guia')->nullable();
            $table->string('sap')->nullable();
            $table->string('usuario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zpecs');
    }
}
