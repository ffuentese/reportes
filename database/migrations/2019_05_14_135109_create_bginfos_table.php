<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBginfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bginfos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hostname')->nullable();
            $table->string('username')->nullable();
            $table->unsignedInteger('model_id');
            $table->string('serie')->nullable();
            $table->string('ip')->nullable();
            $table->dateTime('fecha_conexion')->nullable();
            $table->dateTime('fecha_carga')->nullable();
            $table->timestamps();
        });
        
        Schema::table('bginfos', function (Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bginfos');
    }
}
