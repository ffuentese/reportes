<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSccmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sccms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hostname')->nullable();
            $table->unsignedInteger('model_id');
            $table->string('chassis')->nullable();
            $table->string('serie')->nullable();
            $table->string('segmento')->nullable();
            $table->string('username')->nullable();
            $table->unsignedInteger('site_id')->nullable();
            $table->dateTime('fecha_carga')->nullable();
            $table->dateTime('ultima_conexion')->nullable();
            $table->string('red')->nullable();
            $table->timestamps();
        });
        
        Schema::table('sccms', function (Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('site_id')->references('id')->on('bch_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sccms');
    }
}
