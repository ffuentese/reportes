<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MarcaTecnologiaPvs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pvs', function (Blueprint $table) {
            $table->string('marca')->after('serial');
            $table->string('tecnologia')->after('marca');
            $table->integer('region')->after('marca');
            $table->string('comuna')->after('region');
            $table->string('direccion')->after('comuna');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pvs', function (Blueprint $table) {
            $table->dropColumn('marca');
            $table->dropColumn('tecnologia');
            $table->dropColumn('region');
            $table->dropColumn('comuna');
            $table->dropColumn('direccion');
        });
    }
}
