<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocGuiaIBMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_guia_i_b_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guia'); // El número
            $table->integer('provider_id')->unsigned(); // Entel, IBM, ADEXUS, etc.
            $table->string('filename');
            $table->timestamps();
        });
        Schema::table('doc_guia_i_b_ms', function (Blueprint $table) {
           $table->foreign('provider_id')->references('id')->on('providers'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_guia_i_b_ms');
    }
}
