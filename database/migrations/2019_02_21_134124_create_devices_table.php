<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('serial')->nullable();
            $table->string('rotulo')->nullable();
            $table->string('code')->nullable();
            $table->string('cost_center')->nullable();
            $table->integer('guia_recepcion')->nullable();
            $table->integer('guia_reversa')->nullable();
            $table->string('pep')->nullable();
            $table->string('modified_by')->nullable();
            $table->dateTime('modified_on')->nullable();
            $table->dateTime('fecha_recepcion')->nullable();
            $table->dateTime('fecha_instalacion')->nullable();
            $table->dateTime('fecha_reversa')->nullable();
            $table->integer('status_id')->unsigned();
            $table->string('location_id');
            $table->integer('customer_id')->unsigned();
            $table->integer('str_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('pos_id');
            $table->integer('user_id')->unsigned();
            $table->integer('technician_id')->unsigned();
            $table->string('serial_prev')->nullable();
            $table->dateTime('reversa_prev')->nullable();
            $table->integer('guia_prev')->nullable();
            $table->timestamps();
            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('model_id')->references('id')->on('models');
            //$table->foreign('pos_id')->references('id')->on('pos');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('str_id')->references('id')->on('services');
            $table->index('serial');
        });
        
        Schema::table('devices', function(Blueprint $table) {
            $table->foreign('pos_id')->references('id')->on('pos');
            $table->foreign('technician_id')->references('id')->on('technicians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
