<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices_pos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pos_id')->unsigned();
            $table->integer('device_id')->unsigned();
            $table->datetime('fecha');
            $table->string('serial');
            $table->timestamps();
            $table->unique('pos_id', 'device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices_pos');
    }
}
