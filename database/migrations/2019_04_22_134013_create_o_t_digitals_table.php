<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOTDigitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_t_digitals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticket')->nullable();
            $table->datetime('fecha_atencion')->nullable();
            $table->string('foliodespacho')->nullable();
            $table->string('folioot')->nullable();
            $table->string('cliente')->nullable();
            $table->string('sucursal')->nullable();
            $table->string('nombre')->nullable();
            $table->string('rut')->nullable();
            $table->string('direccion')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('tipo1')->nullable();
            $table->string('marca1')->nullable();
            $table->string('serie1')->nullable();
            $table->string('rotulo1')->nullable();
            $table->string('tipo4')->nullable();
            $table->string('marca4')->nullable();
            $table->string('serie4')->nullable();
            $table->string('rotulo4')->nullable();
            $table->string('usuario')->nullable();
            $table->string('proveedor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_t_digitals');
    }
}
