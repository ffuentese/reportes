<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DevBCHColsZpecSolped extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dev__b_c_hs', function(Blueprint $table){
            // Del ZPEC
            $table->string('guia')->nullable()->after('rut');
            $table->string('sap')->nullable()->after('guia');
            $table->dateTime('fecha_instalacion')->nullable()->after('sap');
            // Desde Solped
            $table->string('ticket')->nullable()->after('fecha_instalacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down()
    {
        //
        Schema::table('dev__b_c_hs', function(Blueprint $table){
            $table->dropColumn('guia');
            $table->dropColumn('sap');
            $table->dropColumn('fecha_instalacion');
            $table->dropColumn('ticket');
        });
    }
}
