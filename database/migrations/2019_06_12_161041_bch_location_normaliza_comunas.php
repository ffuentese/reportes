<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Bch_location;

class BchLocationNormalizaComunas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('bch_locations', function (Blueprint $table) {
            $table->dropColumn('ciudad');
            $table->dropColumn('region');
            $table->unsignedInteger('comuna_id')->nullable()->after('direccion');
            $table->foreign('comuna_id')->references('id')->on('comunas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('bch_locations', function (Blueprint $table) {
            $table->dropForeign('bch_locations_comuna_id_foreign');
            $table->dropColumn('comuna_id');
            $table->string('ciudad')->after('direccion')->nullable();
            $table->string('region')->after('ciudad')->nullable();
        });
    }
}
