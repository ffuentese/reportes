<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIq09sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iq09s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material');
            $table->string('rotulo');
            $table->string('serie')->nullable();
            $table->string('centro');
            $table->string('almacen')->nullable();
            $table->string('tipo_stock');
            $table->string('nombre');
            $table->dateTime('modificado')->nullable();
            $table->string('usuario')->nullable();
            $table->string('status_sistema');
            $table->string('pep')->nullable();
            $table->dateTime('creado');
                    
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iq09s');
    }
}
