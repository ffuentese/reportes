<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBchIpsTable extends Migration
{
    /**
     * Run the migrations.
     * Segmento	Ubicación	Marca	CUI	Sucursal	Dirección	Ciudad	Región	Red
     * @return void
     */
    public function up()
    {
        Schema::create('bch_ips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('segmento');
            $table->string('ubicacion')->nullable();
            $table->string('marca')->nullable();
            $table->string('cui')->nullable();
            $table->string('sucursal')->nullable();
            $table->string('direccion')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('region')->nullable();
            $table->string('red')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bch_ips');
    }
}
