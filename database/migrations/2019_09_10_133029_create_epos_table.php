<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hostname')->unique();
            $table->string('plataforma')->nullable();
            $table->string('ip')->nullable();
            $table->string('mac_address')->nullable();
            $table->string('dominio')->nullable();
            $table->string('usuario')->nullable()->index();
            $table->dateTime('ultima_conexion')->nullable();
            $table->string('portatil')->nullable();
            $table->dateTime('fecha_carga')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epos');
    }
}
