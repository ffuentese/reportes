<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMicrosTable extends Migration
{
    /**
     * Crea la tabla micros (para subir datos de microinformática de BEC)
     *
     * @return void
     */
    public function up()
    {
        Schema::create('micros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial');
            $table->string('tecnologia');
            $table->string('modelo');
            $table->string('marca');
            $table->string('tkt_original')->nullable();
            $table->string('tkt_espejo')->nullable();
            $table->integer('guia');
            $table->string('tipo'); // ENTREGA, REVERSA, ETC
            $table->string('proveedor')->nullable();
            $table->dateTime('fecha');
            $table->string('obs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('micros');
    }
}
