<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BchFechas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('dev__b_c_hs', function (Blueprint $table) {
            //
            $table->string('fecha_reporte')->nullable();
            $table->dateTime('ultima_conexion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('dev__b_c_hs', function (Blueprint $table) {
            //
            $table->dropColumn('fecha_reporte');
            $table->dropColumn('ultima_conexion');
        });
    }
}
