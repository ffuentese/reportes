<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Service;

class ServiceTableSeeder extends Seeder
{
    /**
     * Siembra la base de datos con STRs
     *
     * @return void
     */
    public function run()
    {
        // Ejecuta el sembrado
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        Service::insert([
            [
                'id' => 1,
                'name' => 'ENTEL',
                'created_at' => $now,
                'updated_at' => $now  
            ],
            [
                'id' => 2,
                'name' => 'GST',
                'created_at' => $now,
                'updated_at' => $now  
            ],
            [
                'id' => 3,
                'name' => 'IBM',
                'created_at' => $now,
                'updated_at' => $now  
            ],
        ]);
    }
}
