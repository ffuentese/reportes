<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Agrega estatuses a la tabla 
        
        $noinfo = new Status;
        $noinfo->id = 0;
        $noinfo->name = "00: No info";
        $noinfo->save();
        
        $status = new Status;
        $status->id = 1;
        $status->name = "01: Stock";
        $status->save();
        
        $traslado = new Status;
        $traslado->id = 4;
        $traslado->name = "04: En traslado";
        $traslado->save();
        
        $traslado1 = new Status;
        $traslado1->id = 6;
        $traslado1->name = "06: En traslado";
        $traslado1->save();
        
        $bloqueado = new Status;
        $bloqueado->id = 7;
        $bloqueado->name = "07: Bloqueado";
        $bloqueado->save();
            
               
    }
}
