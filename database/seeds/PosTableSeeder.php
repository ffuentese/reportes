<?php

use Illuminate\Database\Seeder;
use App\Pos;

class PosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Llena la tabla de ubicaciones de cliente 
        // POS 0 es un Pos que no tiene ubicación conocida todavía.
        
        $pos = new Pos;
        $pos->id = 0;
        $pos->vigente = true;
        $pos->save();
    }
}
