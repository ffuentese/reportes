<?php



use Illuminate\Database\Seeder;

class ConnectRelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Get Available Permissions.
         */
        $permissions = config('roles.models.permission')::all();

        /**
         * Attach Permissions to Roles.
         */
        $roleAdmin = config('roles.models.role')::where('name', '=', 'Admin')->first();
        foreach ($permissions as $permission) {
            $roleAdmin->attachPermission($permission);
        }
        $roleSuper = config('roles.models.role')::where('name', '=', 'Super')->first();
        $permExt = config('roles.models.permission')::where('slug', 'NOT LIKE', '%users%')->get();
        foreach ($permExt as $permission) {
            $roleSuper->attachPermission($permission);
        }
        
    }
}
