<?php

use Illuminate\Database\Seeder;
use App\AccesorioTipo;
use Carbon\Carbon;

class AccesorioTiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        AccesorioTipo::insert([
           ['name' => 'CABLE', 'created_at' => $now, 'updated_at' => $now], 
           ['name' => 'TRAFO', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
