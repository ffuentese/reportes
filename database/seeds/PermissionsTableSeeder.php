<?php



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Permission Types
         *
         */
        $Permissionitems = [
            [
                'name'        => 'Can View Users',
                'slug'        => 'view.users',
                'description' => 'Can view users',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Can Create Users',
                'slug'        => 'create.users',
                'description' => 'Can create new users',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Can Edit Users',
                'slug'        => 'edit.users',
                'description' => 'Can edit users',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Can Delete Users',
                'slug'        => 'delete.users',
                'description' => 'Can delete users',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Can Upload Data BEC',
                'slug'        => 'upload.data.bec',
                'description' => 'Puede subir archivos BEC',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Puede Ver BEC',
                'slug'        => 'view.data.bec',
                'description' => 'Puede ver información BEC',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Can Upload Data BCH',
                'slug'        => 'upload.data.bch',
                'description' => 'Puede subir archivos BCH',
                'model'       => 'Permission',
            ],
            [
                'name'        => 'Puede Ver BCH',
                'slug'        => 'view.data.bch',
                'description' => 'Puede ver información BCH',
                'model'       => 'Permission',
            ],
            
        ];

        /*
         * Add Permission Items
         *
         */
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem['slug'])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::create([
                    'name'          => $Permissionitem['name'],
                    'slug'          => $Permissionitem['slug'],
                    'description'   => $Permissionitem['description'],
                    'model'         => $Permissionitem['model'],
                ]);
            }
        }
    }
}
