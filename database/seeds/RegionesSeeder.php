<?php

use Illuminate\Database\Seeder;
use App\Region;
use Carbon\Carbon;

class RegionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        Region::insert([
         [
             'id' => 1,
             'name' => 'TARAPACÁ',
             'created_at' => $now,
             'updated_at' => $now  
         ],
         [
             'id' => 2,
             'name' => 'ANTOFAGASTA',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 3,
             'name' => 'ATACAMA',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 4,
             'name' => 'COQUIMBO',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 5,
             'name' => 'VALPARAÍSO',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 6,
             'name' => 'O\'HIGGINS',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 7,
             'name' => 'MAULE',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 8,
             'name' => 'BIOBIO',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 9,
             'name' => 'LA ARAUCANÍA',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 10,
             'name' => 'LOS LAGOS',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 11,
             'name' => 'AYSÉN',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 12,
             'name' => 'MAGALLANES',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 13,
             'name' => 'METROPOLITANA',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 14,
             'name' => 'LOS RÍOS',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 15,
             'name' => 'ARICA Y PARINACOTA',
             'created_at' => $now,
             'updated_at' => $now  
         ],
                [
             'id' => 16,
             'name' => 'ÑUBLE',
             'created_at' => $now,
             'updated_at' => $now  
         ]
        ]);
    }
}
