<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LaravelRolesSeeder extends Seeder
{
    /**
     * Alimenta las tablas relacionadas con roles y permisos.
     *
     * @return void
     */
    public function run()
    {
        //
        Model::unguard();

            $this->call(PermissionsTableSeeder::class);
            $this->call(RolesTableSeeder::class);
            $this->call(ConnectRelationshipsSeeder::class);
            //$this->call('UsersTableSeeder');

        Model::reguard();
    }
}
