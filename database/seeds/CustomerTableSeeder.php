<?php

use Illuminate\Database\Seeder;
use App\Customer;
use Carbon\Carbon;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        Customer::insert([
            'id' => 1,
            'name' => 'BEC',
            'created_at' => $now,
            'updated_at' => $now  
        ],
            [
            'id' => 2,
            'name' => 'Micro BEC',
            'created_at' => $now,
            'updated_at' => $now  
        ]);
        
    }
}
