<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(PosTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(TechnicianTableSeeder::class);
        $this->call(LaravelRolesSeeder::class);
    }
}
