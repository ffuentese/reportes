<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $view_permission = Permission::where('slug','view-data')->first();
        $admin_permission = Permission::where('slug', 'load-data')->first();

        $view_role = new Role();
        $view_role->slug = 'view';
        $view_role->name = 'Visualizador';
        $view_role->save();
        $view_role->permissions()->attach($view_permission);

        $admin_role = new Role();
        $admin_role->slug = 'admin';
        $admin_role->name = 'Administrador';
        $admin_role->save();
        $admin_role->permissions()->attach($admin_permission);
        $admin_role->permissions()->attach($view_permission);
    }
}

