<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $view_role = Role::where('slug','view')->first();
        $admin_role = Role::where('slug', 'admin')->first();
        $view_perm = Permission::where('slug','view-data')->first();
        $admin_perm = Permission::where('slug','load-data')->first();

        $viewer_emp = new User();
        $viewer_emp->name = 'Usama Muneer';
        $viewer_emp->email = 'usama@thewebtier.com';
        $viewer_emp->password = bcrypt('secret');
        $viewer_emp->save();
        $viewer_emp->roles()->attach($view_role);
        $viewer_emp->permissions()->attach($view_perm);

        $manager = new User();
        $manager->name = 'Francisco';
        $manager->email = 'txi.francisco@gmail.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($admin_role);
        $manager->permissions()->attach($admin_perm);
    }
}
