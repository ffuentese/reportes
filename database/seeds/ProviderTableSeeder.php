<?php

use Illuminate\Database\Seeder;
use App\Provider;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $adexus = new Provider;
        $adexus->id = 1;
        $adexus->name = 'ENTEL';
        $adexus->save();
        
        $ibm = new Provider;
        $ibm->id = 2;
        $ibm->name = 'ADEXUS';
        $ibm->save();
        
        $ibm = new Provider;
        $ibm->id = 3;
        $ibm->name = 'IBM';
        $ibm->save();
    }
}
