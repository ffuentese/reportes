<?php

use Illuminate\Database\Seeder;
use App\Location;
use Carbon\Carbon;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Agrega ubicaciones a la base 
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        Location::insert([
            [
            'id' => '0',
            'name' => 'SIN ALMACÉN',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => '1',
            'name' => 'REZAGO-DONACIONES',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => '2',
            'name' => 'INVENTARIO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => '3',
            'name' => 'PERD. DISTRIBUCION',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'C501',
            'name' => 'CONTRATISTA ESE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'C502',
            'name' => 'CONTRATISTA GST',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'C503',
            'name' => 'CONTRATISTA LA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'C504',
            'name' => 'CONTRATISTA SAO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'C505',
            'name' => 'CONTRATISTA SIIM',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'C506',
            'name' => 'CONTRATISTA 23KY',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D000',
            'name' => 'LICENCIAS SERTI',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D001',
            'name' => 'EQ. REV. LAB.',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D002',
            'name' => 'ENEA DONACIONRSE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D003',
            'name' => 'DARIO URZUA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D004',
            'name' => 'ENEA OBSOLETO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D005',
            'name' => 'DONACION BCH',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OC DISPONIBLES',
            'name' => 'D006',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D007',
            'name' => 'DIR. RT. GV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D008',
            'name' => 'ARICA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D009',
            'name' => 'IQUIQUE STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D010',
            'name' => 'CALAMA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D011',
            'name' => 'ANTOFAGASTA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D012',
            'name' => 'COPIAPO STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D013',
            'name' => 'LA SERENA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D014',
            'name' => 'VALPARAISO STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D015',
            'name' => 'RANCAGUA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D016',
            'name' => 'TALCA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D017',
            'name' => 'CHILLAN STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D018',
            'name' => 'CONCEPCION STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D019',
            'name' => 'TEMUCO STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D020',
            'name' => 'VALDIVIA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D021',
            'name' => 'PUERTO MONTT STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D022',
            'name' => 'COYHAIQUE STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D023',
            'name' => 'PUNTA ARENAS STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D024',
            'name' => 'OSORNO STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D025',
            'name' => 'LOS ANGELES STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D026',
            'name' => 'DEVOLUCION ACLIE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D027',
            'name' => 'BOD.ILLAPEL STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D028',
            'name' => 'BOD.VALLENAR STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D029',
            'name' => 'BOD.MELIPILLA STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D030',
            'name' => 'BOD.LOS ANDESSTK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D031',
            'name' => 'BOD.CASTRO STK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D032',
            'name' => 'REZAGOSERTI CERR',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D033',
            'name' => 'DIRECTA ENEA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D034',
            'name' => 'BCH NUEVOS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D035',
            'name' => 'BCH USADOS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D036',
            'name' => 'REZAGO SOTRASER',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D037',
            'name' => 'SERV.TEC.MORPHO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D038',
            'name' => 'CO BCH AGUSTINAS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D039',
            'name' => 'ENEA REZAGOENTEL',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D040',
            'name' => 'DIRECTA CNT',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D041',
            'name' => 'BHP CHILE INC',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D042',
            'name' => 'D026 BCHSOTRENEA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D043',
            'name' => 'GASPARNV.INGRESO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'D044',
            'name' => 'ENEA NVO.INGRESO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'LB01',
            'name' => 'LAB. IMPRESORAS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'LB02',
            'name' => 'LAB. PCS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'LB03',
            'name' => 'LAB. NOTEBOOK',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'LB04',
            'name' => 'LAB. BCO. CHILE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'LB05',
            'name' => 'LAB.FALTA DEFINI',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP01',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP02',
            'name' => 'BOD. 2:DISPONIBL',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP03',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP04',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP05',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP06',
            'name' => 'BOD.6: LABORATOR',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP08',
            'name' => 'BOD. 8: FALTANTE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP09',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP10',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP12',
            'name' => 'BOD. 12: BAJAS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP21',
            'name' => 'BOD.21:EQUIPOS U',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP22',
            'name' => 'BOD.22:PENDIENTE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP23',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP24',
            'name' => 'SERV. TECN. EXT.',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP31',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP32',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP33',
            'name' => 'CERRAR BODEGA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP34',
            'name' => 'SOTRA.BAJADEVOLU',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP35',
            'name' => 'DIRECTA CODELCO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP36',
            'name' => 'REV.OP35 CODELCO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP37',
            'name' => 'DIRECTA ACHS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP38',
            'name' => 'REV.OP37 ACHS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'OP39',
            'name' => 'OP23 BCHSOTRENEA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R000',
            'name' => 'EQUI.INCOMP.ENEA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R001',
            'name' => 'BODEGA GASPAR',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R002',
            'name' => 'ENEA RT Y TC',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R003',
            'name' => 'DARIO URZUA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R004',
            'name' => 'SOTRASER QUILIC.',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R005',
            'name' => 'REV. BCO CHILE',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R006',
            'name' => 'BODEGA CERRADA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R007',
            'name' => 'REV.RT. GV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R008',
            'name' => 'ARICA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R009',
            'name' => 'IQUIQUE REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R010',
            'name' => 'CALAMA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R011',
            'name' => 'ANTOFAGASTA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R012',
            'name' => 'COPIAPO REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R013',
            'name' => 'LA SERENA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R014',
            'name' => 'VALPARAISO REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R015',
            'name' => 'RANCAGUA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R016',
            'name' => 'TALCA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R017',
            'name' => 'CHILLAN REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R018',
            'name' => 'CONCEPCION REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R019',
            'name' => 'TEMUCO REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R020',
            'name' => 'VALDIVIA REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R021',
            'name' => 'PUERTO MONTT REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R022',
            'name' => 'COYHAIQUE REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R023',
            'name' => 'PUNTA ARENAS REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R024',
            'name' => 'OSORNO REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R025',
            'name' => 'LOS ANGELES REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R027',
            'name' => 'BOD.ILLAPEL REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R028',
            'name' => 'BOD.VALLENAR REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R029',
            'name' => 'BOD.MELIPILLAREV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R030',
            'name' => 'BOD.LOS ANDESREV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R031',
            'name' => 'BOD.CASTRO REV',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R032',
            'name' => 'MORPHO QUILICURA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R033',
            'name' => 'REVERSA ENEA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R034',
            'name' => 'REV. BCH NUEVOS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R035',
            'name' => 'REV. BCH USADOS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R038',
            'name' => 'CO BCH AGUSTINAS',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R040',
            'name' => 'REVERSA CNT',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R041',
            'name' => 'R004 SOTRA.ENEA',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R043',
            'name' => 'GASPARNV.INGRESO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'R044',
            'name' => 'ENEA NVO.INGRESO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'STO',
            'name' => 'STOCK TÉCNICO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'REV',
            'name' => 'REV TÉCNICO',
            'created_at' => $now,
            'updated_at' => $now  
            ],
            [
            'id' => 'IBM',
            'name' => 'IBM',
            'created_at' => $now,
            'updated_at' => $now
            ]
        ]);
            
    }
}
