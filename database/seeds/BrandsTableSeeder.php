<?php

use Illuminate\Database\Seeder;
use App\Brand;
use Carbon\Carbon;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Agrega marcas de productos a la base de datos
        
        $now = Carbon::now('America/Santiago')->toDateTimeString();
        Brand::insert([[
            'id' => '3CO',
            'name' => '3COM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => '3MM',
            'name' => '3M',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ACE',
            'name' => 'ACER',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ADA',
            'name' => 'ADAPTEC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ADV',
            'name' => 'ADV',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AGF',
            'name' => 'AGFA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AMD',
            'name' => 'AMD',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => '3CO',
            'name' => '3COM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AOP',
            'name' => 'AOPEN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'APC',
            'name' => 'APC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'APL',
            'name' => 'APPLE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'APO',
            'name' => 'APOLLO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ARG',
            'name' => 'ARGOX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ARM',
            'name' => 'ARM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ASP',
            'name' => 'ASPIRE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ASS',
            'name' => 'ASSY',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ASU',
            'name' => 'ASUS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ATC',
            'name' => 'ATTACHMATE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AVA',
            'name' => 'AVAYA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AVI',
            'name' => 'AVIGLION',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AVS',
            'name' => 'AVISION',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AVT',
            'name' => 'AVTECH',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'AXI',
            'name' => 'AXIS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BEL',
            'name' => 'BELKIN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BEN',
            'name' => 'BENQ',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BIO',
            'name' => 'BIOMETRICO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BIT',
            'name' => 'BITATEK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BIX',
            'name' => 'BIXOLON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BOO',
            'name' => 'BOOQ',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BOS',
            'name' => 'BOSCH',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BRO',
            'name' => 'BROTHER',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BSE',
            'name' => 'BOSE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'BUR',
            'name' => 'BURLE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CAB',
            'name' => 'CABLETRON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CAM',
            'name' => 'CANON',
            'created_at' => $now,
            'updated_at' => $now
        ],
              [
            'id' => 'CAS',
            'name' => 'CASTLES',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CEN',
            'name' => 'CENTRONICS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CER',
            'name' => 'CERIX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CHM',
            'name' => 'CHECKMATE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CIP',
            'name' => 'CIPHERLAB',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CIS',
            'name' => 'CISCO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CMQ',
            'name' => 'COMPAQ',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CNE',
            'name' => 'CNET',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CRE',
            'name' => 'CREATIVE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CRU',
            'name' => 'CRUCIAL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'CRY',
            'name' => 'CRYSTAL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DAE',
            'name' => 'DAEWOO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DEL',
            'name' => 'DELL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DIA',
            'name' => 'DIAMOND',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DIC',
            'name' => 'DIGICHANNEL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DIG',
            'name' => 'DIGITAL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DIN',
            'name' => 'DINON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DLI',
            'name' => 'DLINK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'DTL',
            'name' => 'DATALOGIC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'EAT',
            'name' => 'EATON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ELE',
            'name' => 'ELECTRONICSARTS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ELI',
            'name' => 'ELECTROIMPORT',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'EME',
            'name' => 'EMERALD',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'EMU',
            'name' => 'EMULEX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ENC',
            'name' => 'ENCORE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ENF',
            'name' => 'ENFORA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                 [
            'id' => 'ENT',
            'name' => 'ENTEL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'EPS',
            'name' => 'EPSON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ERI',
            'name' => 'ERICSSON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ETI',
            'name' => 'EXTECHINSTRUMENTS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'EVG',
            'name' => 'EVGA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'EXT',
            'name' => 'EXTENSA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'FAN',
            'name' => 'FANLESS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'FLT',
            'name' => 'FLATRON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'FOR',
            'name' => 'FORTRAN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'FRS',
            'name' => 'FREEDOMSCIENTIFIC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'FUD',
            'name' => 'FUTUREDOMAIN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'FUJ',
            'name' => 'FUJITSU',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'GEA',
            'name' => 'GEAR',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'GEN',
            'name' => 'GENIUS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'GET',
            'name' => 'GETAC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'GNU',
            'name' => 'GNU',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'GWI',
            'name' => 'GROUPWISE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HAH',
            'name' => 'HANDHELD',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HID',
            'name' => 'HID',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HIK',
            'name' => 'HIKVISION',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HIT',
            'name' => 'HITACHI',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HOW',
            'name' => 'HONEYWELL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HP',
            'name' => 'HP',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HPP',
            'name' => 'HP',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'HUA',
            'name' => 'HUAWEI',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'IBM',
            'name' => 'IBM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'IDR',
            'name' => 'INDURA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'IDX',
            'name' => 'IDEMTIX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'IMA',
            'name' => 'IMATION',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'IND',
            'name' => 'INTRODATA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'INF',
            'name' => 'INFORMIX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'INS',
            'name' => 'INVENTORYSOLUTION',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'INT',
            'name' => 'INTEL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'IOM',
            'name' => 'IOMEGA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'IRI',
            'name' => 'IRIDIUM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ITM',
            'name' => 'INTERMEC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'KES',
            'name' => 'KENSINGTON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'KIS',
            'name' => 'KINGSTON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'KLI',
            'name' => 'KLIP',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'KOD',
            'name' => 'KODAK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'KON',
            'name' => 'KONICA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'KVM',
            'name' => 'KVM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'KYO',
            'name' => 'KYOSERA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'LAN',
            'name' => 'LANIX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'LAT',
            'name' => 'LATTISNET',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'LEN',
            'name' => 'LENOVO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'LEX',
            'name' => 'LEXMARK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'LGG',
            'name' => 'LG',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'LIN',
            'name' => 'LINUX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'MAI',
            'name' => 'MAIBASICFOUR',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MAQ',
            'name' => 'MACINTOSH',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MAT',
            'name' => 'MATROX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MAY',
            'name' => 'MAYLEX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'MCF',
            'name' => 'MCAFFE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MCL',
            'name' => 'MICROLAB',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MIC',
            'name' => 'MICROLAB',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MIC',
            'name' => 'MICROSOFT',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'MIF',
            'name' => 'MICROFOCUS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MIT',
            'name' => 'MITSUBISHI',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MOT',
            'name' => 'MOTOROLA',
            'created_at' => $now,
            'updated_at' => $now
        ],
            [
            'id' => 'MOV',
            'name' => 'MOVISTAR',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MSI',
            'name' => 'MSI',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'MTR',
            'name' => 'METROLOGIC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MXL',
            'name' => 'MAXELL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'MXX',
            'name' => 'MAXXTRO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'NAV',
            'name' => 'NAVMAN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'NEC',
            'name' => 'NEC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'NET',
            'name' => 'NETBACK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'NIK',
            'name' => 'NIKON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'NOR',
            'name' => 'NORTEL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'NOV',
            'name' => 'NOVELL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'NVI',
            'name' => 'NVIDIA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'OKI',
            'name' => 'OKIDATA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'OLT',
            'name' => 'OLIVETTI',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'OLV',
            'name' => 'OLIDATA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ORA',
            'name' => 'ORACLE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'OVE',
            'name' => 'OVERLAN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PAL',
            'name' => 'PALPACK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'PAN',
            'name' => 'PANASONIC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PAW',
            'name' => 'PATHWAY',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PBL',
            'name' => 'PACKARDBELL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PEN',
            'name' => 'PENTIUM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                      [
            'id' => 'PHI',
            'name' => 'PHILLIPS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PIN',
            'name' => 'PINACLE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PLA',
            'name' => 'PLANTRONICS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PLM',
            'name' => 'POLYCOM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'POL',
            'name' => 'POLAROID',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PRT',
            'name' => 'PRINTONIX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PUR',
            'name' => 'PURESTORAGE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'PWS',
            'name' => 'POWERSEL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'PYF',
            'name' => 'PAYFLEX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'QCK',
            'name' => 'QUA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'RAC',
            'name' => 'RACICON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'RED',
            'name' => 'REDFOX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'RIC',
            'name' => 'RICOH',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SAB',
            'name' => 'SABRENT',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SAM',
            'name' => 'SAMSUNG',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SAN',
            'name' => 'SANYO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'SAX',
            'name' => 'SECUGEN',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SBX',
            'name' => 'SANBOX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SEG',
            'name' => 'SEAGATE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SEW',
            'name' => 'SEWOO',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'SHT',
            'name' => 'SHORTHAUL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SLE',
            'name' => 'SLEEK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SMA',
            'name' => 'SIN MARCA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SMB',
            'name' => 'SYMBOL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'SMT',
            'name' => 'SMART',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SOB',
            'name' => 'SOUNDBLASTER',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SOL',
            'name' => 'SOLARIS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SON',
            'name' => 'SONY',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'SPC',
            'name' => 'SPARCSYSTEM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'STP',
            'name' => 'STPMEDIUM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'STU',
            'name' => 'STUDIO7MEDIA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SUN',
            'name' => 'SUPRAPLUS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'SUS',
            'name' => 'SUNSHINE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SUP',
            'name' => 'SUPRAPLUS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'SYM',
            'name' => 'SYMANTEC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'SYN',
            'name' => 'SYNOPTICS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'TAR',
            'name' => 'TARGUS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'TEX',
            'name' => 'TEXASINSTRUMENT',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'TOB',
            'name' => 'TOSHIBA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'TOK',
            'name' => 'TOKENLINK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'TOP',
            'name' => 'TOTALPACK',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'TRD',
            'name' => 'TRIDENT',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'TRE',
            'name' => 'TRENDWARE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'TRI',
            'name' => 'TRITON',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'TRP',
            'name' => 'TRIPP-LITE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'TRX',
            'name' => 'TRIMERX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'TSC',
            'name' => 'TSC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'TWA',
            'name' => 'TWINAX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'UIC',
            'name' => 'UNIFORM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'UNI',
            'name' => 'UNISYS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'USR',
            'name' => 'USROBOTICS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'VAN',
            'name' => 'VANGUARD',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'VEB',
            'name' => 'VERBATIM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'VEF',
            'name' => 'VERIFONE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'VER',
            'name' => 'VERITAS',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'VIG',
            'name' => 'VIGATEC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'VIS',
            'name' => 'VIEWSONIC',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'VLX',
            'name' => 'VLX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'VMW',
            'name' => 'VMWARE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'VSI',
            'name' => 'VSI',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => '3CO',
            'name' => '3COM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'WDD',
            'name' => 'WESTERNDIGITAL',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'WNR',
            'name' => 'WINCOR',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'WYS',
            'name' => 'WYSE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'XER',
            'name' => 'XEROX',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'XIR',
            'name' => 'XIRCOM',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'XPL',
            'name' => 'XPLORE',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'YAM',
            'name' => 'YAMAHA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                          [
            'id' => 'YUS',
            'name' => 'YUSA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                [
            'id' => 'ZEB',
            'name' => 'ZEBRA',
            'created_at' => $now,
            'updated_at' => $now
        ],
                    [
            'id' => 'ZYX',
            'name' => 'ZYXEL',
            'created_at' => $now,
            'updated_at' => $now
        ]]);
    }
}
