<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $view_role = Role::where('slug','viewer')->first();
        $admin_role = Role::where('slug', 'admin')->first();

        $viewData = new Permission();
        $viewData->slug = 'view-data';
        $viewData->name = 'Ver datos';
        $viewData->save();
        $viewData->roles()->attach($view_role);

        $insertData = new Permission();
        $insertData->slug = 'load-data';
        $insertData->name = 'Ingresar planillas';
        $insertData->save();
        $insertData->roles()->attach($admin_role);
    }
}

